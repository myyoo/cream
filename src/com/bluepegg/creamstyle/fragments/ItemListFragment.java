package com.bluepegg.creamstyle.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.account.IConnector;
import com.bluepegg.creamstyle.account.IResponseEvent;
import com.bluepegg.creamstyle.activities.CreamAccountActivity;
import com.bluepegg.creamstyle.activities.CreamMainActivity;
import com.bluepegg.creamstyle.dialogs.CreamStyleDialog;
import com.bluepegg.creamstyle.interfaces.OnFrCartListener;
import com.bluepegg.creamstyle.interfaces.OnFrClosetListener;
import com.bluepegg.creamstyle.interfaces.OnFrContentFeedListener;
import com.bluepegg.creamstyle.interfaces.OnFrRemoveListener;
import com.bluepegg.creamstyle.interfaces.OnShareFacebook;
import com.bluepegg.creamstyle.interfaces.OnUpdateItemList;
import com.bluepegg.creamstyle.kissmetrics.CreamStyleStateEventTypes;
import com.bluepegg.creamstyle.kissmetrics.CreamstyleAnalytics;
import com.bluepegg.creamstyle.localytics.LocalyticsAnalytics;
import com.bluepegg.creamstyle.network.NetworkManager;
import com.bluepegg.creamstyle.objects.CartItem;
import com.bluepegg.creamstyle.objects.ClosetFeed;
import com.bluepegg.creamstyle.objects.ClosetItemDetail;
import com.bluepegg.creamstyle.objects.DetailFeedItem;
import com.bluepegg.creamstyle.objects.FBShareItem;
import com.bluepegg.creamstyle.objects.FragmentEvent;
import com.bluepegg.creamstyle.objects.ItemDetail;
import com.bluepegg.creamstyle.objects.ItemId;
import com.bluepegg.creamstyle.objects.ItemList;
import com.bluepegg.creamstyle.objects.LookItem;
import com.bluepegg.creamstyle.objects.manager.ClosetFeedItemManager;
import com.bluepegg.creamstyle.objects.manager.ClosetItemDetailListManager;
import com.bluepegg.creamstyle.objects.manager.ClosetItemListManager;
import com.bluepegg.creamstyle.objects.manager.ClosetLookItemManager;
import com.bluepegg.creamstyle.objects.manager.ItemDetailListManager;
import com.bluepegg.creamstyle.objects.manager.ItemListManager;
import com.bluepegg.creamstyle.objects.manager.LookItemManager;
import com.bluepegg.creamstyle.objects.manager.ResponseManager;
import com.bluepegg.creamstyle.objecttype.SetCartObject;
import com.bluepegg.creamstyle.objecttype.SetClosetObject;
import com.bluepegg.creamstyle.pager.JazzyViewPager;
import com.bluepegg.creamstyle.pintchzoom.PhotoView;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.share.GooglePlusShareManager;
import com.bluepegg.creamstyle.share.PinterestShareManager;
import com.bluepegg.creamstyle.utils.BasicInfo;
import com.bluepegg.creamstyle.utils.DevLog;
import com.bluepegg.creamstyle.utils.TwitLogin;
import com.bluepegg.creamstyle.utils.Utils;
import com.bluepegg.creamstyle.views.CircleImageView;
import com.bluepegg.creamstyle.views.PathView;
import com.bluepegg.creamstyle.views.VerticalPager;
import com.bluepegg.creamstyle.views.VerticalPager.OnScrollListener;
import com.squareup.picasso.Picasso;

public class ItemListFragment extends Fragment implements OnUpdateItemList {
	public static final String DETAILITEM_OBJ = "detailitem_obj";
	private static final String ADD_ITEM = "add-item";
	private static final String REMOVE_ITEM = "remove-item";
	
	private static final int PINTEREST = 0;
	private static final int TWITTER = 1;
	private static final int FACEBOOK = 2;
	
	private Utils mUtils;
	private IConnector mConnector;
	private NetworkManager mNetworkManager;
	private SettingManager mSettingManager;
	private ResponseManager mResponseManager;
	private LookItemManager mLookItemManager;
	private ItemDetailListManager mItemDetailListManager;
	private ItemListManager mItemListManager;
	private ClosetLookItemManager mClosetLookItemManager;
	private ClosetItemListManager mClosetItemListManager;
	private ClosetItemDetailListManager mClosetItemDetailListManager;
	
	private Resources mRes;
	private LayoutInflater mInflater;
	
	private OnFrRemoveListener mFrRemoveListener = null;
	private OnFrClosetListener mFrClosetListener = null;
	private OnFrCartListener mFrCartListener = null;
	private OnShareFacebook mShareFBListener = null;
	private OnFrContentFeedListener mFrContentFeedListener = null;
	private CreamMainActivity mMainActivity;
	
	private DetailFeedItem mDetailFeedItem;
	private int mLookId, mDetailId, mItemId;
	private boolean mIsCloset;
	private boolean mVerticalScrollBlock;		// It is for single vertical list
	private String mPrice;
	private int mInventoryId;
	
	private VerticalPager mVerticalContainer;
	private LinearLayout mIndicator;
	private LinearLayout mIndicatorVertical;
	private DetailItemPagerAdapter mAdapter;
	
	private ImageView backView;
	private ImageView mGoHome;
	private ImageView mItemHeart;
	private ImageView mMenuIcon;
	private TextView mItemBrand, mItemName, mItemDetail, mItemPrice, mItemKoreanPrice, mItemColor;
	private LinearLayout mColorItemContainer, mSizeItemContainer;
	private RelativeLayout mCartContainer;
	private ImageView mInventoryItem;
	private TextView mCartCount;
	
	private static int length = 120;
	private static final int duration = 100;
	private static final int sub_duration = 200;
	private static final int sub_select_duration = 200;
	private static int sub_offset = 20;
	private List<PathView> mPathViews;
	private boolean isMenuOpened = false;
	private PathView mShare_1, mShare_2, mShare_3, mShare_4;
	private ImageView mItemShare;
	
	private Dialog mDialog;
	private CreamStyleDialog mCreamDialog;
	
	private int mVerticalPosition;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle bundle = getArguments();
		if(bundle != null) {
			mDetailFeedItem = (DetailFeedItem)bundle.getSerializable(DETAILITEM_OBJ);
			mLookId = mDetailFeedItem.getLookId();
			mDetailId = mDetailFeedItem.getDetailId();
			mItemId = mDetailFeedItem.getItemId();
			mIsCloset = mDetailFeedItem.getIsCloset();
			mVerticalScrollBlock = mDetailFeedItem.getVerticalScrollBlock();
		}
	}
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		initManager();
		View view = inflater.inflate(R.layout.item_detail_layout, container, false);
		mVerticalContainer = (VerticalPager)view.findViewById(R.id.verticalContainer);
		mVerticalContainer.addOnScrollListener(scrollListener);
		
		mIndicator = (LinearLayout)view.findViewById(R.id.indicator);
		mIndicatorVertical = (LinearLayout)view.findViewById(R.id.indicatorVertical);
		
		
		if(mAdapter == null) {
			mAdapter = new DetailItemPagerAdapter(getActivity());
		}
		
		updateDetailItem();
		
		
		backView = (ImageView)view.findViewById(R.id.backView);
		backView.setOnClickListener(backListener);
		
		mGoHome = (ImageView)view.findViewById(R.id.goHome);
		mGoHome.setOnClickListener(goHomeListener);
		
		ImageView itemCloset = (ImageView)view.findViewById(R.id.itemCloset);
		itemCloset.setOnClickListener(closetListener);
		
		mMenuIcon = (ImageView)view.findViewById(R.id.menuIcon);
		mMenuIcon.setOnClickListener(menuListener);
		mItemHeart = (ImageView)view.findViewById(R.id.itemHeart);
		mItemHeart.setOnClickListener(setClosetListener);
		
		mItemBrand = (TextView)view.findViewById(R.id.itemBrand);
		mItemName = (TextView)view.findViewById(R.id.itemName);
		mItemDetail = (TextView)view.findViewById(R.id.itemDetail);
		mItemPrice = (TextView)view.findViewById(R.id.price);
		mItemKoreanPrice = (TextView)view.findViewById(R.id.wonprice);
		mItemColor = (TextView)view.findViewById(R.id.color);
		mColorItemContainer = (LinearLayout)view.findViewById(R.id.colorItemContainer);
		mSizeItemContainer = (LinearLayout)view.findViewById(R.id.sizeItemContainer);
		mCartContainer = (RelativeLayout)view.findViewById(R.id.cartContainer);
		mInventoryItem = (ImageView)view.findViewById(R.id.inventoryItem);
		mCartCount = (TextView)view.findViewById(R.id.cartCount);
		mCartCount.setText(String.valueOf(mSettingManager.getCartCount()));
		
		mCartContainer.setOnClickListener(cartListener);
		mInventoryItem.setOnClickListener(addInventoryListener);
		
		mPathViews = new ArrayList<PathView>();
		
		mShare_1 = (PathView)view.findViewById(R.id.share_1);
		mPathViews.add(mShare_1);
		
		mShare_2 = (PathView)view.findViewById(R.id.share_2);
		mPathViews.add(mShare_2);
		
		mShare_3 = (PathView)view.findViewById(R.id.share_3);
		mPathViews.add(mShare_3);
		
		mShare_4 = (PathView)view.findViewById(R.id.share_4);
		mPathViews.add(mShare_4);
		
		mItemShare = (ImageView)view.findViewById(R.id.itemShare);
		
		for(int i = 0; i < mPathViews.size(); ++i) {
			switch(i) {
			case 0:
				mPathViews.get(i).setBackgroundResource(R.drawable.google);
				break;
			
			case 1:
				mPathViews.get(i).setBackgroundResource(R.drawable.pinterest_icon);
				break;
				
			case 2:
				mPathViews.get(i).setBackgroundResource(R.drawable.twitter_icon);
				break;
				
			case 3:
				mPathViews.get(i).setBackgroundResource(R.drawable.facebook_icon);
				break;
			}
			
		}
		
		mItemShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				if(!isMenuOpened) {
					isMenuOpened = true;
				} else {
					isMenuOpened = false;
				}
				
				startMenuAnimation(isMenuOpened);
				
			}
			
		});
		
		mShare_1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Intent intent = shareGooglePlus();
				startActivityForResult(intent, 0);
				startSubButtonSelectedAnimation(0);
			}
			
		});
		
		mShare_2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				shareToPinterest();
				
				startSubButtonSelectedAnimation(1);
			}
			
		});
		
		mShare_3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog = mUtils.getFullScreenDialog(getActivity());
				mDialog.show();
				shareToTwitter();
				startSubButtonSelectedAnimation(2);
			}
			
		});
		
		mShare_4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				shareToFacebook();
				startSubButtonSelectedAnimation(3);
			}
			
		});
		
		return view;
	}
	
	private void startMenuAnimation(boolean open) {
		
		if(open) {
			mItemShare.setImageResource(R.drawable.bt_close);
			pathViewSetVisible(open);
		} else {
			mItemShare.setImageResource(R.drawable.bt_share);
		}
		
		for(int i = 0 ; i < mPathViews.size() ; i++) {
			startSubButtonAnimation(i, open);
		}
	}
	
	private void pathViewSetVisible(boolean pVisible) {
		for(PathView pathView : mPathViews) {
			if(pVisible) {
				pathView.setVisibility(View.VISIBLE);
			} else {
				pathView.setVisibility(View.GONE);
			}
		}
	}
	
	private void startSubButtonAnimation(final int index, final boolean open) {
		PathView view = mPathViews.get(index);
		
		int addValue = 0;
		
		if(mUtils.deviceSize(getActivity()) == 4) {
			length = 200;
			sub_offset = 40;
		} else if(mUtils.deviceSize() == 2) {
			length = 80;
			sub_offset = 0;
		}
		
        float endX = 0;
        float endY = (length + addValue)  + (length * index);
		
		AnimationSet animation = new AnimationSet(false);
		Animation translate;
		Animation rotate = new RotateAnimation(0, 
				360, 
				Animation.RELATIVE_TO_SELF, 0.5f, 
				Animation.RELATIVE_TO_SELF, 0.5f);
		rotate.setDuration(sub_duration);
		rotate.setRepeatCount(1);
		rotate.setInterpolator(AnimationUtils.loadInterpolator(getActivity(), android.R.anim.accelerate_interpolator));
		
		if(open) {
			
			translate = new TranslateAnimation(0.0f, 
					endX, 
					0.0f, 
					-endY);
	        translate.setDuration(sub_duration);
	        translate.setInterpolator(AnimationUtils.loadInterpolator(getActivity(), android.R.anim.overshoot_interpolator));
	        translate.setStartOffset(sub_offset*index);
			
	        view.setOffset(endX, -endY);
		
		} else {
			translate = new TranslateAnimation(0.0f, 
					endX, 
					0.0f, 
					endY);
			translate.setDuration(sub_duration);
			translate.setStartOffset(sub_offset*(mPathViews.size() - (index+1)));
			translate.setInterpolator(AnimationUtils.loadInterpolator(getActivity(), android.R.anim.anticipate_interpolator));
			
			
			view.setOffset(endX, -endY);
		}
		
		animation.addAnimation(rotate);
		animation.addAnimation(translate);
		
		animation.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				movePathButton(index, open);
				
			}
		});
		
		view.startAnimation(animation);
	}
	
	private int orgLeftMargin = -1;
	private int orgBottomMargin = -1;
	private void movePathButton(int index, boolean open) {
		
		PathView view = mPathViews.get(index);
		
		int addValue = 0;
        float endX = 0f;
        float endY = (length + addValue) + (length * index);
		 
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
		if(orgBottomMargin == -1) {
			orgLeftMargin = params.leftMargin;
			orgBottomMargin = params.bottomMargin;
		}
		
		if(open) {
			params.leftMargin = orgLeftMargin + (int)endX;
			params.bottomMargin = orgBottomMargin + (int)endY;
			
		} else {
			
			params.leftMargin = orgLeftMargin;
			params.bottomMargin = orgBottomMargin;
		}
		
		view.setLayoutParams(params);
	}
	
	private void startSubButtonSelectedAnimation(int index) {
		
		for(int i = 0 ; i < mPathViews.size() ; i++) {
			if(index == i) {
				PathView view = mPathViews.get(i);
				
				AnimationSet animation = new AnimationSet(false);
				 
				Animation translate = new TranslateAnimation(
		        		0.0f, view.getXOffset()
		        		, 0.0f, view.getYOffset());
				translate.setDuration(0);
				
				Animation scale = new ScaleAnimation(
						0.5f, 1.0f
						, 0.5f, 1.0f
						, Animation.RELATIVE_TO_SELF, 0.5f
						, Animation.RELATIVE_TO_SELF, 0.5f);
				scale.setDuration(sub_select_duration);
				
				Animation alpha = new AlphaAnimation(1.0f, 0.0f);
				alpha.setDuration(sub_select_duration);
				
				animation.addAnimation(scale);
				animation.addAnimation(translate);
				animation.addAnimation(alpha);
				
				view.startAnimation(animation);
			} else {
				PathView view = mPathViews.get(i);
				
				AnimationSet animation = new AnimationSet(false);
				
				Animation translate = new TranslateAnimation(
		        		0.0f, view.getXOffset()
		        		, 0.0f, view.getYOffset());
				translate.setDuration(0);
				
				Animation scale = new ScaleAnimation(
						1.0f, 0.0f
						, 1.0f, 0.0f
						, Animation.RELATIVE_TO_SELF, 0.5f
						, Animation.RELATIVE_TO_SELF, 0.5f);
				scale.setDuration(sub_select_duration);
				
				Animation alpha = new AlphaAnimation(1.0f, 0.0f);
				alpha.setDuration(sub_select_duration);
				
				animation.addAnimation(scale);
				animation.addAnimation(translate);
				animation.addAnimation(alpha);
				
				view.startAnimation(animation);
			}
			
		}
		
		if(isMenuOpened) {
			isMenuOpened = false;
			mItemShare.setImageResource(R.drawable.bt_share);
			
			Animation rotate = new RotateAnimation(-45, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
			
			rotate.setInterpolator(AnimationUtils.loadInterpolator(getActivity(), android.R.anim.anticipate_overshoot_interpolator));
			rotate.setFillAfter(true);
			rotate.setDuration(sub_select_duration);
			
			for(int i = 0 ; i < mPathViews.size() ; i++) {
				movePathButton(i, false);
			}			
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		mMainActivity.setMenuIcon(View.GONE);
	}
	
	public void onBackPress() {
		backView.performClick();
	}
	
	
	private void initManager() {
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mUtils = app.getUtils();
		mConnector = app.getConnector();
		mNetworkManager = app.getNetworkManager();
		mSettingManager = app.getSettingManager();
		mResponseManager = app.getResponseManager();
		mLookItemManager = app.getLookItemManager();
		mItemDetailListManager = app.getItemDetailListManager();
		mItemListManager = app.getItemListManager();
		mClosetLookItemManager = app.getClosetLookItemManager();
		mClosetItemListManager = app.getClosetItemListManager();
		mClosetItemDetailListManager = app.getClosetItemDetailListManager();
		
		mInflater = LayoutInflater.from(getActivity());
		mRes = app.getResources();
	}
	
	private class AddCartResponseEvent implements IResponseEvent<Object> {
		private String mErrorMessage;
		
		@Override
		public void onResponse(Object pParams) {
			DevLog.Logging("Response", pParams.toString());
			new CartResponseTask().execute(pParams.toString());
		}
		
	}
	
	private class CartResponseTask extends AsyncTask<String, Void, Boolean> {
		private String mErrorMessage = "error";
		private int mCode;
		private int mCount;
		private List<CartItem> mCartItems;
		
		@Override
		protected Boolean doInBackground(String... params) {
			mCartItems = new ArrayList<CartItem>();
			SetCartObject object = new SetCartObject(mCartItems);
			boolean result = object.onResponseListener(params[0]);
			mErrorMessage = object.getErrorMessage();
			mCode = object.getCode();
			mCount = object.getCartCount();
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			dismiss();
			
			if(result) {
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						
						HashMap<String, String> properties = new HashMap<String, String>();
						properties.put("inventory_id", String.valueOf(mInventoryId));
						properties.put("price", mPrice);
						CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_ADD_TO_CART, properties);
						
						LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_ADD_TO_CART, properties);
						
						mSettingManager.setCartCount(mCount);
						mCartCount.setText(String.valueOf(mCount));
						
					}
					
				});
			} else {
				if(mCode == 109) {
					mSettingManager.setLogin(false);
					
					mCreamDialog = new CreamStyleDialog(getActivity(), R.layout.question_layout, R.string.cream, R.drawable.icon_info);
					mCreamDialog.setCancelable(false);
					
					View v = mCreamDialog.getParentView();
					
					TextView contentText = (TextView)v.findViewById(R.id.contentText);
					contentText.setText(R.string.login_notice);
					
					TextView cancel = (TextView)mCreamDialog.getConfirmView();
					cancel.setText(R.string.not_yet);
					TextView login = (TextView)mCreamDialog.getCancelView();
					login.setText(R.string.login);
					mCreamDialog.show();
					
					cancel.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dismissDialog();
						}
						
					});
					
					login.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dismissDialog();
							
							Intent intent = new Intent(getActivity(), CreamAccountActivity.class);
							startActivity(intent);
							getActivity().finish();
						}
						
					});
					
				} else {
					mUtils.showToastMessage(getActivity(), mErrorMessage);
					mInventoryItem.setImageResource(R.drawable.soldout);
				}
			}
		}
		
	}
	
	private void dismissDialog() {
		if(mCreamDialog != null && mCreamDialog.isShowing()) {
			mCreamDialog.dismiss();
			mCreamDialog = null;
		}
	}
	
	OnClickListener addInventoryListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			
			mDialog = mUtils.getFullScreenDialog(getActivity());
			mDialog.show();
			
			if(mNetworkManager.isNetworkAvailable(getActivity())) {
				mConnector.setCart("add-item", String.valueOf(mSettingManager.getUid()), mSettingManager.getLoginToken(), String.valueOf(mInventoryId), String.valueOf(mLookId), new AddCartResponseEvent());
			} else {
				dismissDialog();
				mUtils.showToastMessage(getActivity(), R.string.unconnected_network);
			}
			
		}
		
	};
	
	OnClickListener cartListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mFrCartListener.onEvent(null);
		}
		
	};
	
	
	OnClickListener closetListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mFrClosetListener.onEvent(null);
		}
		
	};
	
	OnClickListener menuListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mMainActivity.clickMenuIcon();
		}
		
	};
	
	OnClickListener setClosetListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mDialog = mUtils.getFullScreenDialog(getActivity());
			mDialog.show();
			
			if(mNetworkManager.isNetworkAvailable(getActivity())) {
				ItemDetail itemDetail = null;
				if(mIsCloset) {
//					if(mExpiredItem) {
//						itemDetail = mClosetItemDetailListManager.getItemDetail(mDetailId);
//					} else {
//						itemDetail = mItemDetailListManager.getItemDetail(mDetailId);
//					}
					itemDetail = mClosetItemDetailListManager.getItemDetail(mDetailId);
					if(itemDetail == null)
						itemDetail = mItemDetailListManager.getItemDetail(mDetailId);
				} else {
//					if(mExpiredItem) {
//						itemDetail = mClosetItemDetailListManager.getItemDetail(mDetailId);
//					} else {
//						itemDetail = mItemDetailListManager.getItemDetail(mDetailId);
//					}
					itemDetail = mItemDetailListManager.getItemDetail(mDetailId);
					if(itemDetail == null)
						itemDetail = mClosetItemDetailListManager.getItemDetail(mDetailId);
				}
				
//				ItemDetail itemDetail = mItemDetailListManager.getItemDetail(mDetailId);
				
				String type = ADD_ITEM;
				if(itemDetail.getCloset()) {
					type = REMOVE_ITEM;
				} else {
					type = ADD_ITEM;
				}
				
				mConnector.setCloset(type, String.valueOf(mSettingManager.getUid()), mSettingManager.getLoginToken(), String.valueOf(mDetailId), String.valueOf(mLookId), new ClosetResponseEvent(type));
			
			} else {
				mUtils.showToastMessage(getActivity(), R.string.unconnected_network);
			}
			
		}
		
	};
	
	private class ClosetResponseEvent implements IResponseEvent<Object> {
		private String mType;
		
		public ClosetResponseEvent(String pType) {
			mType = pType;
		}
		
		@Override
		public void onResponse(Object pParams) {
			DevLog.Logging("Response", pParams.toString());
			
			new ClosetResponseTask(mType).execute(pParams.toString());
		}
		
	}
	
	private class ClosetResponseTask extends AsyncTask<String, Void, Boolean> {
		private String mErrorMessage = "error";
		private String mType;
		private int mCode;
		private String mResponse;
		
		public ClosetResponseTask(String pType) {
			mType = pType;
		}
		
		@Override
		protected Boolean doInBackground(String... params) {
			mResponse = params[0];
			SetClosetObject object = new SetClosetObject();
			boolean result = object.onResponseListener(params[0]);
			mErrorMessage = object.getErrorMessage();
			mCode = object.getCode();
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			
			dismiss();
			
			if(result) {
				
				mResponseManager.setClosetResponse(mResponse);
				
				if(mType.equals(ADD_ITEM)) {
					
					HashMap<String, String> properties = new HashMap<String, String>();
					properties.put("item_detail_id", String.valueOf(mDetailId));
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_ADD_ITEM_TO_CLOSET, properties);
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_ADD_ITEM_TO_CLOSET, properties);
					
					// To keep closet status, we locally save status.
					mItemDetailListManager.getItemDetail(mDetailId).setCloset(true);
					ClosetItemDetail closetItemDetail = new ClosetItemDetail();
					closetItemDetail.setClosetTime(1);
					mClosetItemDetailListManager.putItemDetail(mDetailId, closetItemDetail);
					
					mItemHeart.setImageResource(R.drawable.heart_on);
					
				} else {
					HashMap<String, String> properties = new HashMap<String, String>();
					properties.put("item_detail_id", String.valueOf(mDetailId));
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_REMOVE_ITEM_FROM_CLOSET, properties);
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_REMOVE_ITEM_FROM_CLOSET, properties);
					// To keep closet status, we locally save status.
					mItemDetailListManager.getItemDetail(mDetailId).setCloset(false);
					ClosetItemDetail closetItemDetail = new ClosetItemDetail();
					closetItemDetail.setClosetTime(0);
					mClosetItemDetailListManager.putItemDetail(mDetailId, closetItemDetail);
					mItemHeart.setImageResource(R.drawable.heart_btn);
					
					ClosetItemDetailListManager closetItemDetailManager = CreamStyleApplication.getApplication().getClosetItemDetailListManager();
					ClosetFeedItemManager closetFeedItemManager = CreamStyleApplication.getApplication().getClosetFeedItemManager();
					
					HashMap<Integer, ClosetItemDetail> closetItemDetails = closetItemDetailManager.getItemDetails();
					closetItemDetails.remove(mDetailId);
					
					HashMap<Integer, ClosetFeed> closetFeeds = closetFeedItemManager.getClosetFeeds();
					closetFeeds.remove(mDetailId);
				}
				
			} else {
				if(mCode == 109) {
					mSettingManager.setLogin(false);
					
					mCreamDialog = new CreamStyleDialog(getActivity(), R.layout.question_layout, R.string.cream, R.drawable.icon_info);
					mCreamDialog.setCancelable(false);
					
					View v = mCreamDialog.getParentView();
					
					TextView contentText = (TextView)v.findViewById(R.id.contentText);
					contentText.setText(R.string.login_notice);
					
					TextView cancel = (TextView)mCreamDialog.getConfirmView();
					cancel.setText(R.string.not_yet);
					TextView login = (TextView)mCreamDialog.getCancelView();
					login.setText(R.string.login);
					mCreamDialog.show();
					
					cancel.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dismissDialog();
						}
						
					});
					
					login.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dismissDialog();
							
							Intent intent = new Intent(getActivity(), CreamAccountActivity.class);
							startActivity(intent);
							getActivity().finish();
						}
						
					});
				} else {
					mUtils.showToastMessage(getActivity(), mErrorMessage);
				}
			}
			
		}
		
	}
	
	private void updateDetailItem() {
		new Handler(Looper.getMainLooper()).post(new Runnable() {

			@Override
			public void run() {
				getDetailItems();
			}
			
		});
	}
	
	private Map<Integer, ItemId> setItemIds(List<Integer> pItemIdList) {
		Map<Integer, ItemId> itemIds = new LinkedHashMap<Integer, ItemId>();
		for(int i = 0; i < pItemIdList.size(); ++i) {
			//ItemList itemList = mItemListManager.getItem(pItemIdList.get(i));
			ItemList itemList = null;
			
			if(mIsCloset) {
				itemList = mClosetItemListManager.getItem(pItemIdList.get(i));
			} else {
//				if(mExpiredItem) {
//					itemList = mClosetItemListManager.getItem(pItemIdList.get(i));
//				} else {
//					itemList = mItemListManager.getItem(pItemIdList.get(i));
//				}
				itemList = mItemListManager.getItem(pItemIdList.get(i));
				
				if(itemList == null) {
					itemList = mClosetItemListManager.getItem(pItemIdList.get(i));
				}
			}
			
			int itemIdKey = itemList.getItemId();
			List<Integer> itemDetailIds = itemList.getItemDetailIdList(); // colors
			
			List<ItemDetail> itemDetails = new ArrayList<ItemDetail>();
			
//			for(int n = 0; n < itemDetailIds.size(); ++n) {
//				ItemDetail itemDetail = mItemDetailListManager.getItemDetail(itemDetailIds.get(n));
//				itemDetails.add(itemDetail);
//			}
			
			
			for(int n = 0; n < itemDetailIds.size(); ++n) {
				if(mIsCloset) {
					ItemDetail itemDetail = mClosetItemDetailListManager.getItemDetail(itemDetailIds.get(n));
					
					if(itemDetail != null)
						itemDetails.add(itemDetail);
				} else {
					ItemDetail itemDetail = mItemDetailListManager.getItemDetail(itemDetailIds.get(n));
					if(itemDetail != null)
						itemDetails.add(itemDetail);
					
				}
			}
			
			if(itemDetails.size() == 0) {
				for(int n = 0; n < itemDetailIds.size(); ++n) {
					if(mIsCloset) {
						ItemDetail itemDetail = mItemDetailListManager.getItemDetail(itemDetailIds.get(n));
						
						if(itemDetail != null)
							itemDetails.add(itemDetail);
					} else {
						ItemDetail itemDetail = mClosetItemDetailListManager.getItemDetail(itemDetailIds.get(n));
						itemDetails.add(itemDetail);
					}
				}
			}
			
			ItemId itemId = new ItemId();
			itemId.setItemDetailId(itemDetails);
			itemId.setItemId(itemIdKey);
			itemIds.put(itemIdKey, itemId);
		}
		
		return itemIds;
	}
	
	
	private void setColorItems(final ItemId pItemId, final int pPosition, final JazzyViewPager pPager) {
		
		mColorItemContainer.removeAllViews();
		
		List<String> smallImageList = pItemId.getItemDetailId().get(pPosition).getAltSmallImageList();
		setIndicator(smallImageList.size(), pItemId.getCurrentPosition());
		
		int extraValue = 0;
		if(mUtils.deviceSize() == 1) {
			extraValue = 40;
		} else if(mUtils.deviceSize() == 4) {
			extraValue = 80;
		} else if(mUtils.deviceSize() == 2) {
			extraValue = -30;
		}
		
		LookItem item = null;
//		LookItem item = mLookItemManager.getItem(mLookId);
		if(mIsCloset) {
			item = mClosetLookItemManager.getItem(mLookId);
		} else {
//			if(mExpiredItem) {
//				item = mClosetLookItemManager.getItem(mLookId);
//			} else {
//				item = mLookItemManager.getItem(mLookId);
//			}
			item = mLookItemManager.getItem(mLookId);
			if(item == null)
				item = mClosetLookItemManager.getItem(mLookId);
		}
		
		for(int i = 0; i < pItemId.getItemDetailId().size(); ++i) {
			DevLog.defaultLogging(pItemId.getItemDetailId().get(i).getItemDetailId() + " ?????");
			DevLog.defaultLogging(pItemId.getItemDetailId().get(i).getThumbItemImage());
			
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(80 + extraValue, 80 + extraValue);
			lp.setMargins(5, 5, 5, 5);
			LinearLayout container = new LinearLayout(getActivity());
			
			if(i == pItemId.getSelectedPosition())
				container.setBackgroundResource(R.drawable.white_circle);
			else
				container.setBackgroundResource(R.drawable.trans_circle);
			
			container.setLayoutParams(lp);
			container.setGravity(Gravity.CENTER);
			
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.gravity = Gravity.CENTER;
			final CircleImageView itemColorImage = new CircleImageView(getActivity());
			itemColorImage.setPosition(i);
			itemColorImage.setPoint(70 + extraValue, 70 + extraValue);
			itemColorImage.setLayoutParams(params);
			Picasso.with(getActivity()).load(pItemId.getItemDetailId().get(i).getThumbItemImage()).resize(70 + extraValue, 70 + extraValue).centerCrop().into(itemColorImage);
			
			
			container.addView(itemColorImage);
			mColorItemContainer.addView(container);
			

			ClosetItemDetail closetItemDetail = mClosetItemDetailListManager.getItemDetail(mDetailId);
			
			if(closetItemDetail == null) {
				DevLog.defaultLogging("22222222222222222");
				if(pItemId.getItemDetailId().get(i).getCloset()) {
					mItemHeart.setImageResource(R.drawable.heart_on);
						
				} else {
					mItemHeart.setImageResource(R.drawable.heart_btn);
				}
			} else {
				DevLog.defaultLogging("1111111111111111");
				if(closetItemDetail.getClosetTime() != 0) {
					mItemHeart.setImageResource(R.drawable.heart_on);
				} else {
					mItemHeart.setImageResource(R.drawable.heart_btn);
				}
			}
			
			
			itemColorImage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					
					CircleImageView view = (CircleImageView)v;
					int position = view.getPosition();
					pItemId.setSelectedPosition(position);
					
					
					String color = pItemId.getItemDetailId().get(position).getColor();
					mDetailId = pItemId.getItemDetailId().get(position).getItemDetailId();
					List<String> smallImageList = pItemId.getItemDetailId().get(position).getAltSmallImageList();
					pItemId.setCurrentPosition(0);
					setIndicator(smallImageList.size(), 0);
					
					mItemColor.setText(color);
					
					mAdapter.clear();
					mAdapter.addItems(smallImageList);
					pPager.setAdapter(mAdapter);
					
					HashMap<String, String> properties = new HashMap<String, String>();
					properties.put("item_detail_id", String.valueOf(mDetailId));
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_VIEW_ITEM, properties);
					
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_VIEW_ITEM, properties);
					
					setColorItems(pItemId, position, pPager);
				
				}
				
			});
		}
		
		
		//List<Integer> itemIdList = item.getItemIdList();
		
		int feedMask = item.getFeedMask();
		
		final ItemDetail itemDetail = pItemId.getItemDetailId().get(pItemId.getSelectedPosition());
		
		String color = itemDetail.getColor();
		String price = itemDetail.getPrice();
		mDetailId = itemDetail.getItemDetailId();
		mInventoryId = itemDetail.getInventoryIdList().get(0);
		
		new Handler(Looper.getMainLooper()).post(new Runnable() {

			@Override
			public void run() {
//				if(itemDetail.getCloset()) {
//					mItemHeart.setImageResource(R.drawable.heart_on);
//				} else {
//					mItemHeart.setImageResource(R.drawable.heart_btn);
//				}
				
				ClosetItemDetail closetItemDetail = mClosetItemDetailListManager.getItemDetail(mDetailId);
				
				if(closetItemDetail == null) {
					if(itemDetail.getCloset()) {
						mItemHeart.setImageResource(R.drawable.heart_on);
							
					} else {
						mItemHeart.setImageResource(R.drawable.heart_btn);
					}
				} else {
					if(closetItemDetail.getClosetTime() != 0) {
						mItemHeart.setImageResource(R.drawable.heart_on);
					} else {
						mItemHeart.setImageResource(R.drawable.heart_btn);
					}
				}
			}
			
		});
		
		
		
		List<Integer> inventoryIds = itemDetail.getInventoryIdList();
		List<String> inventorySizes = itemDetail.getInventorySizeList();
		List<Integer> inventoryQtys = itemDetail.getInventoryQtyList();
		
		
		ItemList itemList = null;
//		final ItemList itemList = mItemListManager.getItem(pItemId.getItemId());
		if(mIsCloset) {
			itemList = mClosetItemListManager.getItem(pItemId.getItemId());
			if(itemList == null)
				itemList = mItemListManager.getItem(pItemId.getItemId());
		} else {
			itemList = mItemListManager.getItem(pItemId.getItemId());
			if(itemList == null)
				itemList = mClosetItemListManager.getItem(pItemId.getItemId());
		}

		String itemBrand = itemList.getItemBrand();
		String itemName = itemList.getItemName();
		String itemDetailText = itemList.getItemDetail();

		//List<Integer> itemIdList = item.getItemIdList();
		//Map<Integer, ItemId> itemIds = setItemIds(itemIdList);
		
		setItemSize(inventorySizes, inventoryIds, inventoryQtys, inventorySizes.get(0));

		mItemBrand.setText(itemBrand);
		mItemName.setText(itemName);
		if(feedMask == 0) {
			mPrice = price;
			mItemPrice.setText(String.format(getString(R.string.price), price));
			mItemKoreanPrice.setVisibility(View.GONE);
		} else {
			mPrice = price;
			mItemPrice.setText(String.format(getString(R.string.price), price));
			if(mSettingManager.getDisplayType().equals(CreamStyleApplication.CREAM))
				mItemKoreanPrice.setVisibility(View.GONE);
			else
				mItemKoreanPrice.setVisibility(View.VISIBLE);
			mItemKoreanPrice.setText(String.format(getString(R.string.won_price), mUtils.convertDollarToWon(item.getFeedMask(), price)));
		}
		
		mItemDetail.setText(itemDetailText);
		mItemColor.setText(color);
		
		mInventoryItem.setImageResource(R.drawable.ic_add_cart);
		
	}
	
	private ItemId sort(ItemId pItemId) {
//		LookItem item = mLookItemManager.getItem(mLookId);
		LookItem item = null;
		if(mIsCloset) {
			item = mClosetLookItemManager.getItem(mLookId);
		} else {
//			if(mExpiredItem) {
//				item = mClosetLookItemManager.getItem(mLookId);
//			} else {
//				item = mLookItemManager.getItem(mLookId);
//			}
			item = mLookItemManager.getItem(mLookId);
			if(item == null)
				item = mClosetLookItemManager.getItem(mLookId);
		}
		
		/*
		List<Integer> itemDetailIdList = item.getItemDetailIdList();
		List<Integer> itemIdList = item.getItemIdList();
		int itemDetailId = itemDetailIdList.get(0);
		
		for(int i = 0; i < itemIdList.size(); ++i) {
			if(itemIdList.get(i) == pItemId.getItemId()) {
				itemDetailId = itemDetailIdList.get(i);
				
				DevLog.defaultLogging(itemDetailId + " <<<<<<<<<<<");
				break;
			}
		}
		*/
		
		ItemDetail temp = null;
		
		for(int i = 0; i < pItemId.getItemDetailId().size(); ++i) {
			if(pItemId.getItemDetailId().get(i).getItemDetailId() == mDetailId /*itemDetailId*/) {
				temp = pItemId.getItemDetailId().get(i);
				break;
			}
		}
		
		
		for(int i = 0; i < pItemId.getItemDetailId().size(); ++i) {
			
			if(pItemId.getItemDetailId().get(i).getItemDetailId() == mDetailId /*itemDetailId*/) {
				pItemId.getItemDetailId().remove(i);
				break;
			}
			
		}
		
		pItemId.getItemDetailId().add(0, temp);
		
		return pItemId;
	}
	
	
	private List<ItemId> mItemIdList = new ArrayList<ItemId>();
	
	private void getDetailItems() {
//		LookItem item = mLookItemManager.getItem(mLookId);
		LookItem item = null;
		if(mIsCloset) {
			item = mClosetLookItemManager.getItem(mLookId);
			if(item == null) {
				item = mLookItemManager.getItem(mLookId);
			}
		} else {
			item = mLookItemManager.getItem(mLookId);
			if(item == null) {
				item = mClosetLookItemManager.getItem(mLookId);
			}
		}
		
		
		List<Integer> itemIdList = item.getItemIdList();
		
		if(mVerticalScrollBlock == false)
			setIndicatorVertical(itemIdList.size());
		
		Map<Integer, ItemId> itemIds = setItemIds(itemIdList);
		
		ItemId itemId = itemIds.get(mItemId);
		List<String> smallImageList = itemId.getItemDetailId().get(0).getAltSmallImageList();
		setIndicator(smallImageList.size(), 0);
		
		itemId = sort(itemId);
		
		
		JazzyViewPager pager = setItemContainer(itemId);
		setColorItems(itemId, 0, pager);
		
		pager.setOnPageChangeListener(new PagerListener(itemId));
		
		//setColorItems(mItemIdList.get(0), 0, pager);
		setIndicatorVerticalView(0);
		
		
		mItemIdList.clear();
		mItemIdList.add(itemId);
		
		if(mVerticalScrollBlock == false) {
			Iterator it = itemIds.entrySet().iterator();
			while(it.hasNext()) {
				
				Map.Entry<Integer, ItemId> pairs = (Map.Entry<Integer, ItemId>)it.next();
				
				if(pairs.getKey() != mItemId)
					setItemContainer(pairs.getValue());
			}
		}
		
		
	}
	
	
	private JazzyViewPager setItemContainer(ItemId pItemId) {
		
		DetailItemPagerAdapter adapter = new DetailItemPagerAdapter(getActivity());
		DevLog.defaultLogging("pItemId: " + pItemId + " <<<<");
		DevLog.defaultLogging("pItemId.getItemDetailId(): " + pItemId.getItemDetailId() + " <<<<");
		DevLog.defaultLogging("pItemId.getItemDetailId().get(0): " + pItemId.getItemDetailId().get(0) + " <<<<");
		if(pItemId.getItemDetailId().get(0) == null) {
			boolean temp = pItemId.getItemDetailId().get(1).getCloset();
			pItemId.getItemDetailId().remove(0);
			pItemId.getItemDetailId().get(0).setCloset(temp);
			mDetailId = pItemId.getItemDetailId().get(0).getItemDetailId();
		} 
		
		adapter.addItems(pItemId.getItemDetailId().get(0).getAltSmallImageList());
		
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mUtils.deviceWidth(), Math.round((mUtils.deviceHeight() * 4) / 7));
		JazzyViewPager pager = new JazzyViewPager(getActivity());
		pager.setLayoutParams(params);
		
		pager.setAdapter(adapter);
		
		RelativeLayout container = new RelativeLayout(getActivity());
		container.addView(pager);
		mVerticalContainer.addView(container);
		
		mItemIdList.add(pItemId);
		
		return pager;
	}
	
	private int mCurrentPage = Integer.MAX_VALUE;
	
	OnScrollListener scrollListener = new OnScrollListener() {

		@Override
		public void onScroll(int scrollX) {
			
		}

		@Override
		public void onViewScrollFinished(int currentPage) {
			
			try {
				
				if(mCurrentPage != currentPage) {
					mCurrentPage = currentPage;
					
					setIndicatorVerticalView(currentPage);
					
					RelativeLayout container = (RelativeLayout)mVerticalContainer.getChildAt(currentPage);
					JazzyViewPager pager = (JazzyViewPager)container.getChildAt(0);
					
					ItemId itemId = mItemIdList.get(currentPage);
					//itemId = sort(itemId);
					
					pager.setOnPageChangeListener(new PagerListener(itemId));
					
					//setColorItems(mItemIdList.get(currentPage), 0, pager);
					
					setColorItems(itemId, 0, pager);
					
					HashMap<String, String> properties = new HashMap<String, String>();
					properties.put("item_id", String.valueOf(itemId.getItemId()));
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_VIEW_ITEM, properties);
					
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_VIEW_ITEM, properties);
					
				}
				
				
			} catch(Exception e) {
				e.printStackTrace();
			}
			
		}
		
	};
	
	
	
	private void setItemSize(final List<String> pInventorySizes, final List<Integer> pInventoryIds, final List<Integer> pInventoryQtys, String pSizeText) {
		mSizeItemContainer.removeAllViews();
		for(int i = 0; i < pInventorySizes.size(); ++i) {
			final TextView sizeText = new TextView(getActivity());
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.setMargins(5, 5, 5, 5);
			sizeText.setLayoutParams(params);
			
			if(pInventorySizes.get(i).equals(pSizeText)) {
				
				sizeText.setGravity(Gravity.CENTER);
				
				if(pInventoryQtys == null) {
					sizeText.setTextColor(Color.BLACK);
					sizeText.setBackgroundResource(R.drawable.ic_small_outofstock);
					sizeText.setClickable(false);
					sizeText.setEnabled(false);
				} else {
					if(pInventoryQtys.get(i) == 0) {
						sizeText.setTextColor(Color.BLACK);
						sizeText.setBackgroundResource(R.drawable.ic_small_outofstock);
						sizeText.setClickable(false);
						sizeText.setEnabled(false);
					} else {
						sizeText.setTextColor(Color.WHITE);
						sizeText.setBackgroundResource(R.drawable.black_circle);
						sizeText.setClickable(true);
						sizeText.setEnabled(true);
					}
				}
				
				
			} else {
				sizeText.setTextColor(Color.BLACK);
				sizeText.setGravity(Gravity.CENTER);
				if(pInventoryQtys.get(i) == 0) {
					sizeText.setBackgroundResource(R.drawable.ic_small_outofstock);
					sizeText.setClickable(false);
					sizeText.setEnabled(false);
				} else {
					sizeText.setBackgroundResource(R.drawable.white_circle);
					sizeText.setClickable(true);
					sizeText.setEnabled(true);
				}
				
			}
			
			sizeText.setTextSize(10);
			DevLog.defaultLogging(pInventorySizes.get(i) + " ***");
			sizeText.setText(convertText(pInventorySizes.get(i).trim()));
				
			
			mSizeItemContainer.addView(sizeText);
			
			sizeText.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					
					for(int i = 0; i < pInventorySizes.size(); ++i) {
						if(convertText(pInventorySizes.get(i).trim()).equals(((TextView)v).getText().toString().trim())) {
							mInventoryId = pInventoryIds.get(i);
							break;
						}
					}
					
					mInventoryItem.setImageResource(R.drawable.ic_add_cart);
					setItemSize(pInventorySizes, pInventoryIds, pInventoryQtys, revertText(sizeText.getText().toString()));
					
				}
				
			});
			
		}
		
	}
	
	private void setIndicatorVertical(int pSize) {
		mIndicatorVertical.removeAllViews();
		for(int i = 0; i < pSize; ++i) {
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.setMargins(2, 2, 2, 2);
			ImageView circleIndicator = new ImageView(getActivity());
			circleIndicator.setImageResource(R.drawable.circle_dot_disable);
			circleIndicator.setLayoutParams(params);
			mIndicatorVertical.addView(circleIndicator);
		}
	}
	
	private void setIndicator(int pSize, int pPosition) {
		mIndicator.removeAllViews();
		
		for(int i = 0; i < pSize; ++i) {
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.setMargins(2, 2, 2, 2);
			ImageView circleIndicator = new ImageView(getActivity());
			circleIndicator.setImageResource(R.drawable.circle_dot_disable);
			circleIndicator.setLayoutParams(params);
			mIndicator.addView(circleIndicator);
		}
		
		setIndicatorView(pPosition);
	}
	

	OnClickListener backListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mFrRemoveListener.onEvent(ItemListFragment.this);
		}
		
	};
	
	OnClickListener goHomeListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mFrContentFeedListener.onEvent();
		}
		
	};
	
	
	public void setOnFeedItem(OnFrRemoveListener pFrRemoveListener, OnFrClosetListener pFrClosetListener, OnFrCartListener pFrCartListener, OnShareFacebook pShareFBListener, OnFrContentFeedListener pFrContentFeedListener, CreamMainActivity pMainActivity) {
		mFrRemoveListener = pFrRemoveListener;
		mFrClosetListener = pFrClosetListener;
		mFrCartListener = pFrCartListener;
		mShareFBListener = pShareFBListener;
		mFrContentFeedListener = pFrContentFeedListener;
		mMainActivity = pMainActivity;
	}
	
	
	private class DetailItemPagerAdapter extends PagerAdapter {
		private List<String> mItems;
		
		public DetailItemPagerAdapter(Context pContext) {
			super();
			mItems = new ArrayList<String>();
		}
		
		public void clear() {
			mItems.clear();
		}
		
		public void addItems(List<String> pItems) {
			mItems.addAll(pItems);
			notifyDataSetChanged();
		}
		
		
		@Override
		public int getCount() {
			return mItems.size();
		}
		
		
		@Override
		public int getItemPosition(Object object) {
			if(object instanceof ItemListFragment) {
				((ItemListFragment)object).update();
			}
			
			return super.getItemPosition(object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			PhotoView mItemImage = new PhotoView(container.getContext());
			mItemImage.setScaleType(ScaleType.FIT_XY);
			
			final String itemImageUrl = mItems.get(position);
			
			try {
				
				Picasso.with(container.getContext()).load(itemImageUrl)
				.resize(mUtils.deviceWidth(), Math.round(mUtils.deviceHeight() * 0.6f) - 100)
				.centerCrop()
				.into(mItemImage);
				
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mUtils.deviceWidth(), Math.round(mUtils.deviceHeight() * 0.6f) - 100);
			params.setMargins(0, 0, 0, 45);
			
			
			mItemImage.setLayoutParams(params);
			mItemImage.setTag("currentView_" + position);
			
			container.addView(mItemImage, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			
			return mItemImage;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}
		
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			View view = (View)object;
			((ViewPager)container).removeView(view);
			view = null;
		}
	}
	
	
	private class PagerListener implements OnPageChangeListener {
		private ItemId mItemId;
		
		public PagerListener(ItemId pItemId) {
			mItemId = pItemId;
		}
		
		@Override
		public void onPageScrollStateChanged(int state) {
			
		}

		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixel) {
			
		}

		@Override
		public void onPageSelected(int position) {
			
			setIndicatorView(position);
			mItemId.setCurrentPosition(position);
			
		}
		
	}
	
	
	private void setIndicatorView(final int pPosition) {
		
		new Handler().post(new Runnable() {

			@Override
			public void run() {
				
				for(int i = 0; i < mIndicator.getChildCount(); ++i) {
					ImageView circleIndicator = (ImageView)mIndicator.getChildAt(i);
					if(i == pPosition)
						circleIndicator.setImageResource(R.drawable.circle_dot_enable);
					else
						circleIndicator.setImageResource(R.drawable.circle_dot_disable);
				}
			}
			
		});
		
		
		
	}
	
	private void setIndicatorVerticalView(int pPosition) {
		
		for(int i = 0; i < mIndicatorVertical.getChildCount(); ++i) {
			ImageView circleIndicator = (ImageView)mIndicatorVertical.getChildAt(i);
			if(i == pPosition) {
				circleIndicator.setImageResource(R.drawable.circle_dot_enable);
			} else
				circleIndicator.setImageResource(R.drawable.circle_dot_disable);
		}
		
		
	}
	

	private String convertText(String pStr) {
		if(pStr.equals("Large"))
			return "L";
		else if(pStr.equals("Small"))
			return "S";
		else if(pStr.equals("Medium"))
			return "M";
		else if(pStr.equals("X Small"))
			return "XS";
		else if(pStr.equals("One Size"))
			return "O/S";
		
		return pStr;
	}
	
	
	private String revertText(String pStr) {
		if(pStr.equals("L"))
			return "Large";
		else if(pStr.equals("S"))
			return "Small";
		else if(pStr.equals("M"))
			return "Medium";
		else if(pStr.equals("XS"))
			return "X Small";
		else if(pStr.equals("O/S"))
			return "One Size";
		
		return pStr;
	}
	
	
	private Intent shareGooglePlus() {
		
		ItemDetail itemDetail = mItemDetailListManager.getItemDetail(mDetailId);
		
		GooglePlusShareManager pManager = new GooglePlusShareManager(getActivity());
		return pManager.share(mDetailId, itemDetail.getSmallItemImage(), mItemName.getText().toString(), mItemBrand.getText().toString());
	}
	
	private void shareToPinterest() {

		ItemDetail itemDetail = mItemDetailListManager.getItemDetail(mDetailId);
		
		PinterestShareManager pManager = new PinterestShareManager(getActivity());
		pManager.share(false, mDetailId, itemDetail.getSmallItemImage(), mItemName.getText().toString(), mItemBrand.getText().toString());
		
	}
	
	private void shareToTwitter() {
		if(BasicInfo.TwitLogin == true) {
			new TwitterRequest().execute();
		} else {
			new TwitterRequestToken().execute();
		}
	}
	
	private void shareToFacebook() {
		
		ItemDetail itemDetail = mItemDetailListManager.getItemDetail(mDetailId);
		
		FBShareItem shareItem = new FBShareItem();
		shareItem.setItemName(mItemName.getText().toString());
		shareItem.setItemDetailName(mItemBrand.getText().toString());
		shareItem.setDescription(mItemDetail.getText().toString());
		shareItem.setPictureUrl(itemDetail.getSmallItemImage());
		shareItem.setType(false);
		shareItem.setId(mDetailId);

		FragmentEvent event = new FragmentEvent(shareItem);
		mShareFBListener.onEvent(event);
	}
	
	class TwitterRequest extends AsyncTask<Void, Void, Boolean>{

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setOAuthAccessToken(BasicInfo.TWIT_KEY_TOKEN);
				builder.setOAuthAccessTokenSecret(BasicInfo.TWIT_KEY_TOKEN_SECRET);
				builder.setOAuthConsumerKey(BasicInfo.TWIT_CONSUMER_KEY);
				builder.setOAuthConsumerSecret(BasicInfo.TWIT_CONSUMER_SECRET);
				
				Configuration config = builder.build();
				TwitterFactory tFactory = new TwitterFactory(config);
				BasicInfo.TwitInstance = tFactory.getInstance();
				return true;
			} catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			dismiss();
			if(result == true) {
				new ShareAsync().execute();
			}
		}
	}
	
	class TwitterRequestToken extends AsyncTask<Void, Void, Boolean>{
		private RequestToken mRequestToken;
		
		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setDebugEnabled(true);
				builder.setOAuthConsumerKey(BasicInfo.TWIT_CONSUMER_KEY);
				builder.setOAuthConsumerSecret(BasicInfo.TWIT_CONSUMER_SECRET);
				
				TwitterFactory factory = new TwitterFactory(builder.build());
				Twitter mTwit = factory.getInstance();
				mRequestToken = mTwit.getOAuthRequestToken();
				
				BasicInfo.TwitInstance = mTwit;
				BasicInfo.TwitRequestToken = mRequestToken;
				return true;
			} catch (TwitterException e) {
				e.printStackTrace();
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			dismiss();
			Intent intent = new Intent(getActivity(), TwitLogin.class);
			intent.putExtra("authUrl", mRequestToken.getAuthenticationURL());
			startActivityForResult(intent, BasicInfo.REQ_CODE_TWIT_LOGIN);
		}
		
	}
	
	class ShareAsync extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				
				BasicInfo.TwitInstance.updateStatus(String.format(getString(R.string.share_twitter), mItemName.getText().toString(), mItemBrand.getText().toString()));
				return true;
			} catch (TwitterException e) {
				e.printStackTrace();
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if(result == true) {
				HashMap<String, String> properties = new HashMap<String, String>();
				properties.put("item_detail_id", String.valueOf(mDetailId));
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED_ON_TWITTER, properties);
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED, properties);
				
				LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED_ON_TWITTER, properties);
				LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED, properties);
				
				mUtils.showToastMessage(getActivity(), R.string.share_success);
			} else {
				mUtils.showToastMessage(getActivity(), R.string.share_error);
			}
			
		}
	}
	
	private void dismiss() {
		if(mDialog != null && mDialog.isShowing()) {
			mDialog.dismiss();
			mDialog = null;
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == BasicInfo.REQ_CODE_TWIT_LOGIN) {
			new ResponseAsync(data).execute();
		} else if(requestCode == 0) {
			if(resultCode == getActivity().RESULT_OK) {
				HashMap<String, String> properties = new HashMap<String, String>();
				properties.put("item_detail_id", String.valueOf(mDetailId));
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED_ON_GOOGLE, properties);
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED, properties);
				
				LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED_ON_GOOGLE, properties);
				LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED, properties);
			}
		}
	}
	
	class ResponseAsync extends AsyncTask<Void, Void, Boolean> {
		private Intent mData;
		
		public ResponseAsync(Intent pData) {
			mData = pData;
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				Twitter mTwit = BasicInfo.TwitInstance;
				AccessToken mAccessToken = mTwit.getOAuthAccessToken(BasicInfo.TwitRequestToken, mData.getStringExtra("oauthVerifier"));
				BasicInfo.TwitLogin = true;
				BasicInfo.TWIT_KEY_TOKEN = mAccessToken.getToken();
				BasicInfo.TWIT_KEY_TOKEN_SECRET = mAccessToken.getTokenSecret();
				BasicInfo.TwitAccessToken = mAccessToken;
				return true;
			} catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if(result == true) {
				new ShareAsync().execute();
			}
		}
		
	}

	@Override
	public void update() {
		mAdapter.notifyDataSetChanged();
	}
	
}
