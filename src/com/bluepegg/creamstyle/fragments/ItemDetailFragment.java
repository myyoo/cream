package com.bluepegg.creamstyle.fragments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.account.IConnector;
import com.bluepegg.creamstyle.account.IResponseEvent;
import com.bluepegg.creamstyle.activities.CreamAccountActivity;
import com.bluepegg.creamstyle.activities.CreamMainActivity;
import com.bluepegg.creamstyle.adapters.CreamViewPagerAdapter;
import com.bluepegg.creamstyle.dialogs.CreamStyleDialog;
import com.bluepegg.creamstyle.interfaces.OnFrCartListener;
import com.bluepegg.creamstyle.interfaces.OnFrClosetListener;
import com.bluepegg.creamstyle.interfaces.OnFrContentFeedListener;
import com.bluepegg.creamstyle.interfaces.OnFrItemsListener;
import com.bluepegg.creamstyle.interfaces.OnFrRemoveListener;
import com.bluepegg.creamstyle.interfaces.OnShareFacebook;
import com.bluepegg.creamstyle.kissmetrics.CreamStyleStateEventTypes;
import com.bluepegg.creamstyle.kissmetrics.CreamstyleAnalytics;
import com.bluepegg.creamstyle.localytics.LocalyticsAnalytics;
import com.bluepegg.creamstyle.network.NetworkManager;
import com.bluepegg.creamstyle.objects.ClosetFeed;
import com.bluepegg.creamstyle.objects.DetailFeedItem;
import com.bluepegg.creamstyle.objects.FBShareItem;
import com.bluepegg.creamstyle.objects.FragmentEvent;
import com.bluepegg.creamstyle.objects.HomePage;
import com.bluepegg.creamstyle.objects.LookItem;
import com.bluepegg.creamstyle.objects.manager.ClosetFeedItemManager;
import com.bluepegg.creamstyle.objects.manager.ClosetLookItemManager;
import com.bluepegg.creamstyle.objects.manager.LookItemManager;
import com.bluepegg.creamstyle.objects.manager.ResponseManager;
import com.bluepegg.creamstyle.objecttype.SetClosetObject;
import com.bluepegg.creamstyle.pager.CreamViewPager;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.share.GooglePlusShareManager;
import com.bluepegg.creamstyle.share.PinterestShareManager;
import com.bluepegg.creamstyle.utils.BasicInfo;
import com.bluepegg.creamstyle.utils.DevLog;
import com.bluepegg.creamstyle.utils.TwitLogin;
import com.bluepegg.creamstyle.utils.Utils;
import com.bluepegg.creamstyle.views.PathView;

public class ItemDetailFragment extends Fragment {
	public static final String KEY_VALUE = "key_value";
	public static final String POSITION_VALUE = "position_value";
	public static final String IS_CLOSET = "is_closet";
	private static final String ADD_LOOK = "add-look";
	private static final String REMOVE_LOOK = "remove-look";
	
	private static final int PINTEREST = 0;
	private static final int TWITTER = 1;
	private static final int FACEBOOK = 2;
	
	private Utils mUtils;
	private IConnector mConnector;
	private SettingManager mSettingManager;
	private NetworkManager mNetworkManager;
	private ResponseManager mResponseManager;
	private LookItemManager mLookItemManager;
	private ClosetLookItemManager mClosetLookItemManager;
	
	private OnFrRemoveListener mFrRemoveListener = null;
	private OnFrItemsListener mFrItemsListener = null;
	private OnFrClosetListener mFrClosetListener = null;
	private OnFrCartListener mFrCartListener = null;
	private OnShareFacebook mShareFBListener = null;
	private OnFrContentFeedListener mFrContentFeedListener = null;
	private CreamMainActivity mMainActivity;
	private CreamViewPager mPager;
	private ImageView mBackHomePage, mGoHome;
	private TextView mLookName, mDisplayName;
	
	private ImageView mItemHeart, mItemCloset;
	private RelativeLayout mCartContainer;
	private TextView mCartCount;
	
	private static int length = 120;
	private static final int duration = 100;
	private static final int sub_duration = 200;
	private static final int sub_select_duration = 200;
	private static int sub_offset = 20;
	private List<PathView> mPathViews;
	private boolean isMenuOpened = false;
	private PathView mShare_1, mShare_2, mShare_3, mShare_4;
	private ImageView mItemShare;
	
	private int mCurrentPos;
	private Dialog mDialog;
	private CreamStyleDialog mCreamDialog;
	
	private int mKey;
	private boolean mIsCloset;
	private ArrayList<LookItem> mLookItems;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle bundle = getArguments();
		if(bundle != null) {
			mKey = bundle.getInt(KEY_VALUE);
			mCurrentPos = bundle.getInt(POSITION_VALUE);
			mIsCloset = bundle.getBoolean(IS_CLOSET);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		initManager();
		View view = inflater.inflate(R.layout.item_main_layout, container, false);
		
		if(mSettingManager.getDisplayType().equals(CreamStyleApplication.CREAM)) {
			if(mIsCloset) {
				mLookItems = new ArrayList<LookItem>(mClosetLookItemManager.getLookItems().values());
				Collections.sort(mLookItems, LookItem.closetComparator);
			} else {
				mLookItems = new ArrayList<LookItem>(mLookItemManager.getLookItems().values());
			}
				
		} else {
			
			if(mIsCloset) {
				HashMap<Integer, LookItem> lookItems = mClosetLookItemManager.getLookItems();
				mLookItems = new ArrayList<LookItem>();
				for(Map.Entry<Integer, LookItem> item : lookItems.entrySet()) {
					if(item.getValue().getFeedMask() == 1) {
						mLookItems.add(item.getValue());
					}
				}
				
			} else {
				HashMap<Integer, LookItem> lookItems = mLookItemManager.getLookItems();
				mLookItems = new ArrayList<LookItem>();
				for(Map.Entry<Integer, LookItem> item : lookItems.entrySet()) {
					if(item.getValue().getFeedMask() == 1) {
						mLookItems.add(item.getValue());
					}
				}
			}

		}
		
		
		mBackHomePage = (ImageView)view.findViewById(R.id.backHomePage);
		mGoHome = (ImageView)view.findViewById(R.id.goHome);
		mBackHomePage.setOnClickListener(homeListener);
		mGoHome.setOnClickListener(goHomeListener);
		mLookName = (TextView)view.findViewById(R.id.lookName);
		mDisplayName = (TextView)view.findViewById(R.id.displayName);
		mItemHeart = (ImageView)view.findViewById(R.id.itemHeart);
		mItemCloset = (ImageView)view.findViewById(R.id.itemCloset);
		mCartContainer = (RelativeLayout)view.findViewById(R.id.cartContainer);
		mCartCount = (TextView)view.findViewById(R.id.cartCount);
		
		mPathViews = new ArrayList<PathView>();
		
		mShare_1 = (PathView)view.findViewById(R.id.share_1);
		mPathViews.add(mShare_1);
		
		mShare_2 = (PathView)view.findViewById(R.id.share_2);
		mPathViews.add(mShare_2);
		
		mShare_3 = (PathView)view.findViewById(R.id.share_3);
		mPathViews.add(mShare_3);
		
		mShare_4 = (PathView)view.findViewById(R.id.share_4);
		mPathViews.add(mShare_4);
		
		mItemShare = (ImageView)view.findViewById(R.id.itemShare);
		
		
		for(int i = 0; i < mPathViews.size(); ++i) {
			switch(i) {
			case 0:
				mPathViews.get(i).setBackgroundResource(R.drawable.google);
				break;
			
			case 1:
				mPathViews.get(i).setBackgroundResource(R.drawable.pinterest_icon);
				break;
				
			case 2:
				mPathViews.get(i).setBackgroundResource(R.drawable.twitter_icon);
				break;
				
			case 3:
				mPathViews.get(i).setBackgroundResource(R.drawable.facebook_icon);
				break;
			}
			
		}
		
		mItemShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				if(!isMenuOpened) {
					isMenuOpened = true;
				} else {
					isMenuOpened = false;
				}
				
				startMenuAnimation(isMenuOpened);
				
			}
			
		});
		
		mShare_1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = shareGooglePlus();
				startActivityForResult(intent, 0);
				
				startSubButtonSelectedAnimation(0);
			}
			
		});
		
		mShare_2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				shareToPinterest();
				
				startSubButtonSelectedAnimation(1);
			}
			
		});
		
		mShare_3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog = mUtils.getFullScreenDialog(getActivity());
				mDialog.show();
				shareToTwitter();
				startSubButtonSelectedAnimation(2);
			}
			
		});
		
		mShare_4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				shareToFacebook();
				startSubButtonSelectedAnimation(3);
			}
			
		});
		
		
		
		try {
			if(mCartCount == null) {
				mCartCount = (TextView)view.findViewById(R.id.cartCount);
			}
			
			mCartCount.setText(String.valueOf(mSettingManager.getCartCount()));
		} catch(Exception e) {
			e.printStackTrace();
			mCartCount.setText("0");
		}
		
		
		mPager = (CreamViewPager)view.findViewById(R.id.creamPager);
		CreamViewPagerAdapter adapter = new CreamViewPagerAdapter(getActivity(), mLookItems, mFrItemsListener, mIsCloset);
		mPager.setAdapter(adapter);
		mPager.setOnPageChangeListener(new PagerListener());
		mPager.setPageTransformer(false, new ViewPager.PageTransformer() {
			
			@Override
			public void transformPage(View page, float position) {
				final float normalizedposition = Math.abs(Math.abs(position) - 1);
				page.setAlpha(normalizedposition);
			}
		});
		
		mPager.setCurrentItem(mCurrentPos);
		
		LookItem item = null;
		
		if(mIsCloset) {
			item = mClosetLookItemManager.getItem(mKey);
		} else {
			item = mLookItemManager.getItem(mKey);
		}
		
		if(item != null) {
			mLookName.setText(item.getLookName());
			mDisplayName.setText(item.getDisplayName());
			
			mItemCloset.setOnClickListener(closetListener);
			mItemHeart.setOnClickListener(setClosetListener);
			mCartContainer.setOnClickListener(cartListener);
			
			setCurrentPosition(mCurrentPos);
		} 
		
		return view;
	}
	
	
	
	@Override
	public void onResume() {
		super.onResume();
		mMainActivity.setMenuIcon(View.VISIBLE);
	}

	private void setPathViewVisibility(int pVisible) {
		for(int i = 0; i < mPathViews.size(); ++i) {
			mPathViews.get(i).setVisibility(pVisible);
		}
	}
	
	private void startMenuAnimation(boolean open) {
		
		if(open) {
			mItemShare.setImageResource(R.drawable.bt_close);
			pathViewSetVisible(open);
		} else {
			mItemShare.setImageResource(R.drawable.bt_share);
		}
		
		for(int i = 0 ; i < mPathViews.size() ; i++) {
			startSubButtonAnimation(i, open);
		}
	}
	
	private void pathViewSetVisible(boolean pVisible) {
		for(PathView pathView : mPathViews) {
			if(pVisible) {
				pathView.setVisibility(View.VISIBLE);
			} else {
				pathView.setVisibility(View.GONE);
			}
		}
	}
	
	private void startSubButtonAnimation(final int index, final boolean open) {
		PathView view = mPathViews.get(index);
		
		int addValue = 0;
		
		if(mUtils.deviceSize(getActivity()) == 4) {
			length = 200;
			sub_offset = 40;
		} else if(mUtils.deviceSize() == 2) {
			length = 80;
			sub_offset = 0;
		}
		
        float endX = 0;
        float endY = (length + addValue)  + (length * index);
		
		AnimationSet animation = new AnimationSet(false);
		Animation translate;
		Animation rotate = new RotateAnimation(0, 
				360, 
				Animation.RELATIVE_TO_SELF, 0.5f, 
				Animation.RELATIVE_TO_SELF, 0.5f);
		rotate.setDuration(sub_duration);
		rotate.setRepeatCount(1);
		rotate.setInterpolator(AnimationUtils.loadInterpolator(getActivity(), android.R.anim.accelerate_interpolator));
		
		if(open) {
			
			translate = new TranslateAnimation(0.0f, 
					endX, 
					0.0f, 
					-endY);
	        translate.setDuration(sub_duration);
	        translate.setInterpolator(AnimationUtils.loadInterpolator(getActivity(), android.R.anim.overshoot_interpolator));
	        translate.setStartOffset(sub_offset*index);
			
	        view.setOffset(endX, -endY);
		
		} else {
			translate = new TranslateAnimation(0.0f, 
					endX, 
					0.0f, 
					endY);
			translate.setDuration(sub_duration);
			translate.setStartOffset(sub_offset*(mPathViews.size() - (index+1)));
			translate.setInterpolator(AnimationUtils.loadInterpolator(getActivity(), android.R.anim.anticipate_interpolator));
			
			
			view.setOffset(endX, -endY);
			
			
		}
		
		animation.addAnimation(rotate);
		animation.addAnimation(translate);
		
		animation.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				movePathButton(index, open);
				
			}
		});
		
		view.startAnimation(animation);
	}
	
	private int orgLeftMargin = -1;
	private int orgBottomMargin = -1;
	private void movePathButton(int index, boolean open) {
		
		PathView view = mPathViews.get(index);
		
		int addValue = 0;
        float endX = 0f;
        float endY = (length + addValue) + (length * index);
		
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
		if(orgBottomMargin == -1) {
			orgLeftMargin = params.leftMargin;
			orgBottomMargin = params.bottomMargin;
		}
		
		if(open) {
			params.leftMargin = orgLeftMargin + (int)endX;
			params.bottomMargin = orgBottomMargin + (int)endY;
			
		} else {
			
			params.leftMargin = orgLeftMargin;
			params.bottomMargin = orgBottomMargin;
		}
		
		view.setLayoutParams(params);
	}
	
	public void onBackPress() {
		mBackHomePage.performClick();
	}
	
	
	private void startSubButtonSelectedAnimation(int index) {
		
		for(int i = 0 ; i < mPathViews.size() ; i++) {
			if(index == i) {
				PathView view = mPathViews.get(i);
				
				AnimationSet animation = new AnimationSet(false);
				 
				Animation translate = new TranslateAnimation(
		        		0.0f, view.getXOffset()
		        		, 0.0f, view.getYOffset());
				translate.setDuration(0);
				
				Animation scale = new ScaleAnimation(
						0.5f, 1.0f
						, 0.5f, 1.0f
						, Animation.RELATIVE_TO_SELF, 0.5f
						, Animation.RELATIVE_TO_SELF, 0.5f);
				scale.setDuration(sub_select_duration);
				
				Animation alpha = new AlphaAnimation(1.0f, 0.0f);
				alpha.setDuration(sub_select_duration);
				
				animation.addAnimation(scale);
				animation.addAnimation(translate);
				animation.addAnimation(alpha);
				
				view.startAnimation(animation);
			} else {
				PathView view = mPathViews.get(i);
				
				AnimationSet animation = new AnimationSet(false);
				
				Animation translate = new TranslateAnimation(
		        		0.0f, view.getXOffset()
		        		, 0.0f, view.getYOffset());
				translate.setDuration(0);
				
				Animation scale = new ScaleAnimation(
						1.0f, 0.0f
						, 1.0f, 0.0f
						, Animation.RELATIVE_TO_SELF, 0.5f
						, Animation.RELATIVE_TO_SELF, 0.5f);
				scale.setDuration(sub_select_duration);
				
				Animation alpha = new AlphaAnimation(1.0f, 0.0f);
				alpha.setDuration(sub_select_duration);
				
				animation.addAnimation(scale);
				animation.addAnimation(translate);
				animation.addAnimation(alpha);
				
				view.startAnimation(animation);
			}
			
		}
		
		if(isMenuOpened) {
			isMenuOpened = false;
			mItemShare.setImageResource(R.drawable.bt_share);
			
			Animation rotate = new RotateAnimation(-45, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
			
			rotate.setInterpolator(AnimationUtils.loadInterpolator(getActivity(), android.R.anim.anticipate_overshoot_interpolator));
			rotate.setFillAfter(true);
			rotate.setDuration(sub_select_duration);
			
			for(int i = 0 ; i < mPathViews.size() ; i++) {
				movePathButton(i, false);
			}			
		}
	}
	
	
	private void initManager() {
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mUtils = app.getUtils();
		mConnector = app.getConnector();
		mSettingManager = app.getSettingManager();
		mNetworkManager = app.getNetworkManager();
		mResponseManager = app.getResponseManager();
		mLookItemManager = app.getLookItemManager();
		mClosetLookItemManager = app.getClosetLookItemManager();
	}
	
	
	public void getDetailFeedItem(int pLookId, int pDetailId, int pItemId, int pInventoryId, String pUrl) {
		DetailFeedItem detailItem = new DetailFeedItem();
		detailItem.setLookId(pLookId);
		detailItem.setDetailId(pDetailId);
		detailItem.setItemId(pItemId);
		detailItem.setInventoryId(pInventoryId);
		detailItem.setImageUrl(pUrl);
		detailItem.setVerticalScrollBlock(true);
		FragmentEvent event = new FragmentEvent(detailItem);
		mFrItemsListener.onEvent(event);
	}
	
	private class PagerListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int state) {
			
		}

		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixel) {
			
		}

		@Override
		public void onPageSelected(int position) {
			
			if(isMenuOpened) {
				isMenuOpened = false;
				startMenuAnimation(isMenuOpened);
			}
			
			try {
				mCurrentPos = (position % mLookItems.size());
			} catch(Exception e) {
				e.printStackTrace();
				mCurrentPos = 0;
			}
			
			setCurrentPosition(mCurrentPos);
			
			HashMap<String, String> properties = new HashMap<String, String>();
			properties.put("look_id", String.valueOf(mLookItems.get(mCurrentPos).getLookId()));
			
			if(mIsCloset) {
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_VIEW_LOOK_CLOSET, properties);
				LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_VIEW_LOOK_CLOSET, properties);
			} else {
				if(mSettingManager.getDisplayType().equals(CreamStyleApplication.CREAM)) {
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_VIEW_LOOK, properties);
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_VIEW_LOOK, properties);
				} else {
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_VIEW_LOOK_SURE, properties);
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_VIEW_LOOK_SURE, properties);
				}
			}
			
		}
		
	}
	
	private void setCurrentPosition(int pPosition) {
		
		final LookItem item = mLookItems.get(mCurrentPos);
		mLookName.setText(item.getLookName());
		mDisplayName.setText(item.getDisplayName());
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				
				if(item.getCloset()) {
					mItemHeart.setImageResource(R.drawable.heart_on);
				} else {
					mItemHeart.setImageResource(R.drawable.heart_btn);
				}
			}
			
		});
		
	}
	
	public void setInformation(final HomePage item) {
		mLookName.setText(item.getLookName());
		mDisplayName.setText(item.getDisplayName());
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if(item.getLookCloset() == 1) {
					mItemHeart.setImageResource(R.drawable.heart_on);
				} else {
					mItemHeart.setImageResource(R.drawable.heart_btn);
				}
			}
			
		});
	}
	
	public void setOnFeedItem(OnFrRemoveListener pFrRemoveListener, OnFrItemsListener pFrItemsListener, OnFrClosetListener pFrClosetListener, OnFrCartListener pFrCartListener, OnShareFacebook pShareFBListener, OnFrContentFeedListener pFrContentFeedListener, CreamMainActivity pMainActivity) {
		mFrRemoveListener = pFrRemoveListener;
		mFrItemsListener = pFrItemsListener;
		mFrClosetListener = pFrClosetListener;
		mFrCartListener = pFrCartListener;
		mShareFBListener = pShareFBListener;
		mFrContentFeedListener = pFrContentFeedListener;
		mMainActivity = pMainActivity;
	}
	
	OnClickListener goHomeListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mFrContentFeedListener.onEvent();
		}
		
	};

	OnClickListener homeListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mFrRemoveListener.onEvent(ItemDetailFragment.this);
		}
		
	};
	
	OnClickListener closetListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if(mIsCloset) {
				mFrRemoveListener.onEvent(ItemDetailFragment.this);
			} else {
				mFrClosetListener.onEvent(null);
			}
			
		}
		
	};
	
	OnClickListener setClosetListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mDialog = mUtils.getFullScreenDialog(getActivity());
			mDialog.show();
			
			if(mNetworkManager.isNetworkAvailable(getActivity())) {
				LookItem item = mLookItems.get(mCurrentPos);
				
				String type = ADD_LOOK;
				
				if(item.getCloset()) {
					type = REMOVE_LOOK;
				}  else {
					type = ADD_LOOK;
				}
				
				mConnector.setCloset(type, String.valueOf(mSettingManager.getUid()), mSettingManager.getLoginToken(), String.valueOf(item.getLookId()), null, new ClosetResponseEvent(type, item.getLookId()));
			} else {
				mUtils.showToastMessage(getActivity(), R.string.unconnected_network);
			}
		}
		
	};
	
	OnClickListener cartListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mFrCartListener.onEvent(null);
		}
		
	};
	
	
	private class ClosetResponseEvent implements IResponseEvent<Object> {
		private String mType;
		private int mLookId;
		
		public ClosetResponseEvent(String pType, int pLookId) {
			mType = pType;
			mLookId = pLookId;
		}
		
		@Override
		public void onResponse(Object pParams) {
			DevLog.Logging("Response", pParams.toString());
			
			new ClosetResponseTask(mType, mLookId).execute(pParams.toString());
		}
		
	}
	
	
	private class ClosetResponseTask extends AsyncTask<String, Void, Boolean> {
		private String mErrorMessage = "error";
		private String mType;
		private int mCode;
		private int mLookId;
		private String mResponse;
		
		public ClosetResponseTask(String pType, int pLookId) {
			mType = pType;
			mLookId = pLookId;
		}
		
		@Override
		protected Boolean doInBackground(String... params) {
			mResponse = params[0];
			SetClosetObject object = new SetClosetObject();
			boolean result = object.onResponseListener(params[0]);
			mErrorMessage = object.getErrorMessage();
			mCode = object.getCode();
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			
			dismiss();
			
			if(result) {
				
				HashMap<Integer, LookItem> lookItems = mLookItemManager.getLookItems();
				LookItem lookItem = lookItems.get(mLookId);
				
				mResponseManager.setClosetResponse(mResponse);
				
				if(mType.equals(ADD_LOOK)) {
					
					HashMap<String, String> properties = new HashMap<String, String>();
					properties.put("look_id", String.valueOf(mLookId));
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_ADD_LOOK_TO_CLOSET, properties);
					
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_ADD_LOOK_TO_CLOSET, properties);
					
					mItemHeart.setImageResource(R.drawable.heart_on);
					lookItem.setCloset(true);
					
				} else {
					
					HashMap<String, String> properties = new HashMap<String, String>();
					properties.put("look_id", String.valueOf(mLookId));
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_REMOVE_LOOK_FROM_THEIR_CLOSET, properties);
					
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_REMOVE_LOOK_FROM_THEIR_CLOSET, properties);
					
					mItemHeart.setImageResource(R.drawable.heart_btn);
					lookItem.setCloset(false);
					
					ClosetLookItemManager closetLookImageManager = CreamStyleApplication.getApplication().getClosetLookItemManager();
					ClosetFeedItemManager closetFeedItemManager = CreamStyleApplication.getApplication().getClosetFeedItemManager();
					
					HashMap<Integer, LookItem> closetLookItem = closetLookImageManager.getLookItems();
					closetLookItem.remove(mLookId);
					
					HashMap<Integer, ClosetFeed> closetFeeds = closetFeedItemManager.getClosetFeeds();
					closetFeeds.remove(mLookId);
				}
				
			} else {
				if(mCode == 109) {
					mSettingManager.setLogin(false);
					
					mCreamDialog = new CreamStyleDialog(getActivity(), R.layout.question_layout, R.string.cream, R.drawable.icon_info);
					mCreamDialog.setCancelable(false);
					
					View v = mCreamDialog.getParentView();
					
					TextView contentText = (TextView)v.findViewById(R.id.contentText);
					contentText.setText(R.string.login_notice);
					
					TextView cancel = (TextView)mCreamDialog.getConfirmView();
					cancel.setText(R.string.not_yet);
					TextView login = (TextView)mCreamDialog.getCancelView();
					login.setText(R.string.login);
					mCreamDialog.show();
					
					cancel.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dismissDialog();
						}
						
					});
					
					login.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dismissDialog();
							
							Intent intent = new Intent(getActivity(), CreamAccountActivity.class);
							startActivity(intent);
							getActivity().finish();
						}
						
					});
				} else {
					mUtils.showToastMessage(getActivity(), mErrorMessage);
				}
			}
			
		}
		
	}
	
	private void dismissDialog() {
		if(mCreamDialog != null && mCreamDialog.isShowing()) {
			mCreamDialog.dismiss();
			mCreamDialog = null;
		}
	}
	
	private void dismiss() {
		if(mDialog != null && mDialog.isShowing()) {
			mDialog.dismiss();
			mDialog = null;
		}
	}

	
	private void shareToPinterest() {
		LookItem item = mLookItems.get(mCurrentPos);
		
		PinterestShareManager pManager = new PinterestShareManager(getActivity());
		pManager.share(true, item.getLookId(), item.getSmallLookImage(), item.getLookName(), item.getDisplayName());
	}
	
	private Intent shareGooglePlus() {
		LookItem item = mLookItems.get(mCurrentPos);
		
		GooglePlusShareManager pManager = new GooglePlusShareManager(getActivity());
		return pManager.share(item.getLookId(), item.getSmallLookImage(), item.getLookName(), item.getDisplayName());
	}
	
	
	private void shareToTwitter() {
		if(BasicInfo.TwitLogin == true) {
			new TwitterRequest().execute();
		} else {
			new TwitterRequestToken().execute();
		}
	}
	
	class TwitterRequestToken extends AsyncTask<Void, Void, Boolean>{
		private RequestToken mRequestToken;
		
		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setDebugEnabled(true);
				builder.setOAuthConsumerKey(BasicInfo.TWIT_CONSUMER_KEY);
				builder.setOAuthConsumerSecret(BasicInfo.TWIT_CONSUMER_SECRET);
				
				TwitterFactory factory = new TwitterFactory(builder.build());
				Twitter mTwit = factory.getInstance();
				mRequestToken = mTwit.getOAuthRequestToken();
				
				BasicInfo.TwitInstance = mTwit;
				BasicInfo.TwitRequestToken = mRequestToken;
				return true;
			} catch (TwitterException e) {
				e.printStackTrace();
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			dismiss();
			Intent intent = new Intent(getActivity(), TwitLogin.class);
			intent.putExtra("authUrl", mRequestToken.getAuthenticationURL());
			startActivityForResult(intent, BasicInfo.REQ_CODE_TWIT_LOGIN);
		}
		
		
	}
	
	class TwitterRequest extends AsyncTask<Void, Void, Boolean>{

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setOAuthAccessToken(BasicInfo.TWIT_KEY_TOKEN);
				builder.setOAuthAccessTokenSecret(BasicInfo.TWIT_KEY_TOKEN_SECRET);
				builder.setOAuthConsumerKey(BasicInfo.TWIT_CONSUMER_KEY);
				builder.setOAuthConsumerSecret(BasicInfo.TWIT_CONSUMER_SECRET);
				
				Configuration config = builder.build();
				TwitterFactory tFactory = new TwitterFactory(config);
				BasicInfo.TwitInstance = tFactory.getInstance();
				return true;
			} catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			dismiss();
			if(result == true) {
				new ShareAsync().execute();
			}
		}
		
	}
	
	class ShareAsync extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				LookItem item = mLookItems.get(mCurrentPos);
				
				BasicInfo.TwitInstance.updateStatus(String.format(getString(R.string.share_twitter), item.getLookName(), item.getDisplayName()));
				return true;
			} catch (TwitterException e) {
				e.printStackTrace();
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if(result == true) {
				HashMap<String, String> properties = new HashMap<String, String>();
				properties.put("look_id", String.valueOf(mLookItems.get(mCurrentPos).getLookId()));
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_LOOK_SHARED_ON_TWITTER, properties);
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_LOOK_SHARED, properties);
				
				mUtils.showToastMessage(getActivity(), R.string.share_success);
			} else {
				mUtils.showToastMessage(getActivity(), R.string.share_error);
			}
			
		}
		
	}
	
	private void shareToFacebook() {
		LookItem lookItem = mLookItems.get(mCurrentPos);
		FBShareItem item = new FBShareItem();
		item.setItemName(lookItem.getLookName());
		item.setItemDetailName(lookItem.getDisplayName());
		item.setDescription(getString(R.string.share_description));
		item.setPictureUrl(lookItem.getSmallLookImage());
		item.setType(true);
		item.setId(lookItem.getLookId());
		
		FragmentEvent event = new FragmentEvent(item);
		mShareFBListener.onEvent(event);
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == BasicInfo.REQ_CODE_TWIT_LOGIN) {
			new ResponseAsync(data).execute();
		} else if(requestCode == 0) {
			if(resultCode == getActivity().RESULT_OK) {
				LookItem item = mLookItems.get(mCurrentPos);
				HashMap<String, String> properties = new HashMap<String, String>();
				properties.put("look_id", String.valueOf(item.getLookId()));
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_LOOK_SHARED_ON_GOOGLE, properties);
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_LOOK_SHARED, properties);
				
				LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_LOOK_SHARED_ON_GOOGLE, properties);
				LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_LOOK_SHARED, properties);
			} 
		}
	}
	
	class ResponseAsync extends AsyncTask<Void, Void, Boolean> {
		private Intent mData;
		
		public ResponseAsync(Intent pData) {
			mData = pData;
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				Twitter mTwit = BasicInfo.TwitInstance;
				AccessToken mAccessToken = mTwit.getOAuthAccessToken(BasicInfo.TwitRequestToken, mData.getStringExtra("oauthVerifier"));
				BasicInfo.TwitLogin = true;
				BasicInfo.TWIT_KEY_TOKEN = mAccessToken.getToken();
				BasicInfo.TWIT_KEY_TOKEN_SECRET = mAccessToken.getTokenSecret();
				BasicInfo.TwitAccessToken = mAccessToken;
				return true;
			} catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if(result == true) {
				new ShareAsync().execute();
			}
		}
		
	}
}
