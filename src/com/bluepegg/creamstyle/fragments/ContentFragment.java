package com.bluepegg.creamstyle.fragments;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Dialog;
import android.app.Fragment;
import android.app.LauncherActivity.ListItem;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.activities.CreamAccountActivity;
import com.bluepegg.creamstyle.activities.CreamMainActivity;
import com.bluepegg.creamstyle.adapters.ContentAdapter;
import com.bluepegg.creamstyle.dialogs.CreamStyleDialog;
import com.bluepegg.creamstyle.interfaces.OnFrDetailListener;
import com.bluepegg.creamstyle.interfaces.OnUpdateFeedListener;
import com.bluepegg.creamstyle.kissmetrics.CreamStyleStateEventTypes;
import com.bluepegg.creamstyle.kissmetrics.CreamstyleAnalytics;
import com.bluepegg.creamstyle.localytics.LocalyticsAnalytics;
import com.bluepegg.creamstyle.objects.FragmentEvent;
import com.bluepegg.creamstyle.objects.LookItem;
import com.bluepegg.creamstyle.objects.manager.LookItemManager;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.staggeredview.StaggeredGridView;
import com.bluepegg.creamstyle.staggeredview.StaggeredGridView.OnItemClickListener;
import com.bluepegg.creamstyle.utils.DevLog;
import com.bluepegg.creamstyle.utils.Utils;


public class ContentFragment extends Fragment {
	private static final int CREAM_TAB = 0;
	private static final int SURE_TAB = 1;
	private SettingManager mSettingManager;
	private Utils mUtils;
	private LookItemManager mLookItemManager;
	private LinearLayout mActionbarContainer;
	private StaggeredGridView mStaggeredGridView;
	private OnFrDetailListener mFrDetailListener = null;
	private LinearLayout mFeedLoader;
	private ContentAdapter mAdapter;
	private List<ListItem> mItems;
	private CreamStyleDialog mDialog;
	private Fragment mFragment;
	private CreamMainActivity mMainActivity;
	private ImageView mCreamTab, mSureTab;
	private ImageView mMoveToTop;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mSettingManager = app.getSettingManager();
		mUtils = app.getUtils();
		mLookItemManager = app.getLookItemManager();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.home_fragment_layout, container, false);
	
		mFragment = ContentFragment.this;
		
		if(mAdapter == null)
			mAdapter = new ContentAdapter(getActivity(), R.layout.all_feed_item);
		
		mActionbarContainer = (LinearLayout)view.findViewById(R.id.actionbarContainer);
		mStaggeredGridView = (StaggeredGridView)view.findViewById(R.id.staggeredGridView);
		mFeedLoader = (LinearLayout)view.findViewById(R.id.feedLoader);
		mCreamTab = (ImageView)view.findViewById(R.id.creamTab);
		mSureTab = (ImageView)view.findViewById(R.id.sureTab);
		mMoveToTop = (ImageView)view.findViewById(R.id.moveToTop);
		
		mCreamTab.setOnClickListener(mCreamTabListener);
		mSureTab.setOnClickListener(mSureTabListener);
		
		mStaggeredGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(StaggeredGridView parent, View view, int position, long id) {
				if(mSettingManager.getLogin()) {
					ContentAdapter adapter = (ContentAdapter)parent.getAdapter();
					LookItem item = (LookItem)adapter.getItem(position);
					
					HashMap<String, String> properties = new HashMap<String, String>();
					properties.put("look_id", String.valueOf(item.getLookId()));
					if(mSettingManager.getDisplayType().equals(CreamStyleApplication.CREAM))
						CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_VIEW_LOOK_DIRECT, properties);
					else
						CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_VIEW_LOOK_DIRECT_SURE, properties);
					
					
					FragmentEvent event = new FragmentEvent(item.getLookId(), position, false);
					mFrDetailListener.onEvent(event);
					
				} else {
					mDialog = new CreamStyleDialog(getActivity(), R.layout.question_layout, R.string.cream, R.drawable.icon_info);
					mDialog.setCancelable(false);
					
					View v = mDialog.getParentView();
					
					TextView contentText = (TextView)v.findViewById(R.id.contentText);
					contentText.setText(R.string.login_notice);
					
					TextView cancel = (TextView)mDialog.getConfirmView();
					cancel.setText(R.string.not_yet);
					TextView login = (TextView)mDialog.getCancelView();
					login.setText(R.string.login);
					mDialog.show();
					
					cancel.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dismissDialog(mDialog);
						}
						
					});
					
					login.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dismissDialog(mDialog);
							
							Intent intent = new Intent(getActivity(), CreamAccountActivity.class);
							startActivity(intent);
							getActivity().finish();
						}
						
					});
				}
				
			}
			
		});
		
			
		mMoveToTop.setOnClickListener(topListener);
		
		return view;
	}
	
	OnClickListener topListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mAdapter.notifyDataSetChanged();
		}
		
	};

	
	OnUpdateFeedListener updateFeedListener = new OnUpdateFeedListener() {

		@Override
		public void updateItemList(int pType) {
			if(pType == 0) {
				updateFeedItem();
			} else if(pType == 1) {
				updateSureFeedItem();
			}
		}
		
	};
	
	
	OnClickListener mCreamTabListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			updateFeedItem();
		}
		
	};
	
	OnClickListener mSureTabListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			updateSureFeedItem();
		}
		
	};
	
	private void setTab(int pPosition) {
		mCreamTab.setImageResource(R.drawable.ic_logo);
		mSureTab.setImageResource(R.drawable.ic_sure);
		
		switch(pPosition) {
		case CREAM_TAB:
			mCreamTab.setImageResource(R.drawable.ic_logo_bar);
			break;
			
		case SURE_TAB:
			mSureTab.setImageResource(R.drawable.ic_sure_bar);
			break;
		}
	}
	
	
	
	private void refreshFeedItem() {
		CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_CREAM_FEED, null);
		LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_CREAM_FEED, null);
		
		mSettingManager.setDisplayType(CreamStyleApplication.CREAM);
		
		setTab(CREAM_TAB);
		
		int position = mStaggeredGridView.getFirstPosition();
		
		
		HashMap<Integer, LookItem> lookItems = mLookItemManager.getLookItems();
		int y = mStaggeredGridView.getScrollY();
		
		if(lookItems.size() > 0) {
			mAdapter.clear();
			
			for(Map.Entry<Integer, LookItem> item : lookItems.entrySet()) {
				mAdapter.add(item.getValue());
			}
			
			mFeedLoader.setVisibility(View.GONE);
			mActionbarContainer.setVisibility(View.VISIBLE);
			mStaggeredGridView.setDrawSelectorOnTop(false);
			mStaggeredGridView.setAdapter(mAdapter);
		} else {
			mFeedLoader.setVisibility(View.VISIBLE);
			mActionbarContainer.setVisibility(View.GONE);
		}
		
		
		//mAdapter.notifyDataSetChanged();
	}
	
	private void refreshSureItem() {
		CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_SURE_FEED, null);
		
		LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_SURE_FEED, null);
		
		mSettingManager.setDisplayType(CreamStyleApplication.SURE);
		
		setTab(SURE_TAB);
		
		HashMap<Integer, LookItem> lookItems = mLookItemManager.getLookItems();
		mAdapter.clear();
		
		for(Map.Entry<Integer, LookItem> item : lookItems.entrySet()) {
			if(item.getValue().getFeedMask() == 1) {
				mAdapter.add(item.getValue());
			}
		}
		
		if(lookItems.size() > 0) {
			mFeedLoader.setVisibility(View.GONE);
			mActionbarContainer.setVisibility(View.VISIBLE);
			mStaggeredGridView.setAdapter(mAdapter);
		} else {
			mFeedLoader.setVisibility(View.VISIBLE);
			mActionbarContainer.setVisibility(View.GONE);
		}
		
		mAdapter.notifyDataSetChanged();
	}

	
	public void updateFeedItem() {
		
		new Handler(Looper.getMainLooper()).post(new Runnable() {

			@Override
			public void run() {
				refreshFeedItem();
			}
			
		});
		
	}
	
	public void updateSureFeedItem() {
		
		new Handler(Looper.getMainLooper()).post(new Runnable() {

			@Override
			public void run() {
				refreshSureItem();
			}
			
		});
	}
	
	public void setOnFeedItem(OnFrDetailListener pFrDetailListener, Fragment pFragment, CreamMainActivity pMainActivity) {
		mFrDetailListener = pFrDetailListener;
		mFragment = pFragment;
		mMainActivity = pMainActivity;
	}
	
	
	private void dismissDialog(Dialog pDialog) {
		if(pDialog != null && pDialog.isShowing()) {
			pDialog.dismiss();
			pDialog = null;
		}
	}
	

	public void setClosetItem(int pKey, int pPosition, boolean pIsCloset) {
		FragmentEvent event = new FragmentEvent(pKey, pPosition, pIsCloset);
		mFrDetailListener.onEvent(event);
	}

	@Override
	public void onResume() {
		super.onResume();
		
//		if(mAdapter == null)
//			mAdapter = new ContentAdapter(getActivity(), R.layout.all_feed_item, updateFeedListener);
//		
//		if(mSettingManager.getDisplayType().equals(CreamStyleApplication.CREAM)) {
//			updateFeedItem();
//		} else {
//			updateSureFeedItem();
//		}
		
		DevLog.defaultLogging(mSettingManager.getDisplayType().toString() + " <<<<<<<<<");
		
		if(mSettingManager.getDisplayType().equals(CreamStyleApplication.CREAM)) {
			updateFeedItem();
		} else {
			updateSureFeedItem();
		}
		
		mMainActivity.setMenuIcon(View.VISIBLE);
		mMainActivity.setContentFragment();
		
	}
	
}
