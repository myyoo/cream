package com.bluepegg.creamstyle.fragments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.account.IConnector;
import com.bluepegg.creamstyle.account.IResponseEvent;
import com.bluepegg.creamstyle.activities.CreamAccountActivity;
import com.bluepegg.creamstyle.activities.CreamMainActivity;
import com.bluepegg.creamstyle.adapters.ClosetFeedPageAdapter;
import com.bluepegg.creamstyle.interfaces.OnFrContentFeedListener;
import com.bluepegg.creamstyle.interfaces.OnFrDetailListener;
import com.bluepegg.creamstyle.interfaces.OnFrRemoveListener;
import com.bluepegg.creamstyle.interfaces.OnLoginListener;
import com.bluepegg.creamstyle.interfaces.OnShowClosetDetailListener;
import com.bluepegg.creamstyle.interfaces.OnShowClosetListener;
import com.bluepegg.creamstyle.kissmetrics.CreamStyleStateEventTypes;
import com.bluepegg.creamstyle.kissmetrics.CreamstyleAnalytics;
import com.bluepegg.creamstyle.localytics.LocalyticsAnalytics;
import com.bluepegg.creamstyle.network.NetworkManager;
import com.bluepegg.creamstyle.objects.ClosetFeed;
import com.bluepegg.creamstyle.objects.ClosetFeedCompare;
import com.bluepegg.creamstyle.objects.ClosetItemDetail;
import com.bluepegg.creamstyle.objects.FeedItemList;
import com.bluepegg.creamstyle.objects.ItemDetail;
import com.bluepegg.creamstyle.objects.ItemList;
import com.bluepegg.creamstyle.objects.LookItem;
import com.bluepegg.creamstyle.objects.manager.ClosetFeedItemManager;
import com.bluepegg.creamstyle.objects.manager.ClosetItemDetailListManager;
import com.bluepegg.creamstyle.objects.manager.ClosetItemListManager;
import com.bluepegg.creamstyle.objects.manager.ClosetLookItemManager;
import com.bluepegg.creamstyle.objects.manager.ItemDetailListManager;
import com.bluepegg.creamstyle.objects.manager.ItemListManager;
import com.bluepegg.creamstyle.objects.manager.LookItemManager;
import com.bluepegg.creamstyle.objects.manager.ResponseManager;
import com.bluepegg.creamstyle.objecttype.ClosetFeedObject;
import com.bluepegg.creamstyle.objecttype.ClosetItemDetailListObject;
import com.bluepegg.creamstyle.objecttype.ClosetItemListObject;
import com.bluepegg.creamstyle.objecttype.ClosetLookListObject;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.staggeredview.StaggeredGridView;
import com.bluepegg.creamstyle.staggeredview.StaggeredGridView.OnItemClickListener;
import com.bluepegg.creamstyle.utils.DevLog;
import com.bluepegg.creamstyle.utils.Utils;

public class ClosetFragment extends Fragment {
	private OnFrRemoveListener mFrRemoveListener = null;
	private OnFrContentFeedListener mFrContentFeedListener = null;
	private OnLoginListener mLoginListener = null;
	private OnShowClosetListener mShowClosetListener = null;
	private OnShowClosetDetailListener mShowClosetDetailListener = null;
	private OnFrDetailListener mFrDetailListener = null;
	private CreamMainActivity mMainActivity;
	
	private Context mContext;
	private IConnector mConnector;
	private SettingManager mSettingManager;
	private NetworkManager mNetworkManager;
	private ResponseManager mResponseManager;
	private LookItemManager mLookItemManager;
	private ItemDetailListManager mItemDetailListManager;
	private ItemListManager mItemListManager;
	
	private ClosetLookItemManager mClosetLookItemManager;
	private ClosetItemDetailListManager mClosetItemDetailListManager;
	private ClosetItemListManager mClosetItemListManager;
	private ClosetFeedItemManager mClosetFeedItemManager;
	
	private Utils mUtils;
	
	private LinearLayout mClosetLoader, mEmptyClosetItemContainer;
	private RelativeLayout mClosetItemContainer;
	private ImageView mBackView;
	private ImageView mGoHome, mGoToHome;
	private ImageView mBackHomePage;
	private TextView mErrorMessage;
	private StaggeredGridView mStaggeredGridView;
	private ClosetFeedPageAdapter mAdapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		initManager();
		View view = inflater.inflate(R.layout.closet_fragment_layout, container, false);
		mClosetLoader = (LinearLayout)view.findViewById(R.id.closetLoader);
		mClosetItemContainer = (RelativeLayout)view.findViewById(R.id.closetItemContainer);
		mEmptyClosetItemContainer = (LinearLayout)view.findViewById(R.id.emptyClosetItemContainer);
		mErrorMessage = (TextView)view.findViewById(R.id.errorMessage);
		mBackView = (ImageView)view.findViewById(R.id.backView);
		mBackView.setOnClickListener(backListener);
		mStaggeredGridView = (StaggeredGridView)view.findViewById(R.id.staggeredGridView);
		mBackHomePage = (ImageView)view.findViewById(R.id.backHomePage);
		mBackHomePage.setOnClickListener(removeListener);
		mGoHome = (ImageView)view.findViewById(R.id.goHome);
		mGoHome.setOnClickListener(goHomeListener);
		mGoToHome = (ImageView)view.findViewById(R.id.goToHome);
		mGoToHome.setOnClickListener(goHomeListener);
		
		if(mAdapter == null)
			mAdapter = new ClosetFeedPageAdapter(mContext, R.layout.closet_item);
		
		mStaggeredGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(StaggeredGridView parent, View view, final int position, long id) {
//				mSettingManager.setDisplayType(CreamStyleApplication.CREAM);
				
				ClosetFeed closetFeed = (ClosetFeed)mAdapter.getItem(position);
				
				if(closetFeed.getClosetLook()) {
					HashMap<Integer, LookItem> lookItems = mClosetLookItemManager.getLookItems();
					
					if(lookItems.get(closetFeed.getId()) == null) {
						mUtils.showToastMessage(getActivity(), R.string.expired_closet_item);
					} else {
						HashMap<String, String> properties = new HashMap<String, String>();
						properties.put("look_id", String.valueOf(closetFeed.getId()));
//						
						CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_VIEW_LOOK_CLOSET, properties);
						LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_VIEW_LOOK_CLOSET, properties);
						
						CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_VIEW_LOOK_DIRECT_CLOSET, properties);
						LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_VIEW_LOOK_DIRECT_CLOSET, properties);
							
						mShowClosetListener.onEvent(closetFeed);
					}
					
				} else {
					HashMap<Integer, LookItem> lookItems = mLookItemManager.getLookItems();
					
					boolean exist = false;
					for(Map.Entry<Integer, LookItem> item : lookItems.entrySet()) {
						if(item.getValue().getItemIdList().contains(closetFeed.getId())) {
							exist = true;
							break;
						}
					}
					
					if(!exist) {
						lookItems = mClosetLookItemManager.getLookItems();
						for(Map.Entry<Integer, LookItem> item : lookItems.entrySet()) {
							if(item.getValue().getItemIdList().contains(closetFeed.getId())) {
								exist = true;
//								closetFeed.setExpiredItem(true);
								break;
							}
						}
					}
					
					HashMap<String, String> properties = new HashMap<String, String>();
					properties.put("item_detail_id", String.valueOf(closetFeed.getDetailId()));
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_VIEW_ITEM_DIRECT, properties);
					
					if(exist) {
						FeedItemList feedItem = new FeedItemList();
						feedItem.setClosetFeed(closetFeed);
						mShowClosetDetailListener.onEvent(ClosetFragment.this, feedItem);
					} else {
						mUtils.showToastMessage(getActivity(), R.string.expired_closet_item);
					}
					
				
				}
				
				
			}
		});
		
		
		if(mNetworkManager.isNetworkAvailable(mContext)) {
			mConnector.getCloset(String.valueOf(mSettingManager.getUid()), mSettingManager.getLoginToken(), new ClosetResponseEvent());
		} else {
			mErrorMessage.setVisibility(View.VISIBLE);
			mClosetLoader.setVisibility(View.GONE);
		}
		
		showClosetFeedItem();
		
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		mMainActivity.setMenuIcon(View.VISIBLE);
	}
	
	private void showClosetFeedItem() {
		
		new Handler(Looper.getMainLooper()).post(new Runnable() {

			@Override
			public void run() {
				
				HashMap<Integer, LookItem> closetLookItem = mClosetLookItemManager.getLookItems();
				HashMap<Integer, ItemList> closetItemList = mClosetItemListManager.getItemList();
				HashMap<Integer, ClosetItemDetail> closetItemDetail = mClosetItemDetailListManager.getItemDetails();
				
				
				for(Map.Entry<Integer, ItemList> item : closetItemList.entrySet()) {
					ItemList closetItem = item.getValue();
					List<Integer> closetDetails = closetItem.getItemDetailIdList();
					
					for(int i = 0; i < closetDetails.size(); ++i) {
						ClosetItemDetail closetDetail = closetItemDetail.get(closetDetails.get(i));
						
						if(closetDetail != null && closetDetail.getClosetTime() != 0) {
							ClosetFeed closetFeed = new ClosetFeed();
							closetFeed.setClosetLook(false);
							closetFeed.setId(closetDetail.getItemId());
							closetFeed.setDetailId(closetDetail.getItemDetailId());
							closetFeed.setSmallImage(closetDetail.getSmallItemImage());
							closetFeed.setImageWidth(closetDetail.getItemWidth());
							closetFeed.setImageHeight(closetDetail.getItemHeight());
							closetFeed.setClosetTime(closetDetail.getClosetTime());
							closetFeed.setInventoryId(closetDetail.getInventoryIdList());
							closetFeed.setInventorySize(closetDetail.getInventorySizeList());
							mClosetFeedItemManager.putItem(closetDetail.getItemDetailId(), closetFeed);
					
						}
					}
					
				}
				
				for(Map.Entry<Integer, LookItem> item : closetLookItem.entrySet()) {
					LookItem closetItem = item.getValue();
					
					ClosetFeed closetFeed = new ClosetFeed();
					closetFeed.setClosetLook(true);
					closetFeed.setId(closetItem.getLookId());
					closetFeed.setSmallImage(closetItem.getSmallLookImage());
					closetFeed.setImageWidth(closetItem.getLookWidth());
					closetFeed.setImageHeight(closetItem.getLookHeight());
					closetFeed.setClosetTime(closetItem.getClosetTime());
					closetFeed.setFeedMask(closetItem.getFeedMask());
				
					mClosetFeedItemManager.putItem(closetItem.getLookId(), closetFeed);
				}
				
				
				HashMap<Integer, ClosetFeed> result = mClosetFeedItemManager.getClosetFeeds();
				
				if(result.size() > 0) {
					mClosetLoader.setVisibility(View.GONE);
		    		mClosetItemContainer.setVisibility(View.VISIBLE);
		    		mEmptyClosetItemContainer.setVisibility(View.GONE);
					
		    		mAdapter.clear();
		    		
		    		List<ClosetFeed> values = new ArrayList<ClosetFeed>(result.values());
		    		Collections.sort(values, new ClosetFeedCompare());
		    		
		    		for(int i = 0; i < values.size(); ++i) {
		    			mAdapter.add(values.get(i));
		    		}
		    		
		    		
		    		mAdapter.notifyDataSetChanged();
		    		mStaggeredGridView.setAdapter(mAdapter);
		    		
//		    		if(mClosetState != null)
//		    			mStaggeredGridView.onRestoreInstanceState(mClosetState);
				} else {
					mClosetLoader.setVisibility(View.GONE);
		    		mClosetItemContainer.setVisibility(View.GONE);
		    		mEmptyClosetItemContainer.setVisibility(View.VISIBLE);
				}
				
			}
			
		});
	}
	
	public void onBackPress() {
		mBackView.performClick();
	}
	
	private void initManager() {
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mContext = app.getBaseContext();
		mConnector = app.getConnector();
		mSettingManager = app.getSettingManager();
		mNetworkManager = app.getNetworkManager();
		mResponseManager = app.getResponseManager();
		mUtils = app.getUtils();
		mLookItemManager = app.getLookItemManager();
		mItemDetailListManager = app.getItemDetailListManager();
		mItemListManager = app.getItemListManager();
		
		mClosetLookItemManager = app.getClosetLookItemManager();
		mClosetItemDetailListManager = app.getClosetItemDetailListManager();
		mClosetItemListManager = app.getClosetItemListManager();
		mClosetFeedItemManager = app.getClosetFeedItemManager();
		
	}
	
	private void updateLookItem() {
		HashMap<Integer, LookItem> lookItems = mLookItemManager.getLookItems();
		
		for(Map.Entry<Integer, LookItem> item : lookItems.entrySet()) {
			item.getValue().setCloset(false);
		}
		
		HashMap<Integer, LookItem> closetLookItem = mClosetLookItemManager.getLookItems();
		
		for(Map.Entry<Integer, LookItem> item : closetLookItem.entrySet()) {
			LookItem lookItem = lookItems.get(item.getKey());
			
			if(lookItem != null)
				lookItem.setCloset(true);
		}
	}
	
	private void updateDetailItem() {
		HashMap<Integer, ItemDetail> itemDetails = mItemDetailListManager.getItemDetails();
		
		for(Map.Entry<Integer, ItemDetail> item : itemDetails.entrySet()) {
			item.getValue().setCloset(false);
		}
		
		HashMap<Integer, ClosetItemDetail> closetItemDetail = mClosetItemDetailListManager.getItemDetails();
		
		for(Map.Entry<Integer, ClosetItemDetail> item : closetItemDetail.entrySet()) {
			if(item.getValue().getClosetTime() != 0) {
				ItemDetail itemDetail = itemDetails.get(item.getKey());
				
				if(itemDetail != null)
					itemDetail.setCloset(true);
			}
		}
	}
	
	
	private class ClosetResponseEvent implements IResponseEvent<Object> {

		@Override
		public void onResponse(Object pParams) {
			if(pParams == null) {
				new Handler(Looper.getMainLooper()).post(new Runnable() {

					@Override
					public void run() {
						mUtils.showToastMessage(getActivity(), R.string.unconnected_network);
					}
					
				});
				
				mClosetLoader.setVisibility(View.GONE);
	    		mClosetItemContainer.setVisibility(View.GONE);
	    		mEmptyClosetItemContainer.setVisibility(View.VISIBLE);
				
			} else {
				DevLog.Logging("Response", pParams.toString());
				
				ClosetFeedObject object = new ClosetFeedObject();
				boolean result = object.onResponseListener(pParams.toString());
				int errorCode = object.getCode();
				
				if(result) {
					
					mBackHomePage.setVisibility(View.VISIBLE);
					
					ClosetLookListObject closetLookObject = new ClosetLookListObject();
					closetLookObject.onResponseListener(mResponseManager.getClosetLookListResponse());
					updateLookItem();
					
					ClosetItemDetailListObject closetItemDetailObject = new ClosetItemDetailListObject();
					closetItemDetailObject.onResponseListener(mResponseManager.getClosetItemDetailListResponse());
					
					ClosetItemListObject closetItemListObject = new ClosetItemListObject();
					closetItemListObject.onResponseListener(mResponseManager.getClosetItemListResponse());
					updateDetailItem();
					
				} else {
					if(errorCode == 109) {
						mSettingManager.setLogin(false);
						Intent intent = new Intent(mContext, CreamAccountActivity.class);
						startActivity(intent);
						getActivity().finish();
					}
				}
			}
			
			showClosetFeedItem();
			
		}
	} 
	
	public void setOnFeedItem(OnFrRemoveListener pFrRemoveListener, OnFrContentFeedListener pFrContentFeedListener, OnLoginListener pLoginListener, OnShowClosetListener pShowClosetListener, OnShowClosetDetailListener pShowClosetDetailListener, OnFrDetailListener pFrDetailListener, CreamMainActivity pMainActivity) {
		mFrRemoveListener = pFrRemoveListener;
		mFrContentFeedListener = pFrContentFeedListener;
		mLoginListener = pLoginListener;
		mShowClosetListener = pShowClosetListener;
		mShowClosetDetailListener = pShowClosetDetailListener;
		mFrDetailListener = pFrDetailListener;
		mMainActivity = pMainActivity;
	}
	
	OnClickListener goHomeListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mFrContentFeedListener.onEvent();
		}
		
	};
	
	OnClickListener backListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if(mSettingManager.getLogin()) {
				mFrRemoveListener.onEvent(ClosetFragment.this);
			} else {
				mLoginListener.onEvent();
			}
		}
		
	};
	
	OnClickListener contentFeedListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mFrContentFeedListener.onEvent();
		}
		
	};
	
	OnClickListener removeListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mFrRemoveListener.onEvent(ClosetFragment.this);
		}
		
	};
}
