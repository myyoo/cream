package com.bluepegg.creamstyle.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.account.IConnector;
import com.bluepegg.creamstyle.account.IResponseEvent;
import com.bluepegg.creamstyle.activities.CreamAccountActivity;
import com.bluepegg.creamstyle.activities.CreamMainActivity;
import com.bluepegg.creamstyle.activities.CreamPaymentActivity;
import com.bluepegg.creamstyle.adapters.CartAdapter;
import com.bluepegg.creamstyle.dialogs.CreamStyleDialog;
import com.bluepegg.creamstyle.interfaces.OnFrContentFeedListener;
import com.bluepegg.creamstyle.interfaces.OnFrRemoveListener;
import com.bluepegg.creamstyle.interfaces.OnShowClosetDetailListener;
import com.bluepegg.creamstyle.interfaces.OnUpdateListView;
import com.bluepegg.creamstyle.kissmetrics.CreamStyleStateEventTypes;
import com.bluepegg.creamstyle.kissmetrics.CreamstyleAnalytics;
import com.bluepegg.creamstyle.localytics.LocalyticsAnalytics;
import com.bluepegg.creamstyle.network.NetworkManager;
import com.bluepegg.creamstyle.objects.CartItem;
import com.bluepegg.creamstyle.objecttype.CartObject;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.utils.DevLog;

public class CartFragment extends Fragment {
	private static final int REQUEST_CODE = 1;
	private OnFrRemoveListener mFrRemoveListener = null;
	private OnFrContentFeedListener mFrContentFeedListener = null;
	private OnShowClosetDetailListener mShowClosetDetailListener = null;
	
	private Context mContext;
	private IConnector mConnector;
	private SettingManager mSettingManager;
	private NetworkManager mNetworkManager;
	
	private LinearLayout mCartLoader, mCheckoutItemContainer;
	private RelativeLayout mErrorContainer;
	private TextView mErrorMessage;
	private ImageView mErrorBackView;
	private ImageView mBackView;
	private ImageView mGoHome;
	private TextView mEmptyMessage;
	private RelativeLayout mCartContainer;
	private ListView mCartList;
	private Button mCheckoutButton;
	
	private CartAdapter mAdapter;
	
	private CreamStyleDialog mDialog;
	
	private CreamMainActivity mMainActivity;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		initManager();
		View view = inflater.inflate(R.layout.cart_fragment_layout, container, false);
		
		mCartLoader = (LinearLayout)view.findViewById(R.id.cartLoader);
		mCheckoutItemContainer = (LinearLayout)view.findViewById(R.id.checkoutItemContainer);
		mErrorContainer = (RelativeLayout)view.findViewById(R.id.errorContainer);
		mErrorMessage = (TextView)view.findViewById(R.id.errorMessage);
		mErrorBackView = (ImageView)view.findViewById(R.id.errorBackView);
		mBackView = (ImageView)view.findViewById(R.id.backView);
		mGoHome = (ImageView)view.findViewById(R.id.goHome);
		mEmptyMessage = (TextView)view.findViewById(R.id.emptyMessage);
		mCartContainer = (RelativeLayout)view.findViewById(R.id.cartContainer);
		mCartList = (ListView)view.findViewById(R.id.cartList);
		mCartList.setItemsCanFocus(false);
		mCheckoutButton = (Button)view.findViewById(R.id.checkoutButton);
		
		if(mNetworkManager.isNetworkAvailable(mContext)) {
			mConnector.getCart(String.valueOf(mSettingManager.getUid()), String.valueOf(mSettingManager.getLoginToken()), new CartResponseEvent());
		} else {
			mErrorContainer.setVisibility(View.VISIBLE);
			mCartLoader.setVisibility(View.GONE);
		}
		
		mBackView.setOnClickListener(backListener);
		mGoHome.setOnClickListener(goHomeListener);
		mErrorBackView.setOnClickListener(backListener);
		mCheckoutButton.setOnClickListener(checkoutListener);
		
		
		return view;
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
		mMainActivity.setMenuIcon(View.GONE);
	}

	private void initManager() {
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mContext = app.getBaseContext();
		mConnector = app.getConnector();
		mSettingManager = app.getSettingManager();
		mNetworkManager = app.getNetworkManager();
	}
	
	public void onBackPress() {
		mBackView.performClick();
	}
	
	private class CartResponseEvent implements IResponseEvent<Object> {

		@Override
		public void onResponse(Object pParams) {
			DevLog.Logging("Response", pParams.toString());
			new CartResponseTask().execute(pParams.toString());
		}
		
	}
	
	private class CartResponseTask extends AsyncTask<String, Void, Boolean> {
		private String mError;
		private int mCode;
		private String mSubTotal;
		private List<CartItem> mCartItems;
		private int mCount;
		
		@Override
		protected void onPreExecute() {
			mCartItems = new ArrayList<CartItem>();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			CartObject object = new CartObject(mCartItems);
			boolean result = object.onResponseListener(params[0]);
			mSubTotal = object.getSubTotal();
			mError = object.getErrorMessage();
			mCode = object.getCode();
			mCount = object.getCount();
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			mCartLoader.setVisibility(View.GONE);
			
			if(result) {
				DevLog.defaultLogging("Total price: " + mSubTotal);
				mErrorContainer.setVisibility(View.GONE);
				mCheckoutItemContainer.setVisibility(View.VISIBLE);
				
				if(mCartItems.size() > 0) {
					mAdapter = new CartAdapter(getActivity(), CartFragment.this, mCartItems, updateListViewListener, mShowClosetDetailListener);
					mCartList.setAdapter(mAdapter);
					
					mEmptyMessage.setVisibility(View.GONE);
					mCartContainer.setVisibility(View.VISIBLE);
					mCheckoutButton.setVisibility(View.VISIBLE);
					mSettingManager.setCartCount(mCount);
				} else {
					mSettingManager.setCartCount(0);
					mEmptyMessage.setVisibility(View.VISIBLE);
					mCartContainer.setVisibility(View.GONE);
					mCheckoutButton.setVisibility(View.GONE);
				}
				
			} else {
				mErrorContainer.setVisibility(View.VISIBLE);
				mCheckoutItemContainer.setVisibility(View.GONE);
				mErrorMessage.setText(mError);
				
				if(mCode == 109) {
					mSettingManager.setLogin(false);
				
					mDialog = new CreamStyleDialog(getActivity(), R.layout.question_layout, R.string.cream, R.drawable.icon_info);
					mDialog.setCancelable(false);
					
					View v = mDialog.getParentView();
					
					TextView contentText = (TextView)v.findViewById(R.id.contentText);
					contentText.setText(R.string.login_notice);
					
					TextView cancel = (TextView)mDialog.getConfirmView();
					cancel.setText(R.string.not_yet);
					TextView login = (TextView)mDialog.getCancelView();
					login.setText(R.string.login);
					mDialog.show();
					
					cancel.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dismissDialog();
						}
						
					});
					
					login.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dismissDialog();
							
							Intent intent = new Intent(getActivity(), CreamAccountActivity.class);
							startActivity(intent);
							getActivity().finish();
						}
						
					});
				} 
			}
		}
		
	}
	
	private void dismissDialog() {
		if(mDialog != null && mDialog.isShowing()) {
			mDialog.dismiss();
			mDialog = null;
		}
	}
	
	public void setOnFeedItem(OnFrRemoveListener pFrRemoveListener, OnFrContentFeedListener pFrContentFeedListener, OnShowClosetDetailListener pShowClosetDetailListener, CreamMainActivity pMainActivity) {
		mFrRemoveListener = pFrRemoveListener;
		mFrContentFeedListener = pFrContentFeedListener;
		mShowClosetDetailListener = pShowClosetDetailListener;
		mMainActivity = pMainActivity;
	}
	
	OnClickListener checkoutListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			
			CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_CHECKOUT, null);
			
			LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_CHECKOUT, null);
			
			Intent intent = new Intent(getActivity(), CreamPaymentActivity.class);
			CartFragment.this.startActivityForResult(intent, REQUEST_CODE);
		}
		
	};
	
	OnClickListener goHomeListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mFrContentFeedListener.onEvent();
		}
		
	};
	
	OnClickListener backListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mFrRemoveListener.onEvent(CartFragment.this);
		}
		
	};
	
	OnUpdateListView updateListViewListener = new OnUpdateListView() {

		@Override
		public void onEvent() {
			mEmptyMessage.setVisibility(View.VISIBLE);
			mCartContainer.setVisibility(View.GONE);
			mCheckoutButton.setVisibility(View.GONE);
		}
		
	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
			mSettingManager.setDisplayType(CreamStyleApplication.CREAM);
			mFrContentFeedListener.onEvent();
			
		}
		
		
	}
	
	
}
