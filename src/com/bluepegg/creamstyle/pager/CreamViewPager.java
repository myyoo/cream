package com.bluepegg.creamstyle.pager;

import com.bluepegg.creamstyle.adapters.CreamViewPagerAdapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

public class CreamViewPager extends ViewPager {
	
	private PagerAdapter mAdapter;
	
	public CreamViewPager(Context pContext){
		super(pContext);
	}
	
	public CreamViewPager(Context pContext, AttributeSet pAttrs){
		super(pContext, pAttrs);
	}

	@Override
	public void setAdapter(PagerAdapter adapter) {
		super.setAdapter(adapter);
		mAdapter = adapter;
		setCurrentItem(0);
	}

	@Override
	public void setCurrentItem(int item) {
		item = getOffsetAmount() + (item % getAdapter().getCount());
		super.setCurrentItem(item);
	}
	

	private int getOffsetAmount() {
		if(mAdapter instanceof CreamViewPagerAdapter) {
			return ((CreamViewPagerAdapter) mAdapter).getRealCount() * 100;
		} else {
			return 0;
		}
	}
}
