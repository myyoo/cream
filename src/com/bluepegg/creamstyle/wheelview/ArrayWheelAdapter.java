
package com.bluepegg.creamstyle.wheelview;

import android.content.Context;

/**
 * The simple Array wheel adapter
 * @param <T> the element type
 */
public class ArrayWheelAdapter<T> extends AbstractWheelTextAdapter {
    
    // items
    private T mItems[];

    /**
     * Constructor
     * @param context the current context
     * @param pItems the items
     */
	public ArrayWheelAdapter(Context context, T pItems[]) {
        super(context);
        
        //setEmptyItemResource(TEXT_VIEW_ITEM_RESOURCE);
        mItems = pItems;
    }
    
    @Override
    public CharSequence getItemText(int index) {
        if (index >= 0 && index < mItems.length) {
            T item = mItems[index];
            if (item instanceof CharSequence) {
                return (CharSequence) item;
            }
            return item.toString();
        }
        return null;
    }

    @Override
    public int getItemsCount() {
        return mItems.length;
    }
}
