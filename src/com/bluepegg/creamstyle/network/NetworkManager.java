package com.bluepegg.creamstyle.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import com.bluepegg.creamstyle.utils.DevLog;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;


public class NetworkManager {
	private static final String TAG = "NetworkManager";
	private static final int TIME_OUT = 1000 * 10;
	public static final String SERVER_URL = "https://appbackend.creamstyle.com";

//	private static final String DEV_SERVER_URL = "http://54.205.127.240";
//	private static final String DEV_SERVER_URL = "http://54.198.164.254";
	private static final String DEV_SERVER_URL = "http://54.198.164.254:8080";
	
	private boolean isDev = false;
	private String mServerUrl;
	private HttpClient mClient = null;
	private CookieStore mCookieStore = new BasicCookieStore();
	private HttpContext mLocalContext = new BasicHttpContext();
	
	public NetworkManager() {
		
	}
	
	public boolean isNetworkAvailable(Context pContext) {
		
		try{
			ConnectivityManager connManager = (ConnectivityManager)pContext.getSystemService(Context.CONNECTIVITY_SERVICE);
			State wifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();	// Wifi
			if(wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING) {
				return true;
			}
			
			State mobile = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState();	// mobile
			if(mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING) {
				return true;
			}
		} catch(NullPointerException e) {
			return false;
		}
		
		return false;
	}
	
	
	public int getWifiSignalStrength(Context pContext) {
		WifiManager wifiManager = (WifiManager)pContext.getSystemService(Context.WIFI_SERVICE);
		int signalStrength = 0;
		int state = wifiManager.getWifiState();
		if(state == WifiManager.WIFI_STATE_ENABLED) {
			List<ScanResult> results = wifiManager.getScanResults();
			
			for(ScanResult result : results) {
				if(result.BSSID.equals(wifiManager.getConnectionInfo().getBSSID())) {
					int level = WifiManager.calculateSignalLevel(wifiManager.getConnectionInfo().getRssi(), result.level); 
					
					int difference = level * 100 / result.level;
						
					if(difference >= 100) {
						signalStrength = 4;
					} else if(difference >= 75) {
						signalStrength = 3;
					} else if(difference >= 50) {
						signalStrength = 2;
					} else if(difference >= 25) {
						signalStrength = 1;
					}
					
				}
			}
		}
		
		return signalStrength;
	}
	
	public boolean connect(String pUrl) {
		mServerUrl = pUrl;
		
		try {
			
			TrustManager easyTrustManager = new X509TrustManager() {

				@Override
				public void checkClientTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {
					
				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {
					
				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				
			};
			
			SSLContext sslContext;
			sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, new TrustManager[]{easyTrustManager}, null);
			
			final SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
			socketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			
			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", new PlainSocketFactory(), 80));
			schemeRegistry.register(new Scheme("https", socketFactory, 443));
			
			HttpParams params = new BasicHttpParams();
			params.setParameter(CoreProtocolPNames.USER_AGENT, System.getProperty("http.agent"));
			HttpProtocolParams.setUserAgent(params, "http.agent");
			HttpConnectionParams.setConnectionTimeout(params, TIME_OUT);
			HttpConnectionParams.setSoTimeout(params, TIME_OUT);
			mClient = new DefaultHttpClient(new ThreadSafeClientConnManager(params, schemeRegistry), params);
			mClient.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
			
			mLocalContext.setAttribute(ClientContext.COOKIE_STORE, mCookieStore);
		} catch(Exception e) {
			e.printStackTrace();
			
			return false;
		}
		
		return true;
	}
	
	public String getServerUrl() {
		return mServerUrl;
	}
	
	public void setTimeout(int pMilli) {
		if(mClient != null) {
			HttpParams params = mClient.getParams();
			HttpConnectionParams.setConnectionTimeout(params, pMilli);
			HttpConnectionParams.setSoTimeout(params, pMilli);
		}
	}
	
	public void disconnect() {
		new LogoutAsync().execute();
	}
	
	class LogoutAsync extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			if(mClient.getConnectionManager() != null) {
				mClient.getConnectionManager().shutdown();
				return true;
			}
			
			return false;
		}
		
	}
	
	private HttpEntity post(String pSubUrl, List<NameValuePair> pArgs) {
		HttpEntity resEntity = null;
		String url = mServerUrl + "/" + pSubUrl;
		DevLog.Logging(TAG, url);
		HttpPost post = new HttpPost(url);
		
		UrlEncodedFormEntity entity;
		
		try {
			entity = new UrlEncodedFormEntity(pArgs, "UTF-8");
			post.setEntity(entity);
			
			HttpResponse response = mClient.execute(post, mLocalContext);
			resEntity = response.getEntity();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch(IllegalStateException e) {
			e.printStackTrace();
		}
		
		return resEntity;
	}
	
	public String postAndGetString(String pSubUrl, List<NameValuePair> pArgs) {
		HttpEntity resEntity = post(pSubUrl, pArgs);
		String str = null;
		
		if(resEntity != null) {
			try {
				str = EntityUtils.toString(resEntity);
				resEntity.consumeContent();
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return str;
	}
	
	private HttpEntity get(String pSubUrl) {
		HttpEntity resEntity = null;
		String url = mServerUrl + "/" + pSubUrl;
		DevLog.Logging(TAG, url);
		HttpGet get = new HttpGet(url);
		
		try {
			HttpResponse response = mClient.execute(get, mLocalContext);
			
			resEntity = response.getEntity();
			 
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}
		
		return resEntity;
	}
	
	public String getAndGetString(String pSubUrl) {
		HttpEntity resEntity = get(pSubUrl);
		String str = null;
		
		if(resEntity != null) {
			try {
				str = EntityUtils.toString(resEntity);
				resEntity.consumeContent();
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return str;
	}
	
	public String getCookie(String pStr) {
		List<Cookie> list = mCookieStore.getCookies();
		
		for(int i = 0;  i < list.size(); ++i) {
			Cookie cookie = list.get(i);
			if(cookie.getName().equals(pStr)) 
				return cookie.getValue();
		}
		
		return null;
	}
	
	public Bitmap getBitmap(String pSubUrl, byte pBuffer[]) {
		HttpEntity entity = get(pSubUrl);
		
		if(entity != null) {
			try {
				InputStream in = entity.getContent();
				int len;
				int writeSize = 0;
				if(pBuffer.length < entity.getContentLength())
					pBuffer = new byte[(int)entity.getContentLength()];
				
				while((len = in.read(pBuffer, writeSize, pBuffer.length - writeSize)) > 0) {
					writeSize += len;
				}
				
				in.close();
				
				if(writeSize > 0) {
					try {
						Bitmap bitmap = BitmapFactory.decodeByteArray(pBuffer, 0, (int)entity.getContentLength());
						return bitmap;
					} catch(OutOfMemoryError e) {
						e.printStackTrace();
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					entity.consumeContent();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}
	
	public String analyseSampleResponse(String pUrl) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return getAndGetString(pUrl);
	}
	
	public String analyseSignupResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String anlayseAllFeedResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String analyseCreateAccountResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String analyseLoginAccountResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String analyseLoginFacebookResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String analyseCreateFacebookAccountResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String analyseResetPasswordResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String analyseGetClosetResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String analyseSetClosetResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String analyseGetCartResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String analyseSetCartResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String analyseGetTotalResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String analyseChargeResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String analyseApplyCouponResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String analyseItemDetailListResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String anlayseContentResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
	
	public String anlayseCurrencyResponse(String pUrl, List<NameValuePair> pArgs) {
		connect(isDev ? DEV_SERVER_URL : SERVER_URL);
		return postAndGetString(pUrl, pArgs);
	}
}
