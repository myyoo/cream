package com.bluepegg.creamstyle.interfaces;

import com.bluepegg.creamstyle.objects.FragmentEvent;

public interface OnFrDetailListener {
	public void onEvent(FragmentEvent pEvent);
}
