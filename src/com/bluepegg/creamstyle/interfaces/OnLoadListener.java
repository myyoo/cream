package com.bluepegg.creamstyle.interfaces;

public interface OnLoadListener {
	void onFinishLoad();
}
