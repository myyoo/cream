package com.bluepegg.creamstyle.interfaces;

import com.bluepegg.creamstyle.objects.FeedItemList;

import android.app.Fragment;

public interface OnShowClosetDetailListener {
	public void onEvent(Fragment pFragment, FeedItemList pFeedItemList);
}
