package com.bluepegg.creamstyle.interfaces;

import com.bluepegg.creamstyle.objects.ClosetFeed;


public interface OnShowClosetListener {
	public void onEvent(ClosetFeed pClosetFeed);
}
