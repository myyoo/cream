package com.bluepegg.creamstyle.interfaces;

import com.bluepegg.creamstyle.objects.FragmentEvent;

public interface OnFrItemsListener {
	public void onEvent(FragmentEvent pEvent);
}
