package com.bluepegg.creamstyle.interfaces;

import com.bluepegg.creamstyle.objects.FragmentEvent;

public interface OnShareFacebook {
	public void onEvent(FragmentEvent pEvent);
}
