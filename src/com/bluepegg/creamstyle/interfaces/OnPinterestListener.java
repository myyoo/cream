package com.bluepegg.creamstyle.interfaces;

import com.bluepegg.creamstyle.objects.FragmentEvent;

public interface OnPinterestListener {
	public void onEvent(FragmentEvent pEvent);
}
