package com.bluepegg.creamstyle.interfaces;

import android.app.Fragment;

public interface OnFrRemoveListener {
	public void onEvent(Fragment pFragment);
}
