package com.bluepegg.creamstyle.interfaces;

import com.bluepegg.creamstyle.objects.FragmentEvent;

public interface OnFrCartListener {
	public void onEvent(FragmentEvent pEvent);
}
