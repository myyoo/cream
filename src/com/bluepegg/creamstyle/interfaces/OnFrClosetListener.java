package com.bluepegg.creamstyle.interfaces;

import com.bluepegg.creamstyle.objects.FragmentEvent;

public interface OnFrClosetListener {
	public void onEvent(FragmentEvent pEvent);
}
