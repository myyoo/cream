package com.bluepegg.creamstyle.soundlly;

import java.util.ArrayList;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.bluepegg.creamstyle.utils.DevLog;
import com.soundlly.sdk.SoundllyManager;
import com.soundlly.sdk.net.model.AttributesModel;
import com.soundlly.sdk.net.model.ContentsModel;

public class SoundllyBroadcastReceiver extends BroadcastReceiver {
	private static final String TAG = "Soundlly";
	private NotificationManager mNotificationManager;
	private Notification mNotification;
	private ActivityManager mActivityManager;
	
	private int ScreenState = 0;
	
	private boolean isScreenOn = false;
	private boolean isForground = false;
	
	private String url;
	private String comment;
	private String name;
	private String code;
	private Intent i;
	private PendingIntent penintent;
	
	private Context mContext;
	//private WebThread mWebThread;
	private Handler mHandler = new Handler();
	
	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		mContext = context.getApplicationContext();
		
		if(SoundllyManager.ACTION_DID_LOAD_CONTENTS.equals(action)) {
			doLoadContents(intent);
		}
	}

	
	private void doLoadContents(Intent intent) {
		try {
			ContentsModel contents = intent.getParcelableExtra(SoundllyManager.EXTRA_CONTENTS);
			
			DevLog.Logging(TAG, "name: " + contents.getName());
			DevLog.Logging(TAG, "description: " + contents.getDescription());
			
			name = contents.getName();
			
			ArrayList<AttributesModel> attributes = contents.getAttributes();
			
			if(attributes != null) {
				for(AttributesModel model : attributes) {
					DevLog.Logging(TAG, "type: " + model.getType());
					DevLog.Logging(TAG, "key: " + model.getKey());
					DevLog.Logging(TAG, "value: " + model.getValue());
				}
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
