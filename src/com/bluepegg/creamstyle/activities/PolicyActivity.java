package com.bluepegg.creamstyle.activities;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.utils.Utils;

public class PolicyActivity extends Activity {
	public static final String RESOURCE = "resource";
	private Context mContext;
	private Utils mUtils;
	private int mRes;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
//		Muneris.onCreate(this, savedInstanceState);
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.policy_layout);
		
		mRes = getIntent().getIntExtra(RESOURCE, 0);
		
		initManager();
		init();
	}
	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		Muneris.onActivityResult(this, requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}


	@Override
	protected void onDestroy() {
//		Muneris.onDestroy(this);
		super.onDestroy();
	}


	@Override
	protected void onPause() {
//		Muneris.onPause(this);
		super.onPause();
	}


	@Override
	protected void onRestart() {
		super.onRestart();
//		Muneris.onRestart(this);
	}


	@Override
	protected void onResume() {
		super.onResume();
//		Muneris.onResume(this);
	}


	@Override
	protected void onStart() {
		super.onStart();
//		Muneris.onStart(this);
	}


	@Override
	protected void onStop() {
//		Muneris.onStop(this);
		super.onStop();
	}


	private void initManager() {
		mContext = this;
		
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mUtils = app.getUtils();
	}
	
	private void init() {
		mContext = this;
		

		try {

			InputStream in = getResources().openRawResource(mRes);

			if (in != null) {

				InputStreamReader stream = new InputStreamReader(in, "utf-8");
				BufferedReader buffer = new BufferedReader(stream);

				String read;
				StringBuilder sb = new StringBuilder("");

				while ((read = buffer.readLine()) != null) {
					sb.append(read);
				}

				in.close();

				TextView textView = (TextView) findViewById(R.id.policy);
				textView.setText(mUtils.setTextHtmlStyle(sb.toString()));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
