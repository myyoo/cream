package com.bluepegg.creamstyle.activities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.account.IConnector;
import com.bluepegg.creamstyle.account.IResponseEvent;
import com.bluepegg.creamstyle.animation.AnimationFactory;
import com.bluepegg.creamstyle.animation.AnimationFactory.FlipDirection;
import com.bluepegg.creamstyle.dialogs.CreamStyleDialog;
import com.bluepegg.creamstyle.kissmetrics.CreamStyleStateEventTypes;
import com.bluepegg.creamstyle.kissmetrics.CreamstyleAnalytics;
import com.bluepegg.creamstyle.localytics.LocalyticsAnalytics;
import com.bluepegg.creamstyle.network.NetworkManager;
import com.bluepegg.creamstyle.objects.CartItem;
import com.bluepegg.creamstyle.objects.ClosetItemDetail;
import com.bluepegg.creamstyle.objects.ItemDetail;
import com.bluepegg.creamstyle.objects.ItemList;
import com.bluepegg.creamstyle.objects.LookItem;
import com.bluepegg.creamstyle.objects.manager.ClosetItemDetailListManager;
import com.bluepegg.creamstyle.objects.manager.ClosetItemListManager;
import com.bluepegg.creamstyle.objects.manager.ClosetLookItemManager;
import com.bluepegg.creamstyle.objects.manager.ItemDetailListManager;
import com.bluepegg.creamstyle.objects.manager.ItemListManager;
import com.bluepegg.creamstyle.objects.manager.LookItemManager;
import com.bluepegg.creamstyle.objects.manager.ResponseManager;
import com.bluepegg.creamstyle.objecttype.AccountObject;
import com.bluepegg.creamstyle.objecttype.CartObject;
import com.bluepegg.creamstyle.objecttype.ClosetFeedObject;
import com.bluepegg.creamstyle.objecttype.ClosetItemDetailListObject;
import com.bluepegg.creamstyle.objecttype.ClosetItemListObject;
import com.bluepegg.creamstyle.objecttype.ClosetLookListObject;
import com.bluepegg.creamstyle.objecttype.ContentObject;
import com.bluepegg.creamstyle.objecttype.ItemDetailListObject;
import com.bluepegg.creamstyle.objecttype.ItemListObject;
import com.bluepegg.creamstyle.objecttype.LookListObject;
import com.bluepegg.creamstyle.objecttype.ResetPasswordObject;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.utils.DevLog;
import com.bluepegg.creamstyle.utils.Utils;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

public class CreamAccountActivity extends Activity {
	private static final int DURATION = 300;
	private static final float SCALE = 0.4f;
	private static final int VALID_PASSWORD_LENGTH = 6;
	private Context mContext;
	private SettingManager mSettingManager;
	private IConnector mConnector;
	private NetworkManager mNetworkManager;
	private ResponseManager mResponseManager;
	private LookItemManager mLookItemManager;
	private ItemDetailListManager mItemDetailListManager;
	private ItemListManager mItemListManager;
	
	private ClosetLookItemManager mClosetLookItemManager;
	private ClosetItemDetailListManager mClosetItemDetailListManager;
	private ClosetItemListManager mClosetItemListManager;
	
	private Utils mUtils;
	private UiLifecycleHelper mUiHelper;
	
	private LoginButton mFbBtn;
	
	private ViewAnimator mFlipper;
	
	private EditText mSignupEmailText, mSignupPasswordText, mSignupPasswordConfirmText;
	private ImageView mSignupEmailError, mSignupPasswordError, mSignupPasswordConfirmError;
	private Button mSignupButton, mSkipButton;
	private TextView mExistingAccountText;
	
	private EditText mSigninEmailText, mSigninPasswordText;
	private ImageView mSigninEmailError, mSigninPasswordError;
	private TextView mForgotText, mCreateText;
	private Button mSigninButton;
	
	private CreamStyleDialog mDialog;
	
	private String mEvent;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
//		Muneris.onCreate(this, savedInstanceState);
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.account_layout);
		
		initManager();
		init(savedInstanceState);
	}
	
	

	@Override
	protected void onDestroy() {
//		Muneris.onDestroy(this);
		super.onDestroy();
	}



	@Override
	protected void onPause() {
//		Muneris.onPause(this);
		super.onPause();
	}



	@Override
	protected void onStart() {
		super.onStart();
//		Muneris.onStart(this);
	}



	@Override
	protected void onRestart() {
		super.onRestart();
//		Muneris.onRestart(this);
	}



	@Override
	protected void onStop() {
//		Muneris.onStop(this);
		super.onStop();
	}



	@Override
	protected void onResume() {
		super.onResume();
//		Muneris.onResume(this);
		dismiss();
		
		Session session = Session.getActiveSession();
		
		if(session != null && (session.isClosed())){
			onSessionStateChange(session, session.getState(), null);
		}
				
		mUiHelper.onResume();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mUiHelper.onSaveInstanceState(outState);
	}


	private String mFacebookUserId, mFacebookUserEmail, mFacebookUserName, mFacebookToken;
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		Muneris.onActivityResult(this,requestCode,resultCode,data);
		super.onActivityResult(requestCode, resultCode, data);
		
		final Session session = Session.getActiveSession();
		
		if(session.onActivityResult(this, requestCode, resultCode, data)) {
			Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
				
				@Override
				public void onCompleted(GraphUser user, Response response) {
					try {
						DevLog.defaultLogging(response.toString());
						mFacebookUserId = user.getId();
						mFacebookUserEmail = (String)user.asMap().get("email");
						mFacebookUserName = user.getName();
						mFacebookToken = session.getAccessToken();
						DevLog.defaultLogging(mFacebookUserId);
						
						mEvent = CreamStyleStateEventTypes.STAT_FB_LOGIN;
						
						if(mUtils.validEmail(mFacebookUserEmail))
							mConnector.loginWithFacebook(mFacebookUserId, mFacebookUserEmail, new AccountResponseEvent());
						else 
							mUtils.showToastMessage(mContext, R.string.invalid_facebook_email);
						
					} catch(NullPointerException e) {
						dismiss();
						e.printStackTrace();
						DevLog.defaultLogging(e.toString());
						onAutoFacebookLogout();
						
					}
					
				}
			});
		}
	}




	private Session.StatusCallback callback = new Session.StatusCallback() {
		
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			dismiss();
			
			try {
				mDialog = new CreamStyleDialog(CreamAccountActivity.this, R.layout.progress_dialog_layout, null, 0);
				View view = mDialog.getParentView();
				mDialog.setCancelable(false);
				TextView progressContent = (TextView)view.findViewById(R.id.progressContent);
				progressContent.setText(R.string.connecting_facebook_login);
				mDialog.getButtonContainer().setVisibility(View.GONE);
				mDialog.show();
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			
			try {
				if(session.isOpened()) {
					dismiss();
					
					mDialog = new CreamStyleDialog(CreamAccountActivity.this, R.layout.progress_dialog_layout, null, 0);
					View view = mDialog.getParentView();
					mDialog.setCancelable(false);
					TextView progressContent = (TextView)view.findViewById(R.id.progressContent);
					progressContent.setText(R.string.signing_fb_process);
					mDialog.getButtonContainer().setVisibility(View.GONE);
					mDialog.show();
				}
			} catch(Exception e) {
				e.printStackTrace();
				
				dismiss();
			}
			
			onSessionStateChange(session, state, exception);
		}
	};
	
	private void onSessionStateChange(Session session, SessionState state, Exception exeption){
		if(state.isOpened()){
			DevLog.defaultLogging("Logged in...");
		}else if(state.isClosed()){
			dismiss();
			DevLog.defaultLogging("Logged out...");
		}
	}

	private void initManager() {
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mSettingManager = app.getSettingManager();
		mUtils = app.getUtils();
		mConnector = app.getConnector();
		mNetworkManager = app.getNetworkManager();
		mResponseManager = app.getResponseManager();
		mClosetLookItemManager = app.getClosetLookItemManager();
		mClosetItemDetailListManager = app.getClosetItemDetailListManager();
		mClosetItemListManager = app.getClosetItemListManager();
		mLookItemManager = app.getLookItemManager();
		mItemDetailListManager = app.getItemDetailListManager();
		mItemListManager = app.getItemListManager();
	}

	private void init(Bundle savedInstanceState) {
		mContext = this;
		mFlipper = (ViewAnimator) findViewById(R.id.flipper);
		
		mFbBtn = (LoginButton)findViewById(R.id.fbLogin);
		mFbBtn.setReadPermissions(Arrays.asList("user_birthday", "email"));
		mUiHelper = new UiLifecycleHelper(this, callback);
		mUiHelper.onCreate(savedInstanceState);
		
		// Signup widget
		mSignupEmailText = (EditText)findViewById(R.id.signupEmailText);
		mSignupPasswordText = (EditText)findViewById(R.id.signupPasswordText);
		mSignupPasswordConfirmText = (EditText)findViewById(R.id.signupPasswordConfirmText);
		mSignupEmailError = (ImageView)findViewById(R.id.signupEmailError); 
		mSignupPasswordError = (ImageView)findViewById(R.id.signupPasswordError);
		mSignupPasswordConfirmError = (ImageView)findViewById(R.id.signupPasswordConfirmError);
		mExistingAccountText = (TextView) findViewById(R.id.existingAccountText);
		mSkipButton = (Button)findViewById(R.id.skipButton);
		mSignupButton = (Button)findViewById(R.id.signupButton);
		mExistingAccountText.setOnClickListener(listener2);
		mSkipButton.setOnClickListener(skipListener);
		mSignupButton.setOnClickListener(signupListener);
		
		// Signin widget
		mSigninEmailText = (EditText)findViewById(R.id.signinEmailText);
		mSigninPasswordText = (EditText)findViewById(R.id.signinPasswordText);
		mSigninEmailError = (ImageView)findViewById(R.id.signinEmailError); 
		mSigninPasswordError = (ImageView)findViewById(R.id.signinPasswordError);
		
		mForgotText = (TextView)findViewById(R.id.forgotText);
		mSigninButton = (Button)findViewById(R.id.signinButton);
		mCreateText = (TextView) findViewById(R.id.createText);
		mCreateText.setOnClickListener(listener1);
		mSigninButton.setOnClickListener(signinListener);
		mForgotText.setOnClickListener(forgotListener);
		
		if(mSettingManager.getLogin() == false)
			onAutoFacebookLogout();
	}
	
	/**
	 * If facebook logout is failed, facebook should be changed to logout automatically
	 */
	private void onAutoFacebookLogout(){
		if(Session.getActiveSession() != null){
			Session.getActiveSession().closeAndClearTokenInformation();
			Session.setActiveSession(null);
		}
	}

	OnClickListener listener1 = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mUtils.hideKeyBoard(mSigninPasswordText);
			AnimationFactory.flipTransition(mFlipper, FlipDirection.RIGHT_LEFT, DURATION, true, SCALE);
			setSigninError(View.GONE);
			setSignupError(View.GONE);
		}

	};

	OnClickListener listener2 = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mUtils.hideKeyBoard(mSignupPasswordConfirmText);
			AnimationFactory.flipTransition(mFlipper, FlipDirection.LEFT_RIGHT, DURATION, true, SCALE);
			setSigninError(View.GONE);
			setSignupError(View.GONE);
		}

	};
	
	OnClickListener skipListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mEvent = CreamStyleStateEventTypes.STAT_USER_SKIPPED_LOGIN;
			CreamstyleAnalytics.trackEvent(mEvent, null);
			
			LocalyticsAnalytics.tagEvent(mEvent, null);
			
			Intent intent = new Intent(mContext, CreamMainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
			
		}
		
	};
	
	OnClickListener signinListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mDialog = new CreamStyleDialog(mContext,R.layout.progress_dialog_layout, null, 0);
			mDialog.setCancelable(false);
			View view = mDialog.getParentView();
			TextView progressContent = (TextView)view.findViewById(R.id.progressContent);
			progressContent.setText(R.string.logging_in);
			mDialog.getButtonContainer().setVisibility(View.GONE);
			mDialog.show();
			
			final String email = mSigninEmailText.getText().toString();
			final String password = mSigninPasswordText.getText().toString();
			
			if(TextUtils.isEmpty(email)) {
				dismiss();
				mUtils.showToastMessage(mContext, R.string.empty_email);
				setSigninError(View.VISIBLE);
				return;
			} 
			
			if(!mUtils.validEmail(email)) {
				dismiss();
				mUtils.showToastMessage(mContext, R.string.invalid_email);
				setSigninError(View.VISIBLE);
				return;
			}
			
			if(TextUtils.isEmpty(password)) {
				dismiss();
				mUtils.showToastMessage(mContext, R.string.empty_password);
				setSigninError(View.VISIBLE);
				return;
			} 
			
			if(!mUtils.validPassword(password)) {
				dismiss();
				mUtils.showToastMessage(mContext, R.string.invalid_password2);
				setSigninError(View.VISIBLE);
				return;
			}
			
			if(password.length() < VALID_PASSWORD_LENGTH) {
				dismiss();
				mUtils.showToastMessage(mContext, R.string.invalid_password);
				setSigninError(View.VISIBLE);
				return;
			}
			
			setSigninError(View.GONE);
			mUtils.hideKeyBoard(mSigninPasswordText);
			
			if(!mNetworkManager.isNetworkAvailable(mContext)) {
				dismiss();
				mUtils.showToastMessage(mContext, R.string.unconnected_network);
				return;
			} 
			
			mEvent = CreamStyleStateEventTypes.STAT_USER_LOGIN;
			mConnector.loginAccount(email, password, new AccountResponseEvent());
		}
		
	};
	
	private void setSignupError(int pVisible) {
		final String email = mSignupEmailText.getText().toString();
		final String password = mSignupPasswordText.getText().toString();
		String passwordConfirm = mSignupPasswordConfirmText.getText().toString();
		
		if(TextUtils.isEmpty(email)) {
			mSignupEmailError.setVisibility(pVisible);
		} else {
			mSignupEmailError.setVisibility(View.GONE);
		}
		
		if(!mUtils.validEmail(email)) {
			mSignupEmailError.setVisibility(pVisible);
		} else {
			mSignupEmailError.setVisibility(View.GONE);
		}
		
		if(TextUtils.isEmpty(password)) {
			mSignupPasswordError.setVisibility(pVisible);
			mSignupPasswordConfirmError.setVisibility(pVisible);
		} else {
			mSignupPasswordError.setVisibility(View.GONE);
		}
		
		if(!mUtils.validPassword(password)) {
			mSignupPasswordError.setVisibility(pVisible);
			mSignupPasswordConfirmError.setVisibility(pVisible);
		} else {
			mSignupPasswordError.setVisibility(View.GONE);
		}
		
		if(password.length() < VALID_PASSWORD_LENGTH) {
			mSignupPasswordError.setVisibility(pVisible);
			mSignupPasswordConfirmError.setVisibility(pVisible);
		} else {
			mSignupPasswordError.setVisibility(View.GONE);
		}
		
		if(!password.equals(passwordConfirm)) {
			mSignupPasswordConfirmError.setVisibility(pVisible);
		} else {
			mSignupPasswordConfirmError.setVisibility(View.GONE);
		}
		
	}
	
	private void setSigninError(int pVisible) {
		
		final String email = mSigninEmailText.getText().toString();
		final String password = mSigninPasswordText.getText().toString();
		
		if(TextUtils.isEmpty(email)) {
			mSigninEmailError.setVisibility(pVisible);
		} else {
			mSigninEmailError.setVisibility(View.GONE);
		}
		
		if(!mUtils.validEmail(email)) {
			mSigninEmailError.setVisibility(pVisible);
		} else {
			mSigninEmailError.setVisibility(View.GONE);
		}
		
		if(TextUtils.isEmpty(password)) {
			mSigninPasswordError.setVisibility(pVisible);
		} else {
			mSigninPasswordError.setVisibility(View.GONE);
		}
		
		if(!mUtils.validPassword(password)) {
			mSigninPasswordError.setVisibility(pVisible);
		} else {
			mSigninPasswordError.setVisibility(View.GONE);
		}
		
		if(password.length() < VALID_PASSWORD_LENGTH) {
			mSigninPasswordError.setVisibility(pVisible);
		} else {
			mSigninPasswordError.setVisibility(View.GONE);
		}
		
	}
	
	OnClickListener signupListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mDialog = new CreamStyleDialog(mContext,R.layout.progress_dialog_layout, null, 0);
			mDialog.setCancelable(false);
			View view = mDialog.getParentView();
			TextView progressContent = (TextView)view.findViewById(R.id.progressContent);
			progressContent.setText(R.string.registering);
			mDialog.getButtonContainer().setVisibility(View.GONE);
			mDialog.show();
			
			final String email = mSignupEmailText.getText().toString();
			final String password = mSignupPasswordText.getText().toString();
			String passwordConfirm = mSignupPasswordConfirmText.getText().toString();
			
			if(TextUtils.isEmpty(email)) {
				dismiss();
				mUtils.showToastMessage(mContext, R.string.empty_email);
				setSignupError(View.VISIBLE);
				return;
			} 
			
			if(!mUtils.validEmail(email)) {
				dismiss();
				mUtils.showToastMessage(mContext, R.string.invalid_email);
				setSignupError(View.VISIBLE);
				return;
			}
			
			if(TextUtils.isEmpty(password)) {
				dismiss();
				mUtils.showToastMessage(mContext, R.string.empty_password);
				setSignupError(View.VISIBLE);
				return;
			} 
			
			if(!mUtils.validPassword(password)) {
				dismiss();
				mUtils.showToastMessage(mContext, R.string.invalid_password2);
				setSignupError(View.VISIBLE);
				return;
			}
			
			if(password.length() < VALID_PASSWORD_LENGTH) {
				dismiss();
				mUtils.showToastMessage(mContext, R.string.invalid_password);
				setSignupError(View.VISIBLE);
				return;
			}
			
			if(!password.equals(passwordConfirm)) {
				mUtils.showToastMessage(mContext, R.string.no_matched_password);
				setSignupError(View.VISIBLE);
				return;
			}
			
			setSignupError(View.GONE);
			mUtils.hideKeyBoard(mSignupPasswordConfirmText);
			
			if(!mNetworkManager.isNetworkAvailable(mContext)) {
				dismiss();
				mUtils.showToastMessage(mContext, R.string.unconnected_network);
				return;
			} 
			
			mEvent = CreamStyleStateEventTypes.STAT_USER_ACCOUNT_CREATED;
			mConnector.createAccount(email, password, "", new AccountResponseEvent());
		}
		
	};
	
	OnClickListener forgotListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mDialog = new CreamStyleDialog(mContext, R.layout.reset_password_layout, R.string.reset_password_title, R.drawable.icon_info);
			mDialog.setCancelable(false);
			
			View view = mDialog.getParentView();
			
			final EditText contentText = (EditText)view.findViewById(R.id.resetEmailText);
			final ImageView resetError = (ImageView)view.findViewById(R.id.resetEmailError);
			
			TextView confirm = (TextView)mDialog.getConfirmView();
			TextView cancel = (TextView)mDialog.getCancelView();
			mDialog.show();
			
			cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					
					dismiss();
				}
				
			});
			
			confirm.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String email = contentText.getText().toString();
					
					if(TextUtils.isEmpty(email)) {
						mUtils.showToastMessage(mContext, R.string.empty_email);
						resetError.setVisibility(View.VISIBLE);
						return;
					} 
					
					if(!mUtils.validEmail(email)) {
						mUtils.showToastMessage(mContext, R.string.invalid_email);
						resetError.setVisibility(View.VISIBLE);
						return;
					}
					
					mUtils.hideKeyBoard(contentText);
					dismiss();
					
					mDialog = new CreamStyleDialog(mContext,R.layout.progress_dialog_layout, null, 0);
					mDialog.setCancelable(false);
					View view = mDialog.getParentView();
					TextView progressContent = (TextView)view.findViewById(R.id.progressContent);
					progressContent.setText(R.string.resetting_password);
					mDialog.getButtonContainer().setVisibility(View.GONE);
					mDialog.show();
					
					mConnector.resetPassword(email, new ResetPasswordResponseEvent());
					
				}
				
			});
		}
		
	};
	
	private class ResetPasswordResponseEvent implements IResponseEvent<Object> {

		@Override
		public void onResponse(Object pParams) {
			DevLog.Logging("Response", pParams.toString());
			new ResetPasswordResponseTask().execute(pParams.toString());
		}
		
	}
	
	private class ResetPasswordResponseTask extends AsyncTask<String, Void, Boolean> {
		private String mMessage;
		
		@Override
		protected Boolean doInBackground(String... params) {
			ResetPasswordObject object = new ResetPasswordObject();
			boolean result = object.onResponseListener(params[0]);
			mMessage = object.getErrorMessage();
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			dismiss();
			
			if(result) {
				mUtils.showToastMessage(mContext, mMessage);
			} else {
				mUtils.showToastMessage(mContext, mMessage);
			}
		}
		
		
	}
	
	private class AccountResponseEvent implements IResponseEvent<Object> {

		@Override
		public void onResponse(Object pParams) {
			DevLog.Logging("Response", pParams.toString());
			new AccountResponseTask().execute(pParams.toString());
		}
		
	} 
	
	private class AccountResponseTask extends AsyncTask<String, Void, Boolean> {
		private String mErrorMessage;
		
		@Override
		protected Boolean doInBackground(String... params) {
			AccountObject object = new AccountObject();
			boolean result =  object.onResponseListener(params[0]);
			mErrorMessage = object.getErrorMessage();
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			
			dismiss();
			
			if(result) {
				CreamstyleAnalytics.identify(mSettingManager.getEmail());
				
				CreamstyleAnalytics.trackEvent(mEvent, null);
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_LOGIN, null);
				
				LocalyticsAnalytics.setCustomerEmail(mSettingManager.getEmail());
				LocalyticsAnalytics.tagEvent(mEvent, null);
				LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_LOGIN, null);
				
				mSettingManager.setLogin(true);
				
				if(mNetworkManager.isNetworkAvailable(mContext)) {
					mDialog = new CreamStyleDialog(mContext,R.layout.progress_dialog_layout, null, 0);
					mDialog.setCancelable(false);
					View view = mDialog.getParentView();
					TextView progressContent = (TextView)view.findViewById(R.id.progressContent);
					progressContent.setText(R.string.connecting_server);
					mDialog.getButtonContainer().setVisibility(View.GONE);
					mDialog.show();
					
					mConnector.getContent(mSettingManager.getUid(), mSettingManager.getLoginToken(), new ContentResponseEvent());
					
				}
				
				
			} else {
				if(mErrorMessage.equals("Unknown facebook login.")) {
					mEvent = CreamStyleStateEventTypes.STAT_FB_ACCOUNT_CREATED;
					if(mUtils.validEmail(mFacebookUserEmail)) {
						mConnector.createAccountWithFacebook(mFacebookUserId, mFacebookUserEmail, new AccountResponseEvent());
					} else {
						mUtils.showToastMessage(mContext, R.string.invalid_facebook_email);
					}
				} else {
					mUtils.showToastMessage(mContext, mErrorMessage);
				}
				
			}
		}
		
	}
	
	private class ContentResponseEvent implements IResponseEvent<Object> {

		@Override
		public void onResponse(Object pParams) {
			if(pParams == null) {
				
				dismiss();
				
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						mUtils.showToastMessage(mContext, R.string.unconnected_network);
					}
					
				});
				
			} else {
				DevLog.Logging("Response", pParams.toString());
				
				ContentObject object = new ContentObject();
				boolean result = object.onResponseListener(pParams.toString());
				
				
				if(result) {

					LookListObject lookObject = new LookListObject();
					boolean done = lookObject.onResponseListener(mResponseManager.getLookListResponse());
					
					ItemDetailListObject itemDetailObject = new ItemDetailListObject();
					itemDetailObject.onResponseListener(mResponseManager.getItemDetailListResponse());
					
					ItemListObject itemListObject = new ItemListObject();
					itemListObject.onResponseListener(mResponseManager.getItemListResponse());
					
					dismiss();
					
					
					if(mNetworkManager.isNetworkAvailable(mContext)) {
						
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								mDialog = new CreamStyleDialog(mContext,R.layout.progress_dialog_layout, null, 0);
								mDialog.setCancelable(false);
								View view = mDialog.getParentView();
								TextView progressContent = (TextView)view.findViewById(R.id.progressContent);
								progressContent.setText(R.string.collecting_closet_data);
								mDialog.getButtonContainer().setVisibility(View.GONE);
								mDialog.show();
							}
							
						});
						
						
						mConnector.getCloset(String.valueOf(mSettingManager.getUid()), mSettingManager.getLoginToken(), new ClosetResponseEvent());
					} else {
						
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								mUtils.showToastMessage(mContext, R.string.unconnected_network);
							}
							
						});
						
						
						Intent intent = new Intent(mContext, CreamMainActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						finish();
					}
					
				} else {
					
					dismiss();
					
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							mUtils.showToastMessage(mContext, R.string.unconnected_network);
						}
						
					});
					
				}
			}
			
		}
		
	}
	
	private class ClosetResponseEvent implements IResponseEvent<Object> {

		@Override
		public void onResponse(Object pParams) {
			if(pParams == null) {
				dismiss();
				
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						mUtils.showToastMessage(mContext, R.string.unconnected_network);
					}
					
				});
				
			} else {
				DevLog.Logging("Response", pParams.toString());
				
				ClosetFeedObject object = new ClosetFeedObject();
				boolean result = object.onResponseListener(pParams.toString());
				
				if(result) {
					ClosetLookListObject closetLookObject = new ClosetLookListObject();
					closetLookObject.onResponseListener(mResponseManager.getClosetLookListResponse());
					updateLookItem();
					
					ClosetItemDetailListObject closetItemDetailObject = new ClosetItemDetailListObject();
					closetItemDetailObject.onResponseListener(mResponseManager.getClosetItemDetailListResponse());
					
					ClosetItemListObject closetItemListObject = new ClosetItemListObject();
					closetItemListObject.onResponseListener(mResponseManager.getClosetItemListResponse());
					updateDetailItem();
					
//					dismiss();
					
					if(mNetworkManager.isNetworkAvailable(mContext)) {
						mConnector.getCart(String.valueOf(mSettingManager.getUid()), String.valueOf(mSettingManager.getLoginToken()), new CartResponseEvent());
					} else {
						Intent intent = new Intent(mContext, CreamMainActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						finish();
					}
					
					
				} else {
					
					dismiss();
					
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							mUtils.showToastMessage(mContext, String.format(getString(R.string.cannot_get_closet_information), mSettingManager.getEmail()));
						}
						
					});
					
				}
			}
			
			
		}
	} 
	
	private void updateLookItem() {
		HashMap<Integer, LookItem> lookItems = mLookItemManager.getLookItems();
		
		for(Map.Entry<Integer, LookItem> item : lookItems.entrySet()) {
			item.getValue().setCloset(false);
		}
		
		HashMap<Integer, LookItem> closetLookItem = mClosetLookItemManager.getLookItems();
		
		for(Map.Entry<Integer, LookItem> item : closetLookItem.entrySet()) {
			LookItem lookItem = lookItems.get(item.getKey());
			if(lookItem != null)
				lookItem.setCloset(true);
//			else {
//				LookItem closetItem = closetLookItem.get(item.getKey());
//				LookItem oldLookItem = new LookItem();
//				
//				oldLookItem.setItemDetailIdList(closetItem.getItemDetailIdList());
//				oldLookItem.setItemIdList(closetItem.getItemIdList());
//				oldLookItem.setCloset(true);
//				oldLookItem.setLookId(closetItem.getLookId());
//				oldLookItem.setBigLookImage(closetItem.getBigLookImage());
//				oldLookItem.setSmallLookImage(closetItem.getSmallLookImage());
//				oldLookItem.setThumbLookImage(closetItem.getThumbLookImage());
//				oldLookItem.setLookName(closetItem.getLookName());
//				oldLookItem.setDisplayName(closetItem.getDisplayName());
//				oldLookItem.setLookHeight(closetItem.getLookHeight());
//				oldLookItem.setLookWidth(closetItem.getLookWidth());
//				oldLookItem.setFeedMask(closetItem.getFeedMask());
//				
//				mLookItemManager.putItem(item.getKey(), oldLookItem);
//			}
		}
	}
	
	private void updateDetailItem() {
		LinkedHashMap<Integer, ItemDetail> itemDetails = mItemDetailListManager.getItemDetails();
		
		for(Map.Entry<Integer, ItemDetail> item : itemDetails.entrySet()) {
			item.getValue().setCloset(false);
		}
		
		LinkedHashMap<Integer, ClosetItemDetail> closetItemDetails = mClosetItemDetailListManager.getItemDetails();
		
		for(Map.Entry<Integer, ClosetItemDetail> item : closetItemDetails.entrySet()) {
			if(item.getValue().getClosetTime() != 0) {
				ItemDetail itemDetail = itemDetails.get(item.getKey());
				if(itemDetail != null)
					itemDetail.setCloset(true);
				else {
					ClosetItemDetail closetItemDetail = closetItemDetails.get(item.getKey());
					
					ItemList closetItemList = mClosetItemListManager.getItem(closetItemDetail.getItemId());
					
					ItemList oldItemList = new ItemList();
					oldItemList.setCloset(true);
					oldItemList.setItemBrand(closetItemList.getItemBrand());
					oldItemList.setItemDetail(closetItemList.getItemDetail());
					oldItemList.setItemDetailIdList(closetItemList.getItemDetailIdList());
					oldItemList.setItemId(closetItemList.getItemId());
					oldItemList.setItemName(closetItemList.getItemName());
					mItemListManager.putItem(closetItemDetail.getItemId(), oldItemList);
					
					ClosetItemDetail closetDetail = closetItemDetails.get(item.getKey());
					
					ItemDetail oldItemDetail = new ItemDetail();
					oldItemDetail.setAltBigImageList(closetDetail.getAltBigImageList());
					oldItemDetail.setAltSmallImageList(closetDetail.getAltSmallImageList());
					oldItemDetail.setAltThumbImageList(closetDetail.getAltThumbImageList());
					oldItemDetail.setBigItemImage(closetDetail.getBigItemImage());
					oldItemDetail.setCloset(closetDetail.getClosetTime() == 0 ? false : true);
					oldItemDetail.setColor(closetDetail.getColor());
					oldItemDetail.setInventoryIdList(closetDetail.getInventoryIdList());
					oldItemDetail.setInventorySizeList(closetDetail.getInventorySizeList());
					oldItemDetail.setItemDetailId(closetDetail.getItemDetailId());
					oldItemDetail.setItemHeight(closetDetail.getItemHeight());
					oldItemDetail.setItemWidth(closetDetail.getItemWidth());
					oldItemDetail.setPrice(closetDetail.getPrice());
					oldItemDetail.setSmallItemImage(closetDetail.getSmallItemImage());
					oldItemDetail.setThumbItemImage(closetDetail.getThumbItemImage());
					mItemDetailListManager.putItemDetail(item.getKey(), oldItemDetail);
				}
			}
		}
	}
	
	
	private void dismiss() {
		if(mDialog != null && mDialog.isShowing()) {
			mDialog.dismiss();
			mDialog = null;
		}
	}
	
	private class CartResponseEvent implements IResponseEvent<Object> {

		@Override
		public void onResponse(Object pParams) {
			DevLog.Logging("Response", pParams.toString());
			new CartResponseTask().execute(pParams.toString());
		}
		
	}
	
	private class CartResponseTask extends AsyncTask<String, Void, Boolean> {
		private String mError;
		private int mCode;
		private String mSubTotal;
		private List<CartItem> mCartItems;
		private int mCount;
		
		@Override
		protected void onPreExecute() {
			mCartItems = new ArrayList<CartItem>();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			CartObject object = new CartObject(mCartItems);
			boolean result = object.onResponseListener(params[0]);
			mSubTotal = object.getSubTotal();
			mError = object.getErrorMessage();
			mCode = object.getCode();
			mCount = object.getCount();
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			dismiss();
			
			if(result) {
				
				if(mCartItems.size() > 0) {
					mSettingManager.setCartCount(mCount);
				} else {
					mSettingManager.setCartCount(0);
				}
				
			}
			
			Intent intent = new Intent(mContext, CreamMainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
		}
		
	}
}
