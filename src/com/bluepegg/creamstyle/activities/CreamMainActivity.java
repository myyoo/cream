package com.bluepegg.creamstyle.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.fortuna.ical4j.model.property.Locality;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.adapters.MenuAdapter;
import com.bluepegg.creamstyle.dialogs.CreamStyleDialog;
import com.bluepegg.creamstyle.fragments.CartFragment;
import com.bluepegg.creamstyle.fragments.ClosetFragment;
import com.bluepegg.creamstyle.fragments.ContentFragment;
import com.bluepegg.creamstyle.fragments.ItemDetailFragment;
import com.bluepegg.creamstyle.fragments.ItemListFragment;
import com.bluepegg.creamstyle.interfaces.OnFrCartListener;
import com.bluepegg.creamstyle.interfaces.OnFrClosetListener;
import com.bluepegg.creamstyle.interfaces.OnFrContentFeedListener;
import com.bluepegg.creamstyle.interfaces.OnFrDetailListener;
import com.bluepegg.creamstyle.interfaces.OnFrItemsListener;
import com.bluepegg.creamstyle.interfaces.OnFrRemoveListener;
import com.bluepegg.creamstyle.interfaces.OnLoginListener;
import com.bluepegg.creamstyle.interfaces.OnShareFacebook;
import com.bluepegg.creamstyle.interfaces.OnShowClosetDetailListener;
import com.bluepegg.creamstyle.interfaces.OnShowClosetListener;
import com.bluepegg.creamstyle.kissmetrics.CreamStyleStateEventTypes;
import com.bluepegg.creamstyle.kissmetrics.CreamstyleAnalytics;
import com.bluepegg.creamstyle.localytics.LocalyticsAnalytics;
import com.bluepegg.creamstyle.objects.ClosetFeed;
import com.bluepegg.creamstyle.objects.DetailFeedItem;
import com.bluepegg.creamstyle.objects.FBShareItem;
import com.bluepegg.creamstyle.objects.FeedItemList;
import com.bluepegg.creamstyle.objects.FragmentEvent;
import com.bluepegg.creamstyle.objects.LookItem;
import com.bluepegg.creamstyle.objects.MenuInfo;
import com.bluepegg.creamstyle.objects.manager.ClosetLookItemManager;
import com.bluepegg.creamstyle.objects.manager.LookItemManager;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.utils.DevLog;
import com.bluepegg.creamstyle.utils.Utils;
import com.bluepegg.creamstyle.views.HomeLayout;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.pinterest.pinit.PinItButton;

public class CreamMainActivity extends FragmentActivity {
	private static final int CREAM_TAB = 0;
	private static final int SURE_TAB = 1;
	private static final int SALE_TAB = 2;
	
	private Context mContext;
	private Utils mUtils;
	private SettingManager mSettingManager;
	private LookItemManager mLookItemManager;
	private ClosetLookItemManager mClosetLookItemManager;
	private Resources mRes;
	
	private Fragment mCurrentFragment;
	private ContentFragment mFrContent;
	private ItemDetailFragment mFrItemDetail;
	private ItemListFragment mFrItemList;
	private ClosetFragment mFrCloset;
	private CartFragment mFrCart;
	
	private HomeLayout mHomeLayout;
	private ListView mMenuList;
	private ImageView mMenuIcon;
	private RelativeLayout mBlockLayout;
	
	private ImageView mCreamTab, mSureTab, mSaleTab;
	
	private ImageView mHelpGuide;
	
	private RelativeLayout mContent;
	
	private List<MenuInfo> mInfo = new ArrayList<MenuInfo>();
	private long mLastBackPressedTime = 0;
	private String mCurrentItem = "";
	
	private UiLifecycleHelper mUiHelper;
	
	private CreamStyleDialog mDialog;
	private Toast mToast;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
//		Muneris.onCreate(this, savedInstanceState);
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_cream_main);
		
		mUiHelper = new UiLifecycleHelper(this, callback);
		mUiHelper.onCreate(savedInstanceState);
		
		initManager();
		init();
		
	}
	
	
	@Override
	protected void onPause() {
//		Muneris.onPause(this);
		super.onPause();
		mUiHelper.onPause();
	}
	
	
	
	@Override
	protected void onStart() {
		super.onStart();
//		Muneris.onStart(this);
	}


	@Override
	protected void onRestart() {
		super.onRestart();
//		Muneris.onRestart(this);
	}


	@Override
	protected void onStop() {
//		Muneris.onStop(this);
		super.onStop();
	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mUiHelper.onSaveInstanceState(outState);
	}
	
	
	@Override
	public void onDestroy()
	{
//		Muneris.onDestroy(this);
		super.onDestroy();
		mUiHelper.onDestroy();
	}
	
	private void initManager() {
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		app.setCreamMainActivity(this);
		mRes = app.getResources();
		mSettingManager = app.getSettingManager();
		mUtils = app.getUtils();
		mLookItemManager = app.getLookItemManager();
		mClosetLookItemManager = app.getClosetLookItemManager();
		Typeface font = Typeface.createFromAsset(app.getAssets(), "fonts/gf_albert.ttf");
		mUtils.setTypeface(font);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		Muneris.onActivityResult(this, requestCode, resultCode, data);
		
		super.onActivityResult(requestCode, resultCode, data);
		
		try {
			if(Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data)){
				if(resultCode == Activity.RESULT_OK) {
					Session session = Session.getActiveSession();
					if(session.isOpened())
						shareContent(mShareItem);
				}		
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		 
	}
	
	private void init() {
		mContext = this;
		PinItButton.setPartnerId("1439479");
		
		mFrContent = new ContentFragment();
		mFrContent.setOnFeedItem(onFrDetailListener, mCurrentFragment, CreamMainActivity.this);
		
		mFrItemDetail = new ItemDetailFragment();
		mFrItemDetail.setOnFeedItem(onFrRemoveListener, onFrItemsListener, onFrClosetListener, onFrCartListener, onShareFBListener, onFrContentFeedListener, CreamMainActivity.this);
		
		mFrItemList = new ItemListFragment();
		mFrItemList.setOnFeedItem(onFrRemoveListener, onFrClosetListener, onFrCartListener, onShareFBListener, onFrContentFeedListener, CreamMainActivity.this);
		
		
		mFrCloset = new ClosetFragment();
		mFrCloset.setOnFeedItem(onFrRemoveListener, onFrContentFeedListener, onLoginListener, onShowClosetListener, onShowClosetDetailListener, onFrDetailListener, CreamMainActivity.this);
		
		mFrCart = new CartFragment();
		mFrCart.setOnFeedItem(onFrRemoveListener, onFrContentFeedListener, onShowClosetDetailListener, CreamMainActivity.this);
		
		mContent = (RelativeLayout)findViewById(R.id.content);
		mHomeLayout = (HomeLayout)findViewById(R.id.mainContainer);
		mMenuList = (ListView)findViewById(R.id.menuListView);
		mBlockLayout = (RelativeLayout)findViewById(R.id.blockLayout);
		mMenuIcon = (ImageView)findViewById(R.id.menuIcon);
		mMenuIcon.setOnClickListener(menuListener);
		mCreamTab = (ImageView)findViewById(R.id.creamTab); 
		mSureTab = (ImageView)findViewById(R.id.sureTab); 
		mSaleTab = (ImageView)findViewById(R.id.saleTab);
		
		mHelpGuide = (ImageView)findViewById(R.id.helpGuide);
		
		if(!mSettingManager.getHelpGuide()) {
			mHelpGuide.setVisibility(View.VISIBLE);
		} else {
			mHelpGuide.setVisibility(View.GONE);
		}
		
		mHelpGuide.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {
				mSettingManager.setHelpGuide(true);
				mHelpGuide.setVisibility(View.GONE);
				return true;
			}
			
		});
		
		mMenuList.setAdapter(new MenuAdapter(mContext, mInfo));
		mMenuList.setOnItemClickListener(itemListener);
		setFragment(mFrContent);
		
		mCreamTab.setImageResource(R.drawable.ic_logo_bar);
		
		mCreamTab.setOnClickListener(mCreamTabListener);
		mSureTab.setOnClickListener(mSureTabListener);
		mSaleTab.setOnClickListener(mSaleTabListener);
	}
	
	OnClickListener mCreamTabListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			setFragment(mFrContent);
			mFrContent.updateFeedItem();
			setTab(CREAM_TAB);
		}
		
	};
	
	OnClickListener mSureTabListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			setFragment(mFrContent);
			mFrContent.updateSureFeedItem();
			setTab(SURE_TAB);
		}
		
	};
	
	OnClickListener mSaleTabListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_CART, null);
			LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_CART, null);
			setFragment(mFrCart);
			setTab(SALE_TAB);
		}
		
	};
	
	private void setTab(int pPosition) {
		mCreamTab.setImageResource(R.drawable.ic_logo);
		mSureTab.setImageResource(R.drawable.ic_sure);
		mSaleTab.setImageResource(R.drawable.ic_logo);
		
		switch(pPosition) {
		case CREAM_TAB:
			mCreamTab.setImageResource(R.drawable.ic_logo_bar);
			break;
			
		case SURE_TAB:
			mSureTab.setImageResource(R.drawable.ic_sure_bar);
			break;
			
		case SALE_TAB:
			mSaleTab.setImageResource(R.drawable.ic_logo);
			break;
		}
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
//		Muneris.onResume(this);
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				setMenuList();
			}
			
		});
		
		Session session = Session.getActiveSession();
		if(session != null && (session.isOpened() || session.isClosed())){
			onSessionStateChange(session, session.getState(), null);
		}
		
		mUiHelper.onResume();
		
	}


	OnItemClickListener itemListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			if(mHomeLayout != null && mHomeLayout.isMenuOpen()) {
				onMenuItemClick(parent, view, position, id);
			}
		}
		
	};
	
	private void onMenuItemClick(AdapterView<?> pParent, View pView, int pPosition, long pId) {
		String selectedItem = mInfo.get(pPosition).getMenuItem();
		
		if(selectedItem.compareTo(mCurrentItem) == 0) {
			toggleMenu();
			return;
		}
		
		if(selectedItem.equals(mRes.getString(R.string.login))) {
			Intent intent = new Intent(mContext, CreamAccountActivity.class);
			startActivity(intent);
			finish();
		} else if(selectedItem.equals(mRes.getString(R.string.logout))) {
			mDialog = new CreamStyleDialog(mContext, R.layout.question_layout, R.string.logout_title, R.drawable.icon_info);
			mDialog.setCancelable(false);
			
			View view = mDialog.getParentView();
			
			TextView contentText = (TextView)view.findViewById(R.id.contentText);
			contentText.setText(R.string.logout_message);
			
			TextView confirm = (TextView)mDialog.getConfirmView();
			TextView cancel = (TextView)mDialog.getCancelView();
			mDialog.show();
			
			cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dismissDialog();
				}
				
			});
			
			confirm.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dismissDialog();
					
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_LOGOUT, null);
					
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_LOGOUT, null);
					
					//onAutoFacebookLogout();
					mSettingManager.logout();
					
					Intent intent = new Intent(mContext, SplashActivity.class);
					startActivity(intent);
					finish();
				}
				
			});
			
			
		} else if(selectedItem.equals(mRes.getString(R.string.help))) {
			mHelpGuide.setVisibility(View.VISIBLE);
		} else if(selectedItem.equals(mRes.getString(R.string.my_closet))) {
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_CLOSET, null);
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_CLOSET, null);
					setFragment(mFrCloset);
				}
				
			}, 300);
			
		} else if(selectedItem.equals(mRes.getString(R.string.shopping_bag))) {
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_CART, null);
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_CART, null);
					setFragment(mFrCart);
				}
				
			}, 300);
		} else if(selectedItem.equals(mRes.getString(R.string.privacy_policy))) {
			
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					Intent intent = new Intent(mContext, PolicyActivity.class);
					intent.putExtra(PolicyActivity.RESOURCE, R.raw.privacy_policy);
					startActivity(intent);
				}
				
			}, 150);
			
		} else if(selectedItem.equals(mRes.getString(R.string.return_policy))) {
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					Intent intent = new Intent(mContext, PolicyActivity.class);
					intent.putExtra(PolicyActivity.RESOURCE, R.raw.return_policy);
					startActivity(intent);
				}
				
			}, 150);
		} else if(selectedItem.equals(mRes.getString(R.string.terms_service))) {
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					Intent intent = new Intent(mContext, PolicyActivity.class);
					intent.putExtra(PolicyActivity.RESOURCE, R.raw.terms_service);
					startActivity(intent);
				}
				
			}, 150);
		}
		
		toggleMenu();
	}
	
	private void dismissDialog() {
		if(mDialog != null && mDialog.isShowing()) {
			mDialog.dismiss();
			mDialog = null;
		}
	}
	
	private void setMenuList() {
		if(mInfo != null) {
			mInfo.clear();
			mInfo.add(new MenuInfo(mRes.getString(R.string.shopping_bag), MenuAdapter.TYPE_ITEM, R.drawable.icon_info));
			mInfo.add(new MenuInfo(mRes.getString(R.string.my_closet), MenuAdapter.TYPE_ITEM, R.drawable.icon_info));
			mInfo.add(new MenuInfo(mRes.getString(R.string.help), MenuAdapter.TYPE_ITEM, R.drawable.icon_info));
			mInfo.add(new MenuInfo(mRes.getString(R.string.privacy_policy), MenuAdapter.TYPE_ITEM, R.drawable.icon_info));
			mInfo.add(new MenuInfo(mRes.getString(R.string.return_policy), MenuAdapter.TYPE_ITEM, R.drawable.icon_info));
			mInfo.add(new MenuInfo(mRes.getString(R.string.terms_service), MenuAdapter.TYPE_ITEM, R.drawable.icon_info));
			
			if(mSettingManager.getLogin())
				mInfo.add(new MenuInfo(mRes.getString(R.string.logout), MenuAdapter.TYPE_ITEM, R.drawable.icon_info));
			else
				mInfo.add(new MenuInfo(mRes.getString(R.string.login), MenuAdapter.TYPE_ITEM, R.drawable.icon_info));
		}
	}
	
	
	OnClickListener menuListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			
			setMenuList();
			
			CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_SETTINGS, null);
			
			LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_SETTINGS, null);
			
			toggleMenu();
		}
		
	};
	
	@Override
	public void onBackPressed() {
		if(mHomeLayout != null && mHomeLayout.isMenuOpen()) {
			toggleMenu();
		} else if(mHelpGuide.getVisibility() == View.VISIBLE) {
			mHelpGuide.setVisibility(View.GONE);
		} else {
			
			FragmentManager fm = getFragmentManager();
			int i = fm.getBackStackEntryCount();
			
			if(i > 0) {
				if((i - 1) == 0)
					mCurrentFragment = mFrContent;
			}
			
			if(mCurrentFragment.equals(mFrItemDetail)) {
				mFrItemDetail.onBackPress();
				//removeCurrentFragment(mFrItemDetail);
			} else if(mCurrentFragment.equals(mFrItemList)) {
				mFrItemList.onBackPress();
				//removeCurrentFragment(mFrItemList);
			} else if(mCurrentFragment.equals(mFrCart)) {
				mFrCart.onBackPress();
				//removeCurrentFragment(mFrCart);
			} else if(mCurrentFragment.equals(mFrCloset)) {
				mFrCloset.onBackPress();
				//removeCurrentFragment(mFrCloset);
			} else  {
				long backPressTime = System.currentTimeMillis();
				if(mLastBackPressedTime + 2000 > backPressTime) {
					
					if(mToast != null) {
						mToast.cancel();
						mToast = null;
					}
					
					finish();
					
				} else {
					mToast = mUtils.showToastMessage(mContext, R.string.app_terminate);
				}
				
				mLastBackPressedTime = backPressTime;
			}
		}
	}

	private void toggleMenu() {
		if(mHomeLayout != null) {
			
			if(mHomeLayout.isMenuOpen()) {
				
				enableFragment();
				
			} else {
				
				disableFragment();
				
			}
			
			mHomeLayout.toggleMenu();
			
			
		}
	}
	
	
	private void onAutoFacebookLogout(){
		if(Session.getActiveSession() != null){
			Session.getActiveSession().closeAndClearTokenInformation();
			Session.setActiveSession(null);
		}
	}
	
	public void refreshFeedItem() {
		if(mFrContent != null) {
			mFrContent.updateFeedItem();
		}
	}
	
	
	private void shareContent(final FBShareItem pItem) {
		
		Bundle params = new Bundle();
		params.putString("name", String.format(getString(R.string.share_name), pItem.getItemName(), pItem.getItemDetailName()));
		params.putString("description", pItem.getDescription());
		params.putString("caption", "www.creamstyle.com");
		params.putString("picture", pItem.getPictureUrl());
		
		StringBuilder sb = new StringBuilder();
		String linkUrl = sb.append("http://www.creamstyle.com").append("#").append("com.bluepegg.creamstyle.fragments.DetailFragment").toString();
		params.putString("link", linkUrl);
		
		WebDialog feedDialog = (new WebDialog.FeedDialogBuilder(mContext, Session.getActiveSession(), params)).setOnCompleteListener(new OnCompleteListener() {

			@Override
			public void onComplete(Bundle values, FacebookException error) {
				if(error == null) {
					String postId = values.getString("post_id");
					if(postId != null) {
						HashMap<String, String> properties = new HashMap<String, String>();
						if(pItem.getType()) {
							properties.put("look_id", String.valueOf(pItem.getId()));
							CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_LOOK_SHARED_ON_FB, properties);
							CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_LOOK_SHARED, properties);
							
							LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_LOOK_SHARED_ON_FB, properties);
							LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_LOOK_SHARED, properties);
						
						} else {
							properties.put("item_detail_id", String.valueOf(pItem.getId()));
							CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED_ON_FB, properties);
							CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED, properties);
							
							LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED_ON_FB, properties);
							LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED, properties);
						}
						
						mUtils.showToastMessage(mContext, R.string.share_success);
					} else {
						// User clicked the Cancel button
						mUtils.showToastMessage(mContext, R.string.share_cancel);
					}
				} else if(error instanceof FacebookOperationCanceledException) {
					// User clicked the "x" button
					mUtils.showToastMessage(mContext, R.string.share_cancel);
				} else {
					 // Generic, ex: network error
					mUtils.showToastMessage(mContext, R.string.share_error);
				}
			}
			
		}).build();
		
		feedDialog.show();
	}
	
	private Session.StatusCallback callback = new Session.StatusCallback() {
		
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};
	
	private void onSessionStateChange(Session session, SessionState state, Exception exeption){
		if(state.isOpened()){
			DevLog.defaultLogging("Logged in...");
		}
		else if(state.isClosed()){
			DevLog.defaultLogging("Logged out...");
		}
	}
	
	private FBShareItem mShareItem;
	
	OnShareFacebook onShareFBListener = new OnShareFacebook() {

		@Override
		public void onEvent(FragmentEvent pEvent) {
			mShareItem = pEvent.getHomePageItem();
			
			Session session = Session.getActiveSession();
			if(session == null) {
				Session.openActiveSession((Activity)mContext, true, callback);
			} else {
			if(session.isOpened()) {
				shareContent(mShareItem);
			} else {
				Session.openActiveSession((Activity)mContext, true, callback);
			}
			}
		}
		
	};
	
	
	OnLoginListener onLoginListener = new OnLoginListener() {

		@Override
		public void onEvent() {
			Intent intent = new Intent(mContext, CreamAccountActivity.class);
			startActivity(intent);
			finish();
		}
		
	};
	
	OnFrDetailListener onFrDetailListener = new OnFrDetailListener() {

		@Override
		public void onEvent(FragmentEvent pEvent) {
			int key = pEvent.getKey();
			int position = pEvent.getPosition();
			boolean isCloset = pEvent.isCloset();
			setFragment(key, position, isCloset);
		}
		
	};
	
	
	OnFrContentFeedListener onFrContentFeedListener = new OnFrContentFeedListener() {

		@Override
		public void onEvent() {
			mSettingManager.setDisplayType(CreamStyleApplication.CREAM);
			setFragment(mFrContent);
		}
		
	};
	
	OnShowClosetListener onShowClosetListener = new OnShowClosetListener() {

		@Override
		public void onEvent(ClosetFeed pClosetFeed) {
			mSettingManager.setDisplayType(CreamStyleApplication.CREAM);
			int position = mClosetLookItemManager.getPosition(pClosetFeed.getId());
			mFrContent.setClosetItem(pClosetFeed.getId(), position, true);
		}
		
	};
	
	OnShowClosetDetailListener onShowClosetDetailListener = new OnShowClosetDetailListener() {

		@Override
		public void onEvent(Fragment pFragment, FeedItemList pFeedItemList) {
			mCurrentFragment = pFragment;
			
			ClosetFeed closetFeed = pFeedItemList.getClosetFeed();
			int id = closetFeed.getId();
			
			int lookId = 0;
			
			boolean isContained = false;
			HashMap<Integer, LookItem> lookItems = mLookItemManager.getLookItems();
//			HashMap<Integer, LookItem> lookItems = mClosetLookItemManager.getLookItems();
			
			/*
			if(closetFeed.getExpiredItem()) {
				lookItems = mClosetLookItemManager.getLookItems();
				
				for(Map.Entry<Integer, LookItem> item : lookItems.entrySet()) {
					if(item.getValue().getItemIdList().contains(id)) {
						lookId = item.getValue().getLookId();
//						isContained = true;
						mFrItemDetail.getDetailFeedItem(lookId, closetFeed.getDetailId(), closetFeed.getId(), closetFeed.getInventoryId().get(0), closetFeed.getSmallImage(), closetFeed.getExpiredItem());
						break;
					}
				}
			} else {
				for(Map.Entry<Integer, LookItem> item : lookItems.entrySet()) {
					if(item.getValue().getItemIdList().contains(id)) {
						lookId = item.getValue().getLookId();
//						isContained = true;
						mFrItemDetail.getDetailFeedItem(lookId, closetFeed.getDetailId(), closetFeed.getId(), closetFeed.getInventoryId().get(0), closetFeed.getSmallImage(), closetFeed.getExpiredItem());
						break;
					}
				}
			}
			*/
			
			for(Map.Entry<Integer, LookItem> item : lookItems.entrySet()) {
				if(item.getValue().getItemIdList().contains(id)) {
					lookId = item.getValue().getLookId();
					isContained = true;
					mFrItemDetail.getDetailFeedItem(lookId, closetFeed.getDetailId(), closetFeed.getId(), closetFeed.getInventoryId().get(0), closetFeed.getSmallImage());
					break;
				}
			}
			
			if(!isContained) {
				lookItems = mClosetLookItemManager.getLookItems();
				
				for(Map.Entry<Integer, LookItem> item : lookItems.entrySet()) {
					if(item.getValue().getItemIdList().contains(id)) {
						lookId = item.getValue().getLookId();
						isContained = true;
						mFrItemDetail.getDetailFeedItem(lookId, closetFeed.getDetailId(), closetFeed.getId(), closetFeed.getInventoryId().get(0), closetFeed.getSmallImage());
						break;
					}
				}
			}
			
			
//			mFrItemDetail.getDetailFeedItem(lookId, closetFeed.getDetailId(), closetFeed.getId(), closetFeed.getInventoryId().get(0), closetFeed.getSmallImage());
			
		}
		
	};
	
	OnFrItemsListener onFrItemsListener = new OnFrItemsListener() {

		@Override
		public void onEvent(FragmentEvent pEvent) {
			DetailFeedItem detailItem = pEvent.getDetailFeedItem();
			setFragment(detailItem);
		}
		
	};
	
	OnFrRemoveListener onFrRemoveListener = new OnFrRemoveListener() {

		@Override
		public void onEvent(Fragment pFragment) {
			removeCurrentFragment(pFragment);
		}
		
	};
	
	OnFrClosetListener onFrClosetListener = new OnFrClosetListener() {

		@Override
		public void onEvent(FragmentEvent pEvent) {
			CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_CLOSET, null);
			LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_CLOSET, null);
			setFragment(mFrCloset);
		}
		
	};
	
	OnFrCartListener onFrCartListener = new OnFrCartListener() {

		@Override
		public void onEvent(FragmentEvent pEvent) {
			CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_CART, null);
			LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_CART, null);
			setFragment(mFrCart);
		}
		
	};
	
	public void setMenuIcon(int pVisible) {
		mMenuIcon.setVisibility(pVisible);
	}
	
	public void clickMenuIcon() {
		mMenuIcon.performClick();
	}
	
	private void setFragment(Fragment pFragment) {
		try {
			
			mCurrentFragment = pFragment;
			
			FragmentTransaction ft = getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fragment_animation_fade_in, R.anim.fragment_animation_fade_out);
			ft.replace(R.id.content, pFragment);
			ft.addToBackStack(null);
			ft.commit();
			
		}  catch(IllegalStateException e) {
			e.printStackTrace();
		}
	}
	
	
	private void setFragment(int pKey, int pPosition, boolean pIsCloset) {
		
		try {
			
			ItemDetailFragment frItemDetail = new ItemDetailFragment();
			frItemDetail.setOnFeedItem(onFrRemoveListener, onFrItemsListener, onFrClosetListener, onFrCartListener, onShareFBListener, onFrContentFeedListener, CreamMainActivity.this);
			mCurrentFragment = mFrItemDetail = frItemDetail;
			
			Bundle bundle = new Bundle();
			bundle.putInt(ItemDetailFragment.KEY_VALUE, pKey);
			bundle.putInt(ItemDetailFragment.POSITION_VALUE, pPosition);
			bundle.putBoolean(ItemDetailFragment.IS_CLOSET, pIsCloset);
			frItemDetail.setArguments(bundle);
			
			FragmentTransaction ft = getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fragment_animation_fade_in, R.anim.fragment_animation_fade_out);
			ft.replace(R.id.content, frItemDetail);
			ft.addToBackStack(null);
			ft.commit();
			
			
		} catch(IllegalStateException e) {
			e.printStackTrace();
		}
		
	}
	
	private void setFragment(DetailFeedItem pDetailFeedItem) {
		try {
			
			ItemListFragment frItemList = new ItemListFragment();
			frItemList.setOnFeedItem(onFrRemoveListener, onFrClosetListener, onFrCartListener, onShareFBListener, onFrContentFeedListener, CreamMainActivity.this);
			mCurrentFragment = mFrItemList = frItemList;
			
			Bundle bundle = new Bundle();
			bundle.putSerializable(ItemListFragment.DETAILITEM_OBJ, pDetailFeedItem);
			frItemList.setArguments(bundle);
			
			FragmentTransaction ft = getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fragment_animation_fade_in, R.anim.fragment_animation_fade_out);
			ft.replace(R.id.content, frItemList);
			ft.addToBackStack(null);
			ft.commit();
			
		} catch(IllegalStateException e) {
			e.printStackTrace();
		}
	}
	
	public void removeCurrentFragment(Fragment pFragment) {
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction().setCustomAnimations(R.anim.fragment_animation_no_fade_in, R.anim.fragment_animation_fade_out);
		ft.remove(pFragment);
		ft.commit();
		fm.popBackStack();
	}
	
	private void enableFragment() {
		mBlockLayout.setVisibility(View.GONE);
	}
	
	private void disableFragment() {
		mBlockLayout.setVisibility(View.VISIBLE);
		
	}
	
	/**
	 * When menu is activated, block right side click function. 
	 * @param ev
	 * @return
	 */
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		
		if(mHomeLayout.isMenuOpen()) {
			
			mBlockLayout.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					toggleMenu();
					return true;
				}
				
			});
			
		} else {
			mBlockLayout.setOnTouchListener(new OnTouchListener(){

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					return false;
				}

				
			});
		}
		
		try {
			return super.dispatchTouchEvent(ev); 
		} catch(Exception e) {
			e.printStackTrace();
			return true;
		}
		
	}
	
	
	public void setContentFragment() {
		mCurrentFragment = mFrContent;
	}
}
