package com.bluepegg.creamstyle.activities;

import java.util.Calendar;
import java.util.HashMap;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.account.IConnector;
import com.bluepegg.creamstyle.account.IResponseEvent;
import com.bluepegg.creamstyle.dialogs.CreamStyleDialog;
import com.bluepegg.creamstyle.kissmetrics.CreamStyleStateEventTypes;
import com.bluepegg.creamstyle.kissmetrics.CreamstyleAnalytics;
import com.bluepegg.creamstyle.localytics.LocalyticsAnalytics;
import com.bluepegg.creamstyle.network.NetworkManager;
import com.bluepegg.creamstyle.objects.PaymentTotal;
import com.bluepegg.creamstyle.objecttype.ChargeObject;
import com.bluepegg.creamstyle.objecttype.GetTotalObject;
import com.bluepegg.creamstyle.payment.TokenList;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.utils.DevLog;
import com.bluepegg.creamstyle.utils.Utils;
import com.bluepegg.creamstyle.wheelview.ArrayWheelAdapter;
import com.bluepegg.creamstyle.wheelview.OnWheelChangedListener;
import com.bluepegg.creamstyle.wheelview.OnWheelScrollListener;
import com.bluepegg.creamstyle.wheelview.WheelView;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;

public class CreamPaymentActivity extends Activity {
	private static final int SHIPPING_ADDRESS = 0;
	private static final int PAYMENT_METHOD = 1;
	private static final int BUY = 2;
	private static final int DONE = 3;
	private static final int CLOSE = 4;
	
	private static final String PUBLISHABLE_KEY = "pk_live_WUdXSTLMazFPd7rEgt8hurEd";
	
	// Test
//	private static final String PUBLISHABLE_KEY = "pk_test_AJhLISiWEPRrKlVPIqaJbtel";
	
	private Context mContext;
	private Resources mRes;
	private Utils mUtils;
	private IConnector mConnector;
	private NetworkManager mNetworkManager;
	private SettingManager mSettingManager;
	
	private Animation mSlideInRight, mSlideOutLeft, mSlideOutRight, mSlideInLeft;
	private RelativeLayout mPaymentBackground;
	private ViewFlipper mFlipper;
	private ImageView mClosePayment;
	private Button mButton;
	
	
	private EditText mFullNameText, mAddressText, mCityText, mStateText, mAddressText2, mPostalText, mTelephoneText;
	private ImageView mFullNameError, mAddressError, mCityError, mStateError, mAddress2Error, mPostalError, mTelephoneError;
	private TextView mCountryText;
	
	private EditText mCardNum_1, mCardNum_2, mCardNum_3, mCardNum_4, mFullNameOnCard, mCcvCodeText, mBillingZipText;
	private ImageView mCardNum_1_Error, mCardNum_2_Error, mCardNum_3_Error, mCardNum_4_Error, mFullNameOnCardError, mCcvCodeTextError, mValidDateError, mBillingZipTextError;
	private TextView mValidDate;
	
	private TextView mTotalItem, mTotalItemCost, mShippingCost, mTaxCost, mSubtotalCost, mCouponCost, mTotalCost;
	private ImageView mCouponCodeError;
	private EditText mCouponCode;
	private Button mApplyCoupon;
	
	private Dialog mDialog;
	private CreamStyleDialog mCreamDialog;
	
	private int mStep = SHIPPING_ADDRESS;
	private int mYear;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
//		Muneris.onCreate(this, savedInstanceState);
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.payment_layout);
		
		initManager();
		init();
		
	}
	
	
	@Override
	protected void onDestroy() {
//		Muneris.onDestroy(this);
		super.onDestroy();
	}




	@Override
	protected void onPause() {
//		Muneris.onPause(this);
		super.onPause();
	}




	@Override
	protected void onRestart() {
		super.onRestart();
//		Muneris.onRestart(this);
	}




	@Override
	protected void onStart() {
		super.onStart();
//		Muneris.onStart(this);
	}




	@Override
	protected void onStop() {
//		Muneris.onStop(this);
		super.onStop();
	}




	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		Muneris.onActivityResult(this, requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}


	@Override
	protected void onResume() {
		super.onResume();
//		Muneris.onResume(this);
		mCountries = mRes.getStringArray(R.array.countries);
		mStates = mRes.getStringArray(R.array.state);
		mISOCodes = mRes.getStringArray(R.array.iso_codes);
		
	}



	private void initManager() {
		mContext = this;
		mRes = mContext.getResources();
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mUtils = app.getUtils();
		mConnector = app.getConnector();
		mNetworkManager = app.getNetworkManager();
		mSettingManager = app.getSettingManager();
		Calendar now = Calendar.getInstance();
		mYear = now.get(Calendar.YEAR);
		
		for(int i = 0; i < 20; ++i) {
			wheelYear[i] = String.valueOf(mYear);
			mYear++;
		}
		
	}
	
	private void init() {
		mSlideInRight = AnimationUtils.loadAnimation(mContext, R.anim.show_right);
		mSlideOutLeft = AnimationUtils.loadAnimation(mContext, R.anim.hide_left);
		mSlideOutRight = AnimationUtils.loadAnimation(mContext, R.anim.hide_right); 
		mSlideInLeft = AnimationUtils.loadAnimation(mContext, R.anim.show_left);
		
		mPaymentBackground = (RelativeLayout)findViewById(R.id.paymentBackground);
		mFlipper = (ViewFlipper)findViewById(R.id.paymentFlipper);
		
		
		mClosePayment = (ImageView)findViewById(R.id.closePayment);
		mButton = (Button)findViewById(R.id.button);
		
		mClosePayment.setOnClickListener(closePaymentListener);
		mButton.setOnClickListener(stepListener);
		
		
		// Shipping address
		mFullNameText = (EditText)findViewById(R.id.fullNameText);
		mFullNameText.addTextChangedListener(shippingAddressWatcher);
		mAddressText = (EditText)findViewById(R.id.addressText);
		mAddressText.addTextChangedListener(shippingAddressWatcher);
		mCityText = (EditText)findViewById(R.id.cityText);
		mCityText.addTextChangedListener(shippingAddressWatcher);
		mStateText = (EditText)findViewById(R.id.stateText);
		mStateText.addTextChangedListener(shippingAddressWatcher);
		mCountryText = (TextView)findViewById(R.id.countryText);
		mAddressText2 = (EditText)findViewById(R.id.addressText2);
		mAddressText2.addTextChangedListener(shippingAddressWatcher);
		mPostalText = (EditText)findViewById(R.id.postalText);
		mPostalText.addTextChangedListener(shippingAddressWatcher);
		mTelephoneText = (EditText)findViewById(R.id.telephonText);
		mTelephoneText.addTextChangedListener(shippingAddressWatcher);
		mPostalError = (ImageView)findViewById(R.id.postalError);
		mTelephoneError = (ImageView)findViewById(R.id.telephoneError);
		mFullNameError = (ImageView)findViewById(R.id.fullNameError);
		mAddressError = (ImageView)findViewById(R.id.addressError);
		mCityError = (ImageView)findViewById(R.id.cityError);
		mStateError = (ImageView)findViewById(R.id.stateError);
		mAddress2Error = (ImageView)findViewById(R.id.addressText2Error);
		mCountryText.setOnClickListener(countryListener);
		if(mUtils.getCurrentLanguage().equals("ko")) {
			mCountryText.setText("South Korea");
			mStateText.setText("");
		} else {
			mCountryText.setText("USA");
			mStateText.setText("NY");
		}
		
		mStateText.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				if(event.getAction() == MotionEvent.ACTION_DOWN) {
					mUtils.hideKeyBoard(mStateText);
				}else if(event.getAction() == MotionEvent.ACTION_UP) {
					new Handler().postDelayed(new Runnable() {

						@Override
						public void run() {
							if(mCountryText.getText().toString().equals("USA")) {
								
								mCreamDialog = new CreamStyleDialog(mContext, R.layout.country_view_layout, R.string.select_state, R.drawable.icon_info);
								mCreamDialog.setCancelable(false);
								
								View view = mCreamDialog.getParentView();
								
								final WheelView state = (WheelView)view.findViewById(R.id.country);
								state.setViewAdapter(new ArrayWheelAdapter(mContext, mStates));
								state.setVisibleItems(mDefaultStateIndex);
								state.setCurrentItem(mDefaultStateIndex);
								state.addChangingListener(changedListener);
								state.addScrollingListener(scrolledListener);
								
								TextView ok = (TextView)mCreamDialog.getConfirmView();
								TextView cancel = (TextView)mCreamDialog.getCancelView();
								mCreamDialog.show();
								
								cancel.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View v) {
										dississCreamDialog();
									}
									
								});
								
								ok.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View v) {
										dississCreamDialog();
										
										mDefaultStateIndex = state.getCurrentItem();
										mStateValue = mStates[mDefaultStateIndex];
										mStateText.setText(mStateValue);
										
									}
									
								});
							}
						}
						
					}, 200);
				
				}
				
				return false;
			}
			
		});
		
		
		// Payment method
		mCardNum_1 = (EditText)findViewById(R.id.cardNumber_1);
		mCardNum_1.addTextChangedListener(paymentMethodWatcher);
		mCardNum_2 = (EditText)findViewById(R.id.cardNumber_2);
		mCardNum_2.addTextChangedListener(paymentMethodWatcher);
		mCardNum_3 = (EditText)findViewById(R.id.cardNumber_3);
		mCardNum_3.addTextChangedListener(paymentMethodWatcher);
		mCardNum_4 = (EditText)findViewById(R.id.cardNumber_4);
		mCardNum_4.addTextChangedListener(paymentMethodWatcher);
		mFullNameOnCard = (EditText)findViewById(R.id.fullNameOnCard);
		mFullNameOnCard.addTextChangedListener(paymentMethodWatcher);
		mCcvCodeText = (EditText)findViewById(R.id.ccvCodeText);
		mCcvCodeText.addTextChangedListener(paymentMethodWatcher);
		mBillingZipText = (EditText)findViewById(R.id.billingZipText);
		mBillingZipText.addTextChangedListener(paymentMethodWatcher);
		mCardNum_1_Error = (ImageView)findViewById(R.id.cardNumber_1_Error);
		mCardNum_2_Error = (ImageView)findViewById(R.id.cardNumber_2_Error);
		mCardNum_3_Error = (ImageView)findViewById(R.id.cardNumber_3_Error);
		mCardNum_4_Error = (ImageView)findViewById(R.id.cardNumber_4_Error);
		mValidDateError = (ImageView)findViewById(R.id.validDateError);
		mFullNameOnCardError = (ImageView)findViewById(R.id.fullNameOnCardError);
		mCcvCodeTextError = (ImageView)findViewById(R.id.ccvCodeError);
		mBillingZipTextError = (ImageView)findViewById(R.id.billingZipError);
		mValidDate = (TextView)findViewById(R.id.validDate);
		mValidDate.setOnClickListener(wheelListener);
		
		
		
		mCardNum_1.addTextChangedListener(mReg_01_TextWatcher);
		mCardNum_2.addTextChangedListener(mReg_02_TextWatcher);
		mCardNum_3.addTextChangedListener(mReg_03_TextWatcher);
		
		
		// Checkout
		mTotalItem = (TextView)findViewById(R.id.totalItem);
		mTotalItemCost = (TextView)findViewById(R.id.totalItemCost);
		mShippingCost = (TextView)findViewById(R.id.shippingCost); 
		mTaxCost = (TextView)findViewById(R.id.taxCost); 
		mSubtotalCost = (TextView)findViewById(R.id.subtotalCost); 
		mCouponCost = (TextView)findViewById(R.id.couponCost); 
		mTotalCost = (TextView)findViewById(R.id.totalCost);
		mCouponCodeError = (ImageView)findViewById(R.id.couponCodeError);
		mCouponCode = (EditText)findViewById(R.id.couponCode);
		mApplyCoupon = (Button)findViewById(R.id.applyCoupon);
		
		mApplyCoupon.setOnClickListener(applyListener);
		
	}
	
	private TextWatcher shippingAddressWatcher = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {
			
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			
			scanShippingAddressState();
			
		}
		
	};
	
	private void scanShippingAddressState() {
		if(!TextUtils.isEmpty(mFullNameText.getText().toString()) 
				&& !TextUtils.isEmpty(mAddressText.getText().toString())
				&& !TextUtils.isEmpty(mCityText.getText().toString())
				&& !TextUtils.isEmpty(mStateText.getText().toString())
				&& !TextUtils.isEmpty(mCountryText.getText().toString())
				&& !TextUtils.isEmpty(mPostalText.getText().toString())
				&& !TextUtils.isEmpty(mTelephoneText.getText().toString())) {
			
			mButton.setVisibility(View.VISIBLE);
		} else {
			mButton.setVisibility(View.GONE);
		}
	}
	
	private TextWatcher paymentMethodWatcher = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {
			
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			
			scanPaymentMethodState();
			
		}
		
	};
	
	private void scanPaymentMethodState() {
		
		if(!TextUtils.isEmpty(mCardNum_1.getText().toString()) 
				&& !TextUtils.isEmpty(mCardNum_2.getText().toString())
				&& !TextUtils.isEmpty(mCardNum_3.getText().toString())
				&& !TextUtils.isEmpty(mCardNum_4.getText().toString())
				&& !TextUtils.isEmpty(mFullNameOnCard.getText().toString())
				&& !TextUtils.isEmpty(mCcvCodeText.getText().toString())
				&& !TextUtils.isEmpty(mBillingZipText.getText().toString())
				&& !TextUtils.isEmpty(mValidDate.getText().toString())) {
			mButton.setVisibility(View.VISIBLE);
		} else {
			mButton.setVisibility(View.GONE);
		}
		
	}
	
	private TextWatcher mReg_01_TextWatcher = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable s) { }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if ( s.length() == 4 ) {
            	mCardNum_2.requestFocus();
            }
        }
    };
    
    private TextWatcher mReg_02_TextWatcher = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable s) { }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if ( s.length() == 4 ) {
            	mCardNum_3.requestFocus();
            }
        }
    };
    
    private TextWatcher mReg_03_TextWatcher = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable s) { }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if ( s.length() == 4 ) {
            	mCardNum_4.requestFocus();
            }
        }
    };
    
    private String mCountries[];
    private String mISOCodes[];
    private int mCountryIndex;
    private String mCountryValue;
    private String mISOCode;
    
    private int mDefaultStateIndex = 33;
    private String mStateValue;
    
    OnClickListener countryListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mUtils.hideKeyBoard(mBillingZipText);
			mCreamDialog = new CreamStyleDialog(mContext, R.layout.country_view_layout, R.string.select_countries, R.drawable.icon_info);
			mCreamDialog.setCancelable(false);
			
			View view = mCreamDialog.getParentView();
//			for(int i = 0; i < mCountries.length; ++i) {
//				DevLog.defaultLogging(mCountries[i]);
//			}
			
			final WheelView country = (WheelView)view.findViewById(R.id.country);
			country.setViewAdapter(new ArrayWheelAdapter(mContext, mCountries));
			country.setVisibleItems(0);
			country.setCurrentItem(mCountryIndex);
			country.addChangingListener(changedListener);
			country.addScrollingListener(scrolledListener);
			
			TextView ok = (TextView)mCreamDialog.getConfirmView();
			TextView cancel = (TextView)mCreamDialog.getCancelView();
			mCreamDialog.show();
			
			cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dississCreamDialog();
				}
				
			});
			
			ok.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dississCreamDialog();
					
					mCountryIndex = country.getCurrentItem();
					mCountryValue = mCountries[mCountryIndex];
					mISOCode = mISOCodes[mCountryIndex];
					mCountryText.setText(mCountryValue);
					
					if(mCountryValue.equals("USA")) {
						
						if(TextUtils.isEmpty(mStateText.getText().toString())) {
							mStateText.setText("NY");
						} else {
							mStateText.setText(mStates[mDefaultStateIndex]);
						}
						
					} else {
						mStateText.setText("");
					}
				}
				
			});
			
		}
    	
    };
    
    OnClickListener applyListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			String couponCode = mCouponCode.getText().toString();
			if(TextUtils.isEmpty(couponCode)) {
				mUtils.showToastMessage(mContext, R.string.empty_coupon_code);
				setCouponCode(View.VISIBLE);
				return;
			}
			
			
			
			mUtils.hideKeyBoard(mCouponCode);
			if(mNetworkManager.isNetworkAvailable(mContext)) {
				mDialog = mUtils.getFullScreenDialog(mContext);
				mDialog.show();
				
				HashMap<String, String> properties = new HashMap<String, String>();
				properties.put("code", couponCode);
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_COUPON_APPLIED, properties);
				
				LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_COUPON_APPLIED, properties);
				
				mConnector.applyCoupon(String.valueOf(mSettingManager.getUid()), mSettingManager.getLoginToken(), couponCode, new CouponResponseEvent());
				
			} else {
				mUtils.showToastMessage(mContext, R.string.unconnected_network);
			}
		}
    	
    };
    
    private class CouponResponseEvent implements IResponseEvent<Object> {

		@Override
		public void onResponse(Object pParams) {
			DevLog.Logging("Response", pParams.toString());
			new CouponResponseTask().execute(pParams.toString());
		}
		
	}
    
    
    private class CouponResponseTask extends AsyncTask<String, Void, Boolean> {
		private String mErrorMessage = "error";
		
		@Override
		protected Boolean doInBackground(String... params) {
			if(mPaymentTotal == null)
				mPaymentTotal = new PaymentTotal();
			
			GetTotalObject object = new GetTotalObject(mPaymentTotal);
			boolean result = object.onResponseListener(params[0]);
			mErrorMessage = object.getErrorMessage();
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			dismissDialog();
			
			mCouponCode.setText("");
			setCouponCode(View.GONE);
			
			if(result) {
				
				float totalItemCost = Float.valueOf(mPaymentTotal.getTotal()) - Float.valueOf(mPaymentTotal.getShipping()) - Float.valueOf(mPaymentTotal.getTax()) - Float.valueOf(mPaymentTotal.getDiscount());
				mTotalItem.setText(String.format(mRes.getString(R.string.total_items), mSettingManager.getCartCount()));
				mTotalItemCost.setText(String.format(mRes.getString(R.string.price), String.valueOf(totalItemCost)));
				mShippingCost.setText(String.format(mRes.getString(R.string.price), mPaymentTotal.getShipping()));
				mTaxCost.setText(String.format(mRes.getString(R.string.price), mPaymentTotal.getTax())); 
				mSubtotalCost.setText(String.format(mRes.getString(R.string.price), mPaymentTotal.getSubTotal())); 
				mCouponCost.setText(String.format(mRes.getString(R.string.price), mPaymentTotal.getDiscount())); 
				mTotalCost.setText(String.format(mRes.getString(R.string.price), mPaymentTotal.getTotal()));
				
			} else {
				
				HashMap<String, String> properties = new HashMap<String, String>();
				properties.put("msg", mErrorMessage);
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_ERROR_DURING_CHECKOUT, properties);
				
				LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_ERROR_DURING_CHECKOUT, properties);
				
				mUtils.showToastMessage(mContext, mErrorMessage);
			}
		}
		
	}
    
    
    private String[] mStates;
    private String wheelMonth[] = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private String wheelYear[] = new String[20];
    private int mMonthIndex, mYearIndex;
    private String mMonthValue, mYearValue;
    
    // Wheel scrolled flag
 	private boolean wheelScrolled = false;
    
    OnClickListener wheelListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			mUtils.hideKeyBoard(mTelephoneText);
			mCreamDialog = new CreamStyleDialog(mContext, R.layout.wheel_view_layout, R.string.select_validate_date, R.drawable.icon_info);
			mCreamDialog.setCancelable(false);
			
			View view = mCreamDialog.getParentView();
			
			final WheelView month = (WheelView)view.findViewById(R.id.month);
			month.setViewAdapter(new ArrayWheelAdapter(mContext, wheelMonth));
			month.setVisibleItems(0);
			month.setCurrentItem(mMonthIndex);
			month.addChangingListener(changedListener);
			month.addScrollingListener(scrolledListener);
			
			final WheelView year = (WheelView)view.findViewById(R.id.year);
			year.setViewAdapter(new ArrayWheelAdapter(mContext, wheelYear));
			year.setVisibleItems(0);
			year.setCurrentItem(mYearIndex);
			year.addChangingListener(changedListener);
			year.addScrollingListener(scrolledListener);
			
			TextView ok = (TextView)mCreamDialog.getConfirmView();
			TextView cancel = (TextView)mCreamDialog.getCancelView();
			mCreamDialog.show();
			
			cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dississCreamDialog();
				}
				
			});
			
			ok.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dississCreamDialog();
					
					mMonthIndex = month.getCurrentItem();
					mYearIndex = year.getCurrentItem();
					
					mMonthValue = getMonth(wheelMonth[mMonthIndex]);
					mYearValue = wheelYear[mYearIndex];
					
					mValidDate.setText(mMonthValue + "-" + mYearValue);
					
					scanPaymentMethodState();
				}
				
			});
		}
    	
    };
    
    private String getMonth(String pValue) {
    	if(pValue.equals(wheelMonth[0])) {
    		return "01";
    	} else if(pValue.equals(wheelMonth[1])) {
    		return "02";
    	} else if(pValue.equals(wheelMonth[2])) {
    		return "03";
    	} else if(pValue.equals(wheelMonth[3])) {
    		return "04";
    	} else if(pValue.equals(wheelMonth[4])) {
    		return "05";
    	} else if(pValue.equals(wheelMonth[5])) {
    		return "06";
    	} else if(pValue.equals(wheelMonth[6])) {
    		return "07";
    	} else if(pValue.equals(wheelMonth[7])) {
    		return "08";
    	} else if(pValue.equals(wheelMonth[8])) {
    		return "09";
    	} else if(pValue.equals(wheelMonth[9])) {
    		return "10";
    	} else if(pValue.equals(wheelMonth[10])) {
    		return "11";
    	} 
    		
    	return "12";
    	
    }
    
    private void dississCreamDialog() {
		if(mCreamDialog != null && mCreamDialog.isShowing()) {
			mCreamDialog.dismiss();
			mCreamDialog = null;
		}
	}
    
    // Wheel changed listener
    OnWheelChangedListener changedListener = new OnWheelChangedListener() {
		public void onChanged(WheelView wheel, int oldValue, int newValue) {
			if (!wheelScrolled){
				//updateStatus();
			}
		}
	};
	
	// Wheel scrolled listener
	OnWheelScrollListener scrolledListener = new OnWheelScrollListener() {
		public void onScrollStarts(WheelView wheel) {
			wheelScrolled = true;
		}

		public void onScrollEnds(WheelView wheel) {
			wheelScrolled = false;
			//updateStatus();
		}

		@Override
		public void onScrollingStarted(WheelView wheel) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onScrollingFinished(WheelView wheel) {
			// TODO Auto-generated method stub
			
		}
	};
	
	
	OnClickListener closePaymentListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if(mStep == SHIPPING_ADDRESS) {
				mUtils.hideKeyBoard(mTelephoneText);
			} else if(mStep == PAYMENT_METHOD) {
				mUtils.hideKeyBoard(mBillingZipText);
			}
			
			onBackPressed();
		}
		
	};
	
	private void setCouponCode(int pVisible) {
		mCouponCodeError.setVisibility(pVisible);
	}
	
	private void setShippingAddress(int pVisible) {
		String fullName = mFullNameText.getText().toString();
		String address = mAddressText.getText().toString();
		String city = mCityText.getText().toString(); 
		String state = mStateText.getText().toString();
		String country = mCountryText.getText().toString();
		String postalCode = mPostalText.getText().toString();
		String telephone = mTelephoneText.getText().toString();
		
		
		if(TextUtils.isEmpty(fullName)) {
			mFullNameError.setVisibility(pVisible);
		} else {
			mFullNameError.setVisibility(View.GONE);
		}
		
		if(TextUtils.isEmpty(address)) {
			mAddressError.setVisibility(pVisible);
		} else {
			mAddressError.setVisibility(View.GONE);
		}
		
		if(TextUtils.isEmpty(city)) {
			mCityError.setVisibility(pVisible);
		} else {
			mCityError.setVisibility(View.GONE);
		}
		
		if(TextUtils.isEmpty(state)) {
			mStateError.setVisibility(pVisible);
		} else {
			mStateError.setVisibility(View.GONE);
		}
		
		if(TextUtils.isEmpty(postalCode)) {
			mPostalError.setVisibility(pVisible);
		} else {
			mPostalError.setVisibility(View.GONE);
		}
		
		if(TextUtils.isEmpty(telephone)) {
			mTelephoneError.setVisibility(pVisible);
		} else {
			mTelephoneError.setVisibility(View.GONE);
		}
		
	}
	
	private void setPaymentMethod(int pVisible) {
		String num_1 = mCardNum_1.getText().toString();
		String num_2 = mCardNum_2.getText().toString();
		String num_3 = mCardNum_3.getText().toString();
		String num_4 = mCardNum_4.getText().toString();
		
		String fullNameOnCard = mFullNameOnCard.getText().toString();
		String cvvCode = mCcvCodeText.getText().toString();
		String billingZip = mBillingZipText.getText().toString();
		String validate = mValidDate.getText().toString();
		
		if(TextUtils.isEmpty(fullNameOnCard)) {
			mFullNameOnCardError.setVisibility(pVisible); 
		} else {
			mFullNameOnCardError.setVisibility(View.GONE); 
		}
		
		if(TextUtils.isEmpty(num_1)) {
			mCardNum_1_Error.setVisibility(pVisible); 
		} else {
			mCardNum_1_Error.setVisibility(View.GONE); 
		}
		
		if(!mUtils.isNumber(num_1)) {
			mCardNum_1_Error.setVisibility(pVisible); 
		} else {
			mCardNum_1_Error.setVisibility(View.GONE); 
		}
		
		if(TextUtils.isEmpty(num_2)) {
			mCardNum_2_Error.setVisibility(pVisible); 
		} else {
			mCardNum_2_Error.setVisibility(View.GONE); 
		}
		
		if(!mUtils.isNumber(num_2)) {
			mCardNum_2_Error.setVisibility(pVisible); 
		} else {
			mCardNum_2_Error.setVisibility(View.GONE); 
		}
		
		if(TextUtils.isEmpty(num_3)) {
			mCardNum_3_Error.setVisibility(pVisible); 
		} else {
			mCardNum_3_Error.setVisibility(View.GONE); 
		}
		
		if(!mUtils.isNumber(num_3)) {
			mCardNum_3_Error.setVisibility(pVisible); 
		} else {
			mCardNum_3_Error.setVisibility(View.GONE); 
		}
		
		if(TextUtils.isEmpty(num_4)) {
			mCardNum_4_Error.setVisibility(pVisible); 
		} else {
			mCardNum_4_Error.setVisibility(View.GONE); 
		}
		
		if(!mUtils.isNumber(num_4)) {
			mCardNum_4_Error.setVisibility(pVisible); 
		} else {
			mCardNum_4_Error.setVisibility(View.GONE); 
		}
		
		if(TextUtils.isEmpty(cvvCode)) {
			mCcvCodeTextError.setVisibility(pVisible); 
		} else {
			mCcvCodeTextError.setVisibility(View.GONE); 
		}
		
		if(!mUtils.isNumber(cvvCode)) {
			mCcvCodeTextError.setVisibility(pVisible); 
		} else {
			mCcvCodeTextError.setVisibility(View.GONE); 
		}
		
		if(cvvCode.length() < 3 || cvvCode.length() > 4) {
			mCcvCodeTextError.setVisibility(pVisible); 
		} else {
			mCcvCodeTextError.setVisibility(View.GONE); 
		}
		
		if(TextUtils.isEmpty(validate)) {
			mValidDateError.setVisibility(pVisible);
		} else {
			mValidDateError.setVisibility(View.GONE);
		}
		
		if(TextUtils.isEmpty(billingZip)) {
			mBillingZipTextError.setVisibility(pVisible);
		} else {
			mBillingZipTextError.setVisibility(View.GONE);
		}
		
	}
	
	OnClickListener stepListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			
			if(mStep == SHIPPING_ADDRESS) {
				String fullName = mFullNameText.getText().toString();
				String address = mAddressText.getText().toString();
				String address2 = mAddressText2.getText().toString();
				String city = mCityText.getText().toString(); 
				String state = mStateText.getText().toString();
				String country = mCountryText.getText().toString();
				String postal = mPostalText.getText().toString();
				String telephone = mTelephoneText.getText().toString();
				
				if(TextUtils.isEmpty(fullName)) {
					mUtils.showToastMessage(mContext, R.string.empty_full_name);
					setShippingAddress(View.VISIBLE);
					return;
				}
				
				if(TextUtils.isEmpty(address)) {
					mUtils.showToastMessage(mContext, R.string.empty_address);
					setShippingAddress(View.VISIBLE);
					return;
				}
				
				
				if(TextUtils.isEmpty(city)) {
					mUtils.showToastMessage(mContext, R.string.empty_city);
					setShippingAddress(View.VISIBLE);
					return;
				}
				
				if(TextUtils.isEmpty(state)) {
					mUtils.showToastMessage(mContext, R.string.empty_state);
					setShippingAddress(View.VISIBLE);
					return;
				}
				
				if(TextUtils.isEmpty(country)) {
					mUtils.showToastMessage(mContext, R.string.empty_country);
					setShippingAddress(View.VISIBLE);
					return;
				}
				
				if(TextUtils.isEmpty(postal)) {
					mUtils.showToastMessage(mContext, R.string.empty_postal);
					setShippingAddress(View.VISIBLE);
					return;
				}
				
				if(TextUtils.isEmpty(telephone)) {
					mUtils.showToastMessage(mContext, R.string.empty_telephone);
					setShippingAddress(View.VISIBLE);
					return;
				}
				
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_NAVIGATE_TO_BILLING, null);
				
				mUtils.hideKeyBoard(mTelephoneText);
				mStep = PAYMENT_METHOD;
				
				mBillingZipText.setText(mPostalText.getText().toString());
				
				mFlipper.setInAnimation(mSlideInRight);
				mFlipper.setOutAnimation(mSlideOutLeft);
				mFlipper.showNext();
				
				scanPaymentMethodState();
				
			} else if(mStep == PAYMENT_METHOD) {
				
				String num_1 = mCardNum_1.getText().toString();
				String num_2 = mCardNum_2.getText().toString();
				String num_3 = mCardNum_3.getText().toString();
				String num_4 = mCardNum_4.getText().toString();
				
				String fullNameOnCard = mFullNameOnCard.getText().toString();
				String cvvCode = mCcvCodeText.getText().toString();
				String billingZip = mBillingZipText.getText().toString();
				String validate = mValidDate.getText().toString();
				
				if(TextUtils.isEmpty(fullNameOnCard)) {
					mUtils.showToastMessage(mContext, R.string.empty_full_name_on_card);
					setPaymentMethod(View.VISIBLE);
					return;
				}
				
				if(TextUtils.isEmpty(num_1)) {
					mUtils.showToastMessage(mContext, R.string.empty_card_number);
					setPaymentMethod(View.VISIBLE);
					return;
				}
				
				if(!mUtils.isNumber(num_1)) {
					mUtils.showToastMessage(mContext, R.string.number_valid);
					setPaymentMethod(View.VISIBLE);
					return;
				}
				
				if(TextUtils.isEmpty(num_2)) {
					mUtils.showToastMessage(mContext, R.string.empty_card_number);
					setPaymentMethod(View.VISIBLE);
					return;
				}
				
				if(!mUtils.isNumber(num_2)) {
					mUtils.showToastMessage(mContext, R.string.number_valid);
					setPaymentMethod(View.VISIBLE);
					return;
				}
				
				if(TextUtils.isEmpty(num_3)) {
					mUtils.showToastMessage(mContext, R.string.empty_card_number);
					setPaymentMethod(View.VISIBLE);
					return;
				}
				
				if(!mUtils.isNumber(num_3)) {
					mUtils.showToastMessage(mContext, R.string.number_valid);
					setPaymentMethod(View.VISIBLE);
					return;
				}
				
				if(TextUtils.isEmpty(num_4)) {
					mUtils.showToastMessage(mContext, R.string.empty_card_number);
					setPaymentMethod(View.VISIBLE);
					return;
				}
				
				if(!mUtils.isNumber(num_4)) {
					mUtils.showToastMessage(mContext, R.string.number_valid);
					setPaymentMethod(View.VISIBLE);
					return;
				}
				
				
				
				if(TextUtils.isEmpty(cvvCode)) {
					mUtils.showToastMessage(mContext, R.string.empty_cvv_code);
					setPaymentMethod(View.VISIBLE);
					return;
				}
				
				if(!mUtils.isNumber(cvvCode)) {
					mUtils.showToastMessage(mContext, R.string.number_valid);
					setPaymentMethod(View.VISIBLE);
					return;
				}
				
				if(cvvCode.length() < 3 || cvvCode.length() > 4) {
					mUtils.showToastMessage(mContext, R.string.cvv_scope);
					setPaymentMethod(View.VISIBLE);
					return;
				}
				if(TextUtils.isEmpty(validate)) {
					mUtils.showToastMessage(mContext, R.string.empty_validate);
					setPaymentMethod(View.VISIBLE);
					return;
				}
				
				if(TextUtils.isEmpty(billingZip)) {
					mUtils.showToastMessage(mContext, R.string.empty_billing_zip);
					setPaymentMethod(View.VISIBLE);
					return;
				}
				
				mUtils.hideKeyBoard(mBillingZipText);
				
				if(mNetworkManager.isNetworkAvailable(mContext)) {
					mDialog = mUtils.getFullScreenDialog(mContext);
					mDialog.show();
					
					try {
						payment();
					} catch (AuthenticationException e) {
						e.printStackTrace();
						dismissDialog();
					}
					
					
//					mConnector.getTotal(String.valueOf(mSettingManager.getUid()), mSettingManager.getLoginToken(), mAddressText2.getText().toString(), new TotalResponseEvent());
				} else {
					mUtils.showToastMessage(mContext, R.string.unconnected_network);
				}
				
				
			} else if(mStep == BUY) {
				mClosePayment.setVisibility(View.INVISIBLE);
				
				if(mNetworkManager.isNetworkAvailable(mContext)) {
					mDialog = mUtils.getFullScreenDialog(mContext);
					mDialog.show();
					
//					try {
//						payment();
//					} catch (AuthenticationException e) {
//						e.printStackTrace();
//						dismissDialog();
//					}
					
					buy();
				} else {
					mUtils.showToastMessage(mContext, R.string.unconnected_network);
				}
				
			} else if(mStep == DONE) {
				mClosePayment.setVisibility(View.INVISIBLE);
				
				setResult(RESULT_OK);
				finish();
				
			}
			
		}
		
	};
	
	private class TotalResponseEvent implements IResponseEvent<Object> {

		@Override
		public void onResponse(Object pParams) {
			DevLog.Logging("Response", pParams.toString());
			new TotalResponseTask().execute(pParams.toString());
		}
		
	}
	
	private PaymentTotal mPaymentTotal;
	private class TotalResponseTask extends AsyncTask<String, Void, Boolean> {
		private String mErrorMessage = "error";
		
		@Override
		protected Boolean doInBackground(String... params) {
			mPaymentTotal = new PaymentTotal();
			GetTotalObject object = new GetTotalObject(mPaymentTotal);
			boolean result = object.onResponseListener(params[0]);
			mErrorMessage = object.getErrorMessage();
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			dismissDialog();
			
			if(result) {
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_BILLING_INFO_SUBMITTED, null);
				
				LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_BILLING_INFO_SUBMITTED, null);
				
				float totalItemCost = Float.valueOf(mPaymentTotal.getTotal()) - Float.valueOf(mPaymentTotal.getShipping()) - Float.valueOf(mPaymentTotal.getTax()) - Float.valueOf(mPaymentTotal.getDiscount());
				mTotalItem.setText(String.format(mRes.getString(R.string.total_items), mSettingManager.getCartCount()));
				mTotalItemCost.setText(String.format(mRes.getString(R.string.price), String.valueOf(totalItemCost)));
				mShippingCost.setText(String.format(mRes.getString(R.string.price), mPaymentTotal.getShipping()));
				mTaxCost.setText(String.format(mRes.getString(R.string.price), mPaymentTotal.getTax())); 
				mSubtotalCost.setText(String.format(mRes.getString(R.string.price), mPaymentTotal.getSubTotal())); 
				mCouponCost.setText(String.format(mRes.getString(R.string.price), mPaymentTotal.getDiscount())); 
				mTotalCost.setText(String.format(mRes.getString(R.string.price), mPaymentTotal.getTotal()));
				
				mButton.setText(R.string.buy);
				mStep = BUY;
				
				mFlipper.setInAnimation(mSlideInRight);
				mFlipper.setOutAnimation(mSlideOutLeft);
				mFlipper.showNext();
				
			} else {
				mUtils.showToastMessage(mContext, mErrorMessage);
			}
		}
		
	}

	private int convertToInteger(String pValue) {
    	if(pValue.equals("01")) {
    		return 1;
    	} else if(pValue.equals("02")) {
    		return 2;
    	} else if(pValue.equals("03")) {
    		return 3;
    	} else if(pValue.equals("04")) {
    		return 4;
    	} else if(pValue.equals("05")) {
    		return 5;
    	} else if(pValue.equals("06")) {
    		return 6;
    	} else if(pValue.equals("07")) {
    		return 7;
    	} else if(pValue.equals("08")) {
    		return 8;
    	} else if(pValue.equals("09")) {
    		return 9;
    	} else if(pValue.equals("10")) {
    		return 10;
    	} else if(pValue.equals("11")) {
    		return 11;
    	} 
    		
    	return 12;
    	
    }
	
	private String mTokenId;
	private void payment() throws AuthenticationException {
		StringBuilder sb = new StringBuilder();
		sb.append(mCardNum_1.getText().toString()).append(mCardNum_2.getText().toString()).append(mCardNum_3.getText().toString()).append(mCardNum_4.getText().toString());
		String[] value = mValidDate.getText().toString().split("-");
		int month = convertToInteger(value[0]);
		
		Card card = new Card(sb.toString(), month, Integer.valueOf(value[1]), mCcvCodeText.getText().toString(), 
				mFullNameText.getText().toString(), mAddressText.getText().toString(), 
				mAddressText2.getText().toString(), mCityText.getText().toString(), 
				mStateText.getText().toString(), mPostalText.getText().toString(), 
				mISOCode);
		
		// Virtual test card with dev server
//		Card card = new Card("4242-4242-4242-4242", 12, 2014, "123");
		boolean validation = card.validateCard();
		
		if(validation) {
			Stripe stripe = new Stripe(PUBLISHABLE_KEY);
			stripe.createToken(card, new TokenCallback() {

				@Override
				public void onError(Exception error) {
					dismissDialog();
					mUtils.showToastMessage(mContext, error.getLocalizedMessage());
				}

				@Override
				public void onSuccess(Token token) {
					mTokenId = token.getId();
					mConnector.getTotal(String.valueOf(mSettingManager.getUid()), mSettingManager.getLoginToken(), mAddressText2.getText().toString(), new TotalResponseEvent());
					
				}
				
			});
			
		} else {
			dismissDialog();
			
			HashMap<String, String> properties = new HashMap<String, String>();
			
			if(!card.validateNumber()) {
				properties.put("msg", "The card number that you entered is invalid");
				mUtils.showToastMessage(mContext, "The card number that you entered is invalid");
			} else if(!card.validateExpiryDate()) {
				properties.put("msg", "The expiration date that you entered is invalid");
				mUtils.showToastMessage(mContext, "The expiration date that you entered is invalid");
			} else if(!card.validateCVC()) {
				properties.put("msg", "The CVC code that you entered is invalid");
				mUtils.showToastMessage(mContext, "The CVC code that you entered is invalid");
			} else {
				properties.put("msg", "The card details that you entered are invalid");
				mUtils.showToastMessage(mContext, "The card details that you entered are invalid");
			}
			
			CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_ERROR_DURING_CHECKOUT, properties);
			
			LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_ERROR_DURING_CHECKOUT, properties);
		} 
		
	}
	
	private void buy() {
		mPaymentBackground.setBackgroundResource(R.drawable.ic_background_v3);
		mButton.setText(R.string.continue_shopping);
		
		
		if(mNetworkManager.isNetworkAvailable(mContext)) {
			String country = mCountryText.getText().toString();
			
			if(country.equals("USA")) {
				country = "US";
			}
			
			mConnector.charge(
					String.valueOf(mSettingManager.getUid()),
					mSettingManager.getLoginToken(), 
					mSettingManager.getEmail(), 
					mTokenId, 
					mFullNameText.getText().toString(), 
					mAddressText.getText().toString(), 
					mAddressText2.getText().toString(), 
					mCityText.getText().toString(), 
					mStateText.getText().toString(), 
					mPostalText.getText().toString(), 
					country,
					mTelephoneText.getText().toString(),
					new ChargeResponseEvent());
			
		} else {
			mUtils.showToastMessage(mContext, R.string.unconnected_network);
		}
	}
	
	private class ChargeResponseEvent implements IResponseEvent<Object> {

		@Override
		public void onResponse(Object pParams) {
			DevLog.defaultLogging(pParams.toString());
			new ChargeResponseTask().execute(pParams.toString());
		}
		
	}
	
	private class ChargeResponseTask extends AsyncTask<String, Void, Boolean> {
		private String mErrorMessage = "error";
		
		@Override
		protected Boolean doInBackground(String... params) {
			ChargeObject object = new ChargeObject();
			boolean result = object.onResponseListener(params[0]);
			mErrorMessage = object.getErrorMessage();
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			dismissDialog();
			
			mTokenId = null;
			
			if(result) {
				HashMap<String, String> properties = new HashMap<String, String>();
				properties.put("total", String.valueOf(mPaymentTotal.getTotal()));
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_PURCHASE, properties);
				LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_PURCHASE, properties);
				
				mSettingManager.setCartCount(0);
				mStep = DONE;
				
				mFlipper.setInAnimation(mSlideInRight);
				mFlipper.setOutAnimation(mSlideOutLeft);
				mFlipper.showNext();
				
			} else {
				mUtils.showToastMessage(mContext, mErrorMessage);
			}
		}
		
	}
	
	private void startProgress() {
		
	}
	
	private void finishProgress() {
		
	}
	
	private TokenList getTokenList() {
		return null;
		
	}
	
	private void dismissDialog() {
		if(mDialog != null && mDialog.isShowing()) {
			mDialog.dismiss();
			mDialog = null;
		}
	}


	@Override
	public void onBackPressed() {
		if(mStep == PAYMENT_METHOD) {
			mStep = SHIPPING_ADDRESS;
			
			scanShippingAddressState();
			mButton.setText(R.string.next);
			
			mFlipper.setOutAnimation(mSlideOutRight);
			mFlipper.setInAnimation(mSlideInLeft);
			
			mFlipper.showPrevious();
		} else if(mStep == BUY) {
			mStep = PAYMENT_METHOD;
			
			scanPaymentMethodState();
			mButton.setText(R.string.next);
			
			mFlipper.setOutAnimation(mSlideOutRight);
			mFlipper.setInAnimation(mSlideInLeft);
			mFlipper.showPrevious();
		} else {
			setResult(RESULT_OK);
			finish();
		}
	}
	
	
}
