package com.bluepegg.creamstyle.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.account.IConnector;
import com.bluepegg.creamstyle.account.IResponseEvent;
import com.bluepegg.creamstyle.interfaces.OnLoadListener;
import com.bluepegg.creamstyle.kissmetrics.CreamStyleStateEventTypes;
import com.bluepegg.creamstyle.kissmetrics.CreamstyleAnalytics;
import com.bluepegg.creamstyle.localytics.LocalyticsAnalytics;
import com.bluepegg.creamstyle.network.NetworkManager;
import com.bluepegg.creamstyle.objects.CartItem;
import com.bluepegg.creamstyle.objects.ClosetItemDetail;
import com.bluepegg.creamstyle.objects.CurrencyObject;
import com.bluepegg.creamstyle.objects.ItemDetail;
import com.bluepegg.creamstyle.objects.ItemList;
import com.bluepegg.creamstyle.objects.LookItem;
import com.bluepegg.creamstyle.objects.manager.ClosetItemDetailListManager;
import com.bluepegg.creamstyle.objects.manager.ClosetItemListManager;
import com.bluepegg.creamstyle.objects.manager.ClosetLookItemManager;
import com.bluepegg.creamstyle.objects.manager.ItemDetailListManager;
import com.bluepegg.creamstyle.objects.manager.ItemListManager;
import com.bluepegg.creamstyle.objects.manager.LookItemManager;
import com.bluepegg.creamstyle.objects.manager.ResponseManager;
import com.bluepegg.creamstyle.objecttype.CartObject;
import com.bluepegg.creamstyle.objecttype.ClosetFeedObject;
import com.bluepegg.creamstyle.objecttype.ClosetItemDetailListObject;
import com.bluepegg.creamstyle.objecttype.ClosetItemListObject;
import com.bluepegg.creamstyle.objecttype.ClosetLookListObject;
import com.bluepegg.creamstyle.objecttype.ContentObject;
import com.bluepegg.creamstyle.objecttype.ItemDetailListObject;
import com.bluepegg.creamstyle.objecttype.ItemListObject;
import com.bluepegg.creamstyle.objecttype.LookListObject;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.utils.DevLog;
import com.bluepegg.creamstyle.utils.Utils;
import com.facebook.AppEventsLogger;

public class SplashActivity extends Activity {
	private static final int DELAY_TIME = 1500;
	private Context mContext;
	private NetworkManager mNetworkManager;
	private SettingManager mSettingManager;
	private IConnector mConnector;
	private ResponseManager mResponseManager;
	private LookItemManager mLookItemManager;
	private ItemDetailListManager mItemDetailListManager;
	private ItemListManager mItemListManager;
	
	private ClosetLookItemManager mClosetLookItemManager;
	private ClosetItemDetailListManager mClosetItemDetailListManager;
	private ClosetItemListManager mClosetItemListManager;
	private Utils mUtils;
	private OnLoadListener mListener;
	private TextView mMessageText;
	
	private Timer mTimer;
	private UpdatingTimerTask mTimerTask;
	private int mCount;
	
//	private SoundllyManager mSoundllyManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
//		Muneris.onCreate(this, savedInstanceState);
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash_layout);
		LocalyticsAnalytics.registerPush("818694583127");
		LocalyticsAnalytics.handlePushReceiver(getIntent());
		
		initManager();
		init();
	}
	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		Muneris.onActivityResult(this, requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}



	@Override
	protected void onDestroy() {
//		Muneris.onDestroy(this);
		super.onDestroy();
	}



	@Override
	protected void onPause() {
//		Muneris.onPause(this);
		super.onPause();
//		mSoundllyManager.unbindSoundllyService();
	}



	@Override
	protected void onRestart() {
		super.onRestart();
//		Muneris.onRestart(this);
	}



	@Override
	protected void onStart() {
		super.onStart();
//		Muneris.onStart(this);
	}



	@Override
	protected void onStop() {
//		Muneris.onStop(this);
		super.onStop();
	}



	@Override
	protected void onResume() {
		super.onResume();
//		Muneris.onResume(this);
		AppEventsLogger.activateApp(mContext, getString(R.string.facebook_appid));
		LocalyticsAnalytics.handlePushReceiver(getIntent());
		
//		mSoundllyManager = Soundlly.getManager();
//		mSoundllyManager.bindSoundllyService();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}

	private void initManager() {
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mNetworkManager = app.getNetworkManager();
		mSettingManager = app.getSettingManager();
		mConnector = app.getConnector();
		mResponseManager = app.getResponseManager();
		mListener = app.getLoadListener();
		mUtils = app.getUtils();
		mClosetLookItemManager = app.getClosetLookItemManager();
		mClosetItemDetailListManager = app.getClosetItemDetailListManager();
		mClosetItemListManager = app.getClosetItemListManager();
		mLookItemManager = app.getLookItemManager();
		mItemDetailListManager = app.getItemDetailListManager();
		mItemListManager = app.getItemListManager();
		
		mSettingManager.setDisplayType(CreamStyleApplication.CREAM);
	}
	
	
	private void init() {
		mContext = this;
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/gf_albert.ttf");
		mUtils.setTypeface(font);
		
		mMessageText = (TextView)findViewById(R.id.message);
		nextActivity(mSettingManager.getLogin());
	}
	
	private class UpdatingTimerTask extends TimerTask {
		
		private String mText;
		
		public UpdatingTimerTask(String pText) {
			mText = pText;
		}

		@Override
		public void run() {
			
			if(mCount % 4 == 0) {
				setLoadText(mText);
			} else if(mCount % 4 == 1) {
				setLoadText(mText + ".");
			} else if(mCount % 4 == 2) {
				setLoadText(mText + "..");
			} else if(mCount % 4 == 3) {
				setLoadText(mText + "...");
			}
			
			mCount++;
		}
		
	}
	
	private void nextActivity(final boolean pFlag) {
		if(pFlag) {
			if(mNetworkManager.isNetworkAvailable(mContext)) {
				mTimer = new Timer();
				mTimerTask = new UpdatingTimerTask(getString(R.string.connecting_server));
				mTimer.schedule(mTimerTask, 1000, 1000);
				
				mConnector.getContent(mSettingManager.getUid(), mSettingManager.getLoginToken(), new ContentResponseEvent());
			} else {
				mMessageText.setTextColor(Color.RED);
				mMessageText.setVisibility(View.VISIBLE);
				mMessageText.setText(R.string.unconnected_network);
			}
		} else {
			if(mNetworkManager.isNetworkAvailable(mContext))
				mConnector.getCurrencyRate("USD/KRW", new CurrencyResponseEvent());
			
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					Intent intent = new Intent(mContext, CreamAccountActivity.class);
					startActivity(intent);
					finish();
				}
				
			}, DELAY_TIME);
			
		}
	}
	
	private class ContentResponseEvent implements IResponseEvent<Object> {

		@Override
		public void onResponse(Object pParams) {
			if(pParams == null) {
				
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						mMessageText.setVisibility(View.VISIBLE);
						mMessageText.setTextColor(Color.RED);
						mMessageText.setText(R.string.unconnected_network);
						clearTimer();
					}
					
				});
				
				
			} else {
				DevLog.Logging("Response", pParams.toString());
				
				ContentObject object = new ContentObject();
				boolean result = object.onResponseListener(pParams.toString());
				int errorCode = object.getCode();
				
				if(result) {
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_LOGIN, null);
					
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_LOGIN, null);

					LookListObject lookObject = new LookListObject();
					boolean done = lookObject.onResponseListener(mResponseManager.getLookListResponse());

					ItemDetailListObject itemDetailObject = new ItemDetailListObject();
					itemDetailObject.onResponseListener(mResponseManager.getItemDetailListResponse());
					
					ItemListObject itemListObject = new ItemListObject();
					itemListObject.onResponseListener(mResponseManager.getItemListResponse());
					
					if(mNetworkManager.isNetworkAvailable(mContext)) {
						clearTimer();
						
						mTimer = new Timer();
						mTimerTask = new UpdatingTimerTask(getString(R.string.collecting_closet_data));
						mTimer.schedule(mTimerTask, 1000, 1000);
						
					}
					
					mConnector.getCloset(String.valueOf(mSettingManager.getUid()), mSettingManager.getLoginToken(), new ClosetResponseEvent());
					
				} else {
					clearTimer();
					
					if(errorCode == 109) {
						mSettingManager.setLogin(false);
						Intent intent = new Intent(mContext, CreamAccountActivity.class);
						startActivity(intent);
						finish();
					} 
				}
			}
			
		}
		
	}
	
	private class ClosetResponseEvent implements IResponseEvent<Object> {

		@Override
		public void onResponse(Object pParams) {
			clearTimer();
			
			if(pParams == null) {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						mMessageText.setVisibility(View.VISIBLE);
						mMessageText.setTextColor(Color.RED);
						mMessageText.setText(R.string.unconnected_network);
						
					}
					
				});
				
				
			} else {
				DevLog.Logging("Response", pParams.toString());
				
				ClosetFeedObject object = new ClosetFeedObject();
				boolean result = object.onResponseListener(pParams.toString());
				
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						mMessageText.setVisibility(View.GONE);
					}
					
				});
				
				if(result) {
					
					ClosetLookListObject closetLookObject = new ClosetLookListObject();
					closetLookObject.onResponseListener(mResponseManager.getClosetLookListResponse());
					updateLookItem();
					
					ClosetItemDetailListObject closetItemDetailObject = new ClosetItemDetailListObject();
					closetItemDetailObject.onResponseListener(mResponseManager.getClosetItemDetailListResponse());
					
					ClosetItemListObject closetItemListObject = new ClosetItemListObject();
					closetItemListObject.onResponseListener(mResponseManager.getClosetItemListResponse());
					updateDetailItem();
					
					
					if(mNetworkManager.isNetworkAvailable(mContext)) {
						mConnector.getCart(String.valueOf(mSettingManager.getUid()), String.valueOf(mSettingManager.getLoginToken()), new CartResponseEvent());
					} else {
						Intent intent = new Intent(mContext, CreamMainActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						finish();
					}
					
				} else {
					mUtils.showToastMessage(mContext, String.format(getString(R.string.cannot_get_closet_information), mSettingManager.getEmail()));
					
					HashMap<Integer, LookItem> lookItems = mLookItemManager.getLookItems();
					if(lookItems.size() > 0) {
						Intent intent = new Intent(mContext, CreamMainActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						finish();
					}
				}
			}
			
			
		}
	} 
	
	private void updateLookItem() {
		HashMap<Integer, LookItem> lookItems = mLookItemManager.getLookItems();
		
		for(Map.Entry<Integer, LookItem> item : lookItems.entrySet()) {
			item.getValue().setCloset(false);
		}
		
		HashMap<Integer, LookItem> closetLookItem = mClosetLookItemManager.getLookItems();
		
		for(Map.Entry<Integer, LookItem> item : closetLookItem.entrySet()) {
			LookItem lookItem = lookItems.get(item.getKey());
			if(lookItem != null)
				lookItem.setCloset(true);
//			else {
//				LookItem closetItem = closetLookItem.get(item.getKey());
//				LookItem oldLookItem = new LookItem();
//				
//				oldLookItem.setItemDetailIdList(closetItem.getItemDetailIdList());
//				oldLookItem.setItemIdList(closetItem.getItemIdList());
//				oldLookItem.setCloset(true);
//				oldLookItem.setLookId(closetItem.getLookId());
//				oldLookItem.setBigLookImage(closetItem.getBigLookImage());
//				oldLookItem.setSmallLookImage(closetItem.getSmallLookImage());
//				oldLookItem.setThumbLookImage(closetItem.getThumbLookImage());
//				oldLookItem.setLookName(closetItem.getLookName());
//				oldLookItem.setDisplayName(closetItem.getDisplayName());
//				oldLookItem.setLookHeight(closetItem.getLookHeight());
//				oldLookItem.setLookWidth(closetItem.getLookWidth());
//				oldLookItem.setFeedMask(closetItem.getFeedMask());
//				
//				mLookItemManager.putItem(item.getKey(), oldLookItem);
//			}
		}
	}
	
	private void updateDetailItem() {
		HashMap<Integer, ItemDetail> itemDetails = mItemDetailListManager.getItemDetails();
		
		for(Map.Entry<Integer, ItemDetail> item : itemDetails.entrySet()) {
			item.getValue().setCloset(false);
		}
		
		HashMap<Integer, ClosetItemDetail> closetItemDetails = mClosetItemDetailListManager.getItemDetails();
		
		for(Map.Entry<Integer, ClosetItemDetail> item : closetItemDetails.entrySet()) {
			if(item.getValue().getClosetTime() != 0) {
				ItemDetail itemDetail = itemDetails.get(item.getKey());
				if(itemDetail != null)
					itemDetail.setCloset(true);
				else {
					ClosetItemDetail closetItemDetail = closetItemDetails.get(item.getKey());
					
					ItemList closetItemList = mClosetItemListManager.getItem(closetItemDetail.getItemId());
					
					ItemList oldItemList = new ItemList();
					oldItemList.setCloset(true);
					oldItemList.setItemBrand(closetItemList.getItemBrand());
					oldItemList.setItemDetail(closetItemList.getItemDetail());
					oldItemList.setItemDetailIdList(closetItemList.getItemDetailIdList());
					oldItemList.setItemId(closetItemList.getItemId());
					oldItemList.setItemName(closetItemList.getItemName());
					mItemListManager.putItem(closetItemDetail.getItemId(), oldItemList);
					
					ClosetItemDetail closetDetail = closetItemDetails.get(item.getKey());
					
					ItemDetail oldItemDetail = new ItemDetail();
					oldItemDetail.setAltBigImageList(closetDetail.getAltBigImageList());
					oldItemDetail.setAltSmallImageList(closetDetail.getAltSmallImageList());
					oldItemDetail.setAltThumbImageList(closetDetail.getAltThumbImageList());
					oldItemDetail.setBigItemImage(closetDetail.getBigItemImage());
					oldItemDetail.setCloset(closetDetail.getClosetTime() == 0 ? false : true);
					oldItemDetail.setColor(closetDetail.getColor());
					oldItemDetail.setInventoryIdList(closetDetail.getInventoryIdList());
					oldItemDetail.setInventorySizeList(closetDetail.getInventorySizeList());
					oldItemDetail.setItemDetailId(closetDetail.getItemDetailId());
					oldItemDetail.setItemHeight(closetDetail.getItemHeight());
					oldItemDetail.setItemWidth(closetDetail.getItemWidth());
					oldItemDetail.setPrice(closetDetail.getPrice());
					oldItemDetail.setSmallItemImage(closetDetail.getSmallItemImage());
					oldItemDetail.setThumbItemImage(closetDetail.getThumbItemImage());
					mItemDetailListManager.putItemDetail(item.getKey(), oldItemDetail);
						
				
				}
			}
		}
	}
	
	private void setLoadText(final String pText) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				mMessageText.setVisibility(View.VISIBLE);
				mMessageText.setText(pText);
				mMessageText.setTextColor(Color.WHITE);
			}
			
		});
	}
	
	private void clearTimer() {
		mCount = 0;
		
		if(mTimer != null) {
			mTimer.cancel();
			mTimer = null;
		}
		
		if(mTimerTask != null) {
			mTimerTask.cancel();
			mTimerTask = null;
		}
	}
	
	private class CartResponseEvent implements IResponseEvent<Object> {

		@Override
		public void onResponse(Object pParams) {
			DevLog.Logging("Response", pParams.toString());
			new CartResponseTask().execute(pParams.toString());
		}
		
	}
	
	private class CartResponseTask extends AsyncTask<String, Void, Boolean> {
		private String mError;
		private int mCode;
		private String mSubTotal;
		private List<CartItem> mCartItems;
		private int mCount;
		
		@Override
		protected void onPreExecute() {
			mCartItems = new ArrayList<CartItem>();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			CartObject object = new CartObject(mCartItems);
			boolean result = object.onResponseListener(params[0]);
			mSubTotal = object.getSubTotal();
			mError = object.getErrorMessage();
			mCode = object.getCode();
			mCount = object.getCount();
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			
			
			if(result) {
				
				if(mCartItems.size() > 0) {
					mSettingManager.setCartCount(mCount);
				} else {
					mSettingManager.setCartCount(0);
				}
				
				if(mNetworkManager.isNetworkAvailable(mContext))
					mConnector.getCurrencyRate("USD/KRW", new CurrencyResponseEvent());
				
			}
			
			Intent intent = new Intent(mContext, CreamMainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
		}
		
	}
	
	private class CurrencyResponseEvent implements IResponseEvent<Object> {

		@Override
		public void onResponse(Object pParams) {
			if(pParams != null) {
				DevLog.Logging("Response", pParams.toString());
				new CurrencyResponseTask().execute(pParams.toString());
			} 
			
		}
		
	}
	
	private class CurrencyResponseTask extends AsyncTask<String, Void, Boolean> {
		private int mCount;
		
		@Override
		protected Boolean doInBackground(String... params) {
			CurrencyObject object = new CurrencyObject();
			return object.onResponseListener(params[0]);
		}
		
	}

}
