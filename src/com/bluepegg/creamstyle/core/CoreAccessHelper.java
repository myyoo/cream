package com.bluepegg.creamstyle.core;

import com.bluepegg.creamstyle.account.BluepeggCreamStyle;
import com.bluepegg.creamstyle.account.IConnector;

public class CoreAccessHelper {
	private volatile SharedTask mSharedTask = null;
	private volatile IConnector mConnector = null;
	
	
	private SharedTask getSharedTask() {
		if(mSharedTask == null) {
			
			synchronized(this) {
				if(mSharedTask == null) {
					mSharedTask = new SharedTask("Common_Thread");
					mSharedTask.start();
				}
			}
		}
		
		return mSharedTask;
	}
	
	public IConnector getConnector() {
		if(mConnector == null) {
			
			synchronized(this) {
				if(mConnector == null) 
					mConnector = new  BluepeggCreamStyle(getSharedTask());
			}
			
		}
		
		return mConnector;
	}
}
