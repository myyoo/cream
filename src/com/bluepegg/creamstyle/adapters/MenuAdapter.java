package com.bluepegg.creamstyle.adapters;

import java.util.List;

import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.objects.MenuInfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


public class MenuAdapter extends BaseAdapter {
	private Context mContext;
	public static final int DEFAULT_DRAWABLE = -1;
	public static final int TYPE_ITEM = 0;
	public static final int TYPE_SEPARATOR = 1;
	private LayoutInflater mInflater;
	private List<MenuInfo> mInfo;
	
	public MenuAdapter(Context pContext, List<MenuInfo> pInfo) {
		mContext = pContext;
		mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mInfo = pInfo;
		
	}
	
	@Override
	public int getCount() {
		return mInfo.size();
	}

	@Override
	public Object getItem(int position) {
		return mInfo.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	

	@Override
	public int getItemViewType(int position) {
		return mInfo.get(position).getType();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		try {
			int type = getItemViewType(position);
			
			if(convertView == null) {
				ViewHolder holder = new ViewHolder();
				switch(type) {
				case TYPE_ITEM:
					convertView = mInflater.inflate(R.layout.menu_item, null);
					holder.title = (TextView)convertView.findViewById(R.id.menu_text);
					break;
					
				}
				
				convertView.setTag(holder);
			}
			
			final ViewHolder holder = (ViewHolder)convertView.getTag();
			final MenuInfo info = mInfo.get(position);
			
			switch(type) {
			case TYPE_ITEM:
				holder.title.setText(info.getMenuItem());
				//holder.title.setCompoundDrawablesWithIntrinsicBounds(info.getDrawableIcon(), 0, 0, 0);
				break;
				
			}
			
			return convertView;
			
		} catch(IndexOutOfBoundsException e) {
			return convertView;
		} catch(NullPointerException e) {
			return convertView;
		}
	}
	
	private static class ViewHolder {
		public TextView title;
	}

}
