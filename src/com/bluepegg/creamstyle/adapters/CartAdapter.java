package com.bluepegg.creamstyle.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.account.IConnector;
import com.bluepegg.creamstyle.account.IResponseEvent;
import com.bluepegg.creamstyle.activities.CreamAccountActivity;
import com.bluepegg.creamstyle.dialogs.CreamStyleDialog;
import com.bluepegg.creamstyle.fragments.CartFragment;
import com.bluepegg.creamstyle.fragments.ClosetFragment;
import com.bluepegg.creamstyle.interfaces.OnShowClosetDetailListener;
import com.bluepegg.creamstyle.interfaces.OnUpdateListView;
import com.bluepegg.creamstyle.kissmetrics.CreamStyleStateEventTypes;
import com.bluepegg.creamstyle.kissmetrics.CreamstyleAnalytics;
import com.bluepegg.creamstyle.localytics.LocalyticsAnalytics;
import com.bluepegg.creamstyle.network.NetworkManager;
import com.bluepegg.creamstyle.objects.CartItem;
import com.bluepegg.creamstyle.objects.ClosetFeed;
import com.bluepegg.creamstyle.objects.FeedItemList;
import com.bluepegg.creamstyle.objects.LookItem;
import com.bluepegg.creamstyle.objects.manager.LookItemManager;
import com.bluepegg.creamstyle.objecttype.SetCartObject;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.utils.DevLog;
import com.bluepegg.creamstyle.utils.Utils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

public class CartAdapter extends BaseAdapter {
	private static final String DELETE_ITEM = "delete-item";
	private static final String REMOVE_ITEM = "remove-item";
	private static final String ADD_ITEM = "add-item";
	
	private OnUpdateListView mUpdateListener = null;
	private OnShowClosetDetailListener mShowClosetDetailListener = null;
	
	private SettingManager mSettingManager;
	private NetworkManager mNetworkManager;
	private Utils mUtils;
	private IConnector mConnector;
	private LookItemManager mLookItemManager;
	private Activity mActivity;
	private CartFragment mFragment;
	private Resources mRes;
	private List<CartItem> mItems;
	private LayoutInflater mInflater;
	private Dialog mDialog;
	private CreamStyleDialog mCreamDialog;
	private String mType;
	private int mDeletePosition;
	private int mInventoryId;
	private String mPrice;
	
	public CartAdapter(Activity pContext, CartFragment pFragment, List<CartItem> pItems, OnUpdateListView pUpdateListener, OnShowClosetDetailListener pShowClosetDetailListener) {
		mActivity = pContext;
		mFragment = pFragment;
		mRes = pContext.getResources();
		mItems = pItems;
		mInflater = LayoutInflater.from(pContext);
		mUpdateListener = pUpdateListener;
		mShowClosetDetailListener = pShowClosetDetailListener;
		
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mSettingManager = app.getSettingManager();
		mNetworkManager = app.getNetworkManager();
		mUtils = app.getUtils();
		mConnector = app.getConnector();
		mLookItemManager = app.getLookItemManager();
	}
	
	@Override
	public int getCount() {
		return mItems.size();
	}

	@Override
	public Object getItem(int position) {
		return mItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if(convertView == null) {
			ViewHolder holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.cart_item_layout, null);
			holder.deleteItemIcon = (ImageView)convertView.findViewById(R.id.deleteItem);
			holder.thumbImageIcon = (ImageView)convertView.findViewById(R.id.thumbImage);
			holder.priceText = (TextView)convertView.findViewById(R.id.price);
			holder.brandNameText = (TextView)convertView.findViewById(R.id.brandName);
			holder.productNameText = (TextView)convertView.findViewById(R.id.productName);
			holder.sizeText = (TextView)convertView.findViewById(R.id.size);
			holder.addIcon = (ImageView)convertView.findViewById(R.id.addItem);
			holder.removeIcon = (ImageView)convertView.findViewById(R.id.removeItem);
			holder.quantityText = (TextView)convertView.findViewById(R.id.itemQuantity);
			convertView.setTag(holder);
		}
		
		final ViewHolder holder = (ViewHolder)convertView.getTag();
		
		final CartItem item = mItems.get(position);
		
		final int id = item.getItemId();
		DevLog.defaultLogging(id + " ///////////////");
		
		final int detailId = item.getItemDetailId();
		final String thumbUrl = item.getThumbImageUrl();
		final List<Integer> inventoryIds = new ArrayList<Integer>();
		inventoryIds.add(item.getInventoryId());
		
		holder.priceText.setText(String.format(mRes.getString(R.string.price), item.getPrice()));
		holder.brandNameText.setText(item.getItemBrand());
		holder.productNameText.setText(item.getItemName());
		holder.sizeText.setText(String.format(mRes.getString(R.string.size_message), item.getSize()));
		holder.quantityText.setText(String.valueOf(item.getQuantity()));
		
		holder.thumbImageIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				HashMap<String, String> properties = new HashMap<String, String>();
				properties.put("item_detail_id", String.valueOf(detailId));
				CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_VIEW_ITEM_DIRECT, properties);
				
				LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_VIEW_ITEM_DIRECT, properties);
				
				FeedItemList feedItem = new FeedItemList();
				ClosetFeed closetFeed = new ClosetFeed();
				closetFeed.setId(id);
				closetFeed.setDetailId(detailId);
				closetFeed.setSmallImage(thumbUrl);
				closetFeed.setInventoryId(inventoryIds);
				feedItem.setClosetFeed(closetFeed);
				mShowClosetDetailListener.onEvent(mFragment, feedItem);
			}
			
		});
		
		new Handler(Looper.getMainLooper()).post(new Runnable() {

			@Override
			public void run() {
				Picasso.with(mActivity).load(item.getThumbImageUrl()).into(new Target() {

					@Override
					public void onBitmapFailed(Drawable errorDrawable) {
						
					}

					@Override
					public void onBitmapLoaded(Bitmap bitmap, LoadedFrom from) {
						if(bitmap != null) {
							int width = bitmap.getWidth();
							int height = bitmap.getHeight();
							
							Bitmap output = Bitmap.createBitmap(width, height, Config.ARGB_8888);
							
							Canvas canvas = new Canvas(output);
							Paint paint = new Paint();
							paint.setAntiAlias(true);
							paint.setDither(true);
							paint.setFilterBitmap(true);
							canvas.drawARGB(0, 0, 0, 0);
							
							Bitmap sourceBitmap = bitmap;
							Rect rect = new Rect(0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight());
							canvas.drawCircle((((float)width) / 2), ((float)height) / 2, Math.min((float)width, (float)height / 2.5f), paint);
							paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
							canvas.drawBitmap(sourceBitmap, rect, rect, paint);
							
							holder.thumbImageIcon.setImageBitmap(output);
						} 
					}

					@Override
					public void onPrepareLoad(Drawable placeholderDrawable) {
						
					}
					
				});
			}
			
		});
		
		
		holder.deleteItemIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mCreamDialog = new CreamStyleDialog(mActivity, R.layout.question_layout, R.string.delete_cart_title, R.drawable.icon_info);
				mCreamDialog.setCancelable(false);
				
				View view = mCreamDialog.getParentView();
				
				TextView contentText = (TextView)view.findViewById(R.id.contentText);
				contentText.setText(String.format(mRes.getString(R.string.delete_cart_item), item.getItemName()));
				
				TextView ok = (TextView)mCreamDialog.getConfirmView();
				TextView cancel = (TextView)mCreamDialog.getCancelView();
				mCreamDialog.show();
				
				cancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						dississCreamDialog();
					}
					
				});
				
				ok.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						dississCreamDialog();
						
						mDialog = mUtils.getFullScreenDialog(mActivity);
						mDialog.show();
						
						if(mNetworkManager.isNetworkAvailable(mActivity)) {
							mType = DELETE_ITEM;
							mInventoryId = item.getInventoryId();
							
							float price = Float.valueOf(item.getPrice());
							price = price * item.getQuantity();
							mPrice = String.valueOf(price);
							mDeletePosition = position;
							mConnector.setCart(mType, String.valueOf(mSettingManager.getUid()), mSettingManager.getLoginToken(), String.valueOf(item.getInventoryId()), String.valueOf("0"), new SetCartResponseEvent());
						} else {
							dismissDialog();
							mUtils.showToastMessage(mActivity, R.string.unconnected_network);
						}
					}
					
				});
			}
			
		});
		
		holder.addIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog = mUtils.getFullScreenDialog(mActivity);
				mDialog.show();
				
				if(mNetworkManager.isNetworkAvailable(mActivity)) {
					mType = ADD_ITEM;
					mInventoryId = item.getInventoryId();
					mPrice = item.getPrice();
					
					mConnector.setCart(mType, String.valueOf(mSettingManager.getUid()), mSettingManager.getLoginToken(), String.valueOf(item.getInventoryId()), String.valueOf("0"), new SetCartResponseEvent());
				} else {
					dismissDialog();
					mUtils.showToastMessage(mActivity, R.string.unconnected_network);
				}
			}
			
		});
		
		holder.removeIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(item.getQuantity() > 1) {
					mDialog = mUtils.getFullScreenDialog(mActivity);
					mDialog.show();
					
					if(mNetworkManager.isNetworkAvailable(mActivity)) {
						mType = REMOVE_ITEM;
						mInventoryId = item.getInventoryId();
						mPrice = item.getPrice();
						mConnector.setCart(mType, String.valueOf(mSettingManager.getUid()), mSettingManager.getLoginToken(), String.valueOf(item.getInventoryId()), String.valueOf("0"), new SetCartResponseEvent());
					} else {
						dismissDialog();
						mUtils.showToastMessage(mActivity, R.string.unconnected_network);
					}
				} else {
					mCreamDialog = new CreamStyleDialog(mActivity, R.layout.question_layout, R.string.delete_cart_title, R.drawable.icon_info);
					mCreamDialog.setCancelable(false);
					
					View view = mCreamDialog.getParentView();
					
					TextView contentText = (TextView)view.findViewById(R.id.contentText);
					contentText.setText(String.format(mRes.getString(R.string.delete_cart_item), item.getItemName()));
					
					TextView ok = (TextView)mCreamDialog.getConfirmView();
					TextView cancel = (TextView)mCreamDialog.getCancelView();
					mCreamDialog.show();
					
					cancel.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dississCreamDialog();
						}
						
					});
					
					ok.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dississCreamDialog();
							
							mDialog = mUtils.getFullScreenDialog(mActivity);
							mDialog.show();
							
							if(mNetworkManager.isNetworkAvailable(mActivity)) {
								mType = DELETE_ITEM;
								mInventoryId = item.getInventoryId();
								mPrice = item.getPrice();
								mDeletePosition = position;
								mConnector.setCart(mType, String.valueOf(mSettingManager.getUid()), mSettingManager.getLoginToken(), String.valueOf(item.getInventoryId()), String.valueOf("0"), new SetCartResponseEvent());
							} else {
								dismissDialog();
								mUtils.showToastMessage(mActivity, R.string.unconnected_network);
							}
							
						}
						
					});
				}
				
			}
			
		});
		
		return convertView;
	}
	

	private class SetCartResponseEvent implements IResponseEvent<Object> {
		
		@Override
		public void onResponse(Object pParams) {
			DevLog.Logging("Response", pParams.toString());
			new CartResponseTask().execute(pParams.toString());
		}
		
	}
	
	
	private class CartResponseTask extends AsyncTask<String, Void, Boolean> {
		private String mErrorMessage = "error";
		private int mCode;
		private int mCount;
		private List<CartItem> mCartItems;
		
		@Override
		protected Boolean doInBackground(String... params) {
			mCartItems = new ArrayList<CartItem>();
			SetCartObject object = new SetCartObject(mCartItems);
			boolean result = object.onResponseListener(params[0]);
			mErrorMessage = object.getErrorMessage();
			mCount = object.getCartCount();
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			dismissDialog();
			
			if(result) {
				mSettingManager.setCartCount(mCount);
				
				if(mType.equals(DELETE_ITEM)) {
					HashMap<String, String> properties = new HashMap<String, String>();
					properties.put("inventory_id", String.valueOf(mInventoryId));
					properties.put("price", mPrice);
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_DELETE_FROM_CART, properties);
					
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_DELETE_FROM_CART, properties);
					
					mItems.remove(mDeletePosition);
					notifyDataSetChanged();
					
					if(mItems.size() == 0) {
						mUpdateListener.onEvent();
					}
				} else if(mType.equals(ADD_ITEM)) {
					
					HashMap<String, String> properties = new HashMap<String, String>();
					properties.put("inventory_id", String.valueOf(mInventoryId));
					properties.put("price", mPrice);
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_ADD_TO_CART, properties);
					
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_ADD_TO_CART, properties);
					
					mItems.clear();
					mItems.addAll(mCartItems);
					notifyDataSetChanged(); 
				} else if(mType.equals(REMOVE_ITEM)) {
					
					HashMap<String, String> properties = new HashMap<String, String>();
					properties.put("inventory_id", String.valueOf(mInventoryId));
					properties.put("price", mPrice);
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_SUBTRACT_FROM_CART, properties);
					
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_SUBTRACT_FROM_CART, properties);
					
					mItems.clear();
					mItems.addAll(mCartItems);
					notifyDataSetChanged();
				}
				
			} else {
				if(mCode == 109) {
					mSettingManager.setLogin(false);
					
					mCreamDialog = new CreamStyleDialog(mActivity, R.layout.question_layout, R.string.cream, R.drawable.icon_info);
					mCreamDialog.setCancelable(false);
					
					View v = mCreamDialog.getParentView();
					
					TextView contentText = (TextView)v.findViewById(R.id.contentText);
					contentText.setText(R.string.login_notice);
					
					TextView cancel = (TextView)mCreamDialog.getConfirmView();
					cancel.setText(R.string.not_yet);
					TextView login = (TextView)mCreamDialog.getCancelView();
					login.setText(R.string.login);
					mCreamDialog.show();
					
					cancel.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dississCreamDialog();
						}
						
					});
					
					login.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dississCreamDialog();
							
							Intent intent = new Intent(mActivity, CreamAccountActivity.class);
							mActivity.startActivity(intent);
							((Activity)mActivity).finish();
						}
						
					});
					
				} else {
					mUtils.showToastMessage(mActivity, mErrorMessage);
				}
			}
		}
		
	}
	
	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	private void dismissDialog() {
		if(mDialog != null && mDialog.isShowing()) {
			mDialog.dismiss();
			mDialog = null;
		}
	}
	
	private void dississCreamDialog() {
		if(mCreamDialog != null && mCreamDialog.isShowing()) {
			mCreamDialog.dismiss();
			mCreamDialog = null;
		}
	}


	private static class ViewHolder {
		public ImageView deleteItemIcon;
		public ImageView thumbImageIcon;
		public TextView priceText;
		public TextView brandNameText;
		public TextView productNameText;
		public TextView sizeText;
		public ImageView addIcon;
		public ImageView removeIcon;
		public TextView quantityText;
	}
}
