package com.bluepegg.creamstyle.adapters;


import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.network.NetworkManager;
import com.bluepegg.creamstyle.objects.LookItem;
import com.bluepegg.creamstyle.staggeredview.StaggerImageView;
import com.bluepegg.creamstyle.utils.DevLog;
import com.bluepegg.creamstyle.utils.Utils;
import com.squareup.picasso.Picasso;


public class ContentAdapter extends BaseAdapter {
    
    private NetworkManager mNetworkManager;
    private static final String ICON_URL = "https://s3.amazonaws.com/template.creamstyle.com/app/top_logo.png";

    static class ViewHolder {
    	LinearLayout logoContainer;
    	StaggerImageView feedView;
    	ImageView logoView;
    	ImageView heartIcon;
    }
    
    private Context mContext;
    private Utils mUtils;
    private Resources mRes;
    private LayoutInflater mLayoutInflater;
    private int mResourceId;
    private int mResize;
    private List<LookItem> mContentLists;
    
    private static final float MAX_SWIPE_DISTANCE = 100.0f;
    private float mTouchPoint = 0.0f;

    public ContentAdapter(Context pContext, int pResourceId) {
        super();
        mContext = pContext;
        mContentLists = new ArrayList<LookItem>();
        mResourceId = pResourceId;
        mRes = mContext.getResources();
        mLayoutInflater = LayoutInflater.from(pContext);
        CreamStyleApplication app = CreamStyleApplication.getApplication();
        mUtils = app.getUtils();
        mNetworkManager = app.getNetworkManager();
        mResize = mUtils.deviceWidth() / 2;
    }
    
    public void add(LookItem pItem) {
    	mContentLists.add(pItem);
    }
    
    public void clear() {
    	mContentLists.clear();
    }
    
    public void addAll(List<LookItem> pItems) {
    	mContentLists = pItems;
    }
    
    @Override
	public int getCount() {
		return Integer.MAX_VALUE;
	}

	@Override
	public Object getItem(int position) {
		return mContentLists.get(position % mContentLists.size());
	}

	@Override
	public long getItemId(int position) {
		return (position  % mContentLists.size());
	}

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mResourceId, parent, false);
            ViewHolder holder = new ViewHolder();
            holder.logoContainer = (LinearLayout) convertView.findViewById(R.id.logoContainer);
            holder.feedView = (StaggerImageView) convertView.findViewById(R.id.feedImage);
            holder.heartIcon = (ImageView) convertView.findViewById(R.id.heartIcon);
            holder.logoView = (ImageView) convertView.findViewById(R.id.logoView);
            convertView.setTag(holder);
        } 
        
        final ViewHolder holder = (ViewHolder) convertView.getTag();
        
        LookItem item = mContentLists.get(mContentLists.size() != 0 ? (position % mContentLists.size()) : position);
        
        if(position == 1) {
//        	if(mUtils.deviceSize() == 2) {
//        		mHeight = 120;
//        	} else if(mUtils.deviceSize() == 4) {
//        		mHeight = 300;
//        	} 
        	
//        	int mHeight = (int)(mUtils.pxToDp(item.getLookHeight()) / 1.3);
        	int mHeight = (int)(mResize / 1.5f);
        	
        	RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, mHeight);
        	holder.logoContainer.setLayoutParams(params);
        	holder.logoContainer.setVisibility(View.VISIBLE);
        	
        	
        	if(mNetworkManager.isNetworkAvailable(mContext)) {
        		Picasso.with(mContext).load(ICON_URL).placeholder(R.drawable.white_heart).error(R.drawable.white_heart).resize(mHeight / 2, mHeight / 2 - 15).into(holder.logoView);
        	} else {
        		holder.logoView.setImageResource(R.drawable.white_heart);
        	}
        	
        	holder.logoContainer.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					
					return true;
				}
        		
        	});
        	
        	
        } else {
        	holder.logoContainer.setVisibility(View.GONE);
        	
        } 
        
        holder.feedView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return false;
			}
        	
        });
        
        
        
        holder.feedView.mHeight = item.getLookHeight();
        holder.feedView.mWidth = item.getLookWidth();
        
        DevLog.defaultLogging("Image: " + item.getSmallLookImage());
        
        Picasso.with(mContext)
        .load(item.getSmallLookImage())
        .resize(mResize, mResize)
        .centerInside()
        .into(holder.feedView);
        
        if(item.getCloset()) {
        	holder.heartIcon.setVisibility(View.VISIBLE);
        } else {
        	holder.heartIcon.setVisibility(View.GONE);
        }
        
        
        return convertView;
    }

}