package com.bluepegg.creamstyle.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.interfaces.OnFrItemsListener;
import com.bluepegg.creamstyle.kissmetrics.CreamStyleStateEventTypes;
import com.bluepegg.creamstyle.kissmetrics.CreamstyleAnalytics;
import com.bluepegg.creamstyle.localytics.LocalyticsAnalytics;
import com.bluepegg.creamstyle.objects.ClosetLookItem;
import com.bluepegg.creamstyle.objects.DetailFeedItem;
import com.bluepegg.creamstyle.objects.FragmentEvent;
import com.bluepegg.creamstyle.objects.ItemDetail;
import com.bluepegg.creamstyle.objects.LookItem;
import com.bluepegg.creamstyle.objects.manager.ClosetItemDetailListManager;
import com.bluepegg.creamstyle.objects.manager.ItemDetailListManager;
import com.bluepegg.creamstyle.pintchzoom.PhotoView;
import com.bluepegg.creamstyle.utils.DevLog;
import com.bluepegg.creamstyle.utils.Utils;
import com.bluepegg.creamstyle.views.CircleImageView;
import com.squareup.picasso.Picasso;

public class CreamViewPagerAdapter extends PagerAdapter {
	private OnFrItemsListener mFrItemsListener = null;
	
	private Context mContext;
	private Utils mUtils;
	private ItemDetailListManager mItemDetailListManager;
	private ClosetItemDetailListManager mClosetItemDetailListManager;
	private LayoutInflater mInflater;
	private ArrayList<LookItem> mContentItemList;
	private int mWidth, mHeight;
	private boolean mIsCloset;
	
	public CreamViewPagerAdapter(Context pContext, ArrayList<LookItem> pContentItemList, OnFrItemsListener pFrItemsListener, boolean pIsCloset) {
		super();
		mContext = pContext;
		mContentItemList = pContentItemList;
		mFrItemsListener = pFrItemsListener;
		mIsCloset = pIsCloset;
		mInflater = LayoutInflater.from(pContext);
		
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mUtils = app.getUtils();
		mItemDetailListManager = app.getItemDetailListManager();
		mClosetItemDetailListManager = app.getClosetItemDetailListManager();
		mWidth = mUtils.deviceWidth();
		mHeight = mUtils.deviceHeight() - 48;
	}

	@Override
	public int getCount() {
		return 700000;
	}
	
	public int getRealCount() {
		return mContentItemList.size();
	}
	
	@Override
	public boolean isViewFromObject(View pager, Object obj) {
		return pager == obj;
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		View view = (View)object;
		((ViewPager)container).removeView(view);
		view = null;
	}
	

	@Override
	public Object instantiateItem(ViewGroup container, final int position) {
		int virtualPosition = position % getRealCount();
		View v = mInflater.inflate(R.layout.feed_detail_layout, null);
		RelativeLayout.LayoutParams layout = new RelativeLayout.LayoutParams(mWidth, mHeight);
		PhotoView feedImage = (PhotoView)v.findViewById(R.id.detailFeedItem);
		feedImage.setLayoutParams(layout);
		LinearLayout feedItemContainer = (LinearLayout)v.findViewById(R.id.feedItemContainer);
		
		final LookItem item = mContentItemList.get(virtualPosition);
		
		Picasso.with(mContext)
		.load(item.getBigLookImage())
		.placeholder(R.drawable.ic_background_v3)
		.error(R.drawable.ic_background_v3)
		.resize(mUtils.deviceWidth(), mUtils.deviceHeight())
		.centerCrop()
		.into(feedImage);
		
		final List<Integer> itemDetailIdList = item.getItemDetailIdList();
		final List<Integer> itemIdList = item.getItemIdList();
		Animation ani = AnimationUtils.loadAnimation(mContext, R.anim.item_anim);
		
		for(int i = 0; i < itemDetailIdList.size(); ++i) {
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.setMargins(10, 10, 10, 10);
			
			RelativeLayout relativeLayout = new RelativeLayout(mContext);
			relativeLayout.setLayoutParams(params);
			relativeLayout.startAnimation(ani);
			final CircleImageView feedItem = new CircleImageView(mContext);
			feedItem.setPosition(i);
			ItemDetail itemDetail = null;
			if(mIsCloset) {
				itemDetail = mClosetItemDetailListManager.getItemDetail(itemDetailIdList.get(i));
			} else {
				itemDetail = mItemDetailListManager.getItemDetail(itemDetailIdList.get(i));
			}
			
			
			if(mUtils.deviceSize() == 1) {
				feedItem.setPoint(150, 150);
				try {
					Picasso.with(mContext).load(itemDetail.getThumbItemImage()).resize(150, 150).centerCrop().into(feedItem);
				} catch(Exception e) {
					e.printStackTrace();
					DevLog.defaultLogging(e.toString());
				}
			} else if(mUtils.deviceSize() == 2) {
				feedItem.setPoint(80, 80);
				try {
					Picasso.with(mContext).load(itemDetail.getThumbItemImage()).resize(80, 80).centerCrop().into(feedItem);
				} catch(Exception e) {
					e.printStackTrace();
					DevLog.defaultLogging(e.toString());
				}
			} else if(mUtils.deviceSize() == 4) { 
				feedItem.setPoint(200, 200);
				try {
					Picasso.with(mContext).load(itemDetail.getThumbItemImage()).resize(200, 200).centerCrop().into(feedItem);
				} catch(Exception e) {
					e.printStackTrace();
					DevLog.defaultLogging(e.toString());
				}
			} else {
				feedItem.setPoint(120, 120);
				try {
					Picasso.with(mContext).load(itemDetail.getThumbItemImage()).resize(120, 120).centerCrop().into(feedItem);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			relativeLayout.addView(feedItem);
			
			feedItemContainer.addView(relativeLayout);
			
			feedItem.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					int detailId = itemDetailIdList.get(feedItem.getPosition());
					int itemId = itemIdList.get(feedItem.getPosition());
					
					HashMap<String, String> properties = new HashMap<String, String>();
					properties.put("item_detail_id", String.valueOf(detailId));
					CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_VIEW_ITEM_DIRECT, properties);
					
					LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_VIEW_ITEM_DIRECT, properties);
					
					DetailFeedItem detailItem = new DetailFeedItem();
					detailItem.setLookId(item.getLookId());
					detailItem.setDetailId(detailId);
					detailItem.setItemId(itemId);
					detailItem.setIsCloset(mIsCloset);
					FragmentEvent event = new FragmentEvent(detailItem);
					mFrItemsListener.onEvent(event);
				}
				
			});
			
		}
		
		container.addView(v);
		
		return v;
	}

}
