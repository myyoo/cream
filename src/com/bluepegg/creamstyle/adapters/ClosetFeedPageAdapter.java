package com.bluepegg.creamstyle.adapters;


import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.objects.ClosetFeed;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.staggeredview.StaggerImageView;
import com.bluepegg.creamstyle.utils.Utils;
import com.squareup.picasso.Picasso;


public class ClosetFeedPageAdapter extends BaseAdapter {

    private static final String TAG = "ClosetFeedPageAdapter";

    static class ViewHolder {
    	LinearLayout logoContainer;
    	StaggerImageView feedView;
    	ImageView closetView;
    }
    
    private Context mContext;
    private Utils mUtils;
    private SettingManager mSettingManager;
    private LayoutInflater mLayoutInflater;
    private int mResourceId;
    private int mResize;
    private List<ClosetFeed> mClosetLists;
//    private int mHeight = 240;

    private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();

    public ClosetFeedPageAdapter(Context pContext, int pResourceId) {
        super();
        mContext = pContext;
        mResourceId = pResourceId;
        mClosetLists = new ArrayList<ClosetFeed>();
        mLayoutInflater = LayoutInflater.from(pContext);
        CreamStyleApplication app = CreamStyleApplication.getApplication();
        mUtils = app.getUtils();
        mSettingManager = app.getSettingManager();
        mResize = mUtils.deviceWidth() / 2;
    }
    
    public void clear() {
    	mClosetLists.clear();
    }
    
    public void add(ClosetFeed pItem) {
    	mClosetLists.add(pItem);
    }
    
    
    @Override
	public int getCount() {
		return mClosetLists.size();
	}

	@Override
	public Object getItem(int position) {
		return mClosetLists.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
    

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mResourceId, parent, false);
            ViewHolder holder = new ViewHolder();
            holder.logoContainer = (LinearLayout) convertView.findViewById(R.id.logoContainer);
            holder.feedView = (StaggerImageView) convertView.findViewById(R.id.feedImage);
            holder.closetView = (ImageView) convertView.findViewById(R.id.logoView);
            convertView.setTag(holder);
        }
        
        ViewHolder holder = (ViewHolder) convertView.getTag();
        ClosetFeed item = mClosetLists.get(position);
        
        if(position == 1) {
//        	if(mUtils.deviceSize() == 2) {
//        		mHeight = 120;
//        	} else if(mUtils.deviceSize() == 4) {
//        		mHeight = 300;
//        	}
        	
        	int mHeight = (int)(mResize / 1.5f);
        	
        	RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mResize, mHeight);
        	holder.logoContainer.setLayoutParams(params);
        	holder.logoContainer.setVisibility(View.VISIBLE);
        	holder.closetView.setImageResource(R.drawable.white_closet);
        	
        	holder.closetView.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					return true;
				}
        		
        	});
        } else {
        	holder.logoContainer.setVisibility(View.GONE);
        }
        

        

        holder.feedView.mWidth = item.getImageWidth();
    	holder.feedView.mHeight = item.getImageHeight();
        
        
        String url = item.getSmallImage();
        	
        Picasso.with(mContext)
        .load(url)
        .resize(mResize, mResize)
        .centerInside()
        .into(holder.feedView);
        
        
        return convertView;
    }
}