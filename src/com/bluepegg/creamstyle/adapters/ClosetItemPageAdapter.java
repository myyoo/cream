package com.bluepegg.creamstyle.adapters;


import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.objects.ClosetItem;
import com.bluepegg.creamstyle.staggeredview.StaggerImageView;
import com.bluepegg.creamstyle.utils.Utils;
import com.squareup.picasso.Picasso;


public class ClosetItemPageAdapter extends BaseAdapter {

    private static final String TAG = "ClosetItemPageAdapter";

    static class ViewHolder {
    	LinearLayout logoContainer;
    	StaggerImageView feedView;
    	ImageView closetView;
    }
    
    private Context mContext;
    private Utils mUtils;
    private LayoutInflater mLayoutInflater;
    private int mResourceId;
    private int mResize;
    private List<ClosetItem> mClosetLists;

    private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();

    public ClosetItemPageAdapter(Context pContext, int pResourceId) {
        super();
        mContext = pContext;
        mResourceId = pResourceId;
        mClosetLists = new ArrayList<ClosetItem>();
        mLayoutInflater = LayoutInflater.from(pContext);
        CreamStyleApplication app = CreamStyleApplication.getApplication();
        mUtils = app.getUtils();
        mResize = mUtils.deviceWidth() / 2;
    }
    
    public void clear() {
    	mClosetLists.clear();
    }
    
    public void add(ClosetItem pItem) {
    	mClosetLists.add(pItem);
    }
    
    
    @Override
	public int getCount() {
		return mClosetLists.size();
	}

	@Override
	public Object getItem(int position) {
		return mClosetLists.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
    

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mResourceId, parent, false);
            ViewHolder holder = new ViewHolder();
            holder.logoContainer = (LinearLayout) convertView.findViewById(R.id.logoContainer);
            holder.feedView = (StaggerImageView) convertView.findViewById(R.id.feedImage);
            holder.closetView = (ImageView) convertView.findViewById(R.id.logoView);
            convertView.setTag(holder);
        }
        
        ViewHolder holder = (ViewHolder) convertView.getTag();
        
        if(position == 1) {
        	RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mResize, 300);
        	holder.logoContainer.setLayoutParams(params);
        	holder.logoContainer.setVisibility(View.VISIBLE);
        	holder.closetView.setImageResource(R.drawable.closet_logo);
        	
        	holder.closetView.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					return true;
				}
        		
        	});
        } else {
        	holder.logoContainer.setVisibility(View.GONE);
        }
        

        ClosetItem item = mClosetLists.get(position);
        if(item.getClosetType() == 1) {
        	holder.feedView.mWidth = item.getItemWidth();
        	holder.feedView.mHeight = item.getItemHeight();
        } else {
        	holder.feedView.mWidth = item.getLookWidth();
        	holder.feedView.mHeight = item.getLookHeight();
        }
        
        
        String url = item.getSmallLookImage();
        
        if(item.getClosetType() == 1)
        	url = item.getSmallItemImage();
        	
        Picasso.with(mContext)
        .load(url)
        .resize(mResize, mResize)
        .centerInside()
        .into(holder.feedView);
        
        
        return convertView;
    }
}