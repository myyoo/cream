package com.bluepegg.creamstyle.setting;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.objects.manager.ClosetFeedItemManager;
import com.bluepegg.creamstyle.objects.manager.ClosetItemDetailListManager;
import com.bluepegg.creamstyle.objects.manager.ClosetLookItemManager;
import com.bluepegg.creamstyle.objects.manager.ItemDetailListManager;
import com.bluepegg.creamstyle.objects.manager.LookItemManager;
import com.bluepegg.creamstyle.objects.manager.ResponseManager;


public class SettingManager {
	private static SharedPreferences mSharedPreferences = null;
	private static final String blank = "";
	private static final int initValue = 0;
	public static final String PREFERENCE_LOGIN = "preference_login";
	public static final String PREFERENCE_LOGIN_TOKEN = "preference_login_token";
	public static final String PREFERENCE_USER_ID = "preference_user_id";
	public static final String PREFERENCE_EMAIL = "preference_email";
	public static final String PREFERENCE_HELP_GUIDE = "preference_help_guide";
	public static final String PREFERENCE_CART_COUNT = "preference_cart_count";
	public static final String PREFERENCE_DISPLAY_TYPE = "preference_display_type";
	public static final String PREFERENCE_CURRENCY_RATE = "preference_currency_rate";
	public static final String PREFERENCE_COUNTRY_CODE = "preference_country_code";
	
	
	public SharedPreferences getSharedPreferences() {
		if(SettingManager.mSharedPreferences == null)
			SettingManager.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(CreamStyleApplication.getApplication().getApplicationContext());
		
		return SettingManager.mSharedPreferences;
	}
	
	public void setLogin(boolean pFlag) {
		Editor editor = getSharedPreferences().edit();
		editor.putBoolean(PREFERENCE_LOGIN, pFlag);
		editor.commit();
	}
	
	public boolean getLogin() {
		return getSharedPreferences().getBoolean(PREFERENCE_LOGIN, false);
	}
	
	public void setLoginToken(String pStr) {
		Editor editor = getSharedPreferences().edit();
		editor.putString(PREFERENCE_LOGIN_TOKEN, pStr);
		editor.commit();
	}
	
	public String getLoginToken() {
		return getSharedPreferences().getString(PREFERENCE_LOGIN_TOKEN, blank);
	}
	
	public void setUid(int pValue) {
		Editor editor = getSharedPreferences().edit();
		editor.putInt(PREFERENCE_USER_ID, pValue);
		editor.commit();
	}
	
	public int getUid() {
		return getSharedPreferences().getInt(PREFERENCE_USER_ID, initValue);
	}
	
	public void setEmail(String pStr) {
		Editor editor = getSharedPreferences().edit();
		editor.putString(PREFERENCE_EMAIL, pStr);
		editor.commit();
	}
	
	public String getEmail() {
		return getSharedPreferences().getString(PREFERENCE_EMAIL, blank);
	}
	
	public void setHelpGuide(boolean pFlag) {
		Editor editor = getSharedPreferences().edit();
		editor.putBoolean(PREFERENCE_HELP_GUIDE, pFlag);
		editor.commit();
	}
	
	public boolean getHelpGuide() {
		return getSharedPreferences().getBoolean(PREFERENCE_HELP_GUIDE, false);
	}
	
	public void setCartCount(int pValue) {
		Editor editor = getSharedPreferences().edit();
		editor.putInt(PREFERENCE_CART_COUNT, pValue);
		editor.commit();
	}
	
	public int getCartCount() {
		return getSharedPreferences().getInt(PREFERENCE_CART_COUNT, 0);
	}
	
	public void setDisplayType(String pType) {
		Editor editor = getSharedPreferences().edit();
		editor.putString(PREFERENCE_DISPLAY_TYPE, pType);
		editor.commit();
	}
	
	public String getDisplayType() {
		return getSharedPreferences().getString(PREFERENCE_DISPLAY_TYPE, CreamStyleApplication.CREAM);
	}
	
	public void setCurrencyRate(String pValue) {
		Editor editor = getSharedPreferences().edit();
		editor.putString(PREFERENCE_CURRENCY_RATE, pValue);
		editor.commit();
	}
	
	public String getCurrencyRage() {
		return getSharedPreferences().getString(PREFERENCE_CURRENCY_RATE, "1050.0");
	}
	
	public void setCountryCode(String pValue) {
		Editor editor = getSharedPreferences().edit();
		editor.putString(PREFERENCE_COUNTRY_CODE, pValue);
		editor.commit();
	}
	
	public String getCountryCode() {
		return getSharedPreferences().getString(PREFERENCE_COUNTRY_CODE, null);
	}
	
	public void logout() {
		setLogin(false);
		setLoginToken(blank);
		setUid(initValue);
		setCartCount(initValue);
		setEmail(blank);
		setHelpGuide(false);
		setDisplayType(CreamStyleApplication.CREAM);
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		ResponseManager responseManager = app.getResponseManager();
		responseManager.setLookListResponse(blank);
		responseManager.setItemListResponse(blank);
		responseManager.setItemDetailListResponse(blank);
		responseManager.setClosetLookListResponse(blank);
		
		ClosetLookItemManager closetLookImageManager = app.getClosetLookItemManager();
		closetLookImageManager.clear();
		
		ClosetItemDetailListManager closetItemDetailManager = app.getClosetItemDetailListManager();
		closetItemDetailManager.clear();
		
		ClosetFeedItemManager closetFeedItemManager = app.getClosetFeedItemManager();
		closetFeedItemManager.clear();
		
		LookItemManager lookItemManager = app.getLookItemManager();
		lookItemManager.clear();
		
		ItemDetailListManager itemDetailListManager = app.getItemDetailListManager();
		itemDetailListManager.clear();
	}
}
