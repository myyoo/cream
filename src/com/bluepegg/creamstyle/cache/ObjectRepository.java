package com.bluepegg.creamstyle.cache;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

public final class ObjectRepository {
		
	public static void saveObject(Object pObject , String pFileName) throws FileNotFoundException, IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(pFileName));
		os.writeObject(pObject);
		os.flush();
		os.close();
	}
	
	public static Object readObject (String pFileName) throws StreamCorruptedException, FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(pFileName));
		
		Object object = is.readObject();
		
		is.close();
		return object;
	}
}
