package com.bluepegg.creamstyle.cache;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

public class ImageCacheHandler {
	static final String TAG = ImageCacheHandler.class.getName();

	private final ImageCache mImageCache;

	public ImageCacheHandler(ImageCache pImageCache) {
		mImageCache = pImageCache;
	}

	public void setImageBitmap(String pUrl){
		Bitmap bitmap = mImageCache.getBitmapFromCache(pUrl);
		if (bitmap == null) {
			if (cancelDownloaderTask (pUrl , null)) {
				BitmapDownloaderTask task = new BitmapDownloaderTask(null);
				task.execute(pUrl);
			}
		} 
	}
	
	public void setImageBitmap(String pUrl, ImageView pImageView) {

		Bitmap bitmap = mImageCache.getBitmapFromCache(pUrl);
		if (bitmap == null) {
			if (cancelDownloaderTask (pUrl , pImageView)) {
				BitmapDownloaderTask task = new BitmapDownloaderTask(pImageView);
				DownloaderBitmapDrawable drawable = new DownloaderBitmapDrawable(mDefaultBitmap , task);
				pImageView.setImageDrawable(drawable);
				task.execute(pUrl);
			}
		} else {
			pImageView.setImageBitmap(bitmap);
		}
	}

	private Bitmap mDefaultBitmap;

	public void setDefaultBitmap(Bitmap pBitmap) {
		// Bitmap before set up
		mDefaultBitmap = pBitmap;
	}

	public ImageCache getImageCache() {
		// return image cache 
		return mImageCache;
	}
	
	static boolean cancelDownloaderTask (String pUrl , ImageView pImageView) {
		// cancel download if task url is not equal
		BitmapDownloaderTask task = getBitmapDownloaderTask(pImageView);
		if (task != null) {
			if (task.mDownloaderAddress == null || (!task.mDownloaderAddress.equals(pUrl))) {
				task.cancel(true);
			} else return false;
		} 
		return true;
	}
	
	static BitmapDownloaderTask getBitmapDownloaderTask(ImageView pImageView) {
		// return task object 
		if (pImageView != null) {
			Drawable drawable = pImageView.getDrawable();
			if (drawable instanceof DownloaderBitmapDrawable) {
				DownloaderBitmapDrawable downloaderBitmapDrawable = (DownloaderBitmapDrawable)drawable;
				return downloaderBitmapDrawable.getBitmapDownloaderTask();
			}
		}
		return null;
	}

	static class DownloaderBitmapDrawable extends BitmapDrawable {

		private final WeakReference<BitmapDownloaderTask> mBitmapDownloaderTaskReference;

		DownloaderBitmapDrawable(Bitmap pBitmap,
				BitmapDownloaderTask pBitmapDownloaderTask) {
			super(pBitmap);

			mBitmapDownloaderTaskReference = new WeakReference<BitmapDownloaderTask>(
					pBitmapDownloaderTask);
		}

		public BitmapDownloaderTask getBitmapDownloaderTask() {
			return mBitmapDownloaderTaskReference.get();
		}
	}

	// Bitmap download async task
	class BitmapDownloaderTask extends AsyncTask<String, Void, Bitmap> {
		private String mDownloaderAddress;
		private final WeakReference<ImageView> mImageViewReference;

		public BitmapDownloaderTask(ImageView pImageView) {
			mImageViewReference = new WeakReference<ImageView>(pImageView);
		}

		@Override
		protected Bitmap doInBackground(String... params) {
			mDownloaderAddress = params[0];
			Bitmap bitmap = null;
			try {
				return BitmapManager.fetchImage(mDownloaderAddress);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return bitmap;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap) {
			if (bitmap != null) {
				mImageCache.addBitmapToCache(mDownloaderAddress, bitmap);

				if (mImageViewReference != null) {
					ImageView imageView = mImageViewReference.get();
					BitmapDownloaderTask task = getBitmapDownloaderTask (imageView);
													
					if (this == task) {
						imageView.setImageBitmap(bitmap);
					}
				}
			}
		}
	}

}
