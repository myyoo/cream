package com.bluepegg.creamstyle.cache;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.util.Log;

public class ImageUtil {

	static String TAG = ImageUtil.class.getName();
	/**
	 * Try to find a good inSampleSize size and still get image size smaller than maxBytes
	 */
	public static int findSampleSizeByFileSize(final int pWidth, final int pHeight, final double pMaxBytes, final Config pQualityConf, final boolean pVerbose) {
		// Calculate a safe but small inSampleSize
		double inSampleSize = 1;
		for (; (ImageUtil.getImageSize(pWidth * pHeight, ImageUtil.calculateSizeMultiplier(pQualityConf)) / (inSampleSize * inSampleSize) > pMaxBytes); inSampleSize++) {
			if (pVerbose) {
				Log.d(TAG, "findSampleSizeByFileSize tests inSampleSize=" + inSampleSize + "; " + (pWidth * pHeight * ImageUtil.calculateSizeMultiplier(pQualityConf) / (inSampleSize * inSampleSize) / (1020 * 1024)) + "megabytes; " + (pWidth / inSampleSize) + "x" + (pHeight / inSampleSize));
			}
		}
		if (pVerbose) {
			Log.d(TAG, "findInSampleSizeByFileSize suggests inSampleSize=" + inSampleSize + "; " + (pWidth * pHeight * ImageUtil.calculateSizeMultiplier(pQualityConf) / (inSampleSize * inSampleSize) / (1020 * 1024)) + "megabytes; " + (pWidth / inSampleSize) + "x" + (pHeight / inSampleSize));
		}
		return (int) inSampleSize;
	}
	
	/**
	 * Calculate image size in bytes
	 * @param pPixelCount width*height
	 * @return pixelCount*{multiplier}byte
	 */
	public static double getImageSize(final double pPixelCount, final int pMultiplier) {
		return pPixelCount * ((double) pMultiplier);
	}
	
	/**
	 * Find out how many bytes will one pixel take
	 * @param pConf
	 * @return
	 */
	public static int calculateSizeMultiplier(final Bitmap.Config pConf) {
		// Find the mem footprint multiplier
		int multiplier = 4; // Assume the worst
		if (pConf.equals(Bitmap.Config.ARGB_8888)) {
			multiplier = 4; // one pixel = 4 bytes
		} else if (pConf.equals(Bitmap.Config.ARGB_4444)) {
			multiplier = 2; // one pixel = 2 bytes
		} else if (pConf.equals(Bitmap.Config.RGB_565)) {
			multiplier = 2; // one pixel = 2 bytes
		} else if (pConf.equals(Bitmap.Config.ALPHA_8)) {
			multiplier = 1; // one pixel = 1 bytes
		}
		return multiplier;
	}
}
