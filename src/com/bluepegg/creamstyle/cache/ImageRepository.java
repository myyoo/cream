package com.bluepegg.creamstyle.cache;

import java.io.File;

/**
 * 캐쉬메모리에 이미지를 저장
 * @author manyeon
 */
public class ImageRepository extends ImageCacheHandler {
	private static final String FILE_NAME = "/data/data/com.ancor.mcras/cache/" + "resource_image.cache";

	public ImageRepository() {
		super(new File(FILE_NAME).exists() ? ImageCache.toImageCache(FILE_NAME) : new ImageCache());
	}

	public void save() {
		File f = new File(FILE_NAME);
		File parent = f.getParentFile();

		if (!parent.isDirectory()) {
			parent.mkdir();
		}
		ImageCache.fromImageCache(FILE_NAME, getImageCache());
	}
}
