package com.bluepegg.creamstyle.cache;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

public class BitmapManager {
	private static final int IMAGE_MAX_BYTES = 1024 * 1024 * 2 /* 2 mb */;
	
	boolean mFlag = false;
	
	private final static String TAG = BitmapManager.class.getName();
	
	public static InputStream getInputStream(String pUrl){
		InputStream is = null;
		try{
			URL img = new URL(pUrl);
			is = img.openStream();
		}catch(Exception e){}
		
		return is;
	}
	
	public static Bitmap fetchImage(final String pUrl) throws MalformedURLException, IOException {
		if (TextUtils.isEmpty(pUrl)) {
			// Fail, we do not know what to download
			return null;
		}
		
		final byte[] dataArray = readInputStreamToByteArray(getInputStream(pUrl));
		if (dataArray == null || dataArray.length == 0) {
			// We failed to get anything from the server
			return null;
		}
		
		final BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inPreferredConfig = Config.ARGB_8888;
		opts.inDither = false;
		opts.inPurgeable = true;
		opts.inInputShareable = true;
		opts.inTempStorage = new byte[32 * 1024];
		opts.inJustDecodeBounds = true;
		
		// Check the bitmap width and height
		BitmapFactory.decodeByteArray(dataArray, 0, dataArray.length, opts);
		
		final int srcWidth = opts.outWidth;
		final int srcHeight =  opts.outHeight;
		// Find a good inSampleSize
		opts.inSampleSize = ImageUtil.findSampleSizeByFileSize(srcWidth, srcHeight, IMAGE_MAX_BYTES, opts.inPreferredConfig, false);
		opts.inJustDecodeBounds = false;
		// Decode the bitmap with correct inSampleSize
		return BitmapFactory.decodeByteArray(dataArray, 0, dataArray.length, opts);
	}
	
	
	/**
	 * Copies the contents of the InputStream to a Byte Array.
	 * PS: Does close the InputStream!
	 */
	private static byte[] readInputStreamToByteArray(final InputStream pIs) throws IOException {
		byte[] dataArray = null;
		if (pIs == null) {
			// Failed
			return dataArray;
		}
		

		final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int nRead;
		byte[] data = new byte[1024];
		while ((nRead = pIs.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, nRead);
		}
		buffer.flush();
		pIs.close();
		dataArray = buffer.toByteArray();
		buffer.close();

		return dataArray;
	}
	
	
}
