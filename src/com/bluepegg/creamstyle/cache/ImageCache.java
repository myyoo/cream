package com.bluepegg.creamstyle.cache;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;

// Write file cache via serializable
public class ImageCache implements Serializable {
	
	private static final long serialVersionUID = 3490364931254392574L;
	
	private final Map<String , SynchronizedBitmap> mSynchronizedMap;
	
	public ImageCache() {
		mSynchronizedMap = new HashMap<String , SynchronizedBitmap>();
	}
	
	void addBitmapToCache(String pUrl , Bitmap pBitmap) {
		// Add Bitmap 
		mSynchronizedMap.put(pUrl, new SynchronizedBitmap(pBitmap));	
	}
	
	Bitmap getBitmapFromCache(String pUrl) {
		// get Bitmap
		SynchronizedBitmap bitmap = mSynchronizedMap.get(pUrl);
		if (bitmap != null)
			return bitmap.get();
		return null;
	}
	
	public void clearCache() {
		// Delete all cache
		mSynchronizedMap.clear();
	}
		
	public static ImageCache toImageCache (String pFileName) {
		ImageCache imageCache = null;
		try {
			imageCache = (ImageCache)ObjectRepository.readObject(pFileName);
		} catch (Exception e) {
		}
		return imageCache;  
	}
	
	public static boolean fromImageCache (String pFileName , ImageCache pCcache) {
		// Save object
		try {
			ObjectRepository.saveObject(pCcache, pFileName);
			
			return true;
		} catch (Exception e) {
		}
		return false;
	}
		
	// Declare serializable Bitmap 
	static final class SynchronizedBitmap implements Serializable {

		private static final long serialVersionUID = 1859678728937516189L;
		
		private final Bitmap mBitmap;
		
		public SynchronizedBitmap(Bitmap pBitmap) {
			this.mBitmap = pBitmap;
		}
		
		public Bitmap get() {
			return mBitmap;
		}
	}
}
