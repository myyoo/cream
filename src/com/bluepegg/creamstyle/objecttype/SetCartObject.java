package com.bluepegg.creamstyle.objecttype;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bluepegg.creamstyle.objects.CartItem;
import com.bluepegg.creamstyle.utils.DevLog;

public class SetCartObject extends AbstractObject {
	private static final String TAG_CART = "cart";
	private static final String TAG_SUB_TOTAL = "sub_total";
	private static final String TAG_INVENTORY_ID = "inventory_id";
	private static final String TAG_THUMB_IMAGE_URL = "thumb_image_url";
	private static final String TAG_ITEM_BRAND = "item_brand";
	private static final String TAG_ITEM_NAME = "item_name";
	private static final String TAG_SIZE = "size";
	private static final String TAG_COLOR = "color";
	private static final String TAG_QUANTITY = "quantity";
	private static final String TAG_PRICE = "price";
	
	private int mCount;
	private String mSubTotal;
	private List<CartItem> mCartItems;
	
	public SetCartObject(List<CartItem> pCartItems) {
		mCartItems = pCartItems;
	}
	
	@Override
	public boolean onResponseListener(String pResponse) {
		try {
			JSONObject json = new JSONObject(pResponse);
			boolean success = json.getBoolean(TAG_SUCCESS);
			
			if(!success) {
				mErrorMessage = json.getString(TAG_MSG);
				mCode = json.getInt(TAG_CODE);
				return false;
			}
			
			
			JSONArray cartList = json.getJSONArray(TAG_CART);
			
			mSubTotal = json.getString(TAG_SUB_TOTAL);
			
			
			for(int i = 0; i < cartList.length(); ++i) {
				JSONObject obj = cartList.getJSONObject(i);
				CartItem cartItem = new CartItem();
				
				try {
					int inventoryId = obj.getInt(TAG_INVENTORY_ID);
					cartItem.setInventoryId(inventoryId);
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					String thumbImageUrl = obj.getString(TAG_THUMB_IMAGE_URL);
					cartItem.setThumbImageUrl(thumbImageUrl);
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					String itemBrand = obj.getString(TAG_ITEM_BRAND);
					cartItem.setItemBrand(itemBrand);
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					String itemName = obj.getString(TAG_ITEM_NAME);
					cartItem.setItemName(itemName);
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					String size = obj.getString(TAG_SIZE);
					cartItem.setSize(size);
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					String color = obj.getString(TAG_COLOR);
					cartItem.setColor(color);
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					int quantity = obj.getInt(TAG_QUANTITY);
					mCount += quantity;
					cartItem.setQuantity(quantity);
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					String price = obj.getString(TAG_PRICE);
					cartItem.setPrice(price);
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				mCartItems.add(cartItem);
			}
			
		} catch(JSONException e) {
			e.printStackTrace();
			DevLog.defaultLogging(e.toString());
		}
		return true;
	}

	public int getCartCount() {
		return mCount;
	}
	
	public String getSubTotal() {
		return mSubTotal;
	}
}
