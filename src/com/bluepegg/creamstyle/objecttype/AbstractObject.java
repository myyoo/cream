package com.bluepegg.creamstyle.objecttype;

public abstract class AbstractObject {
	protected String TAG_DATA = "data";
	protected String TAG_SUCCESS = "success";
	protected String TAG_MSG = "msg";
	protected String TAG_CODE = "code";
	
	protected String mErrorMessage = "";
	protected int mCode;
	
	public abstract boolean onResponseListener(String pResponse); 
	
	public String getErrorMessage() {
		return mErrorMessage;
	}
	
	public int getCode() {
		return mCode;
	}
}
