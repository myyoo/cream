package com.bluepegg.creamstyle.objecttype;

import org.json.JSONException;
import org.json.JSONObject;

import com.bluepegg.creamstyle.objects.PaymentTotal;
import com.bluepegg.creamstyle.utils.DevLog;

public class GetTotalObject extends AbstractObject {
	private static final String TAG_SUB_TOTAL = "sub_total";
	private static final String TAG_SHIPPING = "shipping";
	private static final String TAG_DISCOUNT = "discount";
	private static final String TAG_TAX_TOTAL = "tax_total";
	private static final String TAG_TOTAL = "total";
	
	private PaymentTotal mPaymentTotal;
	
	public GetTotalObject(PaymentTotal pPaymentTotal) {
		mPaymentTotal = pPaymentTotal;
	}
	
	@Override
	public boolean onResponseListener(String pResponse) {
		try {
			JSONObject json = new JSONObject(pResponse);
			boolean success = json.getBoolean(TAG_SUCCESS);
			
			if(!success) {
				mErrorMessage = json.getString(TAG_MSG);
				return false;
			}
			
			mPaymentTotal.setSubTotal(json.getString(TAG_SUB_TOTAL));
			mPaymentTotal.setShipping(json.getString(TAG_SHIPPING));
			mPaymentTotal.setDiscount(json.getString(TAG_DISCOUNT));
			mPaymentTotal.setTax(json.getString(TAG_TAX_TOTAL));
			mPaymentTotal.setTotal(json.getString(TAG_TOTAL));
			
		} catch(JSONException e) {
			e.printStackTrace();
			DevLog.defaultLogging(e.toString());
		}
		return true;
	}

}
