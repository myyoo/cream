package com.bluepegg.creamstyle.objecttype;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.utils.DevLog;

public class SetClosetObject extends AbstractObject {
	
	@Override
	public boolean onResponseListener(String pResponse) {
		
		try {
			JSONObject json = new JSONObject(pResponse);
			boolean success = json.getBoolean(TAG_SUCCESS);
			
			if(!success) {
				mErrorMessage = json.getString(TAG_MSG);
				mCode = json.getInt(TAG_CODE);
				return false;
			}
			
		} catch(JSONException e) {
			e.printStackTrace();
			DevLog.defaultLogging(e.toString());
			return false;
		}
		return true;
	}

}
