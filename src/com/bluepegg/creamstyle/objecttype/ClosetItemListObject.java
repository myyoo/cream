package com.bluepegg.creamstyle.objecttype;



import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.objects.ItemList;
import com.bluepegg.creamstyle.objects.manager.ClosetItemListManager;
import com.bluepegg.creamstyle.utils.DevLog;

public class ClosetItemListObject extends AbstractObject {
	private static final String TAG_ITEM_ID = "item_id";
	private static final String TAG_ITEM_DETAIL_LIST = "item_detail_list";
	private static final String TAG_ITEM_DETAIL = "item_detail";
	private static final String TAG_ITEM_BRAND = "item_brand";
	private static final String TAG_ITEM_NAME = "item_name";
	
	private ClosetItemListManager mClosetItemListManager;
	
	public ClosetItemListObject() {
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mClosetItemListManager = app.getClosetItemListManager();
	}
	
	@Override
	public boolean onResponseListener(String pResponse) {
		DevLog.Logging("Result", pResponse);
		
		try {
			JSONArray response = new JSONArray(pResponse);
			
			if(response.length() == 0)
				return false;
			
			mClosetItemListManager.clear();
			
			for(int i = 0; i < response.length(); ++i) {
				JSONObject obj = response.getJSONObject(i);
				ItemList item = new ItemList();
				
				try {
					item.setItemId(obj.getInt(TAG_ITEM_ID));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setItemDetail(obj.getString(TAG_ITEM_DETAIL));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setItemBrand(obj.getString(TAG_ITEM_BRAND));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setItemName(obj.getString(TAG_ITEM_NAME));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				JSONArray itemDetailIds = obj.getJSONArray(TAG_ITEM_DETAIL_LIST);
				List<Integer> itemDetailIdList = new ArrayList<Integer>();
				for(int n = 0; n < itemDetailIds.length(); ++n) {
					Integer itemDetailId = (Integer)itemDetailIds.get(n);
					itemDetailIdList.add(itemDetailId);
				}
				item.setItemDetailIdList(itemDetailIdList);
				
				mClosetItemListManager.putItem(obj.getInt(TAG_ITEM_ID), item);
			}
			
		} catch(JSONException e) {
			e.printStackTrace();
		}
		
		return true;
	}

}
