package com.bluepegg.creamstyle.objecttype;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.objects.LookItem;
import com.bluepegg.creamstyle.objects.manager.LookItemManager;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.utils.DevLog;
import com.bluepegg.creamstyle.utils.Utils;

public class LookListObject extends AbstractObject {
	private static final String TAG_LOOK_ID = "look_id";
	private static final String TAG_BIG_LOOK_IMAGE = "big_look_image";
	private static final String TAG_SMALL_LOOK_IMAGE = "small_look_image";
	private static final String TAG_THUMB_LOOK_IMAGE = "thumb_look_image";
	private static final String TAG_LOOK_NAME = "look_name";
	private static final String TAG_DISPLAY_NAME = "display_name";
	private static final String TAG_LOOK_HEIGHT = "look_height";
	private static final String TAG_LOOK_WIDTH = "look_width";
	private static final String TAG_FEED_MASK = "feed_mask";
	private static final String TAG_ITEM_DETAIL_ID_LIST = "item_detail_id_list";
	private static final String TAG_ITEM_ID_LIST = "item_id_list";
	
	private LookItemManager mLookItemManager;
	private SettingManager mSettingManager;
	private Utils mUtils;
	
	public LookListObject() {
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mLookItemManager = app.getLookItemManager();
		mSettingManager = app.getSettingManager();
		mUtils = app.getUtils();
	}
	
	@Override
	public boolean onResponseListener(String pResponse) {
		
		DevLog.Logging("Result", pResponse);
		
		try {
			JSONArray response = new JSONArray(pResponse);
			
			if(response.length() == 0)
				return false;
			
			mLookItemManager.clear();
			
			for(int i = 0; i < response.length(); ++i) {
				JSONObject obj = response.getJSONObject(i);
				LookItem item = new LookItem();
				
				try {
					item.setLookId(obj.getInt(TAG_LOOK_ID));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setBigLookImage(obj.getString(TAG_BIG_LOOK_IMAGE));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setSmallLookImage(obj.getString(TAG_SMALL_LOOK_IMAGE));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setThumbLookImage(obj.getString(TAG_THUMB_LOOK_IMAGE));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setLookWidth(obj.getInt(TAG_LOOK_WIDTH));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setLookHeight(obj.getInt(TAG_LOOK_HEIGHT));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setFeedMask(obj.getInt(TAG_FEED_MASK));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setLookName(obj.getString(TAG_LOOK_NAME));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setDisplayName(obj.getString(TAG_DISPLAY_NAME));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				JSONArray itemDetails = obj.getJSONArray(TAG_ITEM_DETAIL_ID_LIST);
				List<Integer> itemDetailIdList = new ArrayList<Integer>();
				for(int n = 0; n < itemDetails.length(); ++n) {
					Integer detailId = (Integer)itemDetails.get(n);
					itemDetailIdList.add(detailId);
				}
				item.setItemDetailIdList(itemDetailIdList);
				
				JSONArray itemIds = obj.getJSONArray(TAG_ITEM_ID_LIST);
				List<Integer> itemIdList = new ArrayList<Integer>();
				for(int n = 0; n < itemIds.length(); ++n) {
					Integer itemId = (Integer)itemIds.get(n);
					itemIdList.add(itemId);
				}
				item.setItemIdList(itemIdList);
				
				item.setPosition(i);
				mLookItemManager.putItem(obj.getInt(TAG_LOOK_ID), item);
				
			}
			
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return true;
	}

}
