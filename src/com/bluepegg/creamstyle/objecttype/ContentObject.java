package com.bluepegg.creamstyle.objecttype;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.objects.manager.ResponseManager;
import com.bluepegg.creamstyle.utils.DevLog;

public class ContentObject extends AbstractObject {
	private ResponseManager mResponseManager;
	
	private static final String TAG_LOOK_LIST = "look-list";
	private static final String TAG_ITEM_LIST = "item-list";
	private static final String TAG_ITEM_DETAIL_LIST = "item-detail-list";
	
	private boolean mLookListRefresh;
	private boolean mItemListRefresh;
	private boolean mItemDetailListRefresh;
	
	public ContentObject() {
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mResponseManager = app.getResponseManager();
	}
	
	@Override
	public boolean onResponseListener(String pResponse) {
		try {
			JSONObject json = new JSONObject(pResponse);
			boolean success = json.getBoolean(TAG_SUCCESS);
			
			if(!success) {
				mErrorMessage = json.getString(TAG_MSG);
				mCode = json.getInt(TAG_CODE);
				return false;
			}
			
			JSONObject object = json.getJSONObject(TAG_DATA);
			
			JSONArray lookList = object.getJSONArray(TAG_LOOK_LIST);
			JSONArray itemList = object.getJSONArray(TAG_ITEM_LIST);
			JSONArray itemDetailList = object.getJSONArray(TAG_ITEM_DETAIL_LIST);
			
			if(mResponseManager.getLookListResponse() == null) {
				mResponseManager.setLookListResponse(lookList.toString());
				mLookListRefresh = true;
			} else {
				if(!mResponseManager.getLookListResponse().equals(lookList.toString())) {
					mResponseManager.setLookListResponse(lookList.toString());
					mLookListRefresh = true;
				} else {
					mLookListRefresh = false;
				}
			}
			
			if(mResponseManager.getItemListResponse() == null) {
				mResponseManager.setItemListResponse(itemList.toString());
				mItemListRefresh = true;
			} else {
				if(!mResponseManager.getItemListResponse().equals(itemList.toString())) {
					mResponseManager.setItemListResponse(itemList.toString());
					mItemListRefresh = true;
				} else {
					mItemListRefresh = false;
				}
			}
			
			if(mResponseManager.getItemDetailListResponse() == null) {
				mResponseManager.setItemDetailListResponse(itemDetailList.toString());
				mItemDetailListRefresh = true;
			} else {
				if(!mResponseManager.getItemDetailListResponse().equals(itemDetailList.toString())) {
					mResponseManager.setItemDetailListResponse(itemDetailList.toString());
					mItemDetailListRefresh = true;
				} else {
					mItemDetailListRefresh = false;
				}
			}
			
			/*
			DevLog.defaultLogging(lookList.toString());
			DevLog.defaultLogging(itemList.toString());
			DevLog.defaultLogging(itemDetailList.toString());
			*/
			
			
			
		} catch(JSONException e) {
			e.printStackTrace();
			DevLog.defaultLogging(e.toString());
		}
		
		return true;
	}
	
	public boolean isLookListRefresh() {
		return mLookListRefresh;
	}
	
	public boolean isItemListRefresh() {
		return mItemListRefresh;
	}
	
	public boolean isItemDetailListRefresh() {
		return mItemDetailListRefresh;
	}

}
