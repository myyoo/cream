package com.bluepegg.creamstyle.objecttype;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.utils.DevLog;

public class AccountObject extends AbstractObject {
	private static final String TAG_USER_ID = "user_id";
	private static final String TAG_LOGIN_TOKEN = "login_token";
	private static final String TAG_EMAIL = "email";
	
	private Context mContext;
	private SettingManager mSettingManager;
	
	public AccountObject() {
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mSettingManager = app.getSettingManager();
	}
	
	@Override
	public boolean onResponseListener(String pResponse) {
		
		try {
			JSONObject json = new JSONObject(pResponse);
			boolean success = json.getBoolean(TAG_SUCCESS);
			
			if(!success) {
				mErrorMessage = json.getString(TAG_MSG);
				return false;
			}
			
			mSettingManager.setUid(json.getInt(TAG_USER_ID));
			mSettingManager.setLoginToken(json.getString(TAG_LOGIN_TOKEN));
			mSettingManager.setEmail(json.getString(TAG_EMAIL));
			
		} catch(JSONException e) {
			e.printStackTrace();
			DevLog.defaultLogging(e.toString());
		}
		return true;
	}

}
