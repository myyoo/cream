package com.bluepegg.creamstyle.objecttype;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.objects.manager.ResponseManager;
import com.bluepegg.creamstyle.utils.DevLog;

public class ClosetFeedObject extends AbstractObject {
	private ResponseManager mResponseManager;
	
	private static final String TAG_CLOSET_LOOK_LIST = "closet-look-list";
	private static final String TAG_CLOSET_ITEM_LIST = "closet-item-list";
	private static final String TAG_CLOSET_ITEM_DETAIL_LIST = "closet-item-detail-list";
	
	private boolean mClosetLookListRefresh;
	private boolean mClosetItemListRefresh;
	private boolean mClosetItemDetailListRefresh;
	
	public ClosetFeedObject() {
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mResponseManager = app.getResponseManager();
	}
	
	@Override
	public boolean onResponseListener(String pResponse) {
		try {
			JSONObject json = new JSONObject(pResponse);
			boolean success = json.getBoolean(TAG_SUCCESS);
			
			if(!success) {
				mErrorMessage = json.getString(TAG_MSG);
				mCode = json.getInt(TAG_CODE);
				return false;
			}
			
			JSONObject object = json.getJSONObject(TAG_DATA);
			
			JSONArray closetLookList = object.getJSONArray(TAG_CLOSET_LOOK_LIST);
			JSONArray closetItemList = object.getJSONArray(TAG_CLOSET_ITEM_LIST);
			JSONArray closetItemDetailList = object.getJSONArray(TAG_CLOSET_ITEM_DETAIL_LIST);
			
			if(mResponseManager.getClosetLookListResponse() == null) {
				mResponseManager.setClosetLookListResponse(closetLookList.toString());
				mClosetLookListRefresh = true;
			} else {
				if(!mResponseManager.getClosetLookListResponse().equals(closetLookList.toString())) {
					mResponseManager.setClosetLookListResponse(closetLookList.toString());
					mClosetLookListRefresh = true;
				} else {
					mClosetLookListRefresh = false;
				}
			}
			
			if(mResponseManager.getClosetItemListResponse() == null) {
				mResponseManager.setClosetItemListResponse(closetItemList.toString());
				mClosetItemListRefresh = true;
			} else {
				if(!mResponseManager.getClosetItemListResponse().equals(closetItemList.toString())) {
					mResponseManager.setClosetItemListResponse(closetItemList.toString());
					mClosetItemListRefresh = true;
				} else {
					mClosetItemListRefresh = false;
				}
			}
			
			if(mResponseManager.getClosetItemDetailListResponse() == null) {
				mResponseManager.setClosetItemDetailListResponse(closetItemDetailList.toString());
				mClosetItemDetailListRefresh = true;
			} else {
				if(!mResponseManager.getClosetItemDetailListResponse().equals(closetItemDetailList.toString())) {
					mResponseManager.setClosetItemDetailListResponse(closetItemDetailList.toString());
					mClosetItemDetailListRefresh = true;
				} else {
					mClosetItemDetailListRefresh = false;
				}
			}
			
		} catch(JSONException e) {
			e.printStackTrace();
			DevLog.defaultLogging(e.toString());
		}
		
		return true;
	}
	
	public boolean isClosetLookListRefresh() {
		return mClosetLookListRefresh;
	}
	
	public boolean isClosetItemListRefresh() {
		return mClosetItemListRefresh;
	}
	
	public boolean isClosetItemDetailListRefresh() {
		return mClosetItemDetailListRefresh;
	}

}
