package com.bluepegg.creamstyle.objecttype;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.objects.ClosetItemDetail;
import com.bluepegg.creamstyle.objects.ItemDetail;
import com.bluepegg.creamstyle.objects.manager.ClosetItemDetailListManager;
import com.bluepegg.creamstyle.objects.manager.ItemDetailListManager;
import com.bluepegg.creamstyle.utils.DevLog;

public class ClosetItemDetailListObject extends AbstractObject {
	private static final String TAG_ITEM_DETAIL_ID = "item_detail_id";
	private static final String TAG_THUMB_ITEM_IMAGE = "thumb_item_image";
	private static final String TAG_SMALL_ITEM_IMAGE = "small_item_image";
	private static final String TAG_BIG_ITEM_IMAGE = "big_item_image";
	private static final String TAG_ITEM_WIDTH = "item_width";
	private static final String TAG_ITEM_HEIGHT = "item_height";
	private static final String TAG_COLOR = "color";
	private static final String TAG_PRICE = "price";
	private static final String TAG_ALT_SMALL_IMAGE_LIST = "alt_small_image_list";
	private static final String TAG_ALT_BIG_IMAGE_LIST = "alt_big_image_list";
	private static final String TAG_ALT_THUMB_IMAGE_LIST = "alt_thumb_image_list";
	private static final String TAG_INVENTORY_ID_LIST = "inventory_id_list";
	private static final String TAG_INVENTORY_SIZE_LIST = "inventory_size_list";
	private static final String TAG_INVENTORY_QTY_LIST = "inventory_qty_list";
	private static final String TAG_ITEM_ID = "item_id";
	private static final String TAG_CLOSET_TIME = "closet_time";
	
	private ClosetItemDetailListManager mClosetItemDetailListManager;
	
	public ClosetItemDetailListObject() {
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mClosetItemDetailListManager = app.getClosetItemDetailListManager();
	}
	
	@Override
	public boolean onResponseListener(String pResponse) {
		
		DevLog.Logging("Result", pResponse);
		
		try {
			JSONArray response = new JSONArray(pResponse);
			
			if(response.length() == 0)
				return false;
			
			mClosetItemDetailListManager.clear();
			
			for(int i = 0; i < response.length(); ++i) {
				JSONObject obj = response.getJSONObject(i);
				ClosetItemDetail item = new ClosetItemDetail();
			
				try {
					item.setItemDetailId(obj.getInt(TAG_ITEM_DETAIL_ID));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setThumbItemImage(obj.getString(TAG_THUMB_ITEM_IMAGE));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setSmallItemImage(obj.getString(TAG_SMALL_ITEM_IMAGE));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setBigItemImage(obj.getString(TAG_BIG_ITEM_IMAGE));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setItemWidth(obj.getInt(TAG_ITEM_WIDTH));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setItemHeight(obj.getInt(TAG_ITEM_HEIGHT));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setColor(obj.getString(TAG_COLOR));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setPrice(obj.getString(TAG_PRICE));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setItemId(obj.getInt(TAG_ITEM_ID));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					item.setClosetTime(obj.getLong(TAG_CLOSET_TIME));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				
				JSONArray altSmallImages = obj.getJSONArray(TAG_ALT_SMALL_IMAGE_LIST); 
				List<String> altSmallImageList = new ArrayList<String>();
				for(int n = 0; n < altSmallImages.length(); ++n) {
					String smallImage = (String)altSmallImages.get(n);
					altSmallImageList.add(smallImage);
				}
				item.setAltSmallImageList(altSmallImageList);
				
				JSONArray altBigImages = obj.getJSONArray(TAG_ALT_BIG_IMAGE_LIST);
				List<String> altBigImageList = new ArrayList<String>();
				for(int n = 0; n < altBigImages.length(); ++n) {
					String bigImage = (String)altBigImages.get(n);
					altBigImageList.add(bigImage);
				}
				item.setAltBigImageList(altBigImageList);
				
				JSONArray altThumbImages = obj.getJSONArray(TAG_ALT_THUMB_IMAGE_LIST);
				List<String> altThumbImageList = new ArrayList<String>();
				for(int n = 0; n < altThumbImages.length(); ++n) {
					String thumbImage = (String)altThumbImages.get(n);
					altThumbImageList.add(thumbImage);
				}
				item.setAltThumbImageList(altThumbImageList);
				
				JSONArray inventoryIds = obj.getJSONArray(TAG_INVENTORY_ID_LIST);
				List<Integer> inventoryIdList = new ArrayList<Integer>();
				for(int n = 0; n < inventoryIds.length(); ++n) {
					Integer inventoryId = (Integer)inventoryIds.get(n);
					inventoryIdList.add(inventoryId);
				}
				item.setInventoryIdList(inventoryIdList);
				
				JSONArray inventorySizes = obj.getJSONArray(TAG_INVENTORY_SIZE_LIST);
				List<String> inventorySizeList = new ArrayList<String>();
				for(int n = 0; n < inventorySizes.length(); ++n) {
					String inventorySize = (String)inventorySizes.get(n);
					inventorySizeList.add(inventorySize);
				}
				item.setInventorySizeList(inventorySizeList);
				
				JSONArray inventoryQty = obj.getJSONArray(TAG_INVENTORY_QTY_LIST);
				List<Integer> inventoryQtyList = new ArrayList<Integer>();
				for(int n = 0; n < inventoryQty.length(); ++n) {
					Integer qty = (Integer)inventoryQty.get(n);
					inventoryQtyList.add(qty);
				}
				item.setInventoryQtyList(inventoryQtyList);
				
				mClosetItemDetailListManager.putItemDetail(obj.getInt(TAG_ITEM_DETAIL_ID), item);
			}
			
		} catch(JSONException e) {
			e.printStackTrace();
		}
		
		return true;
	}

}
