package com.bluepegg.creamstyle.objecttype;

import org.json.JSONException;
import org.json.JSONObject;

import com.bluepegg.creamstyle.utils.DevLog;

public class ChargeObject extends AbstractObject {
	
	@Override
	public boolean onResponseListener(String pResponse) {
		try {
			JSONObject json = new JSONObject(pResponse);
			boolean success = json.getBoolean(TAG_SUCCESS);
			
			if(!success) {
				mErrorMessage = json.getString(TAG_MSG);
				return false;
			}
			
			
		} catch(JSONException e) {
			e.printStackTrace();
			DevLog.defaultLogging(e.toString());
		}
		return true;
	}

}
