package com.bluepegg.creamstyle.objects;

import java.io.Serializable;
import java.util.List;

public class ItemDetail implements Serializable {
	
	protected int mItemDetailId;
	protected String mThumbItemImage;
	protected String mSmallItemImage;
	protected String mBigItemImage;
	protected int mItemWidth;
	protected int mItemHeight;
	protected String mColor;
	protected String mPrice;
	protected boolean mCloset;
	
	protected List<String> mAltSmallImageList;
	protected List<String> mAltThumbImageList;
	protected List<String> mAltBigImageList;
	protected List<Integer> mInventoryIdList;
	protected List<String> mInventorySizeList;
	protected List<Integer> mInventoryQtyList;
	
	public void setCloset(boolean pCloset) {
		mCloset = pCloset;
	}
	
	public boolean getCloset() {
		return mCloset;
	}
	
	public void setAltSmallImageList(List<String> pAltSmallImageList) {
		mAltSmallImageList = pAltSmallImageList;
	}
	
	public List<String> getAltSmallImageList() {
		return mAltSmallImageList;
	}
	
	public void setAltThumbImageList(List<String> pAltThumbImageList) {
		mAltThumbImageList = pAltThumbImageList;
	}
	
	public List<String> getAltThumbImageList() {
		return mAltThumbImageList;
	}
	
	public void setInventoryIdList(List<Integer> pInventoryIdList) {
		mInventoryIdList = pInventoryIdList;
	}
	
	public List<Integer> getInventoryIdList() {
		return mInventoryIdList;
	}
	
	public void setInventorySizeList(List<String> pInventorySizeList) {
		mInventorySizeList = pInventorySizeList;
	}
	
	public List<String> getInventorySizeList() {
		return mInventorySizeList;
	}
	
	public void setInventoryQtyList(List<Integer> pInventoryQtyList) {
		mInventoryQtyList = pInventoryQtyList;
	}
	
	public List<Integer> getInventoryQtyList() {
		return mInventoryQtyList;
	}
	
	public void setAltBigImageList(List<String> pAltBigImageList) {
		mAltBigImageList = pAltBigImageList;
	}
	
	public List<String> getAltBigImageList() {
		return mAltBigImageList;
	}
	
	public void setItemDetailId(int pItemDetailId) {
		mItemDetailId = pItemDetailId;
	}
	
	public int getItemDetailId() {
		return mItemDetailId;
	}
	
	public void setThumbItemImage(String pThumbItemImage) {
		mThumbItemImage = pThumbItemImage;
	}
	
	public String getThumbItemImage() {
		return mThumbItemImage;
	}
	
	public void setSmallItemImage(String pSmallItemImage) {
		mSmallItemImage = pSmallItemImage;
	}
	
	public String getSmallItemImage() {
		return mSmallItemImage;
	}
	
	public void setBigItemImage(String pBigItemImage) {
		mBigItemImage = pBigItemImage;
	}
	
	public String getBigItemImage() {
		return mBigItemImage;
	}
	
	public void setItemWidth(int pItemWidth) {
		mItemWidth = pItemWidth;
	}
	
	public int getItemWidth() {
		return mItemWidth;
	}
	
	public void setItemHeight(int pItemHeight) {
		mItemHeight = pItemHeight;
	}
	
	public int getItemHeight() {
		return mItemHeight;
	}
	
	public void setColor(String pColor) {
		mColor = pColor;
	}
	
	public String getColor() {
		return mColor;
	}
	
	public void setPrice(String pPrice) {
		mPrice = pPrice;
	}
	
	public String getPrice() {
		return mPrice;
	}
}
