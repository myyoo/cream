package com.bluepegg.creamstyle.objects;

public class PaymentTotal {
	private String mSubTotal;
	private String mShipping;
	private String mDiscount;
	private String mTax;
	private String mTotal;
	
	public void setSubTotal(String pSubTotal) {
		mSubTotal = pSubTotal;
	}
	
	public String getSubTotal() {
		return mSubTotal;
	}
	
	public void setShipping(String pShipping) {
		mShipping = pShipping;
	}
	
	public String getShipping() {
		return mShipping;
	}
	
	public void setDiscount(String pDiscount) {
		mDiscount = pDiscount;
	}
	
	public String getDiscount() {
		return mDiscount;
	}
	
	public void setTax(String pTax) {
		mTax = pTax;
	}
	
	public String getTax() {
		return mTax;
	}
	
	public void setTotal(String pTotal) {
		mTotal = pTotal;
	}
	
	public String getTotal() {
		return mTotal;
	}
}
