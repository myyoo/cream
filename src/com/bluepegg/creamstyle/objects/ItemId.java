package com.bluepegg.creamstyle.objects;

import java.util.List;

public class ItemId {
	
	private List<ItemDetail> mItemDetailIds;
	private int mItemId;
	private int mSelectedPosition;
	private int mCurrentPosition;
	
	public void setSelectedPosition(int pSelectedPosition) {
		mSelectedPosition = pSelectedPosition;
	}
	
	public int getSelectedPosition() {
		return mSelectedPosition;
	}
	
	public void setCurrentPosition(int pCurrentPosition) {
		mCurrentPosition = pCurrentPosition;
	}
	
	public int getCurrentPosition() {
		return mCurrentPosition;
	}
	
	public void setItemId(int pItemId) {
		mItemId = pItemId;
	}
	
	public int getItemId() {
		return mItemId;
	}
	
	public void setItemDetailId(List<ItemDetail> pItemDetailIds) {
		mItemDetailIds = pItemDetailIds;
	}
	
	public List<ItemDetail> getItemDetailId() {
		return mItemDetailIds;
	}
	
}
