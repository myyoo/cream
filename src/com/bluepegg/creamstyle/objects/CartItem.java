package com.bluepegg.creamstyle.objects;

public class CartItem {
	private int mInventoryId;
	private String mThumbImageUrl;
	private String mItemBrand;
	private String mItemName;
	private String mSize;
	private String mColor;
	private int mQuantity;
	private String mPrice;
	private int mItemId;
	private int mItemDetailId;
	
	public void setItemDetailId(int pItemDetailId) {
		mItemDetailId = pItemDetailId;
	}
	
	public int getItemDetailId() {
		return mItemDetailId;
	}
	
	public void setItemId(int pItemId) {
		mItemId = pItemId;
	}
	
	public int getItemId() {
		return mItemId;
	}
	
	public void setInventoryId(int pInventoryId) {
		mInventoryId = pInventoryId;
	}
	
	public int getInventoryId() {
		return mInventoryId;
	}
	
	public void setThumbImageUrl(String pThumbImageUrl) {
		mThumbImageUrl = pThumbImageUrl;
	}
	
	public String getThumbImageUrl() {
		return mThumbImageUrl;
	}
	
	public void setItemBrand(String pItemBrand) {
		mItemBrand = pItemBrand;
	}
	
	public String getItemBrand() {
		return mItemBrand;
	}
	
	public void setItemName(String pItemName) {
		mItemName = pItemName;
	}
	
	public String getItemName() {
		return mItemName;
	}
	
	public void setSize(String pSize) {
		mSize = pSize;
	}
	
	public String getSize() {
		return mSize;
	}
	
	public void setColor(String pColor) {
		mColor = pColor;
	}
	
	public String getColor() {
		return mColor;
	}
	
	public void setQuantity(int pQuantity) {
		mQuantity = pQuantity;
	}
	
	public int getQuantity() {
		return mQuantity;
	}
	
	public void setPrice(String pPrice) {
		mPrice = pPrice;
	}
	
	public String getPrice() {
		return mPrice;
	}
}
