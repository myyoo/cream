package com.bluepegg.creamstyle.objects;

public class FragmentEvent {
	private FeedItemList mFeedList;
	private DetailFeedItem mDetailFeedItem;
	private FBShareItem mFBShareItem;
	private int mKey;
	private int mPosition;
	private boolean mIsCloset;
	
	public FragmentEvent(FeedItemList pFeedList) {
		mFeedList = pFeedList;
	}
	
	public FragmentEvent(DetailFeedItem pDetailFeedItem) {
		mDetailFeedItem = pDetailFeedItem;
	}
	
	public FragmentEvent(FBShareItem pFBShareItem) {
		mFBShareItem = pFBShareItem;
	}
	
	public FeedItemList getFeedItemList() {
		return mFeedList;
	}
	
	public DetailFeedItem getDetailFeedItem() {
		return mDetailFeedItem;
	}
	
	public FBShareItem getHomePageItem() {
		return mFBShareItem;
	}
	
	
	
	public FragmentEvent(int pKey, int pPosition, boolean pIsCloset) {
		mKey = pKey;
		mPosition = pPosition;
		mIsCloset = pIsCloset;
	}
	
	public int getKey() {
		return mKey;
	}
	
	public int getPosition() {
		return mPosition;
	}
	
	public boolean isCloset() {
		return mIsCloset;
	}
	
}
