package com.bluepegg.creamstyle.objects;

import java.io.Serializable;
import java.util.List;

public class ClosetItemDetail extends ItemDetail implements Serializable {
	
	private int mItemId;
	private long mClosetTime = 0;
	
	public void setItemId(int pItemId) {
		mItemId = pItemId;
	}
	
	public int getItemId() {
		return mItemId;
	}
	
	public void setClosetTime(long pClosetTime) {
		mClosetTime = pClosetTime;
	}
	
	public long getClosetTime() {
		return mClosetTime;
	}
	
	
}
