package com.bluepegg.creamstyle.objects;

import java.util.Comparator;

public class ClosetFeedCompare implements Comparator<ClosetFeed> {

	@Override
	public int compare(ClosetFeed lhs, ClosetFeed rhs) {
		return String.valueOf(rhs.getClosetTime()).compareTo(String.valueOf(lhs.getClosetTime()));
	}

}
