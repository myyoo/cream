package com.bluepegg.creamstyle.objects;

import java.util.Comparator;

public class SizeAscCompare implements Comparator<SizeItem> {

	@Override
	public int compare(SizeItem lhs, SizeItem rhs) {
		return lhs.getSize().compareTo(rhs.getSize());
	}

}
