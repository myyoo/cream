package com.bluepegg.creamstyle.objects;

import java.io.Serializable;
import java.util.List;

public class ClosetLookItem extends LookItem implements Serializable {
	
	private long mClosetTime;
	
	public void setClosetTime(long pClosetTime) {
		mClosetTime = pClosetTime;
	}
	
	public long getClosetTime() {
		return mClosetTime;
	}
	
}
