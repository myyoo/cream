package com.bluepegg.creamstyle.objects;

import java.io.Serializable;


public class HomePage implements Serializable {
	private int mLookId;
	private String mSmallLookImage;
	private String mThumbLookImage;
	private String mBigLookImage;
	private int mStockQuantity;
	private String mLookName;
	private String mDisplayName;
	private int mWidth;
	private int mHeight;
	private int mLookCloset;
	private boolean mLogoSection;
	private int mPosition;
	
	public void setPosition(int pPosition) {
		mPosition = pPosition;
	}
	
	public int getPosition() {
		return mPosition;
	}
	
	public void setLogoSection(boolean pLogoSection) {
		mLogoSection = pLogoSection;
	}
	
	public boolean getLogoSection() {
		return mLogoSection;
	}
	
	public void setLookId(int pLookId) {
		mLookId = pLookId;
	}
	
	public int getLookId() {
		return mLookId;
	}
	
	public void setSmallLookImage(String pSmallLookImage) {
		mSmallLookImage = pSmallLookImage;
	}
	
	public String getSmallLookImage() {
		return mSmallLookImage;
	}
	
	public void setThumbLookImage(String pThumbLookImage) {
		mThumbLookImage = pThumbLookImage;
	}
	
	public String getThumbLookImage() {
		return mThumbLookImage;
	}
	
	public void setBigLookImage(String pBigLookImage) {
		mBigLookImage = pBigLookImage;
	}
	
	public String getBigLookImage() {
		return mBigLookImage;
	}
	
	public void setStockQuantity(int pStockQuantity) {
		mStockQuantity = pStockQuantity;
	}
	
	public int getStockQuantity() {
		return mStockQuantity;
	}
	
	public void setLookName(String pLookName) {
		mLookName = pLookName;
	}
	
	public String getLookName() {
		return mLookName;
	}
	
	public void setDisplayName(String pDisplayName) {
		mDisplayName = pDisplayName;
	}
	
	public String getDisplayName() {
		return mDisplayName;
	}
	
	public void setLookWidth(int pWidth) {
		mWidth = pWidth;
	}
	
	public int getLookWidth() {
		return mWidth;
	}
	
	public void setLookHeight(int pHeight) {
		mHeight = pHeight;
	}
	
	public int getLookHeight() {
		return mHeight;
	}
	
	
	public void setLookCloset(int pLookCloset) {
		mLookCloset = pLookCloset;
	}
	
	public int getLookCloset() {
		return mLookCloset;
	}
}
