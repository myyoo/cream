package com.bluepegg.creamstyle.objects;

import java.util.Comparator;

public class SizeDescCompare implements Comparator<SizeItem> {

	@Override
	public int compare(SizeItem lhs, SizeItem rhs) {
		return rhs.getSize().compareTo(lhs.getSize());
	}

}
