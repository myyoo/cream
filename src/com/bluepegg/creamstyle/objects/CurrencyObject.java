package com.bluepegg.creamstyle.objects;

import org.json.JSONException;
import org.json.JSONObject;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.objecttype.AbstractObject;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.utils.DevLog;

public class CurrencyObject extends AbstractObject {
	private static final String TAG_RATE = "rate";
	private SettingManager mSettingManager;
	
	public CurrencyObject() {
		mSettingManager = CreamStyleApplication.getApplication().getSettingManager();
	}
	
	@Override
	public boolean onResponseListener(String pResponse) {
		try {
			JSONObject json = new JSONObject(pResponse);
			boolean success = json.getBoolean(TAG_SUCCESS);
			
			if(!success) {
				mErrorMessage = json.getString(TAG_MSG);
				mCode = json.getInt(TAG_CODE);
				return false;
			}
			
			try {
				mSettingManager.setCurrencyRate(json.getString(TAG_RATE));
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			
		} catch(JSONException e) {
			e.printStackTrace();
			DevLog.defaultLogging(e.toString());
			return false;
		}
		
		return true;
	}

}
