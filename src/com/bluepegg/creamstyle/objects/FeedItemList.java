package com.bluepegg.creamstyle.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FeedItemList implements Serializable {
	private List<HomePage> mFeedItems = new ArrayList<HomePage>();
	private int mCurrentIndex = 0;
	private int mLookId;
	private int mDetailId;
	private int mItemId;
	private ClosetFeed mClosetFeed;
	private Map<Integer, ItemId> mItemIds;
	private int mItemIdKey;
	
	
	public void setItemIds(Map<Integer, ItemId> pItemIds) {
		mItemIds = pItemIds;
	}
	
	public Map<Integer, ItemId> getItemIds() {
		return mItemIds;
	}
	
	public void setItemIdKey(int pItemIdKey) {
		mItemIdKey = pItemIdKey;
	}
	
	public int getItemIdKey() {
		return mItemIdKey;
	}
	
	public List<HomePage> getFeedItems() {
		return mFeedItems;
	}
	
	public void setClosetFeed(ClosetFeed pClosetFeed) {
		mClosetFeed = pClosetFeed;
	}
	
	public ClosetFeed getClosetFeed() {
		return mClosetFeed;
	}
	
	public void setLookId(int pLookId) {
		mLookId = pLookId;
	}
	
	public int getLookId() {
		return mLookId;
	}
	
	public void setDetailId(int pDetailId) {
		mDetailId = pDetailId;
	}
	
	public int getDetailId() {
		return mDetailId;
	}
	
	public void setItemId(int pItemId) {
		mItemId = pItemId;
	}
	
	public int getItemId() {
		return mItemId;
	}
	
	public void addFeedItem(HomePage pItem) {
		mFeedItems.add(pItem);
	}
	
	public void addAllFeedItem(List<HomePage> pItems) {
		mFeedItems.addAll(pItems);
	}
	
	public void clear() {
		mCurrentIndex = 0;
		mFeedItems.clear();
	}
	
	public int getFeedItemCount() {
		return mFeedItems.size();
	}
	
	public int getLastIndex() {
		return mFeedItems.size() - 1;
	}
	
	public HomePage get(int pIndex) {
		if(pIndex < getFeedItemCount())
			return mFeedItems.get(pIndex);
		
		return null;
	}
	
	public int getCurrentIndex() {
		return mCurrentIndex;
	}
	
	public HomePage getCurrentFeedItem() {
		if(mCurrentIndex < getFeedItemCount())
			return mFeedItems.get(mCurrentIndex);
		
		return null;
	}
	
	public HomePage next() {
		if(mCurrentIndex + 1 < getFeedItemCount()) {
			mCurrentIndex++;
			
			return mFeedItems.get(mCurrentIndex);
		}
		
		return null;
	}
	
	public HomePage prev() {
		if(mCurrentIndex >= 1) {
			mCurrentIndex--;
			
			return mFeedItems.get(mCurrentIndex);
		}
		
		return null;
	}
	
	public HomePage getSelectedPosition(int pPosition) {
		return mFeedItems.get(pPosition);
	}
	
	public void setCurrentIndex(int pCurrentIndex) {
		mCurrentIndex = pCurrentIndex;
	}
	
}
