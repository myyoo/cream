package com.bluepegg.creamstyle.objects;

import java.io.Serializable;

public class DetailFeedItem implements Serializable {
	private int mLookId;
	private int mItemId;
	private int mDetailId;
	private int mPosition;
	private int mInventoryId;
	private String mImageUrl;
	private boolean mIsCloset;
	private boolean mVerticalScrollBlock;
	
	public void setVerticalScrollBlock(boolean pVerticalScrollBlock) {
		mVerticalScrollBlock = pVerticalScrollBlock;
	}
	
	public boolean getVerticalScrollBlock() {
		return mVerticalScrollBlock;
	}
	
	public void setIsCloset(boolean pIsCloset) {
		mIsCloset = pIsCloset;
	}
	
	public boolean getIsCloset() {
		return mIsCloset;
	}
	
	public void setImageUrl(String pImageUrl) {
		mImageUrl = pImageUrl;
	}
	
	public String getImageUrl() {
		return mImageUrl;
	}
	
	public void setInventoryId(int pInventoryId) {
		mInventoryId = pInventoryId;
	}
	
	public int getInventoryId() {
		return mInventoryId;
	}
	
	public void setLookId(int pLookId) {
		mLookId = pLookId;
	}
	
	public int getLookId() {
		return mLookId;
	}
	
	public void setItemId(int pItemId) {
		mItemId = pItemId;
	}
	
	public int getItemId() {
		return mItemId;
	}
	
	public void setDetailId(int pDetailId) {
		mDetailId = pDetailId;
	}
	
	public int getDetailId() {
		return mDetailId;
	}
	
	public void setPosition(int pPosition) {
		mPosition = pPosition;
	}
	
	public int getPosition() {
		return mPosition;
	}
}
