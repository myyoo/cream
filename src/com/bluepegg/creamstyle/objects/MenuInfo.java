package com.bluepegg.creamstyle.objects;

public class MenuInfo {
	private String mMenuItem;
	private int mType;
	private int mDrawable;
	
	public MenuInfo(String pMenuItem, int pType, int pDrawable) {
		mMenuItem = pMenuItem;
		mType = pType;
		mDrawable = pDrawable;
	}
	
	public String getMenuItem() {
		return mMenuItem;
	}
	
	public int getType() {
		return mType;
	}
	
	public int getDrawableIcon() {
		return mDrawable;
	}
}
