package com.bluepegg.creamstyle.objects;

import java.io.Serializable;
import java.util.List;

public class ItemList implements Serializable {

	private int mItemId;
	private String mItemDetail;
	private String mItemBrand;
	private String mItemName;
	private boolean mCloset;
	
	private List<Integer> mItemDetailIdList;
	
	public void setCloset(boolean pCloset) {
		mCloset = pCloset;
	}
	
	public boolean getCloset() {
		return mCloset;
	}
	
	public void setItemId(int pItemId) {
		mItemId = pItemId;
	}
	
	public int getItemId() {
		return mItemId;
	}
	
	public void setItemDetail(String pItemDetail) {
		mItemDetail = pItemDetail;
	}
	
	public String getItemDetail() {
		return mItemDetail;
	}
	
	public void setItemBrand(String pItemBrand) {
		mItemBrand = pItemBrand;
	}
	
	public String getItemBrand() {
		return mItemBrand;
	}
	
	public void setItemName(String pItemName) {
		mItemName = pItemName;
	}
	
	public String getItemName() {
		return mItemName;
	}
	
	public void setItemDetailIdList(List<Integer> pItemDetailIdList) {
		mItemDetailIdList = pItemDetailIdList;
	}
	
	public List<Integer> getItemDetailIdList() {
		return mItemDetailIdList;
	}
}
