package com.bluepegg.creamstyle.objects;

public class FBShareItem {
	private String mDescription;
	private String mItemName;
	private String mItemDetailName;
	private String mPictureUrl;
	private boolean mType;
	private int mId;
	
	public void setId(int pId) {
		mId = pId;
	}
	
	public int getId() {
		return mId;
	}
	
	public void setType(boolean pType) {
		mType = pType;
	}
	
	public boolean getType() {
		return mType;
	}
	
	public void setDescription(String pDescription) {
		mDescription = pDescription;
	}
	
	public String getDescription() {
		return mDescription;
	}
	
	public void setItemName(String pItemName) {
		mItemName = pItemName;
	}
	
	public String getItemName() {
		return mItemName;
	}
	
	public void setItemDetailName(String pItemDetailName) {
		mItemDetailName = pItemDetailName;
	}
	
	public String getItemDetailName() {
		return mItemDetailName;
	}
	
	public void setPictureUrl(String pPictureUrl) {
		mPictureUrl = pPictureUrl;
	}
	
	public String getPictureUrl() {
		return mPictureUrl;
	}
}
