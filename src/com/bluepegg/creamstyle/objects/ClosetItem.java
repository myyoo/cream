package com.bluepegg.creamstyle.objects;


public class ClosetItem {
	private int mLookId;
	private String mSmallLookImage;
	private String mBigLookImage;
	private int mLookWidth;
	private int mLookHeight;
	private String mItemName;
	private String mItemBrand;
	private String mItemDetail;
	private int mItemId;
	private int mItemDetailId;
	private String mBigItemImage;
	private String mSmallItemImage;
	private String mClosetTime;
	private String mCloset;
	private String mPrice;
	private String mColor;
	private int mShoppable;
	private int mItemHeight;
	private int mItemWidth;
	private int mInventoryId;
	private int mStockQuantity;
	private String mSize;
	private String mSku;
	private int mClosetType;
	private int mPosition;
	private String mLookName;
	private String mDisplayName;
	private String mThumbItemImageUrl;
	private int mLookLink;
	private int mFeedMask;
	
	
	public void setFeedMask(int pFeedMask) {
		mFeedMask = pFeedMask;
	}
	
	public int getFeedMask() {
		return mFeedMask;
	}
	
	public void setLookLink(int pLookLink) {
		mLookLink = pLookLink;
	}
	
	public int getLookLink() {
		return mLookLink;
	}
	
	public void setThumbItemImageUrl(String pThumbItemImageUrl) {
		mThumbItemImageUrl = pThumbItemImageUrl;
	}
	
	public String getThumbItemImageUrl() {
		return mThumbItemImageUrl;
	}
	
	public void setDisplayName(String pDisplayName) {
		mDisplayName = pDisplayName;
	}
	
	public String getDisplayName() {
		return mDisplayName;
	}
	
	public void setLookName(String pLookName) {
		mLookName = pLookName;
	}
	
	public String getLookName() {
		return mLookName;
	}
	
	public void setPosition(int pPosition) {
		mPosition = pPosition;
	}
	
	public int getPosition() {
		return mPosition;
	}
	
	public void setCloset(String pCloset) {
		mCloset = pCloset;
	}
	
	public String getCloset() {
		return mCloset;
	}
	
	public void setClosetType(int pClosetType) {
		mClosetType = pClosetType;
	}
	
	public int getClosetType() {
		return mClosetType;
	}
	
	public void setLookId(int pLookId) {
		mLookId = pLookId;
	}
	
	public int getLookId() {
		return mLookId;
	}
	
	public void setSmallLookImage(String pSmallLookImage) {
		mSmallLookImage = pSmallLookImage;
	}
	
	public String getSmallLookImage() {
		return mSmallLookImage;
	}
	
	public void setBigLookImage(String pBigLookImage) {
		mBigLookImage = pBigLookImage;
	}
	
	public String getBiglLookImage() {
		return mBigLookImage;
	}
	
	public void setLookWidth(int pLookWidth) {
		mLookWidth = pLookWidth;
	}
	
	public int getLookWidth() {
		return mLookWidth;
	}
	
	public void setLookHeight(int pLookHeight) {
		mLookHeight = pLookHeight;
	}
	
	public int getLookHeight() {
		return mLookHeight;
	}
	
	public void setSku(String pSku) {
		mSku = pSku;
	}
	
	public String getSku() {
		return mSku;
	}
	
	public void setSize(String pSize) {
		mSize = pSize;
	}
	
	public String getSize() {
		return mSize;
	}
	
	public void setStockQuantity(int pStockQuantity) {
		mStockQuantity = pStockQuantity;
	}
	
	public int getStockQuantity() {
		return mStockQuantity;
	}
	
	public void setInventoryId(int pInventoryId) {
		mInventoryId = pInventoryId;
	}
	
	public int getInventoryId() {
		return mInventoryId;
	}
	
	public void setItemWidth(int pItemWidth) {
		mItemWidth = pItemWidth;
	}
	
	public int getItemWidth() {
		return mItemWidth;
	}
	
	public void setItemHeight(int pItemHeight) {
		mItemHeight = pItemHeight;
	}
	
	public int getItemHeight() {
		return mItemHeight;
	}
	
	public void setShoppable(int pShoppable) {
		mShoppable = pShoppable;
	}
	
	public int getShoppable(int pShoppable) {
		return mShoppable;
	}
	
	public void setColor(String pColor) {
		mColor = pColor;
	}
	
	public String getColor() {
		return mColor;
	}
	
	public void setPrice(String pPrice) {
		mPrice = pPrice;
	}
	
	public String getPrice() {
		return mPrice;
	}
	
	
	public void setSmallItemImage(String pSmallItemImage) {
		mSmallItemImage = pSmallItemImage;
	}
	
	public String getSmallItemImage() {
		return mSmallItemImage;
	}
	
	public void setBigItemImage(String pBigItemImage) {
		mBigItemImage = pBigItemImage;
	}
	
	public String getBigItemImage() {
		return mBigItemImage;
	}
	
	public void setClosetTime(String pClosetTime) {
		mClosetTime = pClosetTime;
	}
	
	public String getClosetTime() {
		return mClosetTime;
	}
	
	public void setItemName(String pItemName) {
		mItemName = pItemName;
	}
	
	public String getItemName() {
		return mItemName;
	}
	
	public void setItemBrand(String pItemBrand) {
		mItemBrand = pItemBrand;
	}
	
	public String getItemBrand() {
		return mItemBrand;
	}
	
	public void setItemDetail(String pItemDetail) {
		mItemDetail = pItemDetail;
	}
	
	public String getItemDetail() {
		return mItemDetail;
	}
	
	public void setItemId(int pItemId) {
		mItemId = pItemId;
	}
	
	public int getItemId() {
		return mItemId;
	}
	
	public void setItemDetailId(int pItemDetailId) {
		mItemDetailId = pItemDetailId;
	}
	
	public int getItemDetailId() {
		return mItemDetailId;
	}
}
