package com.bluepegg.creamstyle.objects;

public class SizeItem {
	private String mSize;
	private int mInventoryId;
	
	public void setSize(String pSize) {
		mSize = pSize;
	}
	
	public String getSize() {
		return mSize;
	}
	
	public void setInventoryId(int pInventoryId) {
		mInventoryId = pInventoryId;
	}
	
	public int getInventoryId() {
		return mInventoryId;
	}
}
