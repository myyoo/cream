package com.bluepegg.creamstyle.objects;

import java.util.List;

public class ClosetFeed {
	private int mId;
	private int mDetailId;
	private boolean mClosetLook;
	private String mSmallImage;
	private int mImageHeight;
	private int mImageWidth;
	private long mClosetTime;
	private int mFeedMask;
	private List<Integer> mInventoryId;
	private List<String> mInventorySize;
	
	public void setInventorySize(List<String> pInventorySize) {
		mInventorySize = pInventorySize;
	}
	
	public List<String> getInventorySize() {
		return mInventorySize;
	}
	
	public void setInventoryId(List<Integer> pInventoryId) {
		mInventoryId = pInventoryId;
	}
	
	public List<Integer> getInventoryId() {
		return mInventoryId;
	}
	
	public void setFeedMask(int pFeedMask) {
		mFeedMask = pFeedMask;
	}
	
	public int getFeedMask() {
		return mFeedMask;
	}
	
	public void setId(int pId) {
		mId = pId;
	}
	
	public int getId() {
		return mId;
	}
	
	public void setDetailId(int pDetailId) {
		mDetailId = pDetailId;
	}
	
	public int getDetailId() {
		return mDetailId;
	}
	
	public void setClosetLook(boolean pClosetLook) {
		mClosetLook = pClosetLook;
	}
	
	public boolean getClosetLook() {
		return mClosetLook;
	}
	
	public void setSmallImage(String pSmallImage) {
		mSmallImage = pSmallImage;
	}
	
	public String getSmallImage() {
		return mSmallImage;
	}
	
	public void setImageHeight(int pImageHeight) {
		mImageHeight = pImageHeight;
	}
	
	public int getImageHeight() {
		return mImageHeight;
	}
	
	public void setImageWidth(int pImageWidth) {
		mImageWidth = pImageWidth;
	}
	
	public int getImageWidth() {
		return mImageWidth;
	}
	
	public void setClosetTime(long pClosetTime) {
		mClosetTime = pClosetTime;
	}
	
	public long getClosetTime() {
		return mClosetTime;
	}
}
