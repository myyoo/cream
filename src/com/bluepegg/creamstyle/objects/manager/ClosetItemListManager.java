package com.bluepegg.creamstyle.objects.manager;

import java.util.HashMap;

import com.bluepegg.creamstyle.objects.ItemList;

public class ClosetItemListManager {
	
	private HashMap<Integer, ItemList> mClosetItemList;
	
	public ClosetItemListManager() {
		mClosetItemList = new HashMap<Integer, ItemList>();
	}
	
	public HashMap<Integer, ItemList> getItemList() {
		return mClosetItemList;
	}
	
	public void putItem(int pKey, ItemList pClosetItemList) {
		mClosetItemList.put(pKey, pClosetItemList);
	}
	
	public ItemList getItem(int pKey) {
		return mClosetItemList.get(pKey);
	}
	
	public void clear() {
		mClosetItemList.clear();
	}
}
