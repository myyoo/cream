package com.bluepegg.creamstyle.objects.manager;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.bluepegg.creamstyle.objects.LookItem;

public class LookItemManager {
	
	private LinkedHashMap<Integer, LookItem> mLookItems;
	
	public LookItemManager() {
		mLookItems = new LinkedHashMap<Integer,LookItem>();
	}
	
	public HashMap<Integer, LookItem> getLookItems() {
		return mLookItems;
	}
	
	public void putItem(int pKey, LookItem pItem) {
		mLookItems.put(pKey, pItem);
	}
	
	public LookItem getItem(int pKey) {
		return mLookItems.get(pKey);
	}
	
	public void clear() {
		mLookItems.clear();
	}
	
	
	public int getPosition(int pKey) {
		int position = -1;
		int count = 0;
		for(Map.Entry<Integer, LookItem> item : mLookItems.entrySet()) {
			if(item.getKey() == pKey) {
				position = count;
				break;
			}
			
			count++;
		}
		
		return position;
	}
}
