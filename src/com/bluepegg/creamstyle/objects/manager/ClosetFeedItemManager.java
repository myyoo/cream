package com.bluepegg.creamstyle.objects.manager;

import java.util.HashMap;

import com.bluepegg.creamstyle.objects.ClosetFeed;

public class ClosetFeedItemManager {
	
	private HashMap<Integer, ClosetFeed> mClosetFeeds;
	
	public ClosetFeedItemManager() {
		mClosetFeeds = new HashMap<Integer, ClosetFeed>();
	}
	
	public HashMap<Integer, ClosetFeed> getClosetFeeds() {
		return mClosetFeeds;
	}
	
	public void putItem(int pKey, ClosetFeed pItem) {
		mClosetFeeds.put(pKey, pItem);
	}
	
	public ClosetFeed getItem(int pKey) {
		return mClosetFeeds.get(pKey);
	}
	
	public void clear() {
		mClosetFeeds.clear();
	}
	
}
