package com.bluepegg.creamstyle.objects.manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.bluepegg.creamstyle.objects.ClosetLookItem;
import com.bluepegg.creamstyle.objects.LookItem;

public class ClosetLookItemManager {
	
	private LinkedHashMap<Integer, LookItem> mClosetLookItems;
	
	public ClosetLookItemManager() {
		mClosetLookItems = new LinkedHashMap<Integer,LookItem>();
	}
	
	public LinkedHashMap<Integer, LookItem> getLookItems() {
		return mClosetLookItems;
	}
	
	public void putItem(int pKey, ClosetLookItem pItem) {
		mClosetLookItems.put(pKey, pItem);
	}
	
	public LookItem getItem(int pKey) {
		return mClosetLookItems.get(pKey);
	}
	
	public void clear() {
		mClosetLookItems.clear();
	}
	
	
	public int getPosition(int pKey) {
		int position = -1;
		int count = 0;
		List<LookItem> keyList = new ArrayList<LookItem>(mClosetLookItems.values());
		Collections.sort(keyList, LookItem.closetComparator);
		
		for(LookItem item : keyList) {
			if(item.getLookId() == pKey) {
				position = count;
				break;
			}
			
			count++;
		}
		
		return position;
	}
}
