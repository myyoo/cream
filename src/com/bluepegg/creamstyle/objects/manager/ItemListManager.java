package com.bluepegg.creamstyle.objects.manager;

import java.util.LinkedHashMap;

import com.bluepegg.creamstyle.objects.ItemList;

public class ItemListManager {
	
	private LinkedHashMap<Integer, ItemList> mItemList;
	
	public ItemListManager() {
		mItemList = new LinkedHashMap<Integer, ItemList>();
	}
	
	public LinkedHashMap<Integer, ItemList> getItemList() {
		return mItemList;
	}
	
	public void putItem(int pKey, ItemList pItemList) {
		mItemList.put(pKey, pItemList);
	}
	
	public ItemList getItem(int pKey) {
		return mItemList.get(pKey);
	}
	
	public void clear() {
		mItemList.clear();
	}
}
