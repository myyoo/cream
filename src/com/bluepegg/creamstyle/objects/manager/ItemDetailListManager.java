package com.bluepegg.creamstyle.objects.manager;

import java.util.LinkedHashMap;

import com.bluepegg.creamstyle.objects.ItemDetail;

public class ItemDetailListManager {

	private LinkedHashMap<Integer, ItemDetail> mItemDetails;
	
	public ItemDetailListManager() {
		mItemDetails = new LinkedHashMap<Integer, ItemDetail>();
	}
	
	public LinkedHashMap<Integer, ItemDetail> getItemDetails() {
		return mItemDetails;
	}
	
	public void putItemDetail(int pKey, ItemDetail pItemDetail) {
		mItemDetails.put(pKey, pItemDetail);
	}
	
	public ItemDetail getItemDetail(int pKey) {
		return mItemDetails.get(pKey);
	}
	
	public void clear() {
		mItemDetails.clear();
	}
}
