package com.bluepegg.creamstyle.objects.manager;

import java.util.LinkedHashMap;

import com.bluepegg.creamstyle.objects.ClosetItemDetail;

public class ClosetItemDetailListManager {

	private LinkedHashMap<Integer, ClosetItemDetail> mClosetItemDetails;
	
	public ClosetItemDetailListManager() {
		mClosetItemDetails = new LinkedHashMap<Integer, ClosetItemDetail>();
	}
	
	public LinkedHashMap<Integer, ClosetItemDetail> getItemDetails() {
		return mClosetItemDetails;
	}
	
	public void putItemDetail(int pKey, ClosetItemDetail pClosetItemDetail) {
		mClosetItemDetails.put(pKey, pClosetItemDetail);
	}
	
	public ClosetItemDetail getItemDetail(int pKey) {
		return mClosetItemDetails.get(pKey);
	}
	
	public void clear() {
		mClosetItemDetails.clear();
	}
}
