package com.bluepegg.creamstyle.objects.manager;

public class ResponseManager {
	private String mClosetResponse;
	private String mItemDetailResponse;
	
	
	public void setClosetResponse(String pClosetResponse) {
		mClosetResponse = pClosetResponse;
	}
	
	public String getClosetResponse() {
		return mClosetResponse;
	}
	
	public void setItemDetailResponse(String pItemDetailResponse) {
		mItemDetailResponse = pItemDetailResponse;
	}
	
	public String getItemDetailResponse() {
		return mItemDetailResponse;
	}
	
	private String mLookListResponse;
	private String mItemListResponse;
	private String mItemDetailListResponse;
	private String mClosetLookListResponse;
	private String mClosetItemListResponse;
	private String mClosetItemDetailListResponse;
	
	public void setLookListResponse(String pLookListResponse) {
		mLookListResponse = pLookListResponse;
	}
	
	public String getLookListResponse() {
		return mLookListResponse;
	}
	
	public void setItemListResponse(String pItemListResponse) {
		mItemListResponse = pItemListResponse;
	}
	
	public String getItemListResponse() {
		return mItemListResponse;
	}
	
	public void setItemDetailListResponse(String pItemDetailListResponse) {
		mItemDetailListResponse = pItemDetailListResponse;
	}
	
	public String getItemDetailListResponse() {
		return mItemDetailListResponse;
	}
	
	public void setClosetLookListResponse(String pClosetLookListResponse) {
		mClosetLookListResponse = pClosetLookListResponse;
	}
	
	public String getClosetLookListResponse() {
		return mClosetLookListResponse;
	}
	
	public void setClosetItemListResponse(String pClosetItemListResponse) {
		mClosetItemListResponse = pClosetItemListResponse;
	}
	
	public String getClosetItemListResponse() {
		return mClosetItemListResponse;
	}
	
	public void setClosetItemDetailListResponse(String pClosetItemDetailListResponse) {
		mClosetItemDetailListResponse = pClosetItemDetailListResponse;
	}
	
	public String getClosetItemDetailListResponse() {
		return mClosetItemDetailListResponse;
	}
}
