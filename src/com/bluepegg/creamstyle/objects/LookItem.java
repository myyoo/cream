package com.bluepegg.creamstyle.objects;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

public class LookItem implements Serializable {
	
	protected int mLookId;
	protected String mBigLookImage;
	protected String mSmallLookImage;
	protected String mThumbLookImage;
	protected int mLookHeight;
	protected int mLookWidth;
	protected String mLookName;
	protected String mDisplayName;
	protected int mFeedMask;
	protected boolean mLogoSection;
	protected int mPosition;
	protected boolean mCloset;
	protected long mClosetTime;
	
	protected List<Integer> mItemDetailIdList;
	protected List<Integer> mItemIdList;
	
	
	public void setClosetTime(long pClosetTime) {
		mClosetTime = pClosetTime;
	}
	
	public long getClosetTime() {
		return mClosetTime;
	}
	
	public void setItemDetailIdList(List<Integer> pItemDetailIdList) {
		mItemDetailIdList = pItemDetailIdList;
	}
	
	public List<Integer> getItemDetailIdList() {
		return mItemDetailIdList;
	}
	
	public void setItemIdList(List<Integer> pItemIdList) {
		mItemIdList = pItemIdList;
	}
	
	public List<Integer> getItemIdList() {
		return mItemIdList;
	}
	
	public void setCloset(boolean pCloset) {
		mCloset = pCloset;
	}
	
	public boolean getCloset() {
		return mCloset;
	}
	
	public void setLookId(int pLookId) {
		mLookId = pLookId;
	}
	
	public int getLookId() {
		return mLookId;
	}
	
	public void setBigLookImage(String pBigLookImage) {
		mBigLookImage = pBigLookImage;
	}
	
	public String getBigLookImage() {
		return mBigLookImage;
	}
	
	public void setSmallLookImage(String pSmallLookImage) {
		mSmallLookImage = pSmallLookImage;
	}
	
	public String getSmallLookImage() {
		return mSmallLookImage;
	}
	
	public void setThumbLookImage(String pThumbLookImage) {
		mThumbLookImage = pThumbLookImage;
	}
	
	public String getThumbLookImage() {
		return mThumbLookImage;
	}
	
	public void setLookName(String pLookName) {
		mLookName = pLookName;
	}
	
	public String getLookName() {
		return mLookName;
	}
	
	public void setDisplayName(String pDisplayName) {
		mDisplayName = pDisplayName;
	}
	
	public String getDisplayName() {
		return mDisplayName;
	}
	
	public void setLookHeight(int pLookHeight) {
		mLookHeight = pLookHeight;
	}
	
	public int getLookHeight() {
		return mLookHeight;
	}
	
	public void setLookWidth(int pLookWidth) {
		mLookWidth = pLookWidth;
	}
	
	public int getLookWidth() {
		return mLookWidth;
	}
	
	public void setFeedMask(int pFeedMask) {
		mFeedMask = pFeedMask;
	}
	
	public int getFeedMask() {
		return mFeedMask;
	}
	
	public void setPosition(int pPosition) {
		mPosition = pPosition;
	}
	
	public int getPosition() {
		return mPosition;
	}
	
	public void setLogoSection(boolean pLogoSection) {
		mLogoSection = pLogoSection;
	}
	
	public boolean getLogoSection() {
		return mLogoSection;
	}

	public static Comparator<LookItem> closetComparator = new Comparator<LookItem>() {

		@Override
		public int compare(LookItem lhs, LookItem rhs) {
			long closetTime1 = lhs.getClosetTime();
			long closetTime2 = rhs.getClosetTime();
			return String.valueOf(closetTime2).compareTo(String.valueOf(closetTime1));
		}
		
	};
}
