package com.bluepegg.creamstyle.account;

/**
 * IConnector interface has responsibility regarding connecting external system(web service)
 * for getting account information.
 * 
 * @author manyeon
 *
 */
public interface IConnector {
	public void requestAllFeed(String pToken, int pUid, IResponseEvent<Object> pResponseEvent);
	public void createAccount(String pEmail, String pPassword, String pUsername, IResponseEvent<Object> pResponseEvent);
	public void loginAccount(String pEmail, String pPassword, IResponseEvent<Object> pResponseEvent);
	public void loginWithFacebook(String pToken, String pEmail, IResponseEvent<Object> pResponseEvent);
	public void createAccountWithFacebook(String pToken, String pEmail, IResponseEvent<Object> pResponseEvent);
	public void resetPassword(String pEmail, IResponseEvent<Object> pResponseEvent);
	public void getCloset(String pUid, String pToken, IResponseEvent<Object> pResponseEvent);
	public void setCloset(String pType, String pUid, String pToken, String pId, String pLookId, IResponseEvent<Object> pResponseEvent);
	public void getCart(String pUid, String pToken, IResponseEvent<Object> pResponseEvent);
	public void setCart(String pType, String pUid, String pToken, String pInventoryId, String pLookId, IResponseEvent<Object> pResponseEvent);
	public void getTotal(String pUid, String pToken, String pZip, IResponseEvent<Object> pResponseEvent);
	public void charge(String pUid, String pToken, String pEmail, String pStripeToken, String pFullName, String pAddress_1, String pAddress_2, String pCity, String pState, String pZip, String pCountry, String pPhone, IResponseEvent<Object> pResponseEvent);
	public void applyCoupon(String pUid, String pToken, String pCouponCode, IResponseEvent<Object> pResponseEvent);
	public void getItemDetail(IResponseEvent<Object> pResponseEvent);
	
	public void getContent(int pUid, String pToken, IResponseEvent<Object> pResponseEvent);
	public void getCurrencyRate(String pCurrencyPair, IResponseEvent<Object> pResponseEvent);
}
