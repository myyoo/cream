package com.bluepegg.creamstyle.account;

import java.io.UnsupportedEncodingException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.core.ITaskHandler;
import com.bluepegg.creamstyle.core.ITaskType;
import com.bluepegg.creamstyle.core.SharedTask;
import com.bluepegg.creamstyle.network.NetworkManager;
import com.bluepegg.creamstyle.utils.DevLog;

public class BluepeggCreamStyle implements ITaskHandler, IConnector {
	private static final String TAG = "BluepeggCreamStyle";
	
	private SharedTask mSharedTask = null;
	private NetworkManager mNetworkManager;
	
	private enum TaskType implements ITaskType {
		ALL_FEED, CREATE_ACCOUNT, LOGIN_ACCOUNT, LOGIN_FACEBOOK, CREATE_FACEBOOK_ACCOUNT, RESET_PASSWORD,
		GET_CLOSET, SET_CLOSET, GET_CART, SET_CART, GET_TOTAL, CHARGE, APPLY_COUPON,
		GET_ITEM_DETAIL_LIST, GET_CONTENT, GET_CURRENCY
	};
	
	static public class TaskParam {
		private List<? extends Object> mParameterValue = null;
		private IResponseEvent mResponse = null;
		
		public TaskParam(List<? extends Object> pParameterValue, IResponseEvent pResponse) {
			mParameterValue = pParameterValue;
			mResponse = pResponse;
		}
		
		public List<? extends Object> getParameterValue() {
			return mParameterValue;
		}
		
		public IResponseEvent getResponse() {
			return mResponse;
		}
	}
	
	public BluepeggCreamStyle(SharedTask pSharedTask) {
		if(pSharedTask == null) {
			DevLog.LoggingError(TAG, "SharedTask is null");
			throw new InvalidParameterException();
		}
		
		CreamStyleApplication app = CreamStyleApplication.getApplication();
		mSharedTask = pSharedTask;
		mNetworkManager  = app.getNetworkManager();
	}
	
	@Override
	public void handleTask(ITaskType pTaskType, Object pParam) throws IllegalArgumentException {
		if(!(pTaskType instanceof TaskType)) {
			throw new IllegalArgumentException("pTaskType should be one of TaskTypes which is defined previous");
		}
		
		if(!(pParam instanceof TaskParam)) {
			throw new IllegalArgumentException("pParam should be TaskParam type");
		}
		
		TaskParam tp = (TaskParam)pParam;
		Object response = null;
		TaskType taskType = (TaskType)pTaskType;
		
		try {
			switch(taskType) {
			
			case ALL_FEED:
				response = requestAllFeed((List<Object>)tp.getParameterValue());
				break;
				
			case CREATE_ACCOUNT:
				response = createAccount((List<Object>)tp.getParameterValue());
				break;
				
			case LOGIN_ACCOUNT:
				response = loginAccount((List<Object>)tp.getParameterValue());
				break;
				
			case LOGIN_FACEBOOK:
				response = loginWithFacebook((List<Object>)tp.getParameterValue());
				break;
				
			case CREATE_FACEBOOK_ACCOUNT:
				response = createAccountWithFacebook((List<Object>)tp.getParameterValue());
				break;
				
			case RESET_PASSWORD:
				response = resetPassword((List<Object>)tp.getParameterValue());
				break;
				
			case GET_CLOSET:
				response = getCloset((List<Object>)tp.getParameterValue());
				break;
				
			case SET_CLOSET:
				response = setCloset((List<Object>)tp.getParameterValue());
				break;
				
			case GET_CART:
				response = getCart((List<Object>)tp.getParameterValue());
				break;
				
			case SET_CART:
				response = setCart((List<Object>)tp.getParameterValue());
				break;
				
			case GET_TOTAL:
				response = getTotal((List<Object>)tp.getParameterValue());
				break;
				
			case CHARGE:
				response = charge((List<Object>)tp.getParameterValue());
				break;
				
			case APPLY_COUPON:
				response = applyCoupon((List<Object>)tp.getParameterValue());
				break;
				
			case GET_ITEM_DETAIL_LIST:
				response = getItemDetail((List<Object>)tp.getParameterValue());
				break;
				
			case GET_CONTENT:
				response = getContent((List<Object>)tp.getParameterValue());
				break;
				
			case GET_CURRENCY:
				response = getCurrencyRate((List<Object>)tp.getParameterValue());
				break;
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			DevLog.LoggingError(TAG, "Cannot call task type: " + taskType.name());
		} finally {
			if(tp.getResponse() != null) {
				tp.getResponse().onResponse(response);
			}
		}
	}

	@Override
	public void requestAllFeed(String pToken, int pUid, IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(pToken, pUid), pResponseEvent);
		mSharedTask.addTask(this, TaskType.ALL_FEED, task);
	}
	
	private Object requestAllFeed(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 2) 
			return null;
		
		String token = (String)pParams.get(0);
		int uid = (Integer)pParams.get(1);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("login_token", token));
		params.add(new BasicNameValuePair("user_id", String.valueOf(uid)));
		
		return mNetworkManager.anlayseAllFeedResponse("api/content/allfeed", params);
	}

	@Override
	public void createAccount(String pEmail, String pPassword, String pUsername, IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(pEmail, pPassword, pUsername), pResponseEvent);
		mSharedTask.addTask(this, TaskType.CREATE_ACCOUNT, task);
	}
	
	private Object createAccount(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 3)
			return null;
		
		String email = (String)pParams.get(0);
		String password = (String)pParams.get(1);
		String username = (String)pParams.get(2);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("password", password));
		params.add(new BasicNameValuePair("username", username));
		
		return mNetworkManager.analyseCreateAccountResponse("api/login/signup-user", params);
	}

	@Override
	public void loginAccount(String pEmail, String pPassword, IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(pEmail, pPassword), pResponseEvent);
		mSharedTask.addTask(this, TaskType.LOGIN_ACCOUNT, task);
	}
	
	private Object loginAccount(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 2)
			return null;
		
		String email = (String)pParams.get(0);
		String password = (String)pParams.get(1);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("username", email));
		params.add(new BasicNameValuePair("password", password));
		
		return mNetworkManager.analyseLoginAccountResponse("api/login/username", params);
	}

	@Override
	public void loginWithFacebook(String pToken, String pEmail, IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(pToken, pEmail), pResponseEvent);
		mSharedTask.addTask(this, TaskType.LOGIN_FACEBOOK, task);
	}
	
	private Object loginWithFacebook(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 2)
			return null;
		
		String token = (String)pParams.get(0);
		String email = (String)pParams.get(1);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("facebook", token));
		params.add(new BasicNameValuePair("email", email));
		
		return mNetworkManager.analyseLoginFacebookResponse("api/login/facebook", params);
	}

	@Override
	public void createAccountWithFacebook(String pToken, String pEmail, IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(pToken, pEmail), pResponseEvent);
		mSharedTask.addTask(this, TaskType.CREATE_FACEBOOK_ACCOUNT, task);
	}
	
	private Object createAccountWithFacebook(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 2)
			return null;
		
		String token = (String)pParams.get(0);
		String email = (String)pParams.get(1);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("facebook", token));
		params.add(new BasicNameValuePair("email", email));
		
		return mNetworkManager.analyseCreateFacebookAccountResponse("api/login/signup-fb", params);
	}

	@Override
	public void resetPassword(String pEmail, IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(pEmail), pResponseEvent);
		mSharedTask.addTask(this, TaskType.RESET_PASSWORD, task);
	}
	
	private Object resetPassword(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 1)
			return null;
		
		String email = (String)pParams.get(0);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("email", email));
		
		return mNetworkManager.analyseResetPasswordResponse("api/login/reset", params);
	}

	@Override
	public void getCloset(String pUid, String pToken, IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(pUid, pToken), pResponseEvent);
		mSharedTask.addTask(this, TaskType.GET_CLOSET, task);
	}
	
	private Object getCloset(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 2)
			return null;
		
		String uid = (String)pParams.get(0);
		String token = (String)pParams.get(1);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("user_id", uid));
		params.add(new BasicNameValuePair("login_token", token));
		
		return mNetworkManager.analyseGetClosetResponse("api/v2.5/closet/closet-feed", params);
	}

	@Override
	public void setCloset(String pType, String pUid, String pToken, String pId, String pLookId, IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(pType, pUid, pToken, pId, pLookId), pResponseEvent);
		mSharedTask.addTask(this, TaskType.SET_CLOSET, task);
	}
	
	private Object setCloset(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 5)
			return null;
		
		String type = (String)pParams.get(0);
		String uid =(String)pParams.get(1);
		String token = (String)pParams.get(2);
		String id = (String)pParams.get(3);
		String lookId = (String)pParams.get(4);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("user_id", uid));
		params.add(new BasicNameValuePair("login_token", token));
		if(type.equals("add-look") || type.equals("remove-look"))
			params.add(new BasicNameValuePair("look_id", id));
		else {
			params.add(new BasicNameValuePair("item_detail_id", id));
			params.add(new BasicNameValuePair("look_id", lookId));
		}
		
		
		return mNetworkManager.analyseSetClosetResponse("api/closet/" + type, params);
	}

	@Override
	public void getCart(String pUid, String pToken, IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(pUid, pToken), pResponseEvent);
		mSharedTask.addTask(this, TaskType.GET_CART, task);
	}
	
	private Object getCart(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 2)
			return null;
		
		String uid = (String)pParams.get(0);
		String token = (String)pParams.get(1);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("user_id", uid));
		params.add(new BasicNameValuePair("login_token", token));
		
		return mNetworkManager.analyseGetCartResponse("api/shopping-cart/get-cart", params);
	}

	@Override
	public void setCart(String pType, String pUid, String pToken, String pInventoryId, String pLookId, IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(pType, pUid, pToken, pInventoryId, pLookId), pResponseEvent);
		mSharedTask.addTask(this, TaskType.SET_CART, task);
	}
	
	private Object setCart(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 5)
			return null;
		
		String type = (String)pParams.get(0);
		String uid =(String)pParams.get(1);
		String token = (String)pParams.get(2);
		String inventoryId = (String)pParams.get(3);
		String lookId = (String)pParams.get(4);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("user_id", uid));
		params.add(new BasicNameValuePair("login_token", token));
		params.add(new BasicNameValuePair("inventory_id", inventoryId));
		if(type.equals("add-item"))
			params.add(new BasicNameValuePair("look_id", lookId));
		
		return mNetworkManager.analyseSetCartResponse("api/shopping-cart/" + type, params);
	}

	@Override
	public void getTotal(String pUid, String pToken, String pZip, IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(pUid, pToken, pZip), pResponseEvent);
		mSharedTask.addTask(this, TaskType.GET_TOTAL, task);
	}
	
	private Object getTotal(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 3)
			return null;
		
		String uid = (String)pParams.get(0);
		String token = (String)pParams.get(1);
		String zip = (String)pParams.get(2);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("user_id", uid));
		params.add(new BasicNameValuePair("login_token", token));
		params.add(new BasicNameValuePair("zip", zip));
		
		return mNetworkManager.analyseGetTotalResponse("api/check-out/get-total", params);
	}

	@Override
	public void charge(String pUid, String pToken, String pEmail,
			String pStripeToken, String pFullName, String pAddress_1,
			String pAddress_2, String pCity, String pState, String pZip, String pCountry, String pPhone,
			IResponseEvent<Object> pResponseEvent) {
		
		TaskParam task = new TaskParam(Arrays.asList(pUid, pToken, pEmail, pStripeToken, pFullName, pAddress_1, pAddress_2, pCity, pState, pZip, pCountry, pPhone), pResponseEvent);
		mSharedTask.addTask(this, TaskType.CHARGE, task);
	}
	
	private Object charge(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 12)
			return null;
		
		String uid = (String)pParams.get(0);
		String token = (String)pParams.get(1);
		String email = (String)pParams.get(2);
		String stripeToken = (String)pParams.get(3);
		String fullName = (String)pParams.get(4);
		String address_1 = (String)pParams.get(5);
		String address_2 = (String)pParams.get(6);
		String city = (String)pParams.get(7);
		String state = (String)pParams.get(8);
		String zip = (String)pParams.get(9);
		String country = (String)pParams.get(10);
		String phone = (String)pParams.get(11);
	
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("user_id", uid));
		params.add(new BasicNameValuePair("login_token", token));
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("stripe_token", stripeToken));
		params.add(new BasicNameValuePair("full_name", fullName));
		params.add(new BasicNameValuePair("address_line_1", address_1));
		params.add(new BasicNameValuePair("address_line_2", address_2));
		params.add(new BasicNameValuePair("city", city));
		params.add(new BasicNameValuePair("state", state));
		params.add(new BasicNameValuePair("zip", zip));
		params.add(new BasicNameValuePair("country", country));
		params.add(new BasicNameValuePair("phone", phone));
		
		return mNetworkManager.analyseChargeResponse("api/check-out/charge", params);
	}

	@Override
	public void applyCoupon(String pUid, String pToken, String pCouponCode, IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(pUid, pToken, pCouponCode), pResponseEvent);
		mSharedTask.addTask(this, TaskType.APPLY_COUPON, task);
	}
	
	private Object applyCoupon(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 3)
			return null;
		
		String uid = (String)pParams.get(0);
		String token = (String)pParams.get(1);
		String couponCode = (String)pParams.get(2);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("user_id", uid));
		params.add(new BasicNameValuePair("login_token", token));
		params.add(new BasicNameValuePair("coupon_code", couponCode));
		
		return mNetworkManager.analyseApplyCouponResponse("api/check-out/apply-coupon", params);
	}

	@Override
	public void getItemDetail(IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(""), pResponseEvent);
		mSharedTask.addTask(this, TaskType.GET_ITEM_DETAIL_LIST, task);
	}
	
	private Object getItemDetail(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null)
			return null;
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		return mNetworkManager.analyseItemDetailListResponse("api/v2.5/content/item-detail-list", params);
	}

	@Override
	public void getContent(int pUid, String pToken, IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(pUid, pToken), pResponseEvent);
		mSharedTask.addTask(this, TaskType.GET_CONTENT, task);
	}
	
	private Object getContent(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 2)
			return null;
		
		int uid = (Integer)pParams.get(0);
		String token = (String)pParams.get(1);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("user_id", String.valueOf(uid)));
		params.add(new BasicNameValuePair("login_token", token));
		
		
		return mNetworkManager.anlayseContentResponse("api/v2.5/content/content-feed", params);
	}

	@Override
	public void getCurrencyRate(String pCurrencyPair, IResponseEvent<Object> pResponseEvent) {
		TaskParam task = new TaskParam(Arrays.asList(pCurrencyPair), pResponseEvent);
		mSharedTask.addTask(this, TaskType.GET_CURRENCY, task);
	}
	
	private Object getCurrencyRate(List<Object> pParams) throws UnsupportedEncodingException {
		if(pParams == null || pParams.size() != 1)
			return null;
		
		String currencyPair = (String)pParams.get(0);
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("currency_pair", currencyPair));
		return mNetworkManager.anlayseCurrencyResponse("api/fx/fx-rate", params);
	}

}
