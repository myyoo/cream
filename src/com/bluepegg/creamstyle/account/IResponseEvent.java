package com.bluepegg.creamstyle.account;

/**
 * IResponseEvent interface indicates callback which is related with the activated events by IConnector.
 * 
 * @author manyeon
 *
 * @param <T>
 */
public interface IResponseEvent<T extends Object> {

	/**
	 * It is called when account information is requested.
	 * 
	 * @param pParams
	 */
	public void onResponse(T pParams);
}
