package com.bluepegg.creamstyle.views;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.Scroller;

public class HomeLayout extends LinearLayout {
	private static final int SLIDING_DURATION = 300;
	private static final int QUERY_INTERVAL = 16;
	private View mMenuView;
	private View mContentView;
	
	private int mLayoutWidth;
	private int mMenuRightMarginValue = 150;
	
	private int mPrevX = 0;
	private boolean mDragging = false;
	private int mLastDiffX = 0;
	
	// content is arranged with  x offset
	private int mContentXOffset;
	
	private enum MenuState { HIDING, HIDDEN, SHOWING, SHOWN };
	
	private MenuState mState = MenuState.HIDDEN;
	
	// It is for animation execution
	private Scroller mScroller = new Scroller(getContext(), new EaseInInteroplator());
	
	private Runnable mMenuRunnable = new MenuRunnable();
	private Handler mMenuHandler = new Handler();
	
	// Natural scroll processing.
	// c.f: http://cyrilmottier.com/2012/05/22/the-making-of-prixing-fly-in-app-menu-part-1/
	class EaseInInteroplator implements Interpolator {

		@Override
		public float getInterpolation(float t) {
			return (float)Math.pow(t - 1, 5) + 1;
		}
		
	}
	
	class MenuRunnable implements Runnable {

		@Override
		public void run() {
			boolean isScrolling = mScroller.computeScrollOffset();
			adjustContentPosition(isScrolling);
		}
		
	}
	
	public void setShownStatus() {
		mState = MenuState.SHOWN;
	}
	
	private void adjustContentPosition(boolean pScrolling) {
		int scrollerXOffset = mScroller.getCurrX();
		mContentView.offsetLeftAndRight(scrollerXOffset - mContentXOffset);		
		mContentXOffset = scrollerXOffset;
		invalidate();
		
		if(pScrolling)
			mMenuHandler.postDelayed(mMenuRunnable, QUERY_INTERVAL);
		else
			onMenuSlidingComplete();
	}
	
	private void onMenuSlidingComplete() {
		switch(mState) {
		case SHOWING:
			mState = MenuState.SHOWN;
			break;
			
		case HIDING:
			mState = MenuState.HIDDEN;
			mMenuView.setVisibility(View.GONE);
			break;
			
		default:
			return;
		}
	}
	
	public boolean isMenuOpen() {
		return mState == MenuState.SHOWN;
	}
	
	public void hideMenuState() {
		mState = MenuState.SHOWN;
	}
	
	public HomeLayout(Context pContext) {
		super(pContext);
	}
	
	public HomeLayout(Context pContext, AttributeSet pAttrs) {
		super(pContext, pAttrs);
	}

	// callback method when HomeLayout is attached on Window
	// Called before running onDraw()
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		
		mMenuView = getChildAt(0);
		mContentView = getChildAt(1);
		
		mMenuView.setVisibility(View.GONE);
		
		/*
		mContentView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return onContentTouch(v, event);
			}
			
		});
		*/
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		mLayoutWidth = MeasureSpec.getSize(widthMeasureSpec);
		mMenuRightMarginValue = mLayoutWidth * 50 / 100;
	}
	
	
	// Callback method for position and size of LinearLayout child views separately.
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		if(changed) {
			LayoutParams contentParams = (LayoutParams)mContentView.getLayoutParams();
			contentParams.height = getHeight();
			contentParams.width = getWidth();
			
			LayoutParams menuParams = (LayoutParams)mMenuView.getLayoutParams();
			menuParams.height = getHeight();
			menuParams.width = getWidth() - mMenuRightMarginValue;
		}
		
		mMenuView.layout(l, t, r - mMenuRightMarginValue, b);
		mContentView.layout(l + mContentXOffset, t, r + mContentXOffset, b);
	}
	
	public void toggleMenu() {
		if(mState == MenuState.HIDING || mState == MenuState.SHOWING)
			return;
		
		switch(mState) {
		case HIDDEN:
			mState = MenuState.SHOWING;
			mMenuView.setVisibility(View.VISIBLE);
			mScroller.startScroll(0, 0, mMenuView.getLayoutParams().width, 0, SLIDING_DURATION);
			break;
			
		case SHOWN:
			mState = MenuState.HIDING;
			mScroller.startScroll(mContentXOffset, 0, -mContentXOffset, 0, SLIDING_DURATION);
			break;
			
		default:
			break;
		}
		
		mMenuHandler.postDelayed(mMenuRunnable, QUERY_INTERVAL);
		invalidate();
	}
	
	/*
	// Handle touch event on content View
    public boolean onContentTouch(View v, MotionEvent event) {
        // Do nothing if sliding is in progress
        if(mState == MenuState.HIDING || mState == MenuState.SHOWING)
            return false;
        
        // getRawX returns X touch point corresponding to screen
        // getX sometimes returns screen X, sometimes returns content View X
        int curX = (int)event.getRawX();
        int diffX = 0;
        
        switch(event.getAction()) {
        case MotionEvent.ACTION_DOWN:
            
        	mPrevX = curX;
            return true;
            
        case MotionEvent.ACTION_MOVE:
            // Set menu to Visible when user start dragging the content View
            if(!mDragging) {
            	mDragging = true;
            	mMenuView.setVisibility(View.VISIBLE);
            }
               
            // How far we have moved since the last position
            diffX = curX - mPrevX;
            
            // Prevent user from dragging beyond border
            if(mContentXOffset + diffX <= 0) {
                // Don't allow dragging beyond left border
                // Use diffX will make content cross the border, so only translate by -contentXOffset
                diffX = -mContentXOffset;
            } else if(mContentXOffset + diffX > mLayoutWidth - mMenuRightMarginValue) {
                // Don't allow dragging beyond menu width
                diffX = mLayoutWidth - mMenuRightMarginValue - mContentXOffset;
            }
            
            // Translate content View accordingly
            mContentView.offsetLeftAndRight(diffX);
            
            mContentXOffset += diffX;

            // Invalite this whole MainLayout, causing onLayout() to be called
            this.invalidate();
            
            mPrevX = curX;
            mLastDiffX = diffX;
            return true;
            
        case MotionEvent.ACTION_UP:
            
            Log.d("MainLayout.java onContentTouch()", "Up lastDiffX " + mLastDiffX);
                  
            // Start scrolling
            // Remember that when content has a chance to cross left border, lastDiffX is set to 0
            if(mLastDiffX > 0) {
                // User wants to show menu
            	mState = MenuState.SHOWING;
                
                // Start scrolling from contentXOffset
            	mScroller.startScroll(mContentXOffset, 0, mMenuView.getLayoutParams().width - mContentXOffset,
                        0, SLIDING_DURATION);
            } else if(mLastDiffX < 0) {
                // User wants to hide menu
                mState = MenuState.HIDING;
                mScroller.startScroll(mContentXOffset, 0, -mContentXOffset, 
                        0, SLIDING_DURATION);
            }
                
            // Begin querying
            mMenuHandler.postDelayed(mMenuRunnable, QUERY_INTERVAL);
            
            // Invalite this whole MainLayout, causing onLayout() to be called
            this.invalidate();

            // Done dragging
            mDragging = false;
            mPrevX = 0;
            mLastDiffX = 0;
            return true;
            
        default:
            break;
        }
        
        return false;
    }
    */
	
}
