package com.bluepegg.creamstyle.views;

import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.pager.JazzyViewPager;
import com.bluepegg.creamstyle.utils.DevLog;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class PageIndicator extends LinearLayout implements ViewPager.OnPageChangeListener {
	private OnPageChangeListener mListener;
	private JazzyViewPager mViewPager;
	private int mSelectedIndex;
	
	public PageIndicator(Context pContext) {
		this(pContext, null);
	}
	
	public PageIndicator(Context pContext, AttributeSet pAttrs) {
		super(pContext, pAttrs);
		setHorizontalScrollBarEnabled(false);
	}
	
	public void setViewPager(JazzyViewPager pView) {
		if(mViewPager == pView) {
			return;
		}
		
//		if(mViewPager != null) {
//			mViewPager.setOnPageChangeListener(null);
//		}
		
		PagerAdapter adapter = pView.getAdapter();
		if(adapter == null) {
			throw new IllegalStateException("ViewPager does not have adapter instance");
		}
		
		mViewPager = pView;
		pView.setOnPageChangeListener(this);
		notifyDataSetChanged();
	}
	
	public void notifyDataSetChanged() {
		removeAllViews();
		int count = mViewPager.getAdapter().getCount();
		for(int i = 0; i < count; ++i) {
			ImageView view = new ImageView(getContext());
			view.setImageResource(R.drawable.page_indicator);
			LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			params.setMargins(15, 0, 15, 0);
			addView(view, params);
		}
		
		if(mSelectedIndex > count) {
			mSelectedIndex = count - 1;
		}
		
		setCurrentItem(mSelectedIndex);
		requestLayout();
	}
	
	private void setCurrentItem(int pIndex) {
		if(mViewPager == null) {
			throw new IllegalStateException("ViewPager has not been found");
		}
		
		mSelectedIndex = pIndex;
		mViewPager.setCurrentItem(pIndex);
		
		int count = getChildCount();
		for(int i = 0; i < count; ++i) {
			View child = getChildAt(i);
			boolean isSelected = (i == pIndex);
			child.setSelected(isSelected);
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		if(mListener != null) {
			mListener.onPageScrollStateChanged(arg0);
		}
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		if(mListener != null) {
			mListener.onPageScrolled(arg0, arg1, arg2);
		}
	}

	@Override
	public void onPageSelected(int arg0) {
		setCurrentItem(arg0);
		if(mListener != null) {
			mListener.onPageSelected(arg0);
		}
	}
}
