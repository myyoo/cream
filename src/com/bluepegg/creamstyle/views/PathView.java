package com.bluepegg.creamstyle.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class PathView extends RelativeLayout {
	private float mXoffset = 0;
	private float mYoffset = 0;
	
	public PathView(Context pContext) {
		super(pContext);
	}
	
	public PathView(Context pContext, AttributeSet pAttrs) {
		super(pContext, pAttrs);
	}

	public PathView(Context pContext, AttributeSet pAttrs, int pDefStyle) {
		super(pContext, pAttrs, pDefStyle);
	}
	
	public void setOffset(float pEndX, float pEndY) {
		mXoffset = pEndX;
		mYoffset = pEndY;
	}
	
	public float getXOffset() {
		return mXoffset;
	}
	
	public float getYOffset() {
		return mYoffset;
	}
	

}
