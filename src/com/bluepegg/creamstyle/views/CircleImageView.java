package com.bluepegg.creamstyle.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class CircleImageView extends ImageView {
	private int mWidth, mHeight;
	private int mPosition;
	
	public CircleImageView(Context pContext) {
		super(pContext);
	}
	
	public CircleImageView(Context pContext, AttributeSet pAttrs) {
		super(pContext, pAttrs);
	}
	
	public CircleImageView(Context pContext, AttributeSet pAttrs, int pDefStyle) {
		super(pContext, pAttrs, pDefStyle);
	}
	
	public void setPoint(int pWidth, int pHeight) {
		mWidth = pWidth;
		mHeight = pHeight;
	}
	

	@Override
	protected void onDraw(Canvas canvas) {
		Drawable drawable = getDrawable();
		
		if(drawable == null)
			return;
		
		if(getWidth() == 0 || getHeight() == 0)
			return;
		
		Bitmap b = ((BitmapDrawable)drawable).getBitmap();
		Bitmap bitmap = b.copy(Bitmap.Config.ARGB_8888, true);
		
		int w = mWidth;
		int h = mHeight;
		
		Bitmap circleBitmap = getCroppedBitmap(bitmap, w);
		canvas.drawBitmap(circleBitmap, 0, 0, null);
		
	}
	
	
	private Bitmap getCroppedBitmap(Bitmap pBitmap, int pRadius) {
		Bitmap bm;
		
		if(pBitmap.getWidth() != pRadius || pBitmap.getHeight() != pRadius)
			bm = Bitmap.createScaledBitmap(pBitmap, pRadius, pRadius, false);
		else
			bm = pBitmap;
		
		Bitmap output = Bitmap.createBitmap(bm.getWidth(), bm.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);
		
		int color = 0xffa19774;
		Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bm.getWidth(), bm.getHeight());

	    paint.setAntiAlias(true);
	    paint.setFilterBitmap(true);
	    paint.setDither(true);
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(Color.parseColor("#BAB399"));
	    canvas.drawCircle(bm.getWidth() / 2+0.7f, bm.getHeight() / 2+0.7f,
	            bm.getWidth() / 2+0.1f, paint);
	    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	    canvas.drawBitmap(bm, rect, rect, paint);


	    return output;
		
	}
	
	
	public void setPosition(int pPosition) {
		mPosition = pPosition;
	}
	
	public int getPosition() {
		return mPosition;
	}
	
}
