package com.bluepegg.creamstyle.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class VerticalScrollView extends ScrollView {

	protected OnEdgeTouchListener onEdgeTouchListener;
	protected ScrollViewListener onScrollerListener;
	
	public VerticalScrollView(Context pContext) {
		super(pContext);
	}
	
	public VerticalScrollView(Context pContext, AttributeSet pAttrs) {
		super(pContext, pAttrs);
	}
	
	public void setOnEdgeTouchListener(OnEdgeTouchListener l) {
		onEdgeTouchListener = l;
	}
	
	public void setScrollViewListener(ScrollViewListener l) {
		onScrollerListener = l;
	}
	
	
	@Override
	public void computeScroll() {
		super.computeScroll();
		if(onEdgeTouchListener != null) {
			if(getScrollY() == 0) {
				onEdgeTouchListener.onEdgeTouch(DIRECTION.TOP);
			} else if((getScrollY() + getWidth()) == computeVerticalScrollRange()) {
				onEdgeTouchListener.onEdgeTouch(DIRECTION.BOTTOM);
			} else {
				onEdgeTouchListener.onEdgeTouch(DIRECTION.NONE);
			}
		}
	}

	


	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		super.onScrollChanged(l, t, oldl, oldt);
		if(onScrollerListener != null) {
			onScrollerListener.onScrollChanged(this, l, t, oldl, oldt);
		}
	}




	public static enum DIRECTION {TOP, BOTTOM, NONE};
	
	public interface OnEdgeTouchListener {
		void onEdgeTouch(DIRECTION pDirection);
	}
	
	public interface ScrollViewListener {
		void onScrollChanged(VerticalScrollView pScrollView, int x, int y, int oldx, int oldy);
	}
}
