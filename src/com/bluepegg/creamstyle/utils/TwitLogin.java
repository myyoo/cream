package com.bluepegg.creamstyle.utils;

import com.bluepegg.creamstyle.R;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class TwitLogin extends Activity {
	private Context mContext;
//	private Dialog dialog;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twit_login);
        mContext = this;
 
        WebView webview = (WebView) findViewById(R.id.webView);
        webview.setWebViewClient(new WebViewClient() {
        	
			public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
 
                if (url != null && url.equals("http://mobile.twitter.com/")) {
                    finish();
                } else if (url != null && url.startsWith(BasicInfo.TWIT_CALLBACK_URL)) {
                	new LoginAsyncTask(url).execute();
                }
            }

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if(url.contains(BasicInfo.TWIT_CALLBACK_URL)) {
//					dialog = new Dialog(mContext);
//					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//					dialog.setCancelable(false);
//					dialog.setContentView(R.layout.progress_dialog_layout);
//					TextView progressContent = (TextView)dialog.findViewById(R.id.progressContent);
//					progressContent.setText(R.string.auth_twitter);
//					dialog.show();
				}
				return super.shouldOverrideUrlLoading(view, url);
			}
        });
 
 
        Intent passedIntent = getIntent();
        String authUrl = passedIntent.getStringExtra("authUrl");
        webview.loadUrl(authUrl);
 
    }
	
	
	class LoginAsyncTask extends AsyncTask<Void, Void, Boolean> {
		private String mUrl;
		private String oauthToken = "";
		private String oauthVerifier = "";
		
		
		public LoginAsyncTask(String pUrl) {
			mUrl = pUrl;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		
		@Override
		protected Boolean doInBackground(Void... param) {
			String[] params = mUrl.split("\\?")[1].split("&");

            try {
                if (params[0].startsWith("oauth_token")) {
                    oauthToken = params[0].split("=")[1];
                } else if (params[1].startsWith("oauth_token")) {
                    oauthToken = params[1].split("=")[1];
                }

                if (params[0].startsWith("oauth_verifier")) {
                    oauthVerifier = params[0].split("=")[1];
                } else if (params[1].startsWith("oauth_verifier")) {
                    oauthVerifier = params[1].split("=")[1];
                }
                
                return true;
                
            } catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
//			if(dialog != null && dialog.isShowing())
//				dialog.dismiss();
			
			if(result) {
				Intent resultIntent = new Intent();
                resultIntent.putExtra("oauthToken", oauthToken);
                resultIntent.putExtra("oauthVerifier", oauthVerifier);
				setResult(RESULT_OK, resultIntent);
	            finish();
			}
		}
		
		
	}
}
