package com.bluepegg.creamstyle.utils;

import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RecentTaskInfo;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.setting.SettingManager;


public class Utils {
	private Context mContext;
	private Typeface mTypeface;
	private static final String EMAIL_ADDRESS_REGEX = "^([a-zA-Z0-9\\+_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+";
	private static final String PASSWORD_REGX = "^[a-zA-Z0-9]*$";
	private static final String NUM_REGX = "^[0-9]*$";

	public Utils(Context pContext) {
		mContext = pContext;
	}
	
	public void hideKeyBoard(View pView) {
		InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(pView.getWindowToken(), 0);
	}
	
	public void hideKeyBoard() {
		InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS, 0);
	}
	
	public boolean validEmail(CharSequence pEmail) {
		Pattern pattern = Pattern.compile(EMAIL_ADDRESS_REGEX, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(pEmail);
		if(matcher.matches())
			return true;
		
		return false;
	}
	
	public boolean validPassword(CharSequence pPassword) {
		Pattern pattern = Pattern.compile(PASSWORD_REGX, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(pPassword);
		if(matcher.matches())
			return true;
		
		return false;
	}
	
	public boolean isNumber(CharSequence pNumber) {
		Pattern pattern = Pattern.compile(NUM_REGX, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(pNumber);
		if(matcher.matches())
			return true;
		
		return false;
	}
	
	public String getAppVersion() {
		String version = "";
		try {
			PackageInfo packageInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), PackageManager.GET_META_DATA);
			version = packageInfo.versionName;
		} catch(NameNotFoundException e) {
			e.printStackTrace();
		}
		
		return version;
	}
	
	public int deviceWidth() {
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(metrics);
		return metrics.widthPixels;
	}
	
	public int deviceHeight() {
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(metrics);
		return metrics.heightPixels;
	}
	
	public boolean modifySize() {
		if(deviceWidth() == 1080)
			return true;
		
		return false;
	}
	
	public int deviceSize(Context pContext) {
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)pContext.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(metrics);
		int deviceWidth = metrics.widthPixels;
		int deviceHeight = metrics.heightPixels;
		DevLog.defaultLogging("DeviceSize: " + deviceWidth);
		
		if(deviceWidth == 1080)
			return 1;
		else if(deviceWidth == 480)
			return 2;
		else if(deviceWidth == 540) 
			return 3;
		else if(deviceWidth == 1440)
			return 4;
		
		
		return 0;
	}
	
	public Toast showToastMessage(Context pContext, int pMessage){
		LayoutInflater inflater = (LayoutInflater)pContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.toast_layout, null);
		TextView toastTitle = (TextView)view.findViewById(R.id.toast_title);
		TextView toastMessage = (TextView)view.findViewById(R.id.toast_message);
		toastMessage.setText(pMessage);
		
		Toast toast = new Toast(pContext);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setView(view);
		toast.show();
		
		return toast;
	}
	
	public Toast showToastMessage(Context pContext, String pMessage){
		LayoutInflater inflater = (LayoutInflater)pContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.toast_layout, null);
		TextView toastTitle = (TextView)view.findViewById(R.id.toast_title);
		TextView toastMessage = (TextView)view.findViewById(R.id.toast_message);
		toastMessage.setText(pMessage);
		
		Toast toast = new Toast(pContext);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setView(view);
		toast.show();
		
		return toast;
	}
	
	public String getDefaultGoogleEmail(Context pContext){
		AccountManager accountManager = AccountManager.get(pContext);
		Account[] accounts = accountManager.getAccountsByType("com.google");
		if(accounts != null && accounts.length > 0){
			return accounts[0].name;
		}
		
		return getDefaultEmail(pContext);
	}
	
	public String getDefaultEmail(Context pContext){
		final Account[] accounts = AccountManager.get(pContext).getAccounts();
		for(Account account : accounts){
			if(Pattern.compile(EMAIL_ADDRESS_REGEX).matcher(account.name).matches()){
				return account.name;
			}
		}
		
		return null;
	}
	
	public String getModel(){
		StringBuilder sb = new StringBuilder();
		sb.append(android.os.Build.MANUFACTURER + " "); 
		sb.append(android.os.Build.MODEL); 
		return sb.toString();
	}
	
	public String getDeviceID(Context pContext){
		TelephonyManager telephonyManager = (TelephonyManager)pContext.getSystemService(Context.TELEPHONY_SERVICE);
		String id = null;
		if(telephonyManager != null){
			id = telephonyManager.getDeviceId();
		}
		
		return (TextUtils.isEmpty(id) ? "N/A" : id);
	}
	
	public CharSequence setTextHtmlStyle(String pText){
		return Html.fromHtml(pText);
	}
	
	public int darkenColor(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= 0.8f;
        return Color.HSVToColor(hsv);
    }
	
	public int dpToPx(Resources res, int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, res.getDisplayMetrics());
	}
		
	
	public boolean isRunningTask(Context pContext) {
		boolean running = false;
		
		ActivityManager am = (ActivityManager)pContext.getSystemService(Activity.ACTIVITY_SERVICE);
		List<RecentTaskInfo> list = am.getRecentTasks(Integer.MAX_VALUE, Intent.FLAG_ACTIVITY_NEW_TASK);
		
		for(RecentTaskInfo taskInfo : list) {
			if(taskInfo.baseIntent.getComponent().getPackageName().equals(pContext.getPackageName())) {
				running = true;
				break;
			}
		}
		
		return running;
	}
	
	public void setTypeface(Typeface pTypeface) {
		mTypeface = pTypeface;
	}
	
	public Typeface getTypeface() {
		return mTypeface;
	}
	
	public Dialog getFullScreenDialog(Context pContext) {
		Dialog dialog = new Dialog(pContext, android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.loading_layout);
		Window window = dialog.getWindow();
	    WindowManager.LayoutParams wlp = window.getAttributes();

	    wlp.gravity = Gravity.CENTER;
	    wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
	    window.setAttributes(wlp);
	    dialog.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.MATCH_PARENT);
	    return dialog;
	}
	
	public int deviceSize() {
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)CreamStyleApplication.getApplication().getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(metrics);
		int deviceWidth = metrics.widthPixels;
		DevLog.defaultLogging("DeviceSize: " + deviceWidth);
		if(deviceWidth == 1080)
			return 1;
		else if(deviceWidth == 480)
			return 2;
		else if(deviceWidth == 540) 
			return 3;
		else if(deviceWidth == 1440) {
			return 4;
		}
		
		return 0;
	}
	
	public double screenSize() {
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)CreamStyleApplication.getApplication().getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(metrics);
		int deviceWidth = metrics.widthPixels;
		int deviceHeight = metrics.heightPixels;
		int density = metrics.densityDpi;
		
		double width = (double)deviceWidth / (double)density;
		double height = (double)deviceHeight / (double)density;
		double x = Math.pow(width, 2);
		double y = Math.pow(height, 2);
		double screenInches = Math.sqrt(x + y);
		
		return screenInches;
	}
	
	public String getCurrentLanguage() {
		return Locale.getDefault().getLanguage();
	}
	
	public String convertDollarToWon(int pFeedMask, String pPrice) {
		if(pFeedMask == 0) {
			return pPrice;
		} else {
			float value = Float.valueOf(pPrice);
			SettingManager settingManager = CreamStyleApplication.getApplication().getSettingManager();
			float currency = Float.valueOf(settingManager.getCurrencyRage());
			value = value * currency;
			
			int index = String.valueOf(value).indexOf(".");
			String result = String.valueOf(value).substring(0, index);
			
			return result;
		}
		
	}
	
	
	public String getAppVersionName() {
		Context context = CreamStyleApplication.getApplication();
		String version = "";
		try{
			PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			version = pi.versionName;
		}catch (NameNotFoundException e){
			e.printStackTrace();
		}
		
		return version;
	}
	
	public int pxToDp(int px)
	{
	    return (int) (px / Resources.getSystem().getDisplayMetrics().density);
	}
	
	
}
