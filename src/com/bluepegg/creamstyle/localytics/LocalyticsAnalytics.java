package com.bluepegg.creamstyle.localytics;

import java.util.HashMap;

import android.content.Context;
import android.content.Intent;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.utils.DevLog;
import com.localytics.android.LocalyticsAmpSession;

public class LocalyticsAnalytics {
	private static final String TAG = "Localytics";
	private static LocalyticsAmpSession mSession = null;
	
	public static void startLocalytics(Context pContext) {
		DevLog.Logging(TAG, "Localytics start - " + pContext.toString());
		mSession = new LocalyticsAmpSession(pContext, CreamStyleApplication.LOCALYTICS_API_KEY);
		mSession.open();
		mSession.upload();
	}
	
	public static void tagEvent(String pEvent, HashMap<String, String> pValues) {
		mSession.tagEvent(pEvent, pValues);
	}
	
	public static void endLocalytics(Context pContext) {
		DevLog.Logging(TAG, "Localytics end - " + pContext.toString());
		mSession.close();
		mSession.upload();
	}
	
	public static void registerPush(String pProjectId) {
		mSession.registerPush(pProjectId);
	}
	
	public static void handlePushReceiver(Intent pIntent) {
		mSession.handlePushReceived(pIntent);
	}
	
	public static void setDisableLocalyticsPush(boolean pFlag) {
		mSession.setPushDisabled(pFlag);
	}
	
	public static void setCustomerEmail(String pEmail) {
		mSession.setCustomerEmail(pEmail);
	}
}
