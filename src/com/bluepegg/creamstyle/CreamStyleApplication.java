package com.bluepegg.creamstyle;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.app.Application;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.bluepegg.creamstyle.account.IConnector;
import com.bluepegg.creamstyle.activities.CreamMainActivity;
import com.bluepegg.creamstyle.core.CoreAccessHelper;
import com.bluepegg.creamstyle.interfaces.OnLoadListener;
import com.bluepegg.creamstyle.kissmetrics.CreamStyleStateEventTypes;
import com.bluepegg.creamstyle.kissmetrics.CreamstyleAnalytics;
import com.bluepegg.creamstyle.localytics.LocalyticsAnalytics;
import com.bluepegg.creamstyle.network.NetworkManager;
import com.bluepegg.creamstyle.objects.manager.ClosetFeedItemManager;
import com.bluepegg.creamstyle.objects.manager.ClosetItemDetailListManager;
import com.bluepegg.creamstyle.objects.manager.ClosetItemListManager;
import com.bluepegg.creamstyle.objects.manager.ClosetLookItemManager;
import com.bluepegg.creamstyle.objects.manager.ItemDetailListManager;
import com.bluepegg.creamstyle.objects.manager.ItemListManager;
import com.bluepegg.creamstyle.objects.manager.LookItemManager;
import com.bluepegg.creamstyle.objects.manager.ResponseManager;
import com.bluepegg.creamstyle.setting.SettingManager;
import com.bluepegg.creamstyle.utils.DevLog;
import com.bluepegg.creamstyle.utils.Utils;
import com.soundlly.sdk.Soundlly;

public class CreamStyleApplication extends Application {
	
	public static final String CREAM = "cream";
	public static final String SURE = "sure";
	public static final String KISSMETRICS_API_KEY = "e385162a070951d2395d251291ad980a916cd2ca";
	public static final String LOCALYTICS_API_KEY = "c16b3ea58584ae9d8e40a4b-b6636464-3ce7-11e4-a1a0-005cf8cbabd8";
	
	//***** TEST key ****************
//	public static final String KISSMETRICS_API_KEY = "a4a8a0a623690a2823e4e0d54b7138a2cac499e0";
//	public static final String LOCALYTICS_API_KEY = "1f337cc51cd30b7e21146f7-a5dbb0fc-3ce7-11e4-2544-004a77f8b47f";
	private static final String SOUNDLLY_API_KEY = "666bf7fb-a84a-4ae5-43f2-b7c160d98d15";
	private static CreamStyleApplication mCore;
	private boolean mStarted;
	
	private volatile CoreAccessHelper mAccessor = null;
	private volatile Utils mUtils = null;
	private volatile NetworkManager mNetworkManager = null;
	private volatile SettingManager mSettingManager = null;
	private volatile ResponseManager mResponseManager = null;
	private volatile LookItemManager mLookItemManager = null;
	private volatile ItemDetailListManager mItemDetailListManager = null;
	private volatile ItemListManager mItemListManager = null;
	private volatile ClosetLookItemManager mClosetLookItemManager = null;
	private volatile ClosetItemDetailListManager mClosetItemDetailListManager = null;
	private volatile ClosetItemListManager mClosetItemListManager = null;
	private volatile ClosetFeedItemManager mClosetFeedItemManager = null;
	
	private OnLoadListener mListener;
	private CreamMainActivity mMainActivity;
	
	private LocationManager mLocManager;
	private LocationListener locListener = new CreamLocationListener();
	private boolean mNetworkEnabled = false;

	@Override
	public void onCreate() {
		super.onCreate();
		CreamStyleApplication.mCore = this;
		mStarted = false;
		
		Soundlly.init(this);
		Soundlly.setApplicationId(SOUNDLLY_API_KEY);
		
		onStart();
		
		getLocationInformation();
		
		
		CreamstyleAnalytics.startAnalytics(getApplicationContext());
		
		if(mSettingManager.getCountryCode() == null) {
			mSettingManager.setCountryCode(Locale.getDefault().getCountry());
		}
		
		HashMap<String, String> properties = new HashMap<String, String>();
		properties.put("Version", mUtils.getAppVersion());
		properties.put("Platform", "Android");
		properties.put("Device", mUtils.getModel());
		properties.put("Location", mSettingManager.getCountryCode());
		CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_APP_INFORMATION, properties);
		
		LocalyticsAnalytics.startLocalytics(getApplicationContext());
		LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_APP_INFORMATION, properties);
	}
	
	private void getLocationInformation() {
		mLocManager = (LocationManager)getSystemService(LOCATION_SERVICE);
		
		try {
			mNetworkEnabled = mLocManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			Location location = mLocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			
			String countryCode = getCountry(location.getLatitude(), location.getLongitude());
			mSettingManager.setCountryCode(countryCode);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		if(mNetworkEnabled) {
			mLocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locListener);
		}
		
		
	}
	
	private String getCountry(double pLat, double pLon) {
		Geocoder geoCoder = new Geocoder(this, Locale.US);
		
		try {
			List<Address> addresses = geoCoder.getFromLocation(pLat, pLon, 1);
			
			Address address = addresses.get(0);
			return address.getCountryCode();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		LocalyticsAnalytics.endLocalytics(getApplicationContext());
	}
	
	public static CreamStyleApplication getApplication() {
		return mCore;
	}
	
	public void onStart() {
		if(!mStarted) {
			mStarted = true;
			connectCoreEngine();
			getUtils();
			getNetworkManager();
			getSettingManager();
			getResponseManager();
			getLookItemManager();
			getItemDetailListManager();
			getItemListManager();
			getClosetLookItemManager();
			getClosetItemDetailListManager();
			getClosetItemListManager();
			getClosetFeedItemManager();
		}
	}
	
	private void connectCoreEngine() {
		if(mAccessor == null) {
			synchronized(this) {
				if(mAccessor == null)
					mAccessor = new CoreAccessHelper();
			}
		}
	}
	
	public IConnector getConnector() {
		if(mAccessor == null)
			connectCoreEngine();
		
		return mAccessor.getConnector();
	}
	
	public Utils getUtils() {
		if(mUtils == null) {
			synchronized(this) {
				if(mUtils == null) {
					mUtils = new Utils(getApplicationContext());
				}
			}
		}
		
		return mUtils;
	}
	
	public NetworkManager getNetworkManager() {
		if(mNetworkManager == null) {
			synchronized(this) {
				if(mNetworkManager == null) {
					mNetworkManager = new NetworkManager();
				}
			}
		}
		
		return mNetworkManager;
	}
	
	public SettingManager getSettingManager() {
		if(mSettingManager == null) {
			synchronized(this) {
				if(mSettingManager == null) {
					mSettingManager = new SettingManager();
				}
			}
		}
		
		return mSettingManager;
	}
	
//	public ItemDetailManager getDetailItemManager() {
//		if(mItemDetailManager == null) {
//			synchronized(this) {
//				if(mItemDetailManager == null) {
//					mItemDetailManager = new ItemDetailManager();
//				}
//			}
//		}
//		
//		return mItemDetailManager;
//	}
	
	public ResponseManager getResponseManager() {
		if(mResponseManager == null) {
			synchronized(this) {
				if(mResponseManager == null) {
					mResponseManager = new ResponseManager();
				}
			}
		}
		
		return mResponseManager;
	}
	
	public LookItemManager getLookItemManager() {
		if(mLookItemManager == null) {
			synchronized(this) {
				if(mLookItemManager == null) {
					mLookItemManager = new LookItemManager();
				}
			}
		}
		
		return mLookItemManager;
	}
	
	public ClosetLookItemManager getClosetLookItemManager() {
		if(mClosetLookItemManager == null) {
			synchronized(this) {
				if(mClosetLookItemManager == null) {
					mClosetLookItemManager = new ClosetLookItemManager();
				}
			}
		}
		
		return mClosetLookItemManager;
	}
	
	public ItemDetailListManager getItemDetailListManager() {
		if(mItemDetailListManager == null) {
			synchronized(this) {
				if(mItemDetailListManager == null) {
					mItemDetailListManager = new ItemDetailListManager();
				}
			}
		}
		
		return mItemDetailListManager;
	}
	
	public ClosetItemDetailListManager getClosetItemDetailListManager() {
		if(mClosetItemDetailListManager == null) {
			synchronized(this) {
				if(mClosetItemDetailListManager == null) {
					mClosetItemDetailListManager = new ClosetItemDetailListManager();
				}
			}
		}
		
		return mClosetItemDetailListManager;
	}
	
	public ItemListManager getItemListManager() {
		if(mItemListManager == null) {
			synchronized(this) {
				if(mItemListManager == null) {
					mItemListManager = new ItemListManager();
				}
			}
		}
		
		return mItemListManager;
	}
	
	public ClosetItemListManager getClosetItemListManager() {
		if(mClosetItemListManager == null) {
			synchronized(this) {
				if(mClosetItemListManager == null) {
					mClosetItemListManager = new ClosetItemListManager();
				}
			}
		}
		
		return mClosetItemListManager;
	}
	
	public ClosetFeedItemManager getClosetFeedItemManager() {
		if(mClosetFeedItemManager == null) {
			synchronized(this) {
				if(mClosetFeedItemManager == null) {
					mClosetFeedItemManager = new ClosetFeedItemManager();
				}
			}
		}
		
		return mClosetFeedItemManager;
	}
	
	
	public OnLoadListener getLoadListener() {
		if(mListener == null) {
			mListener = new OnLoadListener() {

				@Override
				public void onFinishLoad() {
					mMainActivity.refreshFeedItem();
				}
				
			};
		}
		
		
		return mListener;
	}
	
	public void setCreamMainActivity(CreamMainActivity pMainActivity) {
		mMainActivity = pMainActivity;
	}
	
	
	public class CreamLocationListener implements LocationListener {

		@Override
		public void onLocationChanged(Location location) {
			if(location != null) {
				mLocManager.removeUpdates(locListener);
				DevLog.defaultLogging(location.getLongitude() + ", " + location.getLatitude());
			}
		}

		@Override
		public void onProviderDisabled(String arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}

	}
}
