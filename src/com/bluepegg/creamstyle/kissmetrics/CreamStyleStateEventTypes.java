package com.bluepegg.creamstyle.kissmetrics;

public interface CreamStyleStateEventTypes {
	// Kissmetrics events
	public final static String STAT_APP_STARTED = "Application Started";
	public final static String STAT_APP_TERMINATED = "Application terminated";
	public final static String STAT_LOGIN = "Login";
	public final static String STAT_USER_LOGIN = "User Login";
	public final static String STAT_USER_ACCOUNT_CREATED = "User Account Created";
	public final static String STAT_FB_LOGIN = "Facebook Login";
	public final static String STAT_FB_ACCOUNT_CREATED = "Facebook Account Created";
	public final static String STAT_USER_SKIPPED_LOGIN = "User Skipped Login";
	public final static String STAT_LOGOUT = "Logout";
	public final static String STAT_VIEW_LOOK_DIRECT = "View Look Direct";
	
	public final static String STAT_APP_INFORMATION = "Application Information";
	
	public final static String STAT_VIEW_LOOK = "View Look";
	public final static String STAT_VIEW_ITEM_DIRECT = "View Item Direct";
	public final static String STAT_VIEW_ITEM = "View Item";
	public final static String STAT_VIEW_ITEM_DETAILS = "View Item Details";
	public final static String STAT_ALT_ITEM_DIRECT = "Alt Item Direct";
	public final static String STAT_ADD_TO_CART = "Add To Cart";
	public final static String STAT_SUBTRACT_FROM_CART = "Subtract From Cart";
	public final static String STAT_DELETE_FROM_CART = "Delete From Cart";
	public final static String STAT_NAVIGATE_TO_CART = "Navigate To Cart";
	public final static String STAT_BILLING_INFO_SUBMITTED = "Billing Info Submitted";
	public final static String STAT_PURCHASE = "Purchase";
	public final static String STAT_NAVIGATE_TO_CHECKOUT = "Navigate To Checkout";
	public final static String STAT_NAVIGATE_TO_BILLING = "Navigate To Billing";
	public final static String STAT_NAVIGATE_TO_SETTINGS = "Navigate To Settings";
	public final static String STAT_NAVIGATE_TO_CLOSET = "Navigate To Closet";
	public final static String STAT_ADD_LOOK_TO_CLOSET = "Add Look To Closet";
	public final static String STAT_REMOVE_LOOK_FROM_THEIR_CLOSET = "Remove Look From Closet";
	public final static String STAT_ADD_ITEM_TO_CLOSET = "Add Item To Closet";
	public final static String STAT_REMOVE_ITEM_FROM_CLOSET = "Remove Item From Closet";
	public final static String STAT_LOOK_SHARED_ON_FB = "Look Shared On FB";
	public final static String STAT_ITEM_SHARED_ON_FB = "Item Shared On FB";
	public final static String STAT_LOOK_SHARED_ON_PINTEREST = "Look Shared On Pinterest";
	public final static String STAT_LOOK_SHARED_ON_GOOGLE = "Look Shared on Google";
	public final static String STAT_ITEM_SHARED_ON_PINTEREST = "Item Shared On Pinterest";
	public final static String STAT_ITEM_SHARED_ON_GOOGLE = "Item Shared on Google";
	public final static String STAT_LOOK_SHARED_ON_TWITTER = "Look Shared On Twitter";
	public final static String STAT_ITEM_SHARED_ON_TWITTER = "Item Shared On Twitter";
	public final static String STAT_ERROR_DURING_CHECKOUT = "Error During Checkout";
	public final static String STAT_COUPON_APPLIED = "Coupon Applied";
	
	public final static String STAT_CREAM_FEED = "Cream Feed";
	public final static String STAT_SURE_FEED = "Sure Feed";
	public final static String STAT_VIEW_LOOK_DIRECT_SURE = "View Look Direct Sure";
	public final static String STAT_VIEW_LOOK_SURE = "View Look Sure";
	
	public final static String STAT_LOOK_SHARED = "Look Shared";
	public final static String STAT_ITEM_SHARED = "Item Shared";
	
	public final static String STAT_VIEW_LOOK_CLOSET = "View Look Closet";
	public final static String STAT_VIEW_LOOK_DIRECT_CLOSET = "View Look Direct Closet";
	public final static String STAT_VIEW_LOOK_DIRECT_CLOSET_SURE = "View Look Direct Closet Sure";
	public final static String STAT_VIEW_LOOK_CLOSET_SURE = "View Look Closet Sure";
	
}
