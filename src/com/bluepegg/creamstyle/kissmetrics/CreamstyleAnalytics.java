package com.bluepegg.creamstyle.kissmetrics;

import java.util.HashMap;

import com.bluepegg.creamstyle.CreamStyleApplication;
import com.bluepegg.creamstyle.utils.DevLog;

import android.content.Context;


public class CreamstyleAnalytics {
	private static final String TAG = "CreamstyleAnalytics";
	private static KISSmetricsAPI mKiss = null;
	
	public static void startAnalytics(Context pContext) {
		DevLog.Logging(TAG, "KISSmetrics start - " + pContext.toString());
		mKiss = KISSmetricsAPI.sharedAPI(CreamStyleApplication.KISSMETRICS_API_KEY, pContext);
	}
	
	public static void trackEvent(String pEvent, HashMap<String, String> pProperties) {
		DevLog.Logging(TAG, "KISS: " + pEvent);
		mKiss.recordEvent(pEvent, pProperties);
	}
	
	
	public static void endAalytics(Context pContext) {
		DevLog.Logging(TAG, "KISSmetircs end - " + pContext.toString());
		
	}
	
	public static void identify(String pEvent) {
		mKiss.identify(pEvent);
	}
	
}
