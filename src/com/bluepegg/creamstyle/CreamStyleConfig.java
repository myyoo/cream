package com.bluepegg.creamstyle;

import muneris.android.core.configuration.Configuration;

public class CreamStyleConfig implements Configuration {

	@Override
	public String getApiKey() {
		return "https://console.muneris.io/apps/@1556";
	}

	@Override
	public String getBundleConfiguration() {
		return null;
	}

	@Override
	public String getExtraApiParams() {
		return null;
	}

}
