package com.bluepegg.creamstyle.share;

import java.util.HashMap;

import android.content.Context;

import com.bluepegg.creamstyle.R;
import com.bluepegg.creamstyle.kissmetrics.CreamStyleStateEventTypes;
import com.bluepegg.creamstyle.kissmetrics.CreamstyleAnalytics;
import com.bluepegg.creamstyle.localytics.LocalyticsAnalytics;
import com.bluepegg.creamstyle.utils.DevLog;
import com.pinterest.pinit.PinIt;
import com.pinterest.pinit.PinItListener;

public class PinterestShareManager {
	private static final String WEB_URL = "http://www.creamstyle.com";
	private Context mContext;
	
	public PinterestShareManager(Context pContext) {
		mContext = pContext;
	}
	
	// If pType is true, it is lookId. otherwise, itemDetailId
	public void share(final boolean pType, final int pId, String pImageUrl, String pItemName, String pItemDetailName) {
		PinIt pinIt = new PinIt();
		pinIt.setImageUrl(pImageUrl);
		pinIt.setDescription(String.format(mContext.getString(R.string.share_name), pItemName, pItemDetailName));
		pinIt.setUrl(WEB_URL);
		
		pinIt.setListener(new PinItListener() {

			@Override
			public void onComplete(boolean completed) {
				if(completed) {
					
					HashMap<String, String> properties = new HashMap<String, String>();
					
					if(pType) {
						properties.put("look_id", String.valueOf(pId));
						CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_LOOK_SHARED_ON_PINTEREST, properties);
						CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_LOOK_SHARED, properties);
						
						LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_LOOK_SHARED_ON_PINTEREST, properties);
						LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_LOOK_SHARED, properties);
					} else {
						properties.put("item_detail_id", String.valueOf(pId));
						CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED_ON_PINTEREST, properties);
						CreamstyleAnalytics.trackEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED, properties);
						
						LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED_ON_PINTEREST, properties);
						LocalyticsAnalytics.tagEvent(CreamStyleStateEventTypes.STAT_ITEM_SHARED, properties);
					}
					
				} else {
					DevLog.defaultLogging("Share failed");
				}
			}

			@Override
			public void onException(Exception e) {
				super.onException(e);
			}
			
		});
		
		
		pinIt.doPinIt(mContext);
		
		
		
		
		
	}
}
