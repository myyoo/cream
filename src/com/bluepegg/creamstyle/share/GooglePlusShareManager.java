package com.bluepegg.creamstyle.share;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.bluepegg.creamstyle.R;
import com.google.android.gms.plus.PlusShare;

public class GooglePlusShareManager {
	private static final String WEB_URL = "http://www.creamstyle.com";
	private Activity mActivity;
	
	public GooglePlusShareManager(Activity pActivity) {
		mActivity = pActivity;
	}
	
	public Intent share(final int pId, String pImageUrl, String pItemName, String pItemDetailName) {
		Intent shareIntent = new PlusShare.Builder(mActivity)
		.setText(String.format(mActivity.getString(R.string.share_name), pItemName, pItemDetailName))
		.setType("image/jpeg")
		.setContentDeepLinkId(WEB_URL, mActivity.getResources().getString(R.string.share_description), String.format(mActivity.getString(R.string.share_name), pItemName, pItemDetailName), Uri.parse(pImageUrl))
		.getIntent();
		
		return shareIntent;
	}
	
}
